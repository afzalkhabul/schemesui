var servicesDetails=angular.module('servicesDetails', []);
servicesDetails.factory('projectWardWorks',['$http',function($http){
    var projectWardWorks={};
    projectWardWorks.getTasksDetails=function(inputData,cb) {

        $http({
            method: "GET",
            url: 'api/TaskStatuses/?filter={"where":{"status":"Active"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function(response) {
            cb(response);
        }).error(function(response) {
            cb(response);
        })
    };
    projectWardWorks.getPlanDetails=function(inputData,cb) {
            $http({
                method: 'GET',
                url: 'api/ProjectPlans/getProjectDetails',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                cb(response);

            }).error(function (response) {
                cb(response);
            });
    };
    projectWardWorks.getEmployeeDetails=function(inputData,cb) {
        $http({
            method: 'GET',
            url: 'api/Employees?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            cb(response);
        }).error(function (response) {
        });
    };
    projectWardWorks.getPlanDepartment=function(inputData,cb) {
            $http({
                method: 'GET',
                url: 'api/PlanDepartments/?filter={"where":{"status":"Active"}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                cb(response);
            }).error(function (response) {
                cb(response);
            });
    };
    projectWardWorks.getCharterDetails=function(inputData,cb) {
            $http({
                method: 'GET',
                url: 'api/ProjectCharters/getProjectDetails',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                cb(response);
            }).error(function (response) {

            });
    };
    projectWardWorks.getProjectStatus=function(inputData,cb) {
        $http({
            method: 'GET',
            url: 'api/ProjectStates/?filter={"where":{"status":"Active"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            cb(response);
        }).error(function (response) {
            cb(response);
        });
    };
    projectWardWorks.getSelectedPlan=function(planId,cb) {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans/?filter={"where":{"planId":"' + planId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           cb(response);
        }).error(function (response) {
           cb(response);
        });
    };
    projectWardWorks.getFormAccess=function(inputData,cb) {
        var employeeId=inputData.employeeId, formName=inputData.formName;
        $http({
            method: 'GET',
            url: 'api/RoleModels/getRoleStatus?employeeId='+employeeId+'&formName='+formName,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           cb(response);
        }).error(function (response) {
           cb(response);
        });
    };
    projectWardWorks.getBillInfomation=function(inputData,cb) {
        $http({
            method: 'GET',
            url:'api/BillGenerations?filter=%7B%22where%22%3A%7B%22and%22%3A%5B%7B%22finalStatus%22%3A%22Approval%22%7D%2C%7B%22planId%22%3A%22'+inputData +'%22%7D%5D%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           cb(response);
        }).error(function (response) {
           cb(response);
        });
    };
    return projectWardWorks;
}]);

servicesDetails.factory('userAdminstration',['$http',function($http){
    var userAdminstration={};
    userAdminstration.getFormAccess=function(inputData,cb) {
        var employeeId=inputData.employeeId, formName=inputData.formName;
        $http({
            method: 'GET',
            url: 'api/RoleModels/getRoleStatus?employeeId='+employeeId+'&formName='+formName,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           cb(response);
        }).error(function (response) {
           cb(response);
        });
    };
    return userAdminstration;
}]);

servicesDetails.factory('schemeManagement',['$http',function($http){
    var schemeManagement={};
    schemeManagement.getFormAccess=function(inputData,cb) {
        var employeeId=inputData.employeeId, formName=inputData.formName;
        $http({
            method: 'GET',
            url: 'api/RoleModels/getRoleStatus?employeeId='+employeeId+'&formName='+formName,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           cb(response);
        }).error(function (response) {
           cb(response);
        });
    };
    schemeManagement.getSchemeDepartment=function(inputData,cb) {
        $http({
            "method": "GET",
            "url": 'api/Departments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            cb(response);
        }).error(function (response, data) {
            console.log("failure");
        })
    };
    schemeManagement.getMinistry=function(inputData,cb) {
        $http({
            "method": "GET",
            "url": 'api/ministeriesLists',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            cb(response);
        }).error(function (response, data) {
            console.log("failure");
        })
    };
    return schemeManagement;
}]);

servicesDetails.factory('legalManagement',['$http',function($http){
    var legalManagement={};
    legalManagement.getFormAccess=function(inputData,cb) {
        var employeeId=inputData.employeeId, formName=inputData.formName;
        $http({
            method: 'GET',
            url: 'api/RoleModels/getRoleStatus?employeeId='+employeeId+'&formName='+formName,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           cb(response);
        }).error(function (response) {
           cb(response);
        });
    };
    return legalManagement;
}]);