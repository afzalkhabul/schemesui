
// var uploadFileURL='http://139.162.27.78:8865/api/Uploads/dhanbadDb/download/';
var uploadFileURL ='http://54.149.172.244:3004/api/Uploads/dhanbadDb/download/';

angular.module('ui.tinymce', ['ui.calendar'])
    .value('uiTinymceConfig', {})
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
        uiTinymceConfig = uiTinymceConfig || {};
        var generatedIds = 0;
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ngModel) {
                var expression, options, tinyInstance;
                // generate an ID if not present
                if (!attrs.id) {
                    attrs.$set('id', 'uiTinymce' + generatedIds++);
                }
                options = {
                    // Update model when calling setContent (such as from the source editor popup)
                    setup: function(ed) {
                        ed.on('init', function(args) {
                            ngModel.$render();
                        });
                        // Update model on button click
                        ed.on('ExecCommand', function(e) {
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                        // Update model on keypress
                        ed.on('KeyUp', function(e) {
                            console.log(ed.isDirty());
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                    },
                    mode: 'exact',
                    elements: attrs.id,
                };
                if (attrs.uiTinymce) {
                    expression = scope.$eval(attrs.uiTinymce);
                } else {
                    expression = {};
                }
                angular.extend(options, uiTinymceConfig, expression);
                setTimeout(function() {
                    tinymce.init(options);
                });
                ngModel.$render = function() {
                    if (!tinyInstance) {
                        tinyInstance = tinymce.get(attrs.id);
                    }
                    if (tinyInstance) {
                        tinyInstance.setContent(ngModel.$viewValue || '');
                    }
                };
            }
        };
    }]);



var app = angular.module('myApp', ['ngRoute','angular-page-loader','workflow','project','legalMgmt','landAndAsset',  'ui.calendar','ngCalendar', 'ngFileUpload', 'datatables','angularjs-dropdown-multiselect', 'ui.tinymce', 'datatables.bootstrap', 'servicesDetails','ngMap']);

app.factory('httpRequestInterceptor', function ($window,$q,$location) {
    return {
        request: function (config) {
            if($window.localStorage.getItem('tokenId')) {
                config.headers['access_token'] = $window.localStorage.getItem('tokenId');
            }
            return config;
        },
        response: function(response) {
            var deferred = $q.defer();
            deferred.resolve(response);
            return deferred.promise;
        },
        responseError: function (rejection) {
            if(rejection.status === 401) {
                console.log('reject responce '+JSON.stringify(rejection))
                //if(rejection."Unauthorized")
                $window.localStorage.clear();
                location.reload();
             /*   if(rejection.data.error.message=="Authorization Required"){

                }*/

            }if(rejection.status === 500 || rejection.status === 403){
                var errorUrlData=$location.host()+':3003/error';
                location.href="http://"+$location.host()+":3003/error";
                return false;
            }
            return $q.reject(rejection);
        }
    };
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
});

app.directive('noSpecialChar', function() {
        return {
          require: 'ngModel',
          restrict: 'A',
          link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(inputValue) {
              if (inputValue == undefined)
                return ''
              cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
              if (cleanInputValue != inputValue) {
                modelCtrl.$setViewValue(cleanInputValue);
                modelCtrl.$render();
              }
              return cleanInputValue;
            });
          }
        }
      });

app.directive('validNumber', function() {
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            if(!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function(val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split ('.');
                if(!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean =negativeCheck[0] + '-' + negativeCheck[1];
                    if(negativeCheck[0].length > 0) {
                        clean =negativeCheck[0];
                    }
                }
                if(!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0,2);
                    clean =decimalCheck[0] + '.' + decimalCheck[1];
                }
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function(event) {
                if(event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.config(function($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: './home.html',
            controller: 'homeController'
        }).when('/schemes', {
            templateUrl: './schemes.html',
            controller: 'schemesController'
        }).when('/addScheme', {
            templateUrl: './addScheme.html',
            controller: 'addSchemeController'
        }).when('/editScheme/:packId', {
            templateUrl: './editScheme.html',
            controller: 'editSchemeController'
        }).when('/viewScheme/:packId', {
            templateUrl: './viewScheme.html',
            controller: 'viewSchemeController'
        }).when('/viewRequest/:requstId', {
            templateUrl: './viewRequest.html',
            controller: 'viewRequestController'
        }).when('/requestReports', {
            templateUrl: './viewRequest.html',
            controller: 'viewRequestController'
        }).when('/changeStateRequest/:requstId', {
            templateUrl: './editRequest.html',
            controller: 'editRequestController'
        }).when('/requests', {
            templateUrl: './requests.html',
            controller: 'requestsController'
        }).when('/schemeForms', {
            templateUrl: './schemeForms.html',
            controller: 'schemeFormsController'
        }).when('/createSchemeForms', {
            templateUrl: './createSchemeForms.html',
            controller: 'createSchemeFormsController'
        }).when('/viewSchemeForms/:formId', {
            templateUrl: './viewSchemeForms.html',
            controller: 'viewSchemeFormsController'
        }).when('/userProfile', {
            templateUrl: './userProfile.html',
            controller: 'userProfileController'
        }).when('/beneficiariesList', {
            templateUrl: './beneficiariesList.html',
            controller: 'beneficiariesListController'
        }).when('/ministriesList', {
            templateUrl: './ministriesList.html',
            controller: 'ministriesListController'
        }).when('/schemeDepartment', {
            templateUrl: './schemeDepartment.html',
            controller: 'departmentController'
        }).when('/login', {
            templateUrl: './login.html',
            controller: 'loginController'
        }).when('/requestTracker', {
          templateUrl: './requestTracker.html',
          controller: 'requestTrackerController'
        }).when('/listOfScheme', {
          templateUrl: './listOfScheme.html',
          controller: 'listOfSchemeController'
        }).when('/schemeWiseRequests', {
          templateUrl: './schemeWiseRequests.html',
          controller: 'schemeWiseRequestsController'
        }).when('/requestAging', {
          templateUrl: './requestAging.html',
          controller: 'requestAgingController'
        }).when('/workflowTracker', {
          templateUrl: './workflowTracker.html',
          controller: 'workflowTrackerController'
        }).when('/openSchemes/:schemeUniqueId/:status', {
            templateUrl: './openSchemes.html',
            controller: 'openSchemesController'
          }).when('/openSchemesRequest/:schemeUniqueId', {
            templateUrl: './openSchemesRequest.html',
            controller: 'openSchemesRequestController'
          }).when('/schemModulelogin/:employeeId', {
            templateUrl: './blank.html',
            controller: 'schemeModuleController'
          }).otherwise({redirectTo: '/'});

});

var directiveModule = angular.module('angularjs-dropdown-multiselect', []);
directiveModule.directive('ngDropdownMultiselect', ['$filter', '$document', '$compile', '$parse',

    function ($filter, $document, $compile, $parse) {
        return {
            restrict: 'AE',
            scope: {
                selectedModel: '=',
                options: '=',
                extraSettings: '=',
                events: '=',
                searchFilter: '=?',
                translationTexts: '=',
                groupBy: '@'
            },
            template: function (element, attrs) {
                var checkboxes = attrs.checkboxes ? true : false;
                var groups = attrs.groupBy ? true : false;
                var template = '<div class="multiselect-parent btn-group dropdown-multiselect">';
                template += '<button type="button" class="dropdown-toggle" ng-class="settings.buttonClasses" ng-click="toggleDropdown()">{{getButtonText()}}&nbsp;<span class="caret"></span></button>';
                template += '<ul class="dropdown-menu dropdown-menu-form" ng-style="{display: open ? \'block\' : \'none\', height : settings.scrollable ? settings.scrollableHeight : \'auto\' }" style="overflow: scroll" >';
                template += '<li ng-hide="(!settings.showCheckAll || settings.selectionLimit > 0) && !settings.showUncheckAll" class="divider"></li>';
                template += '<li class="abc1 " ng-show="settings.enableSearch"><div class="dropdown-header"><input type="text" class="form-control" style="width: 100%;" ng-model="searchFilter" placeholder="{{texts.searchPlaceholder}}" /></li>';
                template += '<li class="abc2 " ng-hide="!settings.showCheckAll || settings.selectionLimit > 0"><a data-ng-click="selectAll()"><span class="glyphicon glyphicon-ok"></span>  {{texts.checkAll}}</a></li>';
                template += '<li class="abc2 " ng-show="settings.showUncheckAll"><a data-ng-click="deselectAll();"><span class="glyphicon glyphicon-remove"></span>   {{texts.uncheckAll}}</a></li>';
                template += '<li class="abc3 "><button type="button" class="btn btn-success dropdown-toggle" ng-class="settings.buttonClasses" ng-click="toggleDropdown()">OK</button></li>';
                template += '<li ng-show="settings.enableSearch" class="divider"></li>';
                if (groups) {
                    template += '<li ng-repeat-start="option in orderedItems | filter: searchFilter" ng-show="getPropertyForObject(option, settings.groupBy) !== getPropertyForObject(orderedItems[$index - 1], settings.groupBy)" role="presentation" class="dropdown-header">{{ getGroupTitle(getPropertyForObject(option, settings.groupBy)) }}</li>';
                    template += '<li ng-repeat-end role="presentation">';
                } else {
                    template += '<li role="presentation" ng-repeat="option in options | filter: searchFilter">';
                }
                template += '<a role="menuitem" tabindex="-1" ng-click="setSelectedItem(getPropertyForObject(option,settings.idProp))">';
                if (checkboxes) {
                    template += '<div class="checkbox"><label><input class="checkboxInput" type="checkbox" ng-click="checkboxClick($event, getPropertyForObject(option,settings.idProp))" ng-checked="isChecked(getPropertyForObject(option,settings.idProp))" /> {{getPropertyForObject(option, settings.displayProp)}}</label></div></a>';
                } else {
                    template += '<span data-ng-class="{\'glyphicon glyphicon-ok\': isChecked(getPropertyForObject(option,settings.idProp))}"></span> {{getPropertyForObject(option, settings.displayProp)}}</a>';
                }
                template += '</li>';
                template += '<li class="divider" ng-show="settings.selectionLimit > 1"></li>';
                template += '<li role="presentation" ng-show="settings.selectionLimit > 1"><a role="menuitem">{{selectedModel.length}} {{texts.selectionOf}} {{settings.selectionLimit}} {{texts.selectionCount}}</a></li>';
                template += '</ul>';
                template += '</div>';
                element.html(template);
            },

            link: function ($scope, $element, $attrs) {
                var $dropdownTrigger = $element.children()[0];
                $scope.toggleDropdown = function () {
                    $scope.open = !$scope.open;
                };
                $scope.checkboxClick = function ($event, id) {
                    $scope.setSelectedItem(id);
                    $event.stopImmediatePropagation();
                };
                $scope.externalEvents = {
                    onItemSelect: angular.noop,
                    onItemDeselect: angular.noop,
                    onSelectAll: angular.noop,
                    onDeselectAll: angular.noop,
                    onInitDone: angular.noop,
                    onMaxSelectionReached: angular.noop
                };
                $scope.settings = {
                    dynamicTitle: true,
                    scrollable: true,
                    scrollableHeight: '200px',
                    closeOnBlur: true,
                    displayProp: 'label',
                    idProp: 'id',
                    externalIdProp: 'id',
                    enableSearch: true,
                    selectionLimit: 0,
                    showCheckAll: true,
                    showUncheckAll: true,
                    closeOnSelect: false,
                    buttonClasses: 'btn btn-default',
                    closeOnDeselect: false,
                    groupBy: $attrs.groupBy || undefined,
                    groupByTextProvider: null,
                    smartButtonMaxItems: 0,
                    smartButtonTextConverter: angular.noop
                };

                $scope.texts = {
                    checkAll: 'Check All',
                    uncheckAll: 'Uncheck All',
                    selectionCount: 'checked',
                    selectionOf: '/',
                    searchPlaceholder: 'Search...',
                    buttonDefaultText: 'Select',
                    dynamicButtonTextSuffix: 'checked'
                };

                $scope.searchFilter = $scope.searchFilter || '';

                if (angular.isDefined($scope.settings.groupBy)) {
                    $scope.$watch('options', function (newValue) {
                        if (angular.isDefined(newValue)) {
                            $scope.orderedItems = $filter('orderBy')(newValue, $scope.settings.groupBy);
                        }
                    });
                }

                angular.extend($scope.settings, $scope.extraSettings || []);
                angular.extend($scope.externalEvents, $scope.events || []);
                angular.extend($scope.texts, $scope.translationTexts);

                $scope.singleSelection = $scope.settings.selectionLimit === 1;

                function getFindObj(id) {
                    var findObj = {};

                    if ($scope.settings.externalIdProp === '') {
                        findObj[$scope.settings.idProp] = id;
                    } else {
                        findObj[$scope.settings.externalIdProp] = id;
                    }

                    return findObj;
                }

                function clearObject(object) {
                    for (var prop in object) {
                        delete object[prop];
                    }
                }

                if ($scope.singleSelection) {
                    if (angular.isArray($scope.selectedModel) && $scope.selectedModel.length === 0) {
                        clearObject($scope.selectedModel);
                    }
                }

                if ($scope.settings.closeOnBlur) {
                    $document.on('click', function (e) {
                        var target = e.target.parentElement;
                        var parentFound = false;

                        while (angular.isDefined(target) && target !== null && !parentFound) {
                            if (_.contains(target.className.split(' '), 'multiselect-parent') && !parentFound) {
                                if (target === $dropdownTrigger) {
                                    parentFound = true;
                                }
                            }
                            target = target.parentElement;
                        }

                        if (!parentFound) {
                            $scope.$apply(function () {
                                $scope.open = false;
                            });
                        }
                    });
                }

                $scope.getGroupTitle = function (groupValue) {
                    if ($scope.settings.groupByTextProvider !== null) {
                        return $scope.settings.groupByTextProvider(groupValue);
                    }

                    return groupValue;
                };

                $scope.getButtonText = function () {
                    if ($scope.settings.dynamicTitle && ($scope.selectedModel.length > 0 || (angular.isObject($scope.selectedModel) && _.keys($scope.selectedModel).length > 0))) {
                        if ($scope.settings.smartButtonMaxItems > 0) {
                            var itemsText = [];

                            angular.forEach($scope.options, function (optionItem) {
                                if ($scope.isChecked($scope.getPropertyForObject(optionItem, $scope.settings.idProp))) {
                                    var displayText = $scope.getPropertyForObject(optionItem, $scope.settings.displayProp);
                                    var converterResponse = $scope.settings.smartButtonTextConverter(displayText, optionItem);

                                    itemsText.push(converterResponse ? converterResponse : displayText);
                                }
                            });

                            if ($scope.selectedModel.length > $scope.settings.smartButtonMaxItems) {
                                itemsText = itemsText.slice(0, $scope.settings.smartButtonMaxItems);
                                itemsText.push('...');
                            }

                            return itemsText.join(', ');
                        } else {
                            var totalSelected;

                            if ($scope.singleSelection) {
                                totalSelected = ($scope.selectedModel !== null && angular.isDefined($scope.selectedModel[$scope.settings.idProp])) ? 1 : 0;
                            } else {
                                totalSelected = angular.isDefined($scope.selectedModel) ? $scope.selectedModel.length : 0;
                            }

                            if (totalSelected === 0) {
                                return $scope.texts.buttonDefaultText;
                            } else {
                                return totalSelected + ' ' + $scope.texts.dynamicButtonTextSuffix;
                            }
                        }
                    } else {
                        return $scope.texts.buttonDefaultText;
                    }
                };

                $scope.getPropertyForObject = function (object, property) {
                    if (angular.isDefined(object) && object.hasOwnProperty(property)) {
                        return object[property];
                    }

                    return '';
                };

                $scope.selectAll = function () {
                    $scope.deselectAll(false);
                    $scope.externalEvents.onSelectAll();

                    angular.forEach($scope.options, function (value) {
                        $scope.setSelectedItem(value[$scope.settings.idProp], true);
                    });
                };

                $scope.deselectAll = function (sendEvent) {
                    sendEvent = sendEvent || true;

                    if (sendEvent) {
                        $scope.externalEvents.onDeselectAll();
                    }

                    if ($scope.singleSelection) {
                        clearObject($scope.selectedModel);
                    } else {
                        $scope.selectedModel.splice(0, $scope.selectedModel.length);
                    }
                };

                $scope.setSelectedItem = function (id, dontRemove) {
                    var findObj = getFindObj(id);
                    var finalObj = null;

                    if ($scope.settings.externalIdProp === '') {
                        finalObj = _.find($scope.options, findObj);
                    } else {
                        finalObj = findObj;
                    }

                    if ($scope.singleSelection) {
                        clearObject($scope.selectedModel);
                        angular.extend($scope.selectedModel, finalObj);
                        $scope.externalEvents.onItemSelect(finalObj);
                        if ($scope.settings.closeOnSelect) $scope.open = false;

                        return;
                    }

                    dontRemove = dontRemove || false;

                    var exists = _.findIndex($scope.selectedModel, findObj) !== -1;

                    if (!dontRemove && exists) {
                        $scope.selectedModel.splice(_.findIndex($scope.selectedModel, findObj), 1);
                        $scope.externalEvents.onItemDeselect(findObj);
                    } else if (!exists && ($scope.settings.selectionLimit === 0 || $scope.selectedModel.length < $scope.settings.selectionLimit)) {
                        $scope.selectedModel.push(finalObj);
                        $scope.externalEvents.onItemSelect(finalObj);
                    }
                    if ($scope.settings.closeOnSelect) $scope.open = false;
                };

                $scope.isChecked = function (id) {
                    if ($scope.singleSelection) {
                        return $scope.selectedModel !== null && angular.isDefined($scope.selectedModel[$scope.settings.idProp]) && $scope.selectedModel[$scope.settings.idProp] === getFindObj(id)[$scope.settings.idProp];
                    }

                    return _.findIndex($scope.selectedModel, getFindObj(id)) !== -1;
                };

                $scope.externalEvents.onInitDone();
            }
        };
    }]);

app.controller('homeController', function($http, $scope, $window, $location, $rootScope) {
    $rootScope.userName = $window.localStorage.getItem('userName');
    console.log("homeController");
});

app.controller('schemesController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("schemesController");


     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;
    $http({
        "method": "GET",
        "url": 'api/Schemes',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $scope.schemeLists = response;
        $scope.loadingImage=false;
    }).error(function (response, data) {
        console.log("failure");
    })

    $scope.addScheme = function() {
        location.href = '#/addScheme';
        $scope.loadingImage=false;
    }
    $scope.viewRequest = function() {
        $scope.loadingImage=false;
        location.href = '#/viewRequest';
    }
    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.schemeLists!=null &&  $scope.schemeLists.length>0){

            $("<tr>" +
                "<th>Name</th>" +
                "<th>Category</th>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "<th>Budget</th>" +
                "<th>Commincement</th>" +
                "<th>Comment</th>" +
                "<th>Ministry Name</th>" +
                "<th>Contact One</th>" +
                "<th>Contact Two</th>" +
                "<th>Contact Three</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.schemeLists.length;i++){
                var schemes=$scope.schemeLists[i];

                $("<tr>" +
                    "<td style='text-transform : capitalize; color:red;'>"+schemes.name+"</td>" +
                    "<td>"+schemes.category+"</td>" +
                    "<td>"+schemes.departmentName+"</td>" +
                    "<td>"+schemes.status+"</td>" +
                    "<td>"+schemes.budget+"</td>" +
                    "<td>"+schemes.commincement+"</td>" +
                    "<td>"+schemes.comment+"</td>" +
                    "<td>"+schemes.ministryName+"</td>" +
                    "<td><table><tr><td>EmployeeId"+"</td><td>"+":</td><td>"+schemes.contactOne.employeeId+"</td></tr>" +
                    "<tr><td>Address"+"</td><td>"+":</td><td>"+schemes.contactOne.address+"</td></tr>"+
                    "<tr><td>Name"+"</td><td>"+":</td><td>"+schemes.contactOne.name+"</td></tr>"+
                    "<tr><td>Email"+"</td><td>"+":</td><td>"+schemes.contactOne.email+"</td></tr>"+
                    "<tr><td>Designation"+"</td><td>"+":</td><td>"+schemes.contactOne.designation+"</td></tr>"+
                    "<tr><td>Phone No"+"</td><td>"+":</td><td>"+schemes.contactOne.phoneOne+"</td></tr>"+
                    "<tr><td>Work Phone No"+"</td><td>"+":</td><td>"+schemes.contactOne.phoneTwo+"</td></tr>"+
                    "</table></td>" +
                    "<td><table><tr><td>EmployeeId"+"</td><td>"+":</td><td>"+schemes.contactTwo.employeeId+"</td></tr>" +
                    "<tr><td>Address"+"</td><td>"+":</td><td>"+schemes.contactTwo.address+"</td></tr>"+
                    "<tr><td>Name"+"</td><td>"+":</td><td>"+schemes.contactTwo.name+"</td></tr>"+
                    "<tr><td>Email"+"</td><td>"+":</td><td>"+schemes.contactTwo.email+"</td></tr>"+
                    "<tr><td>Designation"+"</td><td>"+":</td><td>"+schemes.contactTwo.designation+"</td></tr>"+
                    "<tr><td>Phone No"+"</td><td>"+":</td><td>"+schemes.contactTwo.phoneOne+"</td></tr>"+
                    "<tr><td>Work Phone No"+"</td><td>"+":</td><td>"+schemes.contactTwo.phoneTwo+"</td></tr>"+
                    "</table></td>" +
                    "<td><table><tr><td>EmployeeId"+"</td><td>"+":</td><td>"+schemes.contactThree.employeeId+"</td></tr>" +
                    "<tr><td>Address"+"</td><td>"+":</td><td>"+schemes.contactThree.address+"</td></tr>"+
                    "<tr><td>Name"+"</td><td>"+":</td><td>"+schemes.contactThree.name+"</td></tr>"+
                    "<tr><td>Email"+"</td><td>"+":</td><td>"+schemes.contactThree.email+"</td></tr>"+
                    "<tr><td>Designation"+"</td><td>"+":</td><td>"+schemes.contactThree.designation+"</td></tr>"+
                    "<tr><td>Phone No"+"</td><td>"+":</td><td>"+schemes.contactThree.phoneOne+"</td></tr>"+
                    "<tr><td>Work Phone No"+"</td><td>"+":</td><td>"+schemes.contactThree.phoneTwo+"</td></tr>"+
                    "</table></td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Lists of Schemes ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

}]);

app.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
});

app.controller('requestsController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("requestsController");

    $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.requests").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.requests .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeRequests'}, function (response) {
//            		alert(JSON.stringify(response));
            		   if(!response){
            			window.location.href = "#/noAccessPage";
            		   }
            		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.viewRequestDiv=true;
    $scope.viewRequestDivBack=true;
    $scope.requestDivBack=false;
    $scope.requestDiv=false;
    $scope.editRequestDivBack=false;
    $scope.editRequestDiv=false;
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;
    $scope.getRequest=function () {
        $http({
            "method": "GET",
            //"url": 'api/Requests/getDetails?employeeId=%7B%22employeeId%22%3A%22'+employeeId+'%22%7D',
           "url": 'http://54.189.195.233:3000/api/Requests?filter={"where":{"status": {"neq":"Completed"}}}',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
           console.log("request details"+ JSON.stringify(response));
            $scope.requestLists = response;
            $scope.loadingImage=false;
        }).error(function (response, data) {
            console.log("failure");
        })

    }
    $scope.getRequest();

     $scope.editAssetType = function(assetType) {
              $("#editAssetTypeModel").modal("show");
              $scope.errorMessage=false;
              /*$scope.editCharterArea = true;
               $scope.createCharterArea = false;*/
              $scope.editAssetTypeData=angular.copy(assetType);
     }

     var editRiskGroupFormSubmit = true;
           $scope.editAssetRiskGroupButton=function () {
               var editAssetTypeData= $scope.editAssetTypeData;
               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
               editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
               editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
               if(editRiskGroupFormSubmit){
                   editRiskGroupFormSubmit = false;
                   $http({
                       "method": "PUT",
                       "url": 'http://54.189.195.233:3000/api/Requests/'+$scope.editAssetTypeData.id+'',
                       "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                       "data": editAssetTypeData
                   }).success(function (response, data) {
                       //console.log("filter Schemes "+ JSON.stringify(response));
                       //$scope.loadingImage=true;
                       $("#editAssetTypeModel").modal("hide");
                       $("#editAssetSuccess").modal("show");
                       setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                       $scope.getRequest();
                       //$window.location.reload();
                   }).error(function (response, data) {

                   });
               }
               editRiskGroupFormSubmit = true;
           }
/*    $scope.changeStatus=function (id) {
        if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
            for(var i=0;i<$scope.requestLists.length;i++){
                if(id==$scope.requestLists[i].id){
                    $scope.editRequestDetails=angular.copy($scope.requestLists[i]) ;
                    $scope.viewRequestDiv=false;
                    $scope.viewRequestDivBack=false;
                    $scope.editRequestDivBack=true;
                    $scope.editRequestDiv=true;
                    $scope.requestDivBack=false;
                    $scope.requestDiv=false;
                }
            }
        }
    }
    $scope.viewStatus=function (id) {
        if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
            for(var i=0;i<$scope.requestLists.length;i++){
                if(id==$scope.requestLists[i].id){
                    $scope.viewRequestDetails=angular.copy($scope.requestLists[i]) ;
                    $scope.viewRequestDiv=false;
                    $scope.viewRequestDivBack=false;
                    $scope.editRequestDivBack=false;
                    $scope.editRequestDiv=false;
                    $scope.requestDivBack=true;
                    $scope.requestDiv=true;
                }
            }
        }
    }
    $scope.clickToBack = function(){
        $window.location.reload();
    }

    $scope.acceptStatus = function (request) {

        var updateDetails = {
            'email':request.email,
            "acceptLevel":request.acceptLevel,
            "acceptStatus": "Yes",
            "requestStatus": "Approval",
            "comment": request.comment,
            "requestId":request.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };

        $scope.approvalScheme = true;

        $http({
            "method": "POST",
            "url": 'api/Requests/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            console.log("Approval Schemes "+ JSON.stringify(response));
            $scope.requestLists = JSON.stringify(response.data);
            $window.localStorage.setItem('requestFlag',true);
            $("#acceptStatus").modal("show");
            *//*setTimeout(function(){$('#acceptStatus').modal('hide')}, 3000);*//*
            *//*$window.location.reload();*//*
            //$window.localStorage.getItem('requestTab');

        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.acceptStatusModal = function() {
        $window.location.reload();
    }
    $scope.rejectStatusModal = function() {
        $window.location.reload();
    };
    $scope.changeToViewPage = function() {
        $window.location.reload();
    }
    $scope.rejectStatus = function (request) {
        var updateDetails = {
            'email':request.email,
            "acceptLevel":request.acceptLevel,
            "acceptStatus": "No",
            "requestStatus": "Rejected",
            "comment": request.comment,
            "requestId":request.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };
        $http({
            "method": "POST",
            "url": 'api/Requests/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            console.log("rejectedx Schemes "+ JSON.stringify(response.data));
            $scope.requestLists = response.data;
            $window.localStorage.setItem('requestFlag',true);
            $("#rejectStatus").modal("show");
        }).error(function (response, data) {
            console.log("failure");
        })
    }*/

    $scope.exportToExcel=function(tableId){
        if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

            $("<tr>" +
                "<th>requestId</th>" +
                "<th>createdTime</th>" +
                "<th>schemeName</th>" +
                "<th>name</th>" +
                "<th>emailId</th>" +
                "<th>mobileNumber</th>" +
                "<th>requestStatus</th>" +
                "<th>acceptStatus</th>" +
                "<th>acceptLevel</th>" +
                "<th>maxlevel</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.requestLists.length;i++){
                var request=$scope.requestLists[i];

                $("<tr>" +
                    "<td>"+request.requestId+"</td>" +
                    "<td>"+request.createdTime+"</td>" +
                    "<td>"+request.schemeName+"</td>" +
                    "<td>"+request.name+"</td>" +
                    "<td>"+request.emailId+"</td>" +
                    "<td>"+request.mobileNumber+"</td>" +
                    "<td>"+request.requestStatus+"</td>" +
                    "<td>"+request.acceptStatus+"</td>" +
                    "<td>"+request.acceptLevel+"</td>" +
                    "<td>"+request.maxlevel+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Requests ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

    $scope.viewRequest = function() {
        $scope.loadingImage=false;
        location.href = '#/viewRequest';
    }
    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

}]);

app.controller('addSchemeController', ['schemeManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$routeParams', function(schemeManagement, $http, $scope,Upload, $timeout, $window, $location, $rootScope, $routeParams) {
    console.log("createEditSchemeController");

    $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #schemesLi").addClass("active");
             $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
             $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
             $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes .collapse").addClass("in");
             $(".sidebar-menu li .collapse").removeClass("in");
         });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.uploadURL=uploadFileURL;
    $scope.clickToBack = function(){
        $location.url("/schemes");
    }

    $scope.getEmployeeType=function () {
        $http({
            method: 'GET',
            url: 'api/Employees?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('Users Response :' + JSON.stringify(response));
            $scope.employeeList = response;
        }).error(function (response) {
        });
    };
    $scope.getEmployeeType();

    $scope.showEMPDetails = false;

    $scope.getEmpData = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP = response[0];
            $scope.showEMPDetails = true;
        }).error(function (response) {
        });
    }

    $scope.getEmpData1 = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP1 = response[0];
            $scope.showEMPDetails1 = true;
        }).error(function (response) {
        });
    }

    $scope.getEmpData2 = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP2 = response[0];
            $scope.showEMPDetails2 = true;
        }).error(function (response) {
        });
    }
   $scope.bineficiaries=[];
    $scope.bineficiariesList=[];
    $scope.getBeneficiaries = function() {
        $http({
            method: 'GET',
            url: 'api/beneficiariesLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.beneficiariesLists = response;
            if($scope.beneficiariesLists!=undefined && $scope.beneficiariesLists !=null && $scope.beneficiariesLists.length>0 ){
                for(var i=0;i<$scope.beneficiariesLists.length;i++ ){
                    var data={
                        label:$scope.beneficiariesLists[i].name,
                        id:{id: $scope.beneficiariesLists[i].id,
                            label: $scope.beneficiariesLists[i].name}
                    }
                    $scope.bineficiariesList.push(data);
                }
            }
        }).error(function (response) {
        });
    };
    $scope.getBeneficiaries();

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/Departments?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.departmentList = response;
        }).error(function (response) {
        });
    };
    $scope.getDepartments();
    $scope.example15settings = {enableSearch: true};
    $scope.getMinistries = function() {
        $http({
            method: 'GET',
            url: 'api/ministeriesLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.ministriesLists = response;
        }).error(function (response) {
        });
    };
    $scope.getMinistries();

    var filedetails=[];
    var fileIdsArray=[];
    $scope.uploadFileURL = [];
    var fileUploadStatus=true;

    $scope.uploadFiles = function (files) {
        var count=1;
        filedetails=[];
        fileIdsArray=[];
        var fileCount = 0;
       angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg1 = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                fileUploadStatus = false;
                var fsize=0;
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                   if (file.size == 0){
                   fsize='0 Byte';
                   }else{
                     var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                                 fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
                   }


                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename,
                           'size': fsize
                        }
                        fileIdsArray.push(fileDetails);
                        $scope.uploadFileURL.push(fileDetails);
                        filedetails.push(response.data);
                        fileUploadStatus = true;
                        file.result = response.data;
                        console.log(file.result)
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
//                alert('Please Upload JPEG or PDF files only');

                        $scope.uploadError1=true;
                    $timeout(function(){
                        $scope.uploadError1=false;
                    }, 5000);
            }
            if (fileCount == files.length) {
                $scope.uploadNitif = true;
                $scope.disable = false;
                $scope.errorMssg1 = false;
                $scope.formNotSelected = '';
            }
        });
    };
    var formIdsArray=[];
    $scope.uploadFormsNotif = [];
    var formUploadStatus=true;
    $scope.uploadForms = function (files) {
        formIdsArray=[];
        var fileCount = 0;
        angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;

                var fsize=0;
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                   if (file.size == 0){
                   fsize='0 Byte';
                   }else{
                     var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                                 fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
                   }

                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename,
                           'size': fsize
                        }
                        formIdsArray.push(fileDetails);
                        $scope.uploadFormsNotif.push(fileDetails);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
//                alert('Please Upload JPEG or PDF files only');
                $scope.uploadError=true;
                                    $timeout(function(){
                                        $scope.uploadError=false;
                                    }, 5000);
            }
            if (fileCount == files.length) {
                $scope.uploadFormNotif = true;
                $scope.disable = false;
                $scope.errorMssg = false;
                $scope.formNotSelected = '';
            }
        });
    };
    $scope.reset=function () {
        $scope.scheme={};
        $scope.uploadFormsNotif=[];
        $scope.uploadFileURL=[];
        $scope.bineficiaries=[];
        $scope.showEMPDetails = false;
                $scope.showEMPDetails1 = false;
                $scope.showEMPDetails2 = false;
    };
    $scope.scheme = {};
      var formRequestSubmit = true;
    $scope.createScheme= function () {
        $scope.schemeMessage="";
        $scope.schemeMessageError=false;
        if(formRequestSubmit=true) {
            formRequestSubmit = false;
            if ($scope.bineficiaries != undefined && $scope.bineficiaries != null && $scope.bineficiaries.length > 0) {

                if (fileUploadStatus && formUploadStatus) {
                    if ($scope.uploadFileURL.length > 0) {
                        if ($scope.uploadFormsNotif.length > 0) {
                            if ($scope.scheme.commincement) {
                                var beneficals = [];
                                if ($scope.bineficiaries != null && $scope.bineficiaries.length > 0) {
                                    for (var i = 0; i < $scope.bineficiaries.length; i++) {
                                        beneficals.push($scope.bineficiaries[i].id.id)
                                    }
                                }
                                            var schemeDetails = $scope.scheme;
                                            schemeDetails.file = $scope.uploadFileURL;
                                            schemeDetails.forms = $scope.uploadFormsNotif;
                                            schemeDetails.status = 'Active';
                                            schemeDetails.bineficiaries = beneficals;
                                            $scope.schemeDetailsAre = schemeDetails;
                                           $http({
                                                method: 'POST',
                                                url: 'api/Schemes',
                                                headers: {"Content-Type": "application/json", "Accept":"application/json"},
                                                "data": $scope.schemeDetailsAre
                                            }).success(function (response, data) {
                                                $('#addSchemeSuccess').modal('show');
                                                $scope.schemeDetailsAre = response;
                                                $scope.uploadFileURL = [];
                                                $scope.uploadFormsNotif = [];
                                             }).error(function (response) {

                                                formRequestSubmit = true;
                                                if (response.error.details.messages.name) {
                                                    $scope.errorMessageData = response.error.details.messages.name[0];
                                                    $scope.errorMessage = true;
                                                    $timeout(function () {
                                                        $scope.errorMessage = false;
                                                    }, 3000);
                                                }
                                            });

                            } else {
                                formRequestSubmit = true;
                                $timeout(function () {
                                    $timeout(function () {
                                        $scope.formNotSelected = "Please Select Date of Commencement";
                                    }, 500);
                                }, 500);
                            }
                        }
                        else {

                            formRequestSubmit = true;
                            $timeout(function () {
                                $timeout(function () {
                                    $scope.formNotSelected = "Please upload the required Forms";
                                }, 500);
                            }, 500);
                        }
                    } else {
                        formRequestSubmit = true;

                        $timeout(function () {
                            $timeout(function () {
                                $scope.formNotSelected = "Please upload the required Notifications";
                            }, 500);
                        }, 500);
                    }
                } else {
                    formRequestSubmit = true;
                    $scope.createScheme();
                }
            } else {
                formRequestSubmit = true;
                $timeout(function () {
                    $timeout(function () {
                        $scope.formNotSelected = "Please Select Beneficiaries";
                    }, 500);
                }, 500);
            }
        }

    };
    $scope.addSchemeSuccess = function() {
        $('#addSchemeSuccess').modal('hide');
        location.href = '#/schemes';
        $window.location.reload();
    };

    $scope.removeFiles = function(index, fileId, type){
        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            if(type == 'file') {
                console.log('File');
                $scope.uploadFileURL.splice(index, 1);
            }else{
                console.log('Form');
                $scope.uploadFormsNotif.splice(index, 1);
            }
        });
    };
}]);

app.controller('editSchemeController', ['schemeManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$routeParams', function(schemeManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $routeParams) {
    console.log("editSchemeController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
    //    alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.clickToBack = function(){
        $location.url("/schemes");
    }
    $scope.uploadURL=uploadFileURL;
    var filedetails=[];
    var fileIdsArray=[];
    $scope.uploadFileName = [];
    var fileUploadStatus=true;
    $scope.uploadFiles = function (files) {
        var count=1;
        filedetails=[];
        fileIdsArray=[];
        var fileCount = 0;
        angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg1 = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                fileUploadStatus = false;
                   var fsize=0;
                                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                                   if (file.size == 0){
                                   fsize='0 Byte';
                                   }else{
                                     var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                                                 fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
                                   }
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename,
                             'size':fsize
                        }
                        fileIdsArray.push(fileDetails);
                        $scope.scheme.file.push(fileDetails);
                        $scope.uploadFileName.push(fileDetails);
                        filedetails.push(response.data);
                        fileUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.filesUplaod = true;
                $scope.disable = false;
                $scope.errorMssg1 = false;
            }
        });
    };

    var formIdsArray=[];
    var formUploadStatus=true;
    $scope.uploadFromName = [];
    $scope.uploadForms = function (files) {
        formIdsArray=[];
        var fileCount = 0;
       angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                   var fsize=0;
                    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                       if (file.size == 0){
                       fsize='0 Byte';
                       }else{
                         var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                                     fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
                       }
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename,
                           'size': fsize
                        };
                        formIdsArray.push(fileDetails);
                        $scope.scheme.forms.push(fileDetails);
                        $scope.uploadFromName.push(fileDetails);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.formsUplaod = true;
                $scope.disable = false;
                $scope.errorMssg = false;
            }
        });
    };

    $scope.reset=function () {
        $scope.scheme={}
    }
    $scope.clickToBack = function(){
        $location.url("/schemes");
    }
    $scope.getEmployeeType=function () {
        $http({
            method: 'GET',
            url: 'api/Employees?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.employeeList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getEmployeeType();
    $scope.bineficiaries=[];
    $scope.bineficiariesList=[];
    $scope.getBeneficiaries = function() {
        $http({
            method: 'GET',
            url: 'api/beneficiariesLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.beneficiariesLists = response;
            if($scope.beneficiariesLists!=undefined && $scope.beneficiariesLists !=null && $scope.beneficiariesLists.length>0 ){
                for(var i=0;i<$scope.beneficiariesLists.length;i++ ){
                    var data={
                        label:$scope.beneficiariesLists[i].name,
                        id: $scope.beneficiariesLists[i].name
                    }
                    $scope.bineficiariesList.push(data);
                }
            }
        }).error(function (response) {
            console.log('Error getBeneficiaries :' + JSON.stringify(response));
        });
    }
    $scope.getBeneficiaries();

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/Departments?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.departmentList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDepartments();

    $scope.getMinistries = function() {
        $http({
            method: 'GET',
            url: 'api/ministeriesLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.ministriesLists = response;
        }).error(function (response) {
            console.log('Error getMinistries :' + JSON.stringify(response));
        });
    }
    $scope.getMinistries();


    $scope.getDesignation=function () {
        $http({
            method: 'GET',
            url: 'api/Designations',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.designationList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDesignation();

    $scope.pack=$routeParams.packId;
    console.log('edit scheme details :' + JSON.stringify($scope.pack));

    $http({
        method: 'GET',
        url: 'api/Schemes/'+$scope.pack,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        $scope.scheme = response;
        console.log("responce......"+ JSON.stringify($scope.scheme));
        var benfiList=response.bineficiaries;
        $scope.getEmpData($scope.scheme.contactOne.employeeId);
        $scope.getEmpData1($scope.scheme.contactTwo.employeeId);
        $scope.getEmpData2($scope.scheme.contactThree.employeeId);

        if(benfiList!=undefined && benfiList!=null && benfiList.length>0){
            for(var i=0;i<benfiList.length;i++){
                if(benfiList[i]!=null){
                    var dataId={
                        id: benfiList[i].label
                    };
                    $scope.bineficiaries.push(dataId);
                }
            }
        }

    }).error(function (response) {
        console.log('Error Response :' + JSON.stringify(response));
    });

    $scope.getEmpData = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('EMP data 1...... :    ' + JSON.stringify(response));
            $scope.selectedEMP = response[0];
            $scope.showEMPDetails = false;
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.getEmpData1 = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP1 = response[0];
            $scope.showEMPDetails1 = false;
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

     $scope.getEmpData2 = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP2 = response[0];
            $scope.showEMPDetails2 = false;
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.editScheme=function(){
        if(fileUploadStatus && formUploadStatus) {
                    if ($scope.scheme.file.length > 0) {
                        if ($scope.scheme.forms.length > 0) {
            var beneficals = [];
            if ($scope.bineficiaries != null && $scope.bineficiaries.length > 0) {
                for (var i = 0; i < $scope.bineficiaries.length; i++) {
                    beneficals.push($scope.bineficiaries[i].id)
                }
                var schemeDetails = $scope.scheme;
                schemeDetails.bineficiaries = beneficals;
                $http({
                    "method": "PUT",
                    "url": 'api/Schemes/' + $scope.scheme.id,
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": $scope.scheme
                }).success(function (response, data) {
                    $scope.scheme = response;
                    $('#editSchemeSuccess').modal('show');
                }).error(function (response, data) {
                    console.log("failure");
                })
            }else{

             $scope.createEmpError = 'Please Select Beneficiaries';
                $scope.empCreationError = true;

                $timeout(function(){
                    $scope.empCreationError=false;
                }, 3000);
            }
            }
                 else {
                     $scope.createEmpError = 'Please upload the required Forms';
                     $scope.empCreationError = true;

                     $timeout(function(){
                         $scope.empCreationError=false;
                     }, 3000);
                 }
             } else {
                 $scope.createEmpError = 'Please upload the required Notifications';
                  $scope.empCreationError = true;

                  $timeout(function(){
                      $scope.empCreationError=false;
                  }, 3000);
             }

        }else{
            $scope.editScheme();
        }
    }
    $scope.editSchemeSuccessModal = function() {
        $('#editSchemeSuccess').modal('hide');
        location.href = '#/schemes';
        $window.location.reload();
    }

    $scope.removeFile = function(index, fileId, type){

        if(type == 'file') {
            console.log('File');
            $scope.scheme.file.splice(index, 1);
            for(var i=0; i<$scope.uploadFileName.length; i++){
                if($scope.uploadFileName[i].id == fileId){
                    $scope.uploadFileName.splice(i,1);
                }
            }
            if($scope.uploadFileName.length == 0) {
                $scope.filesUplaod = false;
            }
        }else{
            console.log('Form');
            $scope.scheme.forms.splice(index, 1);
            for(var i=0; i<$scope.uploadFromName.length; i++){
                if($scope.uploadFromName[i].id == fileId){
                    $scope.uploadFromName.splice(i,1);
                }
            }
            if($scope.uploadFromName.length == 0) {
                $scope.formsUplaod = false;
            }
        }
    };

}]);

app.controller('viewSchemeController',  ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', function(schemeManagement, $http, $scope, $window, $location, $rootScope, $routeParams) {
    console.log("viewSchemeController");

    $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #schemesLi").addClass("active");
             $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
             $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
             $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes .collapse").addClass("in");
             $(".sidebar-menu li .collapse").removeClass("in");
         });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.loadingImage=true;
        $scope.pack=$routeParams.packId;
    $scope.uploadURL=uploadFileURL;
    $http({
        method: 'GET',
        url: 'api/Schemes/'+$scope.pack,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        $scope.loadingImage=false;
        $scope.scheme = response;
    }).error(function (response) {
        console.log('Error Response :' + JSON.stringify(response));
    });
    $scope.clickToBack = function(){
        $location.url("/schemes");
    }

}]);

app.controller('editRequestController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, $routeParams, DTOptionsBuilder, DTColumnBuilder) {
    console.log("editRequestController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #schemes li.requests").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #schemesLi.treeview").addClass("active");
        $(".sidebar-menu #schemes li.requests .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeRequests'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.clickToBack = function(){
        $location.url("/requests");
    }
    $scope.requestDetails=$routeParams.requstId;
    $scope.uploadFile=uploadFileURL;
    $http({
        "method": "GET",
        "url": 'api/Requests/'+$scope.requestDetails,
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $scope.loadingImage=false;
        $scope.requestLists = response;
    }).error(function (response, data) {
        console.log("failure");
    })

    $scope.acceptStatus = function (request) {
        var updateDetails = {
            'email':request.email,
            "acceptStatus": "Yes",
            "requestStatus": "Approval",
            "comment": request.comment
        };
        $scope.approvalScheme = true;
        $http({
            "method": "PUT",
            "url": 'api/Requests/'+ request.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            $scope.requestLists = response;
            $scope.loadingImage=false;
            location.href = '#/requests';
            $window.localStorage.setItem('requestFlag',true);
        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.rejectStatus = function (request) {
        var updateDetails = {
            'email':request.email,
            "acceptStatus": "No",
            "requestStatus": "Rejected",
            "comment": request.comment
        };
        $http({
            "method": "PUT",
            "url": 'api/Requests/'+ request.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            $scope.requestLists = response;
            $scope.loadingImage=false;
            location.href = '#/requests';
            $window.localStorage.setItem('requestFlag',true);
        }).error(function (response, data) {
            console.log("failure");
        })
    }

}]);

app.controller('viewRequestController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, $routeParams,Excel,$timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("viewRequestController");

    $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.requestsReport").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.requestsReport .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeRequests'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.clickToBack = function(){
        $scope.viewRequestDiv=true;
        $scope.viewRequestDivBack=true;
        $scope.requestDivBack=false;
        $scope.requestDiv=false;
    }
    $scope.requestDetails=$routeParams.requstId;
    $scope.uploadFile=uploadFileURL;
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    $http({
        "method": "GET",
        //"url": 'api/Requests?filter=%7B%22where%22%3A%7B%22or%22%3A%5B%7B%20%22requestStatus%22%3A%20%22Rejected%22%7D%2C%7B%20%22requestStatus%22%3A%20%22Approval%22%7D%5D%7D%7D',
        "url": 'http://54.189.195.233:3000/api/Requests?filter={"where":{"status": "Completed"}}',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $scope.loadingImage=false;
        $scope.requestLists = response;
    }).error(function (response, data) {
        console.log("failure");
    })
    $scope.viewRequestDivBack=true;
    $scope.viewRequestDiv=true;
    /*$scope.viewStatus=function (id) {
        $scope.loadingImage=false;
        if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
            for(var i=0;i<$scope.requestLists.length;i++){
                if(id==$scope.requestLists[i].id){
                    $scope.viewRequestDetails=angular.copy($scope.requestLists[i]) ;
                    $scope.viewRequestDiv=false;
                    $scope.viewRequestDivBack=false;
                    $scope.requestDivBack=true;
                    $scope.requestDiv=true;
                }
            }
        }
    }*/
    $scope.exportToExcel=function(tableId){
        if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

            $("<tr>" +
                "<th>schemeName</th>" +
                "<th>requestId</th>" +
                "<th>Name</th>" +
                "<th>emailId</th>" +
                "<th>mobileNumber</th>" +
                "<th>createdTime</th>" +
                "<th>acceptLevel</th>" +
                "<th>maxlevel</th>" +
                "<th>finalStatus</th>" +
                "<th>requestStatus</th>" +
                "<th>acceptStatus</th>" +
                "<th>comment</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.requestLists.length;i++){
                var schemes=$scope.requestLists[i];

                $("<tr>" +
                    "<td>"+schemes.schemeName+"</td>" +
                    "<td>"+schemes.requestId+"</td>" +
                    "<td>"+schemes.name+"</td>" +
                    "<td>"+schemes.emailId+"</td>" +
                    "<td>"+schemes.mobileNumber+"</td>" +
                    "<td>"+schemes.createdTime+"</td>" +
                    "<td>"+schemes.acceptLevel+"</td>" +
                    "<td>"+schemes.maxlevel+"</td>" +
                    "<td>"+schemes.finalStatus+"</td>" +
                    "<td>"+schemes.requestStatus+"</td>" +
                    "<td>"+schemes.acceptStatus+"</td>" +
                    "<td>"+schemes.comment+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);


//*********** schemeFormsController**************************************
app.controller('schemeFormsController', function($http, $scope, $window, $location, $rootScope) {
    $scope.getScheme = function () {
        $http({
            "method": "GET",
            "url": 'api/SchemeForms',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.SchemeData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getScheme();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.scheme = {};
});
//*********** schemeFormsController**************************************

//*********** createSchemeFormsController*********************************

app.controller('createSchemeFormsController', function($http, $scope, Upload, $timeout,$window, $location, $rootScope) {
    var filedetails=[];
    var fileIdsArray=[];
    var fileUploadStatus=true;
    $scope.getModules = function() {
        $http({
            method: 'GET',
            url: 'api/Schemes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.module = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getModules();
    $scope.getScheme = function () {
        $http({
            "method": "GET",
            "url": 'api/SchemeForms',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.SchemeData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getScheme();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.scheme = {};
    var formIdsArray=[];
    var formUploadStatus=true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        formIdsArray=[];
        $scope.forms = files;
        angular.forEach(files, function(file) {
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });
    };
    $scope.createSchemeForm = function() {
       if(fileUploadStatus && formUploadStatus){
            var schemeDetails=$scope.user;
            schemeDetails.file=fileIdsArray;
            schemeDetails.forms=$scope.docList;
            $http({
                method: 'POST',
                url: 'api/SchemeForms',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: schemeDetails
            }).success(function (response) {
                location.href = '#/schemeForms';
                $rootScope.SchemeData.push(response);
                $scope.user = {};
                $scope.docList = [];
                $('#myModal').modal('hide');
            }).error(function (response) {
                console.log('Error Response1 :' + JSON.stringify(response));
            });

        }else{
            $scope.createSchemeForm();
        }
    }
});
//*********** createSchemeFormsController*********************************
//*********** viewSchemeFormsController*********************************

app.controller('viewSchemeFormsController', function($http, $scope, $window, $location, $rootScope, $routeParams) {
    $scope.schemeDetails=$routeParams.formId;
       $http({
        "method": "GET",
        "url": 'api/SchemeForms/'+$scope.schemeDetails,
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        console.log("formId Schemes "+ JSON.stringify(response));
        $scope.schemeLists = response;
    }).error(function (response, data) {
        console.log(" schemeDetails failure");
    })
});


app.controller('beneficiariesListController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(schemeManagement, $http, $scope, $window, $location, $rootScope, $routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {


  $(document).ready(function () {
              $('html,body').scrollTop(0);
              $(".sidebar-menu li ul > li").removeClass('active1');
              $('[data-toggle="tooltip"]').tooltip();
              $(".sidebar-menu #schemesLi").addClass("active");
              $(".sidebar-menu #schemesLi.treeview #configLi").addClass("active");
              $(".sidebar-menu #schemesLi.treeview #configLi.active #masterDataschemeSetUp").addClass("menu-open");
              $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.beneficiariesList").addClass("active1").siblings('.active1').removeClass('active1');
              $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.beneficiariesList .collapse").addClass("in");
              $(".sidebar-menu li .collapse").removeClass("in");
          });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'beneficiaries'}, function (response) {
//        		alert(JSON.stringify(response));
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.loadingImage=true;
    $scope.errorMessage=false;
    $scope.addBeneficiariesModel = function() {
        $("#addBeneficiaries").modal("show");
        $scope.beneficiaries = {};
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.addBeneficiaries=function () {
        if($scope.beneficiaries.name != null && $scope.beneficiaries.name != "" && $scope.beneficiaries.name != undefined) {
            if ($scope.beneficiaries.status != null && $scope.beneficiaries.status != "" && $scope.beneficiaries.status != undefined) {
                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                var beneficiaries=$scope.beneficiaries;
                beneficiaries['createdPerson']=loginPersonDetails.name;
                $http({
                    method: 'POST',
                    url: 'api/beneficiariesLists',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": beneficiaries
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $("#addBeneficiaries").modal("hide");
                    $("#addBenifSuccess").modal("show");
                    setTimeout(function(){$('#addBenifSuccess').modal('hide')}, 3000);
                    $scope.getBeneficiaries();
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }
                });
            } else {
                $scope.errorShow = true;
                $scope.showError = 'Please Select Status';
                $timeout(function(){
                    $scope.errorShow=false;
                }, 3000);
            }
        } else {
            $scope.errorShow = true;
            $scope.showError = 'Please Enter Beneficiaries Name ';
            $timeout(function(){
                $scope.errorShow=false;
            }, 3000);
        }
    }
    $scope.getBeneficiaries = function() {
        $http({
            method: 'GET',
            url: 'api/beneficiariesLists',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.beneficiariesLists = response;
        }).error(function (response) {
        });
    }
    $scope.getBeneficiaries();
    $scope.editBeneficiaries = function(beneficiaries) {
        $("#editBeneficiaries").modal("show");
        $scope.editBeneficiariesLists = angular.copy(beneficiaries);
    }
    $scope.editBeneficiariesButton=function () {
        var editBeneficiariesDetails= $scope.editBeneficiariesLists;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editBeneficiariesDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/beneficiariesLists/'+$scope.editBeneficiariesLists.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editBeneficiariesDetails
        }).success(function (response, data) {
            $scope.loadingImage=false;
            $("#editBeneficiaries").modal("hide");
            $("#editBenifSuccess").modal("show");
            setTimeout(function(){$('#editBenifSuccess').modal('hide')}, 3000);
            $scope.getBeneficiaries();
        }).error(function (response, data) {
            console.log("failure");
        })
    }


    $scope.exportToExcel=function(tableId) {
        if( $scope.beneficiariesLists!=null &&  $scope.beneficiariesLists.length>0){

            $("<tr>" +
                "<th>Beneficiaries Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.beneficiariesLists.length;i++){
                var beneficiaries=$scope.beneficiariesLists[i];
                $("<tr>" +
                    "<td>"+beneficiaries.name+"</td>" +
                    "<td>"+beneficiaries.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

//app.controller('ministriesListController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(schemeManagement, $http, $scope, $timeout, $window, $location, $rootScope, $routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
app.controller('ministriesListController', ['schemeManagement', '$http', '$scope', '$timeout', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(schemeManagement, $http, $scope, $timeout, $window, $location, $rootScope, $routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("ministriesListController");


    $(document).ready(function () {
                $('html,body').scrollTop(0);
                $(".sidebar-menu li ul > li").removeClass('active1');
                $('[data-toggle="tooltip"]').tooltip();
                $(".sidebar-menu #schemesLi").addClass("active");
                $(".sidebar-menu #schemesLi.treeview #configLi").addClass("active");
                $(".sidebar-menu #schemesLi.treeview #configLi.active #masterDataschemeSetUp").addClass("menu-open");
                $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.ministriesList").addClass("active1").siblings('.active1').removeClass('active1');
                $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.ministriesList .collapse").addClass("in");
                $(".sidebar-menu li .collapse").removeClass("in");
            });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'ministries'}, function (response) {
//        		alert(JSON.stringify(response));
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.loadingImage=true;
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.errorMessage=false;
    $scope.addministryModal = function(){
        $scope.ministries = {}
        $("#addMinistries").modal("show");
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.addMinistries=function () {
        var ministries=$scope.ministries;
        ministries['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/ministeriesLists',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data": ministries
        }).success(function (response) {
            $scope.loadingImage=false;
            $("#addMinistries").modal("hide");
            $('#addMinistriesSuccess').modal('show');
            setTimeout(function(){$('#addMinistriesSuccess').modal('hide')}, 3000);
            $scope.getMinistries();
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
                $timeout(function(){
                    $scope.errorMessage=false;
                }, 3000);
            }

            console.log('Error addMinistries :' + JSON.stringify(response));
        });
    }
    $scope.getMinistries = function() {
        $http({
            method: 'GET',
            url: 'api/ministeriesLists',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.ministriesLists = response;
        }).error(function (response) {
        });
    }
    $scope.getMinistries();

    $scope.editMinistries = function(ministries) {
        $("#editMinistries").modal("show");
        $scope.editMinistriesLists = angular.copy(ministries);
        $scope.editMinistriesListsOld = angular.copy(ministries);
    }

    $scope.editMinistriesButton=function () {
        var editMinistriesDetails= $scope.editMinistriesLists;
        var editMinistriesDetailsOld= $scope.editMinistriesListsOld;
        $scope.editMinistriesDetailsOld1 = editMinistriesDetailsOld;
        editMinistriesDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/ministeriesLists/'+$scope.editMinistriesLists.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editMinistriesDetails
        }).success(function (response, data) {
            $scope.loadingImage=false;
            $("#editMinistries").modal("hide");
            $('#editMinistriesSuccess').modal('show');
            setTimeout(function(){$('#editMinistriesSuccess').modal('hide')}, 3000);
            $scope.getMinistries();
        }).error(function (response, data) {
            //console.log("failure");
        })
    }
    $scope.exportToExcel=function(tableId){
        if( $scope.ministriesLists!=null &&  $scope.ministriesLists.length>0){

            $("<tr>" +
                "<th>Ministry Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.ministriesLists.length;i++){
                var ministries=$scope.ministriesLists[i];

                $("<tr>" +
                    "<td>"+ministries.name+"</td>" +
                    "<td>"+ministries.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Ministry List Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }
}]);

//********************* Admin loginController For SSO integration****************************************
/*app.controller('loginController', function($scope, $http, $rootScope, $window, $location) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    console.log('Login Controller');
    $scope.user = {};
    $scope.showLoginError = false;
    $scope.loginError = '';
    $scope.loginSubmit = function(){
        $scope.showLoginError = false;
        $scope.loginError = '';
        if(checkMailFormat($scope.user.email)) {
            $http({
                method: 'POST',
                url: 'api/Employees/login',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.user
            }).success(function (response) {
               /!* $window.localStorage.setItem('accessToken', response.id);
                $window.localStorage.setItem('tokenId', response.id);*!/
                 $rootScope.userLogin = true;
                $scope.user = {};
                if(response.userInfo) {
                    //$rootScope.userName = response.userInfo.name;
                    //$window.localStorage.setItem('userName', response.userInfo.name);
                    $window.localStorage.setItem('employeeId', response.userInfo.employeeId);
                    window.location.href ="selectModule.html";
                   /!* $window.localStorage.setItem('userDetails', JSON.stringify(response.userInfo));
                    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
                    $window.localStorage.setItem('afterLogin', 'true');
                    $scope.roleList= response.roleObject;
                    $window.localStorage.setItem('roleList', JSON.stringify( $scope.roleList));

                    var employeeId=response.userInfo.employeeId;
                    var details={
                        'employeeId':employeeId
                    }*!/
                }
            }).error(function (response) {
                $scope.showLoginError = true;
                $scope.loginError = 'Invalid Email or Password';
            });
        }else{
            $scope.showLoginError = true;
            $scope.loginError = 'Please enter a valid Email';
        }
    };
    $scope.forgotPasswordData = function (email) {
        $rootScope.email = '';
        $scope.successMessage='';
        $scope.message='';
        var email1 = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        $rootScope.email = document.getElementById('email').value;
        var searchData=$rootScope.email;
        if ($rootScope.email == undefined || $rootScope.email == '' || $rootScope.email == null) {
            $rootScope.notRegisterd = "Enter your Email";
        } else if (email1.test($rootScope.email)) {
            $http({
                method: "GET",
                url: 'api/Employees/forgotPassword?searchData={"emailId":"' + $rootScope.email+'"}',
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
            }).success(function (response) {
              if(response.data.message=="Please Register With US")
                {
                    $scope.message="Invalid Email"
                }
                else {
                    $scope.successMessage="Your password is send to your Email"
                }
            }).error(function (response) {
            });
        } else {
            $rootScope.notRegisterd = "Please Enter Valid Email";
        }
    }
});*/
//********************* Admin loginController*****************************************


app.controller('loginController', function($scope, $http, $rootScope, $window, $location) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    console.log('Login Controller');
    $scope.user = {};
    $scope.showLoginError = false;
    $scope.loginError = '';
    $scope.loginSubmit = function(){
        $scope.showLoginError = false;
        $scope.loginError = '';
        if(checkMailFormat($scope.user.email)) {
            // alert($scope.user);
            $http({
                method: 'POST',
                url: 'api/Employees/login',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.user
            }).success(function (response) {
               // alert('2');
                console.log('login responseeeeeeeeee ', response);
                $window.localStorage.setItem('accessToken', response.id);
                $window.localStorage.setItem('tokenId', response.id);
                $rootScope.userLogin = true;
                $scope.user = {};
                if(response.userInfo) {
                    $rootScope.userName = response.userInfo.name;
                    $window.localStorage.setItem('userName', response.userInfo.name);
                    $window.localStorage.setItem('employeeId', response.userInfo.employeeId);
                    //$window.localStorage.setItem('selectModule', response.userInfo.name);
                    $window.localStorage.setItem('userDetails', JSON.stringify(response.userInfo));
                    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
                    $window.localStorage.setItem('afterLogin', 'true');
                    $scope.roleList= response.roleObject;
                    $window.localStorage.setItem('roleList', JSON.stringify( $scope.roleList));
                    window.location.href = "/admin/#/schemModulelogin/"+response.userInfo.employeeId;
                    var employeeId=response.userInfo.employeeId;
                    var details={
                        'employeeId':employeeId
                    }
                }
            }).error(function (response) {
              //  alert(JSON.stringify(response));
                $scope.showLoginError = true;
                $scope.loginError = 'Invalid Email or Password';
            });
        }else{
            $scope.showLoginError = true;
            $scope.loginError = 'Please enter a valid Email';
        }
    };
    $scope.forgotPasswordData = function (email) {
        $rootScope.email = '';
        $scope.successMessage='';
        $scope.message='';
        var email1 = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        $rootScope.email = document.getElementById('email').value;
        var searchData=$rootScope.email;
        if ($rootScope.email == undefined || $rootScope.email == '' || $rootScope.email == null) {
            $rootScope.notRegisterd = "Enter your Email";
        } else if (email1.test($rootScope.email)) {
            $http({
                method: "GET",
                url: 'api/Employees/forgotPassword?searchData={"emailId":"' + $rootScope.email+'"}',
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
            }).success(function (response) {
               // alert('1')
                if(response.data.message=="Please Register With US")
                {
                    $scope.message="Invalid Email"
                }
                else {
                    $scope.successMessage="Your password is send to your Email"
                }
            }).error(function (response) {
            });
        } else {
            $rootScope.notRegisterd = "Please Enter Valid Email";
        }
    }
});



//*********************index controller*******************
app.controller('indexController', function($scope, $rootScope, $window, $location, $http) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        var a = moment().format('MMMM Do YYYY, h:mm:ss a');
        $scope.currentDate = a;

    });
            $scope.loading = true;
    $scope.clickMe = function() {
    console.log("..................");
                $scope.loading = false;
          }
          $scope.clickMe();

    $rootScope.userLogin = false;
    $rootScope.userName = $window.localStorage.getItem('userName');
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));

     var documentURL=document.URL;
    var res = documentURL.split("schemModulelogin");
    if($scope.userInfo){
        $rootScope.userLogin = true;
    }else{
        if(res.length>1){

        }else{
            location.href="http://"+$location.host()+":3003/admin/login.html#/";
        }

    }
    var roleLogin=   JSON.parse($window.localStorage.getItem('roleList'));
    if(roleLogin){
        $rootScope.roleLogin=roleLogin;
    }
    $scope.getNotification=function (employeeId) {
       // alert('empid', employeeId)
        $http({
            method: 'GET',
            url: 'api/notifications/getNotificationDetails?employeeId='+employeeId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $rootScope.notificationData=response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $rootScope.updateNotificationStatus=function(notificaion){
        var notificationData={
            'notificationId':notificaion.id,
            'employeeId':$scope.userInfo.employeeId
        }
        $http({
            method: 'POST',
            url: 'api/notifications/updateNotification',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data:notificationData
        }).success(function (response) {
            window.location.href = '#/'+notificaion.urlData;
            $window.location.reload();
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }


    if($scope.userInfo){
        $scope.getNotification($scope.userInfo.employeeId);
    }
    $rootScope.logout = function () {
        //debugger;
        //alert('h')
        $window.localStorage.clear();
        window.location.href = "http://"+$location.host()+":3003/admin/login.html";
        $window.location.reload();
    };
});
//***********************index controller******************************
//***********************userProfileController******************************
app.controller('userProfileController', function($http, $scope, $timeout, $window, $location, $rootScope) {
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
     $scope.getEmployee = function () {
        $http({
            "method": "GET",
            "url": 'api/Employees',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.employeeData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getEmployee();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.employee = {};
    $scope.userNameChange = function (name,id) {
        $scope.accessToken = $window.localStorage.getItem('accessToken');
        $http({
            method: "PUT",
            url: 'api/Employees/'+ id, /*+ '?access_token=' + $scope.accessToken*/
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: {
                "name": name
            }
        }).success(function (response) {
            $window.localStorage.setItem('userDetails', JSON.stringify(response));
            $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
            $("#changeNameField").modal('hide');
            $window.location.reload();
        }).error(function (response) {
        });

    }
    $scope.changeMobileNumber = function(userInfo) {
        $("#editPhone").modal("show");
        $scope.editMobileNumber=angular.copy(userInfo);
    }
    var phoneno = /^[0-9]{1}[0-9]{9}$/;
    $scope.editMobileSubmit = function (mobile,id) {
        if (phoneno.test($scope.editMobileNumber.mobile) || $scope.editMobileNumber.mobile<10) {
                $scope.accessToken = $window.localStorage.getItem('accessToken');
                $http({
                    method: "PUT",
                    url: 'api/Employees/'+id,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: {
                        "mobile": mobile
                    }
                }).success(function (response) {
                    $window.localStorage.setItem('userDetails', JSON.stringify(response));
                    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
                    $("#editPhone").modal('hide');
                    $("#mobileNumberSuccess").modal('show');
                    setTimeout(function(){$('#mobileNumberSuccess').modal('hide')}, 3000);
                    //$window.location.reload();
    
                }).error(function (response) {
                });

         } else {
            $scope.errorMessage = true;
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);
        }
    }

    $scope.editEmailSubmit = function (email,id) {
        $scope.accessToken = $window.localStorage.getItem('accessToken');
        $http({
            method: "PUT",
            url: 'api/Employees/'+id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: {
                "email": email
            }
        }).success(function (response) {
            $window.localStorage.setItem('userDetails', JSON.stringify(response));
            $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
            $window.location.reload();
            $("#editEmailField").modal('hide');
        }).error(function (response) {
        });
    }
    /*change password start*/
    $scope.changePasswordModal = function () {
        $('#changePassword').modal('show');
        /*$scope.user = {};*/
        document.getElementById("password").value = '';
        document.getElementById("pswd").value = '';
        document.getElementById("pswd1").value = '';
    }
    $scope.user = {};
    $scope.changePasswordSubmit = function () {

        $scope.tokenId = $window.localStorage.getItem("tokenId");
        //alert("tokenId" +$scope.tokenId);

        var currentpassword = document.getElementById("password").value;
        //alert("current password:::" +currentpassword);
        var password = document.getElementById("pswd").value;
        //alert(" password:::" +password);
        var cpasswor = document.getElementById("pswd1").value;
        //alert("confirm password:::" +cpasswor);
        $scope.passwordError1 = "";

        if (currentpassword != '' && currentpassword != undefined && currentpassword != null
            && password != '' && password != undefined && password != null
            && cpasswor != '' && cpasswor != undefined && cpasswor != null) {
            if (password.length >= 6) {
                if (password == cpasswor) {
                    $http({
                        "method": "POST",
                        "url": 'api/Employees/login',
                        "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                        "data": {
                            "email": $scope.userInfo.email,
                            "password": $scope.user.password
                        }
                    }).success(function (response) {

                        console.log("response" +JSON.stringify(response));
                        $scope.tokenId = response.id;
                        $scope.userId = response.userId;
                        $window.localStorage.setItem('accessToken', $scope.tokenId);

                        $http({
                            method: "PUT",
                            url:   "api/Employees/" + $scope.userInfo.id ,
                            headers: {'Accept': 'application/json'},
                            data: {"password": $scope.user.newPassword}
                        }).success(function (response, data) {
                            console.log("Password Response:" + JSON.stringify(response));
                            //document.getElementById('myform').reset();
                            $('#changePassword').modal('hide');
                            $('#changePasswordSuccess').modal('show');
                            setTimeout(function(){$('#changePasswordSuccess').modal('hide')}, 2000);
                            //$scope.passwordError = "Password Updated Successfully";
                            //console.log("Password Updated Successfully");

                        }).error(function (response) {
                            //console.log("Error:" + JSON.stringify(response));
                            //$scope.passwordError = true;
                            $scope.passwordError1 = "Something Went Wrong. Please Try Again Later";
                            //console.log("Something Went Wrong. Please Try Again Later");

                            $scope.errorMessagePSW = true;
                            $timeout(function(){
                                $scope.errorMessagePSW=false;
                            }, 3000);

                        })
                    }).error(function (data) {
                        $scope.passwordError1 = "Please Enter Correct Password";
                        //console.log("Please Enter Correct Password");

                        $scope.errorMessagePSW = true;
                        $timeout(function(){
                            $scope.errorMessagePSW=false;
                        }, 3000);

                    })
                } else {
                    $scope.passwordError1 = "New password & confirm password does not match";
                    //console.log("Password Confirmation Unsuccessful.");
                    $scope.errorMessagePSW = true;
                    $timeout(function(){
                        $scope.errorMessagePSW=false;
                    }, 3000);
                }
            }
            else {
                console.log("Enter Minimum 6 Characters");

            }
        } else {
            console.log("All Fields Are Mandatory.");

        }
    }


    /*change password end*/






});
//***********************userProfileController******************************

function checkMailFormat(checkData){
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(checkData.match(mailFormat)){
        return true;
    }else{
        ////////alert('Entered');
        return false;
    }
}

function checkAlphaNumeric(checkData){
    var alphaNumeric =/^[a-zA-Z0-9]+$/;
    if(checkData.match(alphaNumeric)){
        return true;
    }else{
        return false;
    }

}

function checkPhoneNumber(checkData){
    var phoneNumber =/^[7-9]{1}[0-9]{9}$/;
    if(checkData.match(phoneNumber)){
        return true;
    }
    else{
        return false;
    }
}

//app.controller('departmentController', function($http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
app.controller('departmentController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("departmentController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #schemesLi").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #configLi").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #configLi.active #masterDataschemeSetUp").addClass("menu-open");
            $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.schemeDepartment").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.schemeDepartment .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

 var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeDepartment'}, function (response) {
//    		alert(JSON.stringify(response));
    		   if(!response){
    			window.location.href = "#/noAccessPage";
    		   }
    		});

    /*function WithBootstrapOptionsCtrl(DTOptionsBuilder, DTColumnBuilder) {
        var vm = this;
        vm.dtOptions = DTOptionsBuilder
            // .fromSource('data.json')
            // .withDOM('&lt;\'row\'&lt;\'col-xs-6\'l&gt;&lt;\'col-xs-6\'f&gt;r&gt;t&lt;\'row\'&lt;\'col-xs-6\'i&gt;&lt;\'col-xs-6\'p&gt;&gt;')
            // Add Bootstrap compatibility
            .withBootstrap()
            .withBootstrapOptions({
                TableTools: {
                    classes: {
                        container: 'btn-group',
                        buttons: {
                            normal: 'btn btn-danger'
                        }
                    }
                },
                ColVis: {
                    classes: {
                        masterButton: 'btn btn-primary'
                    }
                },
                pagination: {
                    classes: {
                        ul: 'pagination pagination-sm'
                    }
                }
            })

            // Add ColVis compatibility
            .withColVis()

            // Add Table tools compatibility
            .withTableTools('vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf')
            .withTableToolsButtons([
                'copy',
                'print', {
                    'sExtends': 'collection',
                    'sButtonText': 'Save',
                    'aButtons': ['csv', 'xls', 'pdf']
                }
            ]);
        vm.dtColumns = [
            DTColumnBuilder.newColumn('name').withTitle('Department Name').withClass('text-danger'),
            DTColumnBuilder.newColumn('status').withTitle('Status'),
            DTColumnBuilder.newColumn('action').withTitle('Action')
        ];
    }*/

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    /*$scope.dtColumns = [
        DTColumnBuilder.newColumn('name').withHtml("<span lang=en>").withTitle('Department Name').withClass('text-danger'),
        DTColumnBuilder.newColumn('status').withTitle('Status'),
        DTColumnBuilder.newColumn('action').withTitle('Action')
    ];*/


    $scope.editDepartment = function(department) {
        $("#editDepartment").modal("show");
        /*$scope.editCharterArea = true;
         $scope.createCharterArea = false;*/
        $scope.editDepartmentData=angular.copy(department);
    }

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/Departments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.addDepartmentModal = function() {
        $("#addDepartment").modal("show");
        $scope.department = {};
        $scope.errorMessageData = '';
        $scope.errorMessage=false;
    }
    $scope.addDepartment=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var department=$scope.department;
        department['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/Departments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":department
        }).success(function (response) {
            console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $("#addDepartment").modal("hide");
            $("#addDeptSuccess").modal("show");
            setTimeout(function(){$('#addDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
            }
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);

        });
    }

    $scope.editDepartmentButton=function () {
        var editCharterDetails= $scope.editDepartmentData;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/Departments/'+$scope.editDepartmentData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editCharterDetails
        }).success(function (response, data) {
            //console.log("filter Schemes "+ JSON.stringify(response));
            $scope.loadingImage=false;
            $("#editDepartment").modal("hide");
            $("#editDeptSuccess").modal("show");
            setTimeout(function(){$('#editDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response, data) {
            if(response.error.details.messages.name) {
                $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                $scope.errorUpdateMessage=true;
            }
            $timeout(function(){
                $scope.errorUpdateMessage=false;
            }, 3000);
            console.log("failure");
        })
    }

    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('listOfSchemeController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("listOfSchemeController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro.active #reportsMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.listOfScheme").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.listOfScheme .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
        		   if(!response){
//        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.exportToExcel=function(tableId){
            if( $scope.schemeList!=null &&  $scope.schemeList.length>0){

                $("<tr>" +
                    "<th>Scheme Name</th>" +
                    "<th>Status</th>" +
                    "<th>Category</th>" +
                    "<th>Commencement</th>" +
                    "<th>Budget</th>" +
                    "<th>Department Name</th>" +
                    "<th>Ministry Name</th>" +
                    "<th>Scheme UniqueId</th>" +
                    "<th>Contact One</th>" +
                    "<th>Contact Two</th>" +
                    "<th>Contact Three</th>" +
                    "</tr>").appendTo("table"+tableId);

                for(var i=0;i<$scope.schemeList.length;i++){
                    var schmeData=$scope.schemeList[i];

                    $("<tr>" +
                        "<td>"+schmeData.name+"</td>" +
                        "<td>"+schmeData.status+"</td>" +
                        "<td>"+schmeData.category+"</td>" +
                        "<td>"+schmeData.commincement+"</td>" +
                        "<td>"+schmeData.budget+"</td>" +
                        "<td>"+schmeData.departmentName+"</td>" +
                        "<td>"+schmeData.ministryName+"</td>" +
                        "<td>"+schmeData.schemeUniqueId+"</td>" +
                        "<td><table><tr><th>Name</th><td>"+schmeData.contactOne.name+"</td></tr>"+
                        "<tr><th>Email</th><td>"+schmeData.contactOne.email+"</td></tr>"+
                        "<tr><th>Phone One</th><td>"+schmeData.contactOne.phoneOne+"</td></tr>"+
                        "<tr><th>Phone Two</th><td>"+schmeData.contactOne.phoneTwo+"</td></tr>"+
                        "<tr><th>Designation</th><td>"+schmeData.contactOne.designation+"</td></tr>"+
                        "<tr><th>Employee Id</th><td>"+schmeData.contactOne.employeeId+"</td></tr>"+
                        "<tr><th>Address</th><td>"+schmeData.contactOne.address+"</td></tr>"+
                        "</table></td>" +
                        "<td><table><tr><th>Name</th><td>"+schmeData.contactTwo.name+"</td></tr>"+
                        "<tr><th>Email</th><td>"+schmeData.contactTwo.email+"</td></tr>"+
                        "<tr><th>Phone One</th><td>"+schmeData.contactTwo.phoneOne+"</td></tr>"+
                        "<tr><th>Phone Two</th><td>"+schmeData.contactTwo.phoneTwo+"</td></tr>"+
                        "<tr><th>Designation</th><td>"+schmeData.contactTwo.designation+"</td></tr>"+
                        "<tr><th>Employee Id</th><td>"+schmeData.contactTwo.employeeId+"</td></tr>"+
                        "<tr><th>Address</th><td>"+schmeData.contactTwo.address+"</td></tr>"+
                        "</table></td>" +
                        "<td><table><tr><th>Name</th><td>"+schmeData.contactThree.name+"</td></tr>"+
                        "<tr><th>Email</th><td>"+schmeData.contactThree.email+"</td></tr>"+
                        "<tr><th>Phone One</th><td>"+schmeData.contactThree.phoneOne+"</td></tr>"+
                        "<tr><th>Phone Two</th><td>"+schmeData.contactThree.phoneTwo+"</td></tr>"+
                        "<tr><th>Designation</th><td>"+schmeData.contactThree.designation+"</td></tr>"+
                        "<tr><th>Employee Id</th><td>"+schmeData.contactThree.employeeId+"</td></tr>"+
                        "<tr><th>Address</th><td>"+schmeData.contactThree.address+"</td></tr>"+
                        "</table></td>" +
                        "</tr>").appendTo("table"+tableId);
                }
                var exportHref = Excel.tableToExcel(tableId, 'List of schemes Details');
                $timeout(function () {
                    location.href = exportHref;
                }, 100);
            } else {
                $("#emptyDataTable").modal("show");
                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
            }
        }

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;
    schemeManagement.getSchemeDepartment({}, function (response) {
        $scope.deptList = response;
        var allObject={
            'id':"all",
            'name':"All"
        }
        $scope.deptList.unshift(allObject);
        $scope.loadingImage=false;
    });
    schemeManagement.getMinistry({}, function (response) {
        $scope.ministryList = response;
        var allObject={
            'id':"all",
            'name':"All"
        }
        $scope.ministryList.unshift(allObject);
        $scope.loadingImage=false;
    });
var selectedDept;
var selectMinistry;
    $scope.selectDepartmentName= function (deptid) {
        selectedDept=deptid;
    }
    $scope.selectMinistryName= function (ministryId) {
        selectMinistry=ministryId;
    }
    $scope.search={};
    $scope.searchData=function(){
            if($scope.search.category != '' && $scope.search.category != undefined && $scope.search.category != null) {
            if(selectedDept != '' && selectedDept != undefined && selectedDept != null) {
            if(selectMinistry != '' && selectMinistry != undefined && selectMinistry != null) {
            if($scope.search.status != '' && $scope.search.status != undefined && $scope.search.status != null) {
                var filterObject={
                    'deptId':selectedDept,
                    'minstryId':selectMinistry,
                    'category':$scope.search.category,
                    'status':$scope.search.status
                }
            $http({
                method: 'POST',
                url: 'api/Schemes/getSchemeReport',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":filterObject
            }).success(function (response) {
            console.log("response" + JSON.stringify(response))

                $scope.schemeList=response;
                $scope.loadingImage=false;
                $scope.selectProjectDetails=true;
                $scope.showAction=true;
            }).error(function (response) {
            });
            } else {
//                alert("status")
                $scope.errorUpdateMessage="Please Select Status";
                $scope.editDoubleClick=true;
                $timeout(function(){
                 $scope.editDoubleClick=false;
                }, 3000);
            }
            } else {
//                alert("selectMinistry")
                $scope.errorUpdateMessage="Please Select Ministry";
                $scope.editDoubleClick=true;
                $timeout(function(){
                 $scope.editDoubleClick=false;
                }, 3000);
            }
            } else {
//                alert("selectedDept")
                $scope.errorUpdateMessage="Please Select Department";
                $scope.editDoubleClick=true;
                $timeout(function(){
                 $scope.editDoubleClick=false;
                }, 3000);
            }
            } else {
//                alert("cat")
                $scope.errorUpdateMessage="Please Select Category";
                $scope.editDoubleClick=true;
                $timeout(function(){
                 $scope.editDoubleClick=false;
                }, 3000);
            }
    }

}]);

app.controller('schemeWiseRequestsController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("schemeWiseRequestsController");

     $(document).ready(function () {
              $('html,body').scrollTop(0);
              $(".sidebar-menu li ul > li").removeClass('active1');
              $('[data-toggle="tooltip"]').tooltip();
              $(".sidebar-menu #schemesLi").addClass("active");
              $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro").addClass("active");
              $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro.active #reportsMgmtSetup").addClass("menu-open");
              $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.schemeWiseRequests").addClass("active1").siblings('.active1').removeClass('active1');
              $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.schemeWiseRequests .collapse").addClass("in");
              $(".sidebar-menu li .collapse").removeClass("in");
          });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
       if(!response){
//        			window.location.href = "#/noAccessPage";
       }
    });

    $scope.exportToExcel=function(tableId){
                if( $scope.schemeWiseRequest!=null &&  $scope.schemeWiseRequest.length>0){

                    $("<tr>" +
                        "<th>Scheme Name</th>" +
                        "<th>Status</th>" +
                        "<th>Category</th>" +
                        "<th>Commencement</th>" +
                        "<th>Budget</th>" +
                        "<th>Department Name</th>" +
                        "<th>Ministry Name</th>" +
                        "<th>Scheme UniqueId</th>" +
                        "<th>False Count</th>" +
                        "<th>Approved Count</th>" +
                        "<th>Reject Count</th>" +
                        "<th>Total Count</th>" +
                        "<th>Contact One</th>" +
                        "<th>Contact Two</th>" +
                        "<th>Contact Three</th>" +
                        "</tr>").appendTo("table"+tableId);

                    for(var i=0;i<$scope.schemeWiseRequest.length;i++){
                        var schmeData=$scope.schemeWiseRequest[i];

                        $("<tr>" +
                            "<td>"+schmeData.schmeDetails.name+"</td>" +
                            "<td>"+schmeData.schmeDetails.status+"</td>" +
                            "<td>"+schmeData.schmeDetails.category+"</td>" +
                            "<td>"+schmeData.schmeDetails.commincement+"</td>" +
                            "<td>"+schmeData.schmeDetails.budget+"</td>" +
                            "<td>"+schmeData.schmeDetails.departmentName+"</td>" +
                            "<td>"+schmeData.schmeDetails.ministryName+"</td>" +
                            "<td>"+schmeData.schmeDetails.schemeUniqueId+"</td>" +
                            "<td>"+schmeData.falseCount+"</td>" +
                            "<td>"+schmeData.approvedCount+"</td>" +
                            "<td>"+schmeData.rejectCount+"</td>" +
                            "<td>"+schmeData.totalCount+"</td>" +
                            "<td><table><tr><th>Name</th><td>"+schmeData.schmeDetails.contactOne.name+"</td></tr>"+
                            "<tr><th>Email</th><td>"+schmeData.schmeDetails.contactOne.email+"</td></tr>"+
                            "<tr><th>Phone One</th><td>"+schmeData.schmeDetails.contactOne.phoneOne+"</td></tr>"+
                            "<tr><th>Phone Two</th><td>"+schmeData.schmeDetails.contactOne.phoneTwo+"</td></tr>"+
                            "<tr><th>Designation</th><td>"+schmeData.schmeDetails.contactOne.designation+"</td></tr>"+
                            "<tr><th>Employee Id</th><td>"+schmeData.schmeDetails.contactOne.employeeId+"</td></tr>"+
                            "<tr><th>Address</th><td>"+schmeData.schmeDetails.contactOne.address+"</td></tr>"+
                            "</table></td>" +
                            "<td><table><tr><th>Name</th><td>"+schmeData.schmeDetails.contactTwo.name+"</td></tr>"+
                            "<tr><th>Email</th><td>"+schmeData.schmeDetails.contactTwo.email+"</td></tr>"+
                            "<tr><th>Phone One</th><td>"+schmeData.schmeDetails.contactTwo.phoneOne+"</td></tr>"+
                            "<tr><th>Phone Two</th><td>"+schmeData.schmeDetails.contactTwo.phoneTwo+"</td></tr>"+
                            "<tr><th>Designation</th><td>"+schmeData.schmeDetails.contactTwo.designation+"</td></tr>"+
                            "<tr><th>Employee Id</th><td>"+schmeData.schmeDetails.contactTwo.employeeId+"</td></tr>"+
                            "<tr><th>Address</th><td>"+schmeData.schmeDetails.contactTwo.address+"</td></tr>"+
                            "</table></td>" +
                            "<td><table><tr><th>Name</th><td>"+schmeData.schmeDetails.contactThree.name+"</td></tr>"+
                            "<tr><th>Email</th><td>"+schmeData.schmeDetails.contactThree.email+"</td></tr>"+
                            "<tr><th>Phone One</th><td>"+schmeData.schmeDetails.contactThree.phoneOne+"</td></tr>"+
                            "<tr><th>Phone Two</th><td>"+schmeData.schmeDetails.contactThree.phoneTwo+"</td></tr>"+
                            "<tr><th>Designation</th><td>"+schmeData.schmeDetails.contactThree.designation+"</td></tr>"+
                            "<tr><th>Employee Id</th><td>"+schmeData.schmeDetails.contactThree.employeeId+"</td></tr>"+
                            "<tr><th>Address</th><td>"+schmeData.schmeDetails.contactThree.address+"</td></tr>"+
                            "</table></td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                    var exportHref = Excel.tableToExcel(tableId, 'List of schemes Details');
                    $timeout(function () {
                        location.href = exportHref;
                    }, 100);
                } else {
                    $("#emptyDataTable").modal("show");
                    setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                }
            }

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');

    $scope.loadingImage=false;
    $scope.uploadURL=uploadFileURL;
        $scope.selectDur=function(selectValue){
            if(selectValue!=''){
                    var data={
                    'duration':selectValue
                }
//                alert('ahi');
                $window.localStorage.setItem('schemeData', JSON.stringify(data));
                $scope.searchData(data);
            }
        }
    $scope.getReportsCount= function () {
       if($scope.startDate != '' && $scope.startDate != undefined && $scope.startDate != null) {
        if($scope.endDate != '' && $scope.endDate != undefined && $scope.endDate != null) {
        var data={
                    'startDate':$scope.startDate,
                    'endDate':$scope.endDate
                }
                $window.localStorage.setItem('schemeData', JSON.stringify(data));
                $scope.searchData(data);

        } else {
            $scope.errorUpdateMessage="Please Select End Date";
             $scope.editDoubleClick=true;
             $timeout(function(){
                 $scope.editDoubleClick=false;
             }, 3000);
       }
       } else {
         $scope.errorUpdateMessage="Please Select Start Date";
          $scope.editDoubleClick=true;
          $timeout(function(){
              $scope.editDoubleClick=false;
          }, 3000);
       }

    }

    $scope.searchData=function(inputData){
           $http({
                method: 'POST',
                url: 'api/Schemes/getReport',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data": inputData
            }).success(function (response) {
                console.log('responce'+JSON.stringify(response));
                $scope.schemeWiseRequest = response;
                $scope.selectProjectDetails = true;
                $scope.showAction = true;
//                $scope.loadingImage = true;
            }).error(function (response) {
            });
    }

/*$scope.openschemes = function(id, status) {
    $rootScope.uniqId = id;
    $rootScope.statusIs = status;
    window.location.href = "#/openSchemes";
}*/

}]);

app.controller('openSchemesController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$routeParams', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $routeParams, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("openSchemesController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #schemesLi").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro.active #reportsMgmtSetup").addClass("menu-open");
            $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.schemeWiseRequests").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.schemeWiseRequests .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
        		   if(!response){
//        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');

        $scope.exportToExcel=function(tableId){

           if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

                                   $("<tr>" +
                                       "<th colspan='13' style='background-color: #ebebeb;'><h3>Payment Order Details</h3></th>" +
                                       "</tr>").appendTo("table" + tableId);

                                $("<tr>" +
                                    "<th>schemeName</th>" +
                                    "<th>requestStatus</th>" +
                                    "<th>requestId</th>" +
                                    "<th colspan='3' style='background-color: #ccc;'>Assigned Person Details"+
                                    "<table><tr><th>name</th><th>Email Id</th><th>Mobile Number</th></tr></table></th>" +
                                    "<th colspan='4' style='background-color: #ccc;'>Workflow Details"+
                                    "<table><tr><th>workflowId</th><th>levelId</th><th>status</th><th>employeeName</th></tr></table></th>" +
                                    "</tr>").appendTo("table"+tableId);

                                for(var i=0;i<$scope.requestLists.length;i++){
                                    var schemes=$scope.requestLists[i];

                                    $("<tr>" +
                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.schemeName+"</td>"+
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.requestStatus+"</td>" +
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.requestId+"</td>" +
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.name+"</td>" +
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.emailId+"</td>" +
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.mobileNumber+"</td>" +
                                    "<td>").appendTo("table"+tableId);
                                    if(schemes.workflow != null) {

                                                $scope.schemeIs= [];
                                                for(var j=0; j<schemes.workflow.length;j++) {
                                                        $scope.schemeIs.push(schemes.workflow[j]);
    //                                                                                            alert(schemes.rows.length);

                                                    $("<tr><td>"+$scope.schemeIs[j].workflowId+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].levelId+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].status+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].employeeName+"</td>" +
                                                    "</tr>").appendTo("table"+tableId);

                                                    }}
                                                    $("</td>"+
                                                    "</tr>").appendTo("table"+tableId);
                                   }
                                var exportHref = Excel.tableToExcel(tableId, 'Payment Order Details ');
                                $timeout(function () { location.href = exportHref; }, 100);
                            } else {
                                $("#emptyDataTable").modal("show");
                                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                            }

           var exportHref = Excel.tableToExcel(tableId, 'Payment Order Details');
           $timeout(function () {
               location.href = exportHref;
           }, 100);
       }

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;

        $scope.schemeId = $routeParams.schemeUniqueId;
        $scope.schemeStatus = $routeParams.status;
        $scope.schemeStatus = $routeParams.status;
    var details=JSON.parse($window.localStorage.getItem('schemeData'));
    var data={
        'schemeId':$routeParams.schemeUniqueId
    };
    if($routeParams.status=="false"){
        data.schemeStatus=false
    }else{
        data.schemeStatus=$routeParams.status;
    }
    if(details!=undefined && details!=null && details!='' && details.duration!=undefined&& details.duration!=null){
        data.duration=details.duration
    }if(details!=undefined && details!=null && details!='' && details.startDate!=undefined&& details.startDate!=null&& details.endDate!=undefined&& details.endDate!=null){
        data.startDate=details.startDate;
        data.endDate=details.endDate;
    }
    $scope.getRequest=function () {
        $http({
            "method": "POST",
            "url": 'api/Requests/getRequestData',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data":data
        }).success(function (response, data) {
//            alert('request data'+JSON.stringify(response));
            console.log("request details"+ JSON.stringify(response.data));
            $scope.requestLists = response.data;
            $scope.loadingImage=false;
            $scope.selectProjectDetails=true;
        }).error(function (response, data) {
            console.log("failure");
        })

    }
    $scope.getRequest();

}]);

app.controller('openSchemesRequestController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$routeParams', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $routeParams, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("openSchemesRequestController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #schemesLi").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro.active #reportsMgmtSetup").addClass("menu-open");
            $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.schemeWiseRequests").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.schemeWiseRequests .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
        		   if(!response){
//        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');

        $scope.exportToExcel=function(tableId){

           if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

                                   $("<tr>" +
                                       "<th colspan='13' style='background-color: #ebebeb;'><h3>Payment Order Details</h3></th>" +
                                       "</tr>").appendTo("table" + tableId);

                                $("<tr>" +
                                    "<th>schemeName</th>" +
                                    "<th>requestStatus</th>" +
                                    "<th>requestId</th>" +
                                    "<th colspan='3' style='background-color: #ccc;'>Assigned Person Details"+
                                    "<table><tr><th>name</th><th>Email Id</th><th>Mobile Number</th></tr></table></th>" +
                                    "<th colspan='4' style='background-color: #ccc;'>Workflow Details"+
                                    "<table><tr><th>workflowId</th><th>levelId</th><th>status</th><th>employeeName</th></tr></table></th>" +
                                    "</tr>").appendTo("table"+tableId);

                                for(var i=0;i<$scope.requestLists.length;i++){
                                    var schemes=$scope.requestLists[i];

                                    $("<tr>" +
                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.schemeName+"</td>"+
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.requestStatus+"</td>" +
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.requestId+"</td>" +
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.name+"</td>" +
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.emailId+"</td>" +
                                                    "<td rowspan='"+(schemes.workflow.length+1)+"'>"+schemes.mobileNumber+"</td>" +
                                    "<td>").appendTo("table"+tableId);
                                    if(schemes.workflow != null) {

                                                $scope.schemeIs= [];
                                                for(var j=0; j<schemes.workflow.length;j++) {
                                                        $scope.schemeIs.push(schemes.workflow[j]);
    //                                                                                            alert(schemes.rows.length);

                                                    $("<tr><td>"+$scope.schemeIs[j].workflowId+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].levelId+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].status+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].employeeName+"</td>" +
                                                    "</tr>").appendTo("table"+tableId);

                                                    }}
                                                    $("</td>"+
                                                    "</tr>").appendTo("table"+tableId);
                                   }
                                var exportHref = Excel.tableToExcel(tableId, 'Payment Order Details ');
                                $timeout(function () { location.href = exportHref; }, 100);
                            } else {
                                $("#emptyDataTable").modal("show");
                                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                            }

           var exportHref = Excel.tableToExcel(tableId, 'Payment Order Details');
           $timeout(function () {
               location.href = exportHref;
           }, 100);
       }

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;

        $scope.schemeId = $routeParams.schemeUniqueId;
        $scope.schemeStatus = $routeParams.status;
        $scope.schemeStatus = $routeParams.status;
    var details=JSON.parse($window.localStorage.getItem('schemeData'));
    var data={
        'schemeId':$routeParams.schemeUniqueId,
        'schemeStatus':$routeParams.status
    };
    if(details!=undefined && details!=null && details!='' && details.duration!=undefined&& details.duration!=null){
        data.duration=details.duration
    }if(details!=undefined && details!=null && details!='' && details.startDate!=undefined&& details.startDate!=null&& details.endDate!=undefined&& details.endDate!=null){
        data.startDate=details.startDate;
        data.endDate=details.endDate;
    }
    $scope.getRequest=function () {
        $http({
            "method": "POST",
            "url": 'api/Requests/getRequestData',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data":data
        }).success(function (response, data) {
//            alert('request data'+JSON.stringify(response));
            console.log("request details"+ JSON.stringify(response.data));
            $scope.requestLists = response.data;
            $scope.loadingImage=false;
            $scope.selectProjectDetails=true;
        }).error(function (response, data) {
            console.log("failure");
        })

    }
    $scope.getRequest();

}]);

app.controller('requestAgingController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("requestAgingController");

     $(document).ready(function () {
                   $('html,body').scrollTop(0);
                   $(".sidebar-menu li ul > li").removeClass('active1');
                   $('[data-toggle="tooltip"]').tooltip();
                   $(".sidebar-menu #schemesLi").addClass("active");
                   $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro").addClass("active");
                   $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro.active #reportsMgmtSetup").addClass("menu-open");
                   $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.requestAging").addClass("active1").siblings('.active1').removeClass('active1');
                   $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.requestAging .collapse").addClass("in");
                   $(".sidebar-menu li .collapse").removeClass("in");
               });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
        		   if(!response){
//        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.exportToExcel=function(tableId){
                if( $scope.schemeRanging!=null &&  $scope.schemeRanging.length>0){

                    $("<tr>" +
                        "<th>Scheme Name</th>" +
                        "<th>Status</th>" +
                        "<th>Category</th>" +
                        "<th>Commencement</th>" +
                        "<th>Budget</th>" +
                        "<th>Department Name</th>" +
                        "<th>Ministry Name</th>" +
                        "<th>Scheme UniqueId</th>" +
                        "<th>Ten Days Count</th>" +
                        "<th>One Month Count</th>" +
                        "<th>Three Month Count</th>" +
                        "<th>Contact One</th>" +
                        "<th>Contact Two</th>" +
                        "<th>Contact Three</th>" +
                        "</tr>").appendTo("table"+tableId);

                    for(var i=0;i<$scope.schemeRanging.length;i++){
                        var schmeData=$scope.schemeRanging[i];

                        $("<tr>" +
                            "<td>"+schmeData.schmeDetails.name+"</td>" +
                            "<td>"+schmeData.schmeDetails.status+"</td>" +
                            "<td>"+schmeData.schmeDetails.category+"</td>" +
                            "<td>"+schmeData.schmeDetails.commincement+"</td>" +
                            "<td>"+schmeData.schmeDetails.budget+"</td>" +
                            "<td>"+schmeData.schmeDetails.departmentName+"</td>" +
                            "<td>"+schmeData.schmeDetails.ministryName+"</td>" +
                            "<td>"+schmeData.schmeDetails.schemeUniqueId+"</td>" +
                            "<td>"+schmeData.tenDaysCount+"</td>" +
                            "<td>"+schmeData.oneMonthCount+"</td>" +
                            "<td>"+schmeData.threeMonthCount+"</td>" +
                            "<td><table><tr><th>Name</th><td>"+schmeData.schmeDetails.contactOne.name+"</td></tr>"+
                            "<tr><th>Email</th><td>"+schmeData.schmeDetails.contactOne.email+"</td></tr>"+
                            "<tr><th>Phone One</th><td>"+schmeData.schmeDetails.contactOne.phoneOne+"</td></tr>"+
                            "<tr><th>Phone Two</th><td>"+schmeData.schmeDetails.contactOne.phoneTwo+"</td></tr>"+
                            "<tr><th>Designation</th><td>"+schmeData.schmeDetails.contactOne.designation+"</td></tr>"+
                            "<tr><th>Employee Id</th><td>"+schmeData.schmeDetails.contactOne.employeeId+"</td></tr>"+
                            "<tr><th>Address</th><td>"+schmeData.schmeDetails.contactOne.address+"</td></tr>"+
                            "</table></td>" +
                            "<td><table><tr><th>Name</th><td>"+schmeData.schmeDetails.contactTwo.name+"</td></tr>"+
                            "<tr><th>Email</th><td>"+schmeData.schmeDetails.contactTwo.email+"</td></tr>"+
                            "<tr><th>Phone One</th><td>"+schmeData.schmeDetails.contactTwo.phoneOne+"</td></tr>"+
                            "<tr><th>Phone Two</th><td>"+schmeData.schmeDetails.contactTwo.phoneTwo+"</td></tr>"+
                            "<tr><th>Designation</th><td>"+schmeData.schmeDetails.contactTwo.designation+"</td></tr>"+
                            "<tr><th>Employee Id</th><td>"+schmeData.schmeDetails.contactTwo.employeeId+"</td></tr>"+
                            "<tr><th>Address</th><td>"+schmeData.schmeDetails.contactTwo.address+"</td></tr>"+
                            "</table></td>" +
                            "<td><table><tr><th>Name</th><td>"+schmeData.schmeDetails.contactThree.name+"</td></tr>"+
                            "<tr><th>Email</th><td>"+schmeData.schmeDetails.contactThree.email+"</td></tr>"+
                            "<tr><th>Phone One</th><td>"+schmeData.schmeDetails.contactThree.phoneOne+"</td></tr>"+
                            "<tr><th>Phone Two</th><td>"+schmeData.schmeDetails.contactThree.phoneTwo+"</td></tr>"+
                            "<tr><th>Designation</th><td>"+schmeData.schmeDetails.contactThree.designation+"</td></tr>"+
                            "<tr><th>Employee Id</th><td>"+schmeData.schmeDetails.contactThree.employeeId+"</td></tr>"+
                            "<tr><th>Address</th><td>"+schmeData.schmeDetails.contactThree.address+"</td></tr>"+
                            "</table></td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                    var exportHref = Excel.tableToExcel(tableId, 'List of schemes Details');
                    $timeout(function () {
                        location.href = exportHref;
                    }, 100);
                } else {
                    $("#emptyDataTable").modal("show");
                    setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                }
            }

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;

    $scope.schemesList = function () {
            $http({
                    "method": "GET",
                    "url": 'api/Schemes/getPendingReq',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response, data) {
                    console.log('responce'+JSON.stringify(response));
                    $scope.schemeRanging = response;
                    $scope.loadingImage=false;
                    $scope.selectProjectDetails=true;
                    $scope.showAction=true;
                }).error(function (response, data) {
                    console.log("failure");
                })
        }
        $scope.schemesList();

}]);

app.controller('requestTrackerController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("requestTrackerController");

     $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #schemesLi").addClass("active");
        $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro").addClass("active");
        $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro.active #reportsMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.requestTracker").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.requestTracker .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
        		   if(!response){
//        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.exportToExcel=function(tableId){
//        alert()
                    if( $scope.schemeListData!=null &&  $scope.schemeListData.length>0){

                        $("<tr>" +
                            "<th>Scheme Name</th>" +
                            "<th>Status</th>" +
                            "<th>Category</th>" +
                            "<th>Commencement</th>" +
                            "<th>Budget</th>" +
                            "<th>Department Name</th>" +
                            "<th>Ministry Name</th>" +
                            "<th>Scheme UniqueId</th>" +
                            "<th>Contact One</th>" +
                            "<th>Contact Two</th>" +
                            "<th>Contact Three</th>" +
                            "</tr>").appendTo("table"+tableId);

                        for(var i=0;i<$scope.schemeListData.length;i++){
                            var schmeData=$scope.schemeListData[i];

                            $("<tr>" +
                                "<td>"+schmeData.name+"</td>" +
                                "<td>"+schmeData.status+"</td>" +
                                "<td>"+schmeData.category+"</td>" +
                                "<td>"+schmeData.commincement+"</td>" +
                                "<td>"+schmeData.budget+"</td>" +
                                "<td>"+schmeData.departmentName+"</td>" +
                                "<td>"+schmeData.ministryName+"</td>" +
                                "<td>"+schmeData.schemeUniqueId+"</td>" +
                                "<td><table><tr><th>Name</th><td>"+schmeData.contactOne.name+"</td></tr>"+
                                "<tr><th>Email</th><td>"+schmeData.contactOne.email+"</td></tr>"+
                                "<tr><th>Phone One</th><td>"+schmeData.contactOne.phoneOne+"</td></tr>"+
                                "<tr><th>Phone Two</th><td>"+schmeData.contactOne.phoneTwo+"</td></tr>"+
                                "<tr><th>Designation</th><td>"+schmeData.contactOne.designation+"</td></tr>"+
                                "<tr><th>Employee Id</th><td>"+schmeData.contactOne.employeeId+"</td></tr>"+
                                "<tr><th>Address</th><td>"+schmeData.contactOne.address+"</td></tr>"+
                                "</table></td>" +
                                "<td><table><tr><th>Name</th><td>"+schmeData.contactTwo.name+"</td></tr>"+
                                "<tr><th>Email</th><td>"+schmeData.contactTwo.email+"</td></tr>"+
                                "<tr><th>Phone One</th><td>"+schmeData.contactTwo.phoneOne+"</td></tr>"+
                                "<tr><th>Phone Two</th><td>"+schmeData.contactTwo.phoneTwo+"</td></tr>"+
                                "<tr><th>Designation</th><td>"+schmeData.contactTwo.designation+"</td></tr>"+
                                "<tr><th>Employee Id</th><td>"+schmeData.contactTwo.employeeId+"</td></tr>"+
                                "<tr><th>Address</th><td>"+schmeData.contactTwo.address+"</td></tr>"+
                                "</table></td>" +
                                "<td><table><tr><th>Name</th><td>"+schmeData.contactThree.name+"</td></tr>"+
                                "<tr><th>Email</th><td>"+schmeData.contactThree.email+"</td></tr>"+
                                "<tr><th>Phone One</th><td>"+schmeData.contactThree.phoneOne+"</td></tr>"+
                                "<tr><th>Phone Two</th><td>"+schmeData.contactThree.phoneTwo+"</td></tr>"+
                                "<tr><th>Designation</th><td>"+schmeData.contactThree.designation+"</td></tr>"+
                                "<tr><th>Employee Id</th><td>"+schmeData.contactThree.employeeId+"</td></tr>"+
                                "<tr><th>Address</th><td>"+schmeData.contactThree.address+"</td></tr>"+
                                "</table></td>" +
                                "</tr>").appendTo("table"+tableId);
                        }
                        var exportHref = Excel.tableToExcel(tableId, 'List of schemes Details');
                        $timeout(function () {
                            location.href = exportHref;
                        }, 100);
                    } else {
                        $("#emptyDataTable").modal("show");
                        setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                    }
                }

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;


    $scope.schemesList = function () {
            $http({
                    "method": "GET",
                    "url": 'api/Schemes',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response, data) {
                    $scope.schemeList = response;
                     var allObject={
                                'name':"All",
                                'status':"All"
                            }
                            $scope.schemeList.unshift(allObject);
//                    console.log("$scope.schemeLists" + JSON.stringify($scope.schemeLists))
                    $scope.loadingImage=false;
                }).error(function (response, data) {
                    console.log("failure");
                })
        }
        $scope.schemesList();

//        var selectedDept;
                $scope.selectName= function (name) {
                    $scope.selectedDept=name;
                }
                $scope.search={};
                $scope.searchData=function(){
                    $scope.schemeName = $scope.selectedDept;
                        if($scope.schemeName!= undefined && $scope.schemeName!= '' && $scope.schemeName!= null) {
                        if($scope.search.status!= undefined && $scope.search.status!= '' && $scope.search.status!= null) {


                        if($scope.schemeName!= undefined && $scope.schemeName!='' && $scope.search.status!=undefined && $scope.search.status!=''){
                                                        if($scope.search.status!='all'){
                                                            $http({
                                                                method: 'GET',
                                                                url: 'api/Requests/?filter={"where":{"and":[{"schemeName":"' + $scope.schemeName + '"},{"requestStatus":"' + $scope.search.status  + '"}]}}',
                                                                headers: {"Content-Type": "application/json", "Accept": "application/json"}
                                                            }).success(function (response) {
                                                                $scope.schemeListDetails=response;
                                                                $scope.loadingImage=false;
                                                                $scope.selectProjectDetails=true;

                                                                console.log("response"+JSON.stringify($scope.schemeList))
                                                            }).error(function (response) {
                                                                console.log("error"+JSON.stringify(response))
                                                            });
                                                        }else{
                                                            $http({
                                                                method: 'GET',
                                                                url: 'api/Requests/?filter={"where":{"schemeName":"' + $scope.schemeName + '"}}',
                                                                headers: {"Content-Type": "application/json", "Accept": "application/json"}
                                                            }).success(function (response) {
                                                                $scope.schemeListDetails=response;
                                                                $scope.loadingImage=false;
                                                                $scope.selectProjectDetails=true;

                                                                console.log("response"+JSON.stringify($scope.schemeList))
                                                            }).error(function (response) {
                                                                console.log("error"+JSON.stringify(response))
                                                            });
                                                        }

                                            }

                        } else {
                            $scope.errorMessage = true;
                            $scope.showError = "Please Select Status";
                            $timeout(function () {
                                $scope.errorMessage = false;
                            }, 3000);
                        }
                        } else {
                           $scope.errorMessage = true;
                           $scope.showError = "Please Select Scheme Name";
                           $timeout(function () {
                               $scope.errorMessage = false;
                           }, 3000);
                        }

                }



}]);

app.controller('workflowTrackerController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("workflowTrackerController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #schemesLi").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #reportsMgmtPro.active #reportsMgmtSetup").addClass("menu-open");
            $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.workflowTracker").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #scheme #reportsMgmtPro.active #reportsMgmtSetup li.workflowTracker .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
        		   if(!response){
//        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');
    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;
    $scope.workflowName = function () {
        $http({
                "method": "GET",
                "url": 'api/Workflows',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response, data) {
                $scope.workflowName = response;
            $scope.loadingImage=false;
            }).error(function (response, data) {
            })
    }
    $scope.workflowName();
     $scope.workflowLevels = function (worklfowId) {
         $scope.workflowId=worklfowId;
         $scope.workflowLevelNo=[];
         $scope.workflowEmployee=[];
            $http({
                    "method": "GET",
                    "url": 'api/WorkflowLevels?filter=%7B%22where%22%3A%7B%22workflowId%22%3A%20%22'+worklfowId+'%22%7D%7D',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response, data) {
                    $scope.workflowLevelNo = response;
                }).error(function (response, data) {
                })
        }
    $scope.selectLevel=function(workflowLevel){
            var details={
                'workflowId':$scope.workflowId,
                'workflowLevel':workflowLevel
            }
        $scope.workflowLevel=workflowLevel;
        $scope.workflowEmployee=[];
        $http({
            "method": "POST",
            "url": 'api/WorkflowEmployees/getEmployee',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data":details
        }).success(function (response, data) {
            $scope.workflowEmployee = response;
        }).error(function (response, data) {
        })
    }
     $scope.selectEmployee=function(workflowemployee){
          $scope.employee=workflowemployee
    }
    $scope.getData= function () {
        if($scope.workflowId != '' && $scope.workflowId != undefined && $scope.workflowId != null) {
        if($scope.workflowLevel != '' && $scope.workflowLevel != undefined && $scope.workflowLevel != null) {
        if($scope.employee != '' && $scope.employee != undefined && $scope.employee != null) {
        if($scope.startDate != '' && $scope.startDate != undefined && $scope.startDate != null) {
        if($scope.endDate != '' && $scope.endDate != undefined && $scope.endDate != null) {
            var inputData={
                        'workflowId':$scope.workflowId,
                        'workflowLevel':$scope.workflowLevel,
                        'employeeId':$scope.employee,
                        'startDate':$scope.startDate,
                        'endDate':$scope.endDate
                    }
                    $http({
                        "method": "POST",
                        "url": 'api/Schemes/getEmpReport',
                        "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                        "data":inputData
                    }).success(function (response, data) {
            //            console.log("response"+JSON.stringify(response));
                        $scope.workflowLevelData = response;
                        $scope.selectProjectDetails = true;
                        $scope.loadingImage = false;
                        $scope.showAction = true;
                    }).error(function (response, data) {
                    })
        } else {
//            alert("endDate");
                    $scope.errorUpdateMessage="Please Select End Date";
                    $scope.editDoubleClick=true;
                    $timeout(function(){
                        $scope.editDoubleClick=false;
                    }, 3000);
        }
        } else {
//            alert("startDate");
                    $scope.errorUpdateMessage="Please Select Start Date";
                    $scope.editDoubleClick=true;
                    $timeout(function(){
                        $scope.editDoubleClick=false;
                    }, 3000);
        }
        } else {
//            alert("employee");
                    $scope.errorUpdateMessage="Please Select Employee";
                    $scope.editDoubleClick=true;
                    $timeout(function(){
                        $scope.editDoubleClick=false;
                    }, 3000);
        }
        } else {
//             alert("workflowLevel");
                    $scope.errorUpdateMessage="Please Select Workflow Level";
                    $scope.editDoubleClick=true;
                    $timeout(function(){
                        $scope.editDoubleClick=false;
                    }, 3000);
        }
        } else {
//            alert("id");
                    $scope.errorUpdateMessage="Please Select Workflow Name";
                    $scope.editDoubleClick=true;
                    $timeout(function(){
                        $scope.editDoubleClick=false;
                    }, 3000);
        }
    }

    $scope.exportToExcel=function(tableId){
//        alert()
                    if( $scope.workflowLevelData!=null &&  $scope.workflowLevelData.length>0){

                        $("<tr>" +
                            "<th>Scheme Name</th>" +
                            "<th>Scheme UniqueId</th>" +
                            "<th>Request Id</th>" +
                            "<th>Approved Employee</th>" +
                            "<th>Name</th>" +
                            "<th>Email Id</th>" +
                            "<th>Mobile Number</th>" +
                            "<th>Final Status</th>" +
                            "<th>Approved Employee</th>" +
                            "<th>Commnet</th>" +
                            "</tr>").appendTo("table"+tableId);

                        for(var i=0;i<$scope.workflowLevelData.length;i++){
                            var schmeData=$scope.workflowLevelData[i];

                            $("<tr>" +
                                "<td>"+schmeData.name+"</td>" +
                                "<td>"+schmeData.schemeUniqueId+"</td>" +
                                "<td>"+schmeData.requestId+"</td>" +
                                "<td>"+schmeData.approvedEmployee+"</td>" +
                                "<td>"+schmeData.name+"</td>" +
                                "<td>"+schmeData.emailId+"</td>" +
                                "<td>"+schmeData.mobileNumber+"</td>" +
                                "<td>"+schmeData.finalStatus+"</td>" +
                                "<td>"+schmeData.approvedEmployee+"</td>" +
                                "<td>"+schmeData.commnet+"</td>" +
                                "</tr>").appendTo("table"+tableId);
                        }
                        var exportHref = Excel.tableToExcel(tableId, 'List of schemes Details');
                        $timeout(function () {
                            location.href = exportHref;
                        }, 100);
                    } else {
                        $("#emptyDataTable").modal("show");
                        setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                    }
                }

}]);

app.controller('schemeModuleController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope','$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope,$routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("schemeModuleController");
    //var eemail= $routeParams.employeeId;
    //console.log('encrypted...mail...'+eemail);
    //decrypt the employeeId(email) and store in $scope.employeeId
    /*$http({
        "method": "GET",
        "url": 'api/Employees/decryptMail?mail='+eemail,
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        console.log('decrypted....'+JSON.stringify(response));
        $scope.employeeId=response.decrypted;
        //alert('success....'+JSON.stringify(response));
    }).error(function (response) {
        console.log("failure---"+JSON.stringify(response));
        //alert('failure....'+JSON.stringify(response));
    });*/

   var data={
        'employeeId':$routeParams.employeeId
    }
    $http({
        "method": "GET",
        "url": 'api/Schemes/getEmployeeData?searchData=%7B%22employeeId%22%20%3A%20%22'+$routeParams.employeeId+'%22%7D',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $window.localStorage.setItem('accessToken', response.data.id);
        $window.localStorage.setItem('tokenId', response.data.id);
        $rootScope.userLogin = true;
        $scope.user = {};
        if(response.data.userInfo) {
            $rootScope.userName = response.data.userInfo.name;
            $window.localStorage.setItem('userName', response.data.userInfo.name);
            //$window.localStorage.setItem('employeeId', response.userInfo.employeeId);
          $window.localStorage.setItem('userDetails', JSON.stringify(response.data.userInfo));
            $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
            $window.localStorage.setItem('afterLogin', 'true');
            $scope.roleList= response.data.roleObject;
            $window.localStorage.setItem('roleList', JSON.stringify( response.data.roleObject));
            //window.location.href ="selectModule.html";
            //var employeeId=response.data.userInfo.employeeId;
            location.href="http://"+$location.host()+":3003/admin/"

        }

    }).error(function (response, data) {
        console.log("failure");
    })

}]);

app.controller('selectModuleController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope','$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder','NgMap', function(schemeManagement, $http, $scope, $window, $location, $rootScope,$routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder,NgMap) {

    $scope.loadingImage=true;
    $scope.employeeData= $window.localStorage.getItem('employeeId');

    $scope.employeeId= $window.localStorage.getItem('employeeId');
    //encrypt the employeeData and put in $scope.employeeData
    /*$http.get('api/Employees/encryptMail?mail='+$scope.employeeData).success(function (response, data) {
        $scope.employeeId=response.encrypted;
        $scope.loadingImage=false;
        console.log('response--encrypted'+JSON.stringify(response));
    }).error(function (response, data) {
        $scope.loadingImage=false;
        console.log("failure"+JSON.stringify(response));
    });*/



    console.log("schemeModuleController"+$scope.employeeId);
        var data={
            'employeeId':$scope.employeeId
        }

    $http({
        "method": "POST",
        "url": 'api/Employees/getEmpReport',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"},
        "data":data
    }).success(function (response, data) {
        //alert('response');
        //alert('response'+JSON.stringify(response));

    }).error(function (response, data) {
        console.log("failure");
        //alert('failure');
    })




    $(function() {
        $('marquee').mouseover(function() {
            $(this).attr('scrollamount',5);
        }).mouseout(function() {
             $(this).attr('scrollamount',0);
        });
    });

    // Google Map Code

    NgMap.getMap().then(function(map) {
      console.log('map', map);
      $scope.map = map;
    });

    $scope.clicked = function() {
      //alert('Clicked a link inside infoWindow');
    };

    /* $scope.shops = [
       {id:'CCMC', "name":['Existing Properties - 150000, ','New Properties - 50000'], position:[11.010117,76.990068],imageUrl:'images/police station.png'},
       {id:'TNEB', "name":['Total Cosumers - 200000,','Commercial Users - 80000,','Residential Users - 120000'], position:[11.024264, 76.944877],imageUrl:'images/school icon.png'},
       {id:'RTO Office', "name": ['Total Vehicles - 500000,',' New Registrations - 50000,',' Public Transport - 25000'], position:[11.025165,76.999848],imageUrl:'images/office.png'},
       {id:'Labour Office', "name":['Total Employees - 150000,',' Employees on leave - 200'], position:[11.010340,76.974175],imageUrl:'images/office.png'},
       {id:'SP Office', "name":['Vivekananda Rd,',' Ram Nagar, Coimbatore,',' Tamil Nadu 641009'], position:[11.000222,76.967110],imageUrl:'images/office.png'},
     ];*/

      $scope.shops = [
            {id:'General/Establishment Section', "name":['Integrated Personnel Management System','Employee Self Service Module','Inventory / Materials Management Module'], position:[11.011957, 76.981695],imageUrl:'images/police station.png'},
            {id:'Public Health Section', "name":['Birth and Death Module','Solid Waste Management Module','Vehicle Management Module'], position:[11.007260, 77.071948],imageUrl:'images/school icon.png'},
            {id:'Town Planning Section', "name": ['Land and Estate Management Module'], position:[11.055933, 77.102339],imageUrl:'images/office.png'},
            {id:'Engineering Section', "name":['Water Supply Module','Underground Drainage Module','Procurement Module','WardWorks Module'], position:[10.983310, 77.091387],imageUrl:'images/office.png'},
            {id:'Accounts Section', "name":['Accounts Module ','Audit Module'], position:[11.023664, 77.039075],imageUrl:'images/office.png'},
            {id:'Revenue Section', "name":['Property Tax Module','Profession Tax Module'], position:[11.062250, 76.994227],imageUrl:'images/office.png'},
            {id:'Transportation Section', "name":['Licenses Module'], position:[11.005554, 77.008713],imageUrl:'images/office.png'}
          ];



$scope.showClient=function(key){
if(key=="Integrated Personnel Management System"){
 //location.href = "http://localhost:3023/admin/#/ipmLogin";
 $window.open('http://54.149.172.244:3023/admin/#/ipmLogin/'+$scope.employeeData, '_blank');
 }else if(key=="Employee Self Service Module"){
 $window.open('http://54.149.172.244:3025/admin/#/eselfServiceLogin/'+$scope.employeeData, '_blank');
// location.href = "http://54.149.172.244:3025/admin/#/eselfServiceLogin";
 }else if(key=="Inventory / Materials Management Module'"){
 }else if(key=="Birth and Death Module"){
// location.href = "http://54.149.172.244:3013/admin/#/birthlogin/";
$window.open('http://54.149.172.244:3013/admin/#/birthlogin/'+$scope.employeeData, '_blank');
 }else if(key=="Licenses Module"){
// location.href="http://54.149.172.244:3021/admin/#/licenseLogin";
 $window.open('http://54.149.172.244:3021/admin/#/licenseLogin/'+$scope.employeeData, '_blank');
 }else if(key=="Vehicle Management Module"){
// location.href="http://54.149.172.244:3011/admin/#/vehicleLogin";
 $window.open('http://54.149.172.244:3011/admin/#/vehicleLogin/'+$scope.employeeData, '_blank');
 }else if(key=="Land and Estate Management Module"){
// location.href="http://54.149.172.244:3019/admin/#/assetModulelogin";
 $window.open('http://54.149.172.244:3019/admin/#/assetModulelogin/'+$scope.employeeData, '_blank');
 }else if(key=="WardWorks Module"){
// location.href="http://54.149.172.244:3005/admin/#/projectModulelogin";
 $window.open('http://54.149.172.244:3005/admin/#/projectModulelogin/'+$scope.employeeData, '_blank');
 }else if(key=="Property Tax Module"){
// location.href="http://54.149.172.244:3017/admin/#/propertyModulelogin";
 $window.open('http://54.149.172.244:3017/admin/#/propertyModulelogin/'+$scope.employeeData, '_blank');
 }else
 //alert("Module Not Found")
 $("#addEmployeeSuccess").modal("show");
 setTimeout(function(){$('#addEmployeeSuccess').modal('hide')}, 3000);
}


  //  $scope.shop = $scope.shops[0];

    $scope.showDetail = function(e, shop) {
      $scope.shop = shop;
      $scope.map.showInfoWindow('foo-iw', shop.id);
    };

    $scope.hideDetail = function() {
      $scope.map.hideInfoWindow('foo-iw');
    };

          // End GoogleMap code



    

}]);

