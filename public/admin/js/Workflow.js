
 angular.module('ui.tinymce', ['ui.calendar'])
     .value('uiTinymceConfig', {})
     .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
         uiTinymceConfig = uiTinymceConfig || {};
         var generatedIds = 0;
         return {
             require: 'ngModel',
             link: function(scope, elm, attrs, ngModel) {
                 var expression, options, tinyInstance;
                 // generate an ID if not present
                 if (!attrs.id) {
                     attrs.$set('id', 'uiTinymce' + generatedIds++);
                 }
                 options = {
                     // Update model when calling setContent (such as from the source editor popup)
                     setup: function(ed) {
                         ed.on('init', function(args) {
                             ngModel.$render();
                         });
                         // Update model on button click
                         ed.on('ExecCommand', function(e) {
                             ed.save();
                             ngModel.$setViewValue(elm.val());
                             if (!scope.$$phase) {
                                 scope.$apply();
                             }
                         });
                         // Update model on keypress
                         ed.on('KeyUp', function(e) {
                             console.log(ed.isDirty());
                             ed.save();
                             ngModel.$setViewValue(elm.val());
                             if (!scope.$$phase) {
                                 scope.$apply();
                             }
                         });
                     },
                     mode: 'exact',
                     elements: attrs.id,
                 };
                 if (attrs.uiTinymce) {
                     expression = scope.$eval(attrs.uiTinymce);
                 } else {
                     expression = {};
                 }
                 angular.extend(options, uiTinymceConfig, expression);
                 setTimeout(function() {
                     tinymce.init(options);
                 });
                 ngModel.$render = function() {
                     if (!tinyInstance) {
                         tinyInstance = tinymce.get(attrs.id);
                     }
                     if (tinyInstance) {
                         tinyInstance.setContent(ngModel.$viewValue || '');
                     }
                 };
             }
         };
     }]);

app.directive('validNumber', function() {
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            if(!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function(val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if(!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean =negativeCheck[0] + '-' + negativeCheck[1];
                    if(negativeCheck[0].length > 0) {
                        clean =negativeCheck[0];
                    }
                }
                if(!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0,2);
                    clean =decimalCheck[0] + '.' + decimalCheck[1];
                }
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function(event) {
                if(event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

/*** reset ui-select value **/

/*** reset ui-select value **/

var app = angular.module('workflow', ['ngRoute','ui.calendar','ngCalendar', 'ngFileUpload', 'datatables', 'servicesDetails' ,'angularjs-dropdown-multiselect', 'ui.tinymce', 'datatables.bootstrap']);

 app.config(function($routeProvider) {
    $routeProvider
    .when('/employeeList', {
        templateUrl: './employee.html',
        controller: 'employeeController'
    }).when('/designation', {
        templateUrl: './Designation.html',
        controller: 'designationController'
    }).when('/emailTemplete', {
        templateUrl: './EmailTemplete.html',
        controller: 'emailTempleteController'
    }).when('/appConfig', {
        templateUrl: './appConfig.html',
        controller: 'appConfigController'
    }).when('/employeeType', {
        templateUrl: './EmployeeType.html',
        controller: 'employeeTypeController'
    }).when('/employeeDepartment', {
        templateUrl: './employeeDepartment.html',
        controller: 'employeeDepartmentController'
    }).when('/workFlowLevel', {
        templateUrl: './createWorkFlowLevel.html',
        controller: 'workFlowLevelController'
    }).when('/workFlowEmployee', {
        templateUrl: './createWorkFlowEmployees.html',
        controller: 'workFlowWithEmployeeController'
    }).when('/workFlowForms', {
        templateUrl: './createWorkFlowForms.html',
        controller: 'workFlowWithFormsController'
    }).when('/role', {
        templateUrl: './rolemodel.html',
        controller: 'rolemodelController'
    }).when('/createRole', {
        templateUrl: './createRole.html',
        controller: 'createRoleController'
    }).when('/roleEmployees', {
        templateUrl: './rolemodelEmployees.html',
        controller: 'rolemodelEmployeesController'

    }).when('/createRoleEmployees', {
        templateUrl: './createRoleEmployees.html',
        controller: 'createRoleEmployeesController'
    }).when('/workFlowList', {
        templateUrl: './createWorkFlow.html',
        controller: 'workFlowListController'
    }).when('/languageSelection', {
            templateUrl: './languageSelection.html',
            controller: 'languageSelectionController'
        }).when('/contractorsList', {
            templateUrl: './contractorsList.html',
            controller: 'contractorsListController'
     });
});

app.controller('languageSelectionController', ['userAdminstration', '$http', '$scope', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration,$http, $scope, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {
    console.log("languageSelectionController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #userAdminLi").addClass("active");
         $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .languageSelection").addClass("active1");
         $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .languageSelection").addClass("menu-open");
         $(".sidebar-menu #scheme #userAdminiProSetUp .languageSelection").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu li .collapse").removeClass("in");
     });


               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                          userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'languageData'}, function (response) {
               //           alert(JSON.stringify(response));
                                if(!response){
                                 window.location.href = "#/noAccessPage";
                                }
                             });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.rowCount=[];
    $scope.timeList= [];
    var timeDetails={
        "english":'',
        "hindi":''
    };
    $scope.loadingImage=true;
    //$scope.timeList.push(timeDetails);
    $scope.rowsLength=1;
    $scope.addIme = function(){
        $scope.addButton=true;
      var lengthOfRows=$scope.timeList.length+1;
        var englishName='english'+lengthOfRows;
        var hindiName='hindi'+lengthOfRows;
        var totalTime='total'+lengthOfRows;
        var projectRows = {
            englishName:'',
            hindiName:'',
        };
        var sdfsd={
            "english":'',
            "hindi":''
        };
        $scope.timeList.push(sdfsd);
    };
    $scope.removePerson = function(index){
        $scope.timeList.splice(index, 1);
    };
    $scope.addButton=false;
            $scope.getLanguage=function(){
                    $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
                    $http({
                        method: 'GET',
                        url: 'api/LanguageModels/getLanguageData',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                    }).success(function (response) {
                        $scope.loadingImage=false;
                        $scope.languageData = response;
                        var data=$scope.languageData.token;
                        var keys = [];
                        for(var k in data) keys.push(k);
                        if(keys!=null && keys.length>0){
                            $scope.languageList=[];
                            for(var i=0;i<keys.length;i++){
                                var object={
                                    'english':keys[i],
                                    "hindi":data[keys[i]]
                                }
                                $scope.languageList.push(object);
                            }
                        }

                    }).error(function (response) {
                        console.log('Error Response :' + JSON.stringify(response));
                    });
                }
    $scope.editLanguageModal=function(editlanguageData){
        $scope.editData=angular.copy(editlanguageData);
        $("#editLanguageModel").modal("show");
    }

    $scope.editLanguage=function(){
                console.log(JSON.stringify($scope.editData));
              var arrayData=[$scope.editData];
                var finalData={
                    "word":arrayData
                }
                $http({
                    method: 'POST',
                    url: 'api/LanguageModels/updateData',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":finalData
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $("#editLanguageModel").modal("hide");
                    $scope.getLanguage();

                }).error(function (response) {
                });

            }

            $scope.getLanguage();


            $scope.languageDataButton=function(){
                var data=$scope.timeList;

                console.log(JSON.stringify(data));
                var finalData={
                    "word":data
                }
                $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');

                $http({
                    method: 'POST',
                    url: 'api/LanguageModels/postLanguageWords',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":finalData
                }).success(function (response) {
                    console.log('Users Response emp created :' + JSON.stringify(response));
                    $scope.loadingImage=false;
                        if(response!=null &&  response.length>0){
                        var responceData=response;
                            $scope.getLanguage();
                            $scope.timeList=[];
                                $scope.addButton=false;
                    }else{
                            $scope.getLanguage();
                            $scope.timeList=[];
                            $scope.addButton=false;
                        console.log('successfully inserted');
                    }
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                    if(response.error.details.messages.email) {
                        $scope.createEmpError=response.error.details.messages.email[0];
                        $scope.createEmpError = 'Email already Exist';
                        $scope.empCreationError = true;
                    }
                    if(response.error.details.messages.employeeId) {
                        $scope.createEmpError=response.error.details.messages.employeeId[0];
                        $scope.createEmpError = 'EmployeeId already Exist';
                        $scope.empCreationError = true;
                    }
                });

            }

    $scope.exportToExcel=function(tableId) {
        if( $scope.languageList!=null &&  $scope.languageList.length>0){

            $("<tr>" +
                "<th>Beneficiaries Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.languageList.length;i++){
                var language=$scope.languageList[i];
                $("<tr>" +
                    "<td>"+language.english+"</td>" +
                    "<td>"+language.hindi+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Language Selection Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }


}]);

app.controller('employeeController', ['userAdminstration', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', 'Upload', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration, $http, $scope, $window, $timeout, $location,Upload, $rootScope, Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("employeeController");
//    console.log("employeeController");

     $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #userAdminLi").addClass("active");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .employeeList ").addClass("active1");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .employeeList ").addClass("menu-open");
             $(".sidebar-menu #scheme #userAdminiProSetUp .employeeList  ").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu li .collapse").removeClass("in");
         });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'employee'}, function (response) {
//        		alert(JSON.stringify(response));
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.employeeList!=null &&  $scope.employeeList.length>0){

            $("<tr>" +
                "<th>Employee Id</th>" +
                "<th>Name</th>" +
                "<th>Designation</th>" +
                "<th>Work Number</th>" +
                "<th>Email</th>" +
                "<th>Department</th>" +
                "<th>Employee Type</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.employeeList.length;i++){
                var employee=$scope.employeeList[i];

                $("<tr>" +
                    "<td>"+employee.employeeId+"</td>" +
                    "<td>"+employee.name+"</td>" +
                    "<td>"+employee.designation+"</td>" +
                    "<td>"+employee.workMobile+"</td>" +
                    "<td>"+employee.email+"</td>" +
                    "<td>"+employee.department+"</td>" +
                    "<td>"+employee.employeeType+"</td>" +
                    "<td>"+employee.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Employee Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    var importDoubleClick=false;
    $scope.importData=function(){
       if(!importDoubleClick){
           importDoubleClick=true;
           $http({
               "method": "POST",
               "url": 'api/EmployeeTypes/importEmployeeData',
               "headers": {"Content-Type": "application/json", "Accept": "application/json"},
               "data": $scope.uploadFileURL
           }).success(function (response, data) {
               //alert('succesfully updated');
               $('#exportData').modal('hide');
               importDoubleClick=false;
               $scope.importedEmployeeData=response;
               $scope.importedEmployee=true;
           }).error(function (response, data) {
               console.log("failure");
               importDoubleClick=false;

           })
       }

    }
    var filedetails=[];
    var fileIdsArray=[];
    $scope.uploadFileURL = [];
    var fileUploadStatus=true;
    $scope.uploadFiles = function (files) {
        var count=1;
        filedetails=[];
        fileIdsArray=[];
        var fileCount = 0;
        //$scope.uploadFileURL = [];

        angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg1 = true;
            fileCount++;/*
             if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {*/
            fileUploadStatus = false;
            file.upload = Upload.upload({
                url:  'api/Uploads/dhanbadDb/upload',
                data: {file: file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    var fileDetails = {
                        'id': response.data._id,
                        'name': response.data.filename
                    }
                    fileIdsArray.push(fileDetails);
                    $scope.uploadFileURL.push(fileDetails);
                    filedetails.push(response.data);
                    fileUploadStatus = true;
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
            });
            /* }else{
             alert('Please Upload JPEG or PDF files only');
             }*/

            if (fileCount == files.length) {
                $scope.uploadNitif = true;
                $scope.disable = false;
                $scope.errorMssg1 = false;
                $scope.formNotSelected = '';
            }
        });
    };

    $scope.loadingImage=true;
    $scope.getEmployeeType=function () {
        $http({
            method: 'GET',
            url: 'api/EmployeeTypes?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.employeeTypesList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getEmployeeType();
    $scope.clickToView=function () {
        $scope.loadingImage=false;
        $scope.showEmployee = true;
        $scope.createEmployee = false;
        $scope.editEmployee=false;
    }
    $scope.getDesignation=function () {
        $http({
            method: 'GET',
            url: 'api/Designations?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.designationList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDesignation();
    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url:'api/EmployeeDepartments?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.departmentList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDepartments();
    $scope.showEmployee = true;
    $scope.createEmployee = false;
    $scope.editEmployee=false;
    $scope.showCreateEmployee = function() {
        $scope.addEmployee = {};
        $scope.loadingImage=false;
        $scope.createEmployee = true;
        $scope.showEmployee = false;
    }
    $scope.getEmployees=function () {
        $http({
            method: 'GET',
            url: 'api/Employees',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.employeeList = response
            $scope.createEmployee = false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    var alphaNumeric =/^[a-zA-Z0-9]+$/;
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;

    $scope.addEmployee={};
    var createEmployeeOnce = true;
    $scope.createEmployeeButton=function () {
        if(createEmployeeOnce) {
            createEmployeeOnce = false;
            $scope.loadingImage=false;
            $scope.empCreationError = false;
            $scope.createEmpError = '';
            //console.log(" addEmployee entered" +JSON.stringify($scope.addEmployee));
            if ($scope.addEmployee.employeeId.length < 20) {
//                if ($scope.addEmployee.name.length < 30) {
                    if ($scope.addEmployee.name.length <= 30) {
                        if ($scope.addEmployee.mobile.match(phoneNumber) && $scope.addEmployee.mobile.length == 10) {
                            if(($scope.addEmployee.password) ==($scope.addEmployee.confirmPassword)){

                                var employee=$scope.addEmployee;
                                ////alert('hai'+JSON.stringify(employee));
                                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                                employee['createdPerson']=loginPersonDetails.name;
                                employee['role']='employee';
                                //console.log("before post method");
                                $http({
                                    method: 'POST',
                                    url: 'api/Employees',
                                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                    "data":employee
                                }).success(function (response) {
                                    console.log('Users Response emp created :' + JSON.stringify(response));
                                    //   $("#addDepartment").modal("hide");
                                    $("#addEmployeeSuccess").modal("show");
                                    setTimeout(function(){$('#addEmployeeSuccess').modal('hide')}, 3000);
                                    $scope.getEmployees();
                                    createEmployeeOnce = true;
                                    $scope.createEmployee = false;
                                    $scope.showEmployee = true;
                                }).error(function (response) {
                                    console.log('Error Response :' + JSON.stringify(response));
                                    createEmployeeOnce = true;
                                    if(response.error.details.messages.email) {
                                        $scope.createEmpError=response.error.details.messages.email[0];
                                        $scope.createEmpError = 'Email already Exist';
                                        $scope.empCreationError = true;
                                    }
                                    if(response.error.details.messages.employeeId) {
                                        $scope.createEmpError=response.error.details.messages.employeeId[0];
                                        $scope.createEmpError = 'EmployeeId already Exist';
                                        $scope.empCreationError = true;
                                    }
                                    $timeout(function(){
                                        $scope.empCreationError=false;
                                    }, 3000);
                                });
                            }else {
                                $scope.createEmpError = 'Password and Confirm Passwords are not matched';
                                $scope.empCreationError = true;
                                createEmployeeOnce = true;

                                $timeout(function(){
                                    $scope.empCreationError=false;
                                }, 3000);
                            }
                        }else {
                            createEmployeeOnce = true;
                            $scope.createEmpError = 'Please enter the Mobile Number in the Correct format';
                            $scope.empCreationError = true;

                            $timeout(function(){
                                $scope.empCreationError=false;
                            }, 3000);
                        }
                    }else {
                        createEmployeeOnce = true;
                        $scope.createEmpError = 'Please enter the Name in the Correct format';
                        $scope.empCreationError = true;

                        $timeout(function(){
                            $scope.empCreationError=false;
                        }, 3000);
                    }
                /*}else {
                    createEmployeeOnce = true;
                    $scope.createEmpError = 'Please enter the Display Name in the Correct format';
                    $scope.empCreationError = true;

                    $timeout(function(){
                        $scope.empCreationError=false;
                    }, 3000);
                }*/
            }else {
                createEmployeeOnce = true;
                $scope.createEmpError = 'Please enter the employeeId in the Correct format';
                $scope.empCreationError = true;
                $timeout(function(){
                    $scope.empCreationError=false;
                }, 3000);
            }

        }
    }


    $scope.editEmployeeDetails={};
    $scope.editEmployeeData=function (editData) {

        $scope.loadingImage=false;
        $scope.editEmployeeDetails=editData;
        $scope.editEmployee=true;
        $scope.showEmployee = false;





        console.log(" editEmployeeDetails entered  " +JSON.stringify($scope.editEmployeeDetails));
    }

    $scope.editEmployeeButton=function () {
        $scope.loadingImage=false;

        $scope.empUpdationError = false;
        $scope.updateEmpError = '';
        // //alert('hai');
        if ($scope.editEmployeeDetails.employeeId.length < 20) {
//            if ($scope.editEmployeeDetails.name.length < 20) {
                if ($scope.editEmployeeDetails.name.length < 30) {
                    if ($scope.editEmployeeDetails.mobile.match(phoneNumber) && $scope.editEmployeeDetails.mobile.length == 10) {
                        var editEmployeeDetails= $scope.editEmployeeDetails;
                        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                        editEmployeeDetails['lastEditPerson']=loginPersonDetails.name;
                        $http({
                            "method": "PUT",
                            "url": 'api/Employees/'+$scope.editEmployeeDetails.id,
                            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                            "data": editEmployeeDetails
                        }).success(function (response, data) {
                            $scope.createEmployee = false;
                            $scope.editEmployee=false;
                            $scope.showEmployee = true;
                            $scope.getEmployees();
                            console.log("update employee details: "+JSON.stringify($scope.editEmployeeDetails) );
                            $("#editEmployeeSuccess").modal("show");
                            setTimeout(function(){$('#editEmployeeSuccess').modal('hide')}, 3000);
                        }).error(function (response, data) {
                            console.log("failure");

                            if(response.error.details.messages.email) {
                                $scope.updateEmpError=response.error.details.messages.email[0];
                                $scope.updateEmpError = 'Email already Exist';
                                $scope.empUpdationError = true;
                            }
                            if(response.error.details.messages.employeeId) {
                                $scope.updateEmpError=response.error.details.messages.employeeId[0];
                                $scope.updateEmpError = 'EmployeeId already Exist';
                                $scope.empUpdationError = true;
                            }
                            $timeout(function(){
                                $scope.empUpdationError=false;
                            }, 3000);
                        })

                    }else {
                        $scope.updateEmpError = 'Please enter the Phone Number in the Correct format';
                        $scope.empUpdationError = true;
                        $timeout(function(){
                            $scope.empUpdationError=false;
                        }, 3000);

                    }
                }else {
                    $scope.updateEmpError = 'Please enter the Name in the Correct format';
                    $scope.empUpdationError = true;
                    $timeout(function(){
                        $scope.empUpdationError=false;
                    }, 3000);

                }
            }/*else {
                $scope.updateEmpError = 'Please enter the Name in the Correct format';
                $scope.empUpdationError = true;
                $timeout(function(){
                    $scope.empUpdationError=false;
                }, 3000);

            }
        }*/else {
            $scope.updateEmpError = 'Please enter the employeeId in the Correct format';
            $scope.empUpdationError = true;
            $timeout(function(){
                $scope.empUpdationError=false;
            }, 3000);

        }
    }
    $scope.getEmployees();
    $scope.editEmployeeSuccessModal = function() {
        $("#editEmployeeSuccess").modal("hide");
        //$window.location.reload();
        location.href = '#/employeeList';
    }
    $scope.addEmployeeSuccessModal = function() {
        $("#addEmployeeSuccess").modal("hide");
        //$window.location.reload();
        location.href = '#/employeeList';
    }
/*
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #userAdmini li.employeeList").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #userAdminLi.treeview").addClass("active");
        $(".sidebar-menu #userAdmini li.employeeList .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.employeeList!=null &&  $scope.employeeList.length>0){

            $("<tr>" +
                "<th>Employee Id</th>" +
                "<th>Name</th>" +
                "<th>Designation</th>" +
                "<th>Work Number</th>" +
                "<th>Email</th>" +
                "<th>Department</th>" +
                "<th>Employee Type</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.employeeList.length;i++){
                var employee=$scope.employeeList[i];

                $("<tr>" +
                    "<td>"+employee.employeeId+"</td>" +
                    "<td>"+employee.name+"</td>" +
                    "<td>"+employee.designation+"</td>" +
                    "<td>"+employee.workMobile+"</td>" +
                    "<td>"+employee.email+"</td>" +
                    "<td>"+employee.department+"</td>" +
                    "<td>"+employee.employeeType+"</td>" +
                    "<td>"+employee.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Employee Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

        $scope.loadingImage=true;
        $scope.getEmployeeType=function () {
            $http({
                method: 'GET',
                url: 'api/EmployeeTypes?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                $scope.employeeTypesList = response;
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
        $scope.getEmployeeType();
        $scope.clickToView=function () {
            $scope.loadingImage=false;
            $scope.showEmployee = true;
            $scope.createEmployee = false;
            $scope.editEmployee=false;
        }
        $scope.getDesignation=function () {
            $http({
                method: 'GET',
                url: 'api/Designations?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                //console.log('Users Response :' + JSON.stringify(response));
                $scope.designationList = response;
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
        $scope.getDesignation();
        $scope.getDepartments=function () {
            $http({
                method: 'GET',
                url: 'api/EmployeeDepartments?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                //console.log('Users Response :' + JSON.stringify(response));
                $scope.departmentList = response;
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
        $scope.getDepartments();
        $scope.showEmployee = true;
        $scope.createEmployee = false;
        $scope.editEmployee=false;
        $scope.showCreateEmployee = function() {
            $scope.addEmployee = {};
            $scope.loadingImage=false;
            $scope.createEmployee = true;
            $scope.showEmployee = false;
        }
        $scope.getEmployees=function () {
            $http({
                method: 'GET',
                url: 'api/Employees',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                //console.log('Users Response :' + JSON.stringify(response));
                $scope.employeeList = response
                $scope.createEmployee = false;
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
        var alphaNumeric =/^[a-zA-Z0-9]+$/;
        var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
        $scope.addEmployee={};
        var createEmployeeOnce = true;
        $scope.createEmployeeButton=function () {
            if(createEmployeeOnce) {
                createEmployeeOnce = false;
            $scope.loadingImage=false;
            $scope.empCreationError = false;
            $scope.createEmpError = '';
            //console.log(" addEmployee entered" +JSON.stringify($scope.addEmployee));
            if ($scope.addEmployee.employeeId.length < 20) {
                if ($scope.addEmployee.name.length < 30) {
                    if ($scope.addEmployee.firstName.length < 30) {
                        if ($scope.addEmployee.mobile.match(phoneNumber) && $scope.addEmployee.mobile.length == 10) {
                            if(($scope.addEmployee.password) ==($scope.addEmployee.confirmPassword)){

                                var employee=$scope.addEmployee;
                                ////alert('hai'+JSON.stringify(employee));
                                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                                employee['createdPerson']=loginPersonDetails.name;
                                employee['role']='employee';
                                //console.log("before post method");
                                $http({
                                    method: 'POST',
                                    url: 'api/Employees',
                                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                    "data":employee
                                }).success(function (response) {
                                    console.log('Users Response emp created :' + JSON.stringify(response));
                                    //   $("#addDepartment").modal("hide");
                                    $("#addEmployeeSuccess").modal("show");
                                    setTimeout(function(){$('#addEmployeeSuccess').modal('hide')}, 3000);
                                    $scope.getEmployees();
                                    createEmployeeOnce = true;
                                    $scope.createEmployee = false;
                                    $scope.showEmployee = true;
                                }).error(function (response) {
                                    console.log('Error Response :' + JSON.stringify(response));
                                    createEmployeeOnce = true;
                                    if(response.error.details.messages.email) {
                                        $scope.createEmpError=response.error.details.messages.email[0];
                                        $scope.createEmpError = 'Email already Exist';
                                        $scope.empCreationError = true;
                                    }
                                    if(response.error.details.messages.employeeId) {
                                        $scope.createEmpError=response.error.details.messages.employeeId[0];
                                        $scope.createEmpError = 'Employee Id already Exist';
                                        $scope.empCreationError = true;
                                    }
                                    $timeout(function(){
                                        $scope.empCreationError=false;
                                    }, 3000);
                                });
                            }else {
                                $scope.createEmpError = 'Password and Confirm Passwords are not matched';
                                $scope.empCreationError = true;
                                createEmployeeOnce = true;

                                $timeout(function(){
                                    $scope.empCreationError=false;
                                }, 3000);
                            }
                        }else {
                            createEmployeeOnce = true;
                            $scope.createEmpError = 'Please enter the Phone Number in the Correct format';
                            $scope.empCreationError = true;

                            $timeout(function(){
                                $scope.empCreationError=false;
                            }, 3000);
                        }
                    }else {
                        createEmployeeOnce = true;
                        $scope.createEmpError = 'Please enter the First Name in the Correct format';
                        $scope.empCreationError = true;

                        $timeout(function(){
                            $scope.empCreationError=false;
                        }, 3000);
                    }
                }else {
                    createEmployeeOnce = true;
                    $scope.createEmpError = 'Please enter the Name in the Correct format';
                    $scope.empCreationError = true;

                    $timeout(function(){
                        $scope.empCreationError=false;
                    }, 3000);
                }
            }else {
                createEmployeeOnce = true;
                $scope.createEmpError = 'Please enter the Employee Id in the Correct format';
                $scope.empCreationError = true;
                $timeout(function(){
                    $scope.empCreationError=false;
                }, 3000);
            }

            }
        }
        $scope.editEmployeeDetails={};
        $scope.editEmployeeData=function (editData) {
            $scope.loadingImage=false;
            $scope.editEmployeeDetails=editData;
            $scope.editEmployee=true;
            $scope.showEmployee = false;
        }
        $scope.editEmployeeButton=function () {
            $scope.loadingImage=false;
            $scope.empUpdationError = false;
            $scope.updateEmpError = '';
            if ($scope.editEmployeeDetails.employeeId.length < 20) {
                if ($scope.editEmployeeDetails.name.length < 20) {
                    if ($scope.editEmployeeDetails.firstName.length < 20) {
                        if ($scope.editEmployeeDetails.mobile.match(phoneNumber) && $scope.editEmployeeDetails.mobile.length == 10) {
                            var editEmployeeDetails= $scope.editEmployeeDetails;
                            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                            editEmployeeDetails['lastEditPerson']=loginPersonDetails.name;
                            $http({
                                "method": "PUT",
                                "url": 'api/Employees/'+$scope.editEmployeeDetails.id,
                                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                                "data": editEmployeeDetails
                            }).success(function (response, data) {
                                $scope.createEmployee = false;
                                $scope.editEmployee=false;
                                $scope.showEmployee = true;
                                $scope.getEmployees();
                                console.log("update employee details: "+JSON.stringify($scope.editEmployeeDetails) );
                                $("#editEmployeeSuccess").modal("show");
                                setTimeout(function(){$('#editEmployeeSuccess').modal('hide')}, 3000);
                            }).error(function (response, data) {
                                console.log("failure");

                                if(response.error.details.messages.email) {
                                    $scope.updateEmpError=response.error.details.messages.email[0];
                                    $scope.updateEmpError = 'Email already Exist';
                                    $scope.empUpdationError = true;
                                }
                                if(response.error.details.messages.employeeId) {
                                    $scope.updateEmpError=response.error.details.messages.employeeId[0];
                                    $scope.updateEmpError = 'EmployeeId already Exist';
                                    $scope.empUpdationError = true;
                                }
                                $timeout(function(){
                                    $scope.empUpdationError=false;
                                }, 3000);
                            })

                        }else {
                            $scope.updateEmpError = 'Please enter the Phone Number in the Correct format';
                            $scope.empUpdationError = true;
                            $timeout(function(){
                                $scope.empUpdationError=false;
                            }, 3000);

                        }
                    }else {
                        $scope.updateEmpError = 'Please enter the First Name in the Correct format';
                        $scope.empUpdationError = true;
                        $timeout(function(){
                            $scope.empUpdationError=false;
                        }, 3000);

                    }
                }else {
                    $scope.updateEmpError = 'Please enter the Name in the Correct format';
                    $scope.empUpdationError = true;
                    $timeout(function(){
                        $scope.empUpdationError=false;
                    }, 3000);

                }
            }else {
                $scope.updateEmpError = 'Please enter the employeeId in the Correct format';
                $scope.empUpdationError = true;
                $timeout(function(){
                    $scope.empUpdationError=false;
                }, 3000);

            }
        }
        $scope.getEmployees();
        $scope.editEmployeeSuccessModal = function() {
            $("#editEmployeeSuccess").modal("hide");
            //$window.location.reload();
            location.href = '#/employeeList';
        }
        $scope.addEmployeeSuccessModal = function() {
            $("#addEmployeeSuccess").modal("hide");
            //$window.location.reload();
            location.href = '#/employeeList';
        }

    $scope.importExcel= function () {
        alert('hai');
    }*/
    }]);

app.controller('designationController', ['userAdminstration', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("designationController");

     $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #userAdminLi").addClass("active");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .designation ").addClass("active1");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .designation ").addClass("menu-open");
             $(".sidebar-menu #scheme #userAdminiProSetUp .designation  ").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu li .collapse").removeClass("in");
         });


               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                          userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'employeeDesignation'}, function (response) {
               //           alert(JSON.stringify(response));
                                if(!response){
                                 window.location.href = "#/noAccessPage";
                                }
                             });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.designationList!=null &&  $scope.designationList.length>0){

            $("<tr>" +
                "<th>Designation Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.designationList.length;i++){
                var designation=$scope.designationList[i];

                $("<tr>" +
                    "<td>"+designation.name+"</td>" +
                    "<td>"+designation.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Designation Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    $scope.loadingImage=true;
    $scope.getDesignation=function () {
        $http({
            method: 'GET',
            url: 'api/Designations',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.designationList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDesignation();
    $scope.addDesignationModal = function() {
        $scope.designation = {};
        $("#addDesignation").modal("show");
    }
    $scope.addDesignation=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var designation=$scope.designation;
        designation['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/Designations',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":designation
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            /*$scope.designation = {};*/
            $("#addDesignation").modal("hide");
            $scope.getDesignation();
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 3000);
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
            }

        });
    }

    $scope.editDesignationModal=function (editData) {
        $scope.loadingImage=false;
        $scope.editDesignation=angular.copy(editData);
        $("#editDesignation").modal('show');
    }
    $scope.editDesignationButton=function () {
        ////alert('hai');
        var editDepartmentData= $scope.editDesignation;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editDepartmentData.lastEditPerson=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/Designations/'+$scope.editDesignation.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editDepartmentData
        }).success(function (response, data) {
            $scope.loadingImage=false;
            //console.log("filter Schemes "+ JSON.stringify(response));
            $("#editDesignation").modal("hide");
            $('#editDepartmentSuccess').modal('show');
            setTimeout(function(){$('#editDepartmentSuccess').modal('hide')}, 3000);
            $scope.getDesignation();
        }).error(function (response, data) {
            console.log("failure");
        })
    }

}]);

app.controller('emailTempleteController', function($http, $scope, $window, $location, $rootScope) {
    console.log("emailTempleteController");

    $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #userAdminLi").addClass("active");
         $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .emailTemplete").addClass("active1");
         $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .emailTemplete").addClass("menu-open");
         $(".sidebar-menu #scheme #userAdminiProSetUp .emailTemplete ").addClass("active1").siblings('.active1').removeClass('active1');
//         $(".sidebar-menu #scheme #userAdminiProSetUp.active #schemeMgmtSetup li.requests .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


    $scope.loadingImage=true;

    $scope.editEmailTemp = function() {

        var editEmailData= $scope.email;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editEmailData.lastEditPerson=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/EmailTempletes/'+$scope.email.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editEmailData
        }).success(function (response, data) {
            $scope.loadingImage=false;
           // console.log("filter Schemes "+ JSON.stringify(response));
            $scope.emailAlert = response;
            //$window.location.reload();
            //console.log("filter Schemes "+ JSON.stringify($scope.emailAlert));
            $("#registerSuccess").modal("show");
            setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
            /*$scope.getEmail();*/
        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.getEmail=function () {

        $http({
            method: 'GET',
            url: 'api/EmailTempletes?filter=%7B%22where%22%3A%7B%22emailType%22%3A%20%22scheme%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.email = response[0];
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getEmail();

});

app.controller('appConfigController', ['userAdminstration', '$http', '$scope', '$window', '$location', 'Upload','$timeout','$rootScope', function(userAdminstration, $http, $scope, $window, $location, Upload,$timeout ,$rootScope) {
    console.log("appConfigController");

     $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #userAdminLi").addClass("active");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .appConfig").addClass("active1");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .appConfig").addClass("menu-open");
             $(".sidebar-menu #scheme #userAdminiProSetUp .appConfig").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu li .collapse").removeClass("in");
         });
    $scope.docUploadURL = uploadFileURL;

               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                          userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'appConfig'}, function (response) {
               //           alert(JSON.stringify(response));
                                if(!response){
                                 window.location.href = "#/noAccessPage";
                                }
                             });

    $scope.loadingImage=true;

    $scope.appConfig = {
        "autoEscalation":"false",
        "nocAmount":"false",
        "currentFinancialYear":"false"
    }

    $scope.editEmailTemp = function() {

        var editEmailData= $scope.appConfig;
        editEmailData.escalationDays=parseInt($scope.appConfig.escalationDays);
        editEmailData.nocAmountAction=parseInt($scope.appConfig.nocAmountAction);
        editEmailData.billReferenceId=$scope.docList;
        editEmailData.measurementReferenceId=$scope.docListData;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editEmailData.lastEditPerson=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/AppConfigs/'+$scope.appConfig.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editEmailData
        }).success(function (response, data) {
            $scope.loadingImage=false;
            $scope.emailAlert = response;
            $("#registerSuccess").modal("show");
            setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
        }).error(function (response, data) {

        })
    }

    $scope.getEmail=function () {

        $http({
            method: 'GET',
            url: 'api/AppConfigs',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            console.log('Users Response.... :' + JSON.stringify(response));
            $scope.appConfig = response[0];
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getEmail();
    var formIdsArray=[];
    var fileCount = 0;
    var formUploadStatus=true;
    $scope.docList = [];
    $scope.uploadFiles = function (files) {
        formIdsArray=[];
        angular.forEach(files, function(file) {
            $scope.uploadFormNotif = false;
            $scope.disable = true;
            $scope.errorMssg = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename
                        }
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
                $scope.fileUpload = true;
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
            if (fileCount == files.length) {
                $scope.uploadFormNotif = true;
                $scope.disable = false;
                $scope.errorMssg = false;
                $scope.formNotSelected = '';
            }
        });
    };
    $scope.docListData = [];
    $scope.uploadFilesDoc = function (files) {
        formIdsArray=[];
        angular.forEach(files, function(file) {
            $scope.uploadFormNotif = false;
            $scope.disable = true;
            $scope.errorMssg = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename
                        }
                        formIdsArray.push(response.data);
                        $scope.docListData.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
                $scope.fileUpload = true;
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
            if (fileCount == files.length) {
                $scope.uploadFormNotif = true;
                $scope.disable = false;
                $scope.errorMssg = false;
                $scope.formNotSelected = '';
            }
        });
    };
}]);

app.controller('employeeTypeController', ['userAdminstration', '$http', '$scope', '$timeout', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(userAdminstration, $http, $scope, $timeout, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("employeeTypeController");

     $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #userAdminLi").addClass("active");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .employeeType ").addClass("active1");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .employeeType ").addClass("menu-open");
             $(".sidebar-menu #scheme #userAdminiProSetUp .employeeType  ").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu li .collapse").removeClass("in");
         });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'employeeType'}, function (response) {
//        		alert(JSON.stringify(response));
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.employeeTypesList!=null &&  $scope.employeeTypesList.length>0){

            $("<tr>" +
                "<th>Employee Type</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.employeeTypesList.length;i++){
                var employeeType=$scope.employeeTypesList[i];

                $("<tr>" +
                    "<td>"+employeeType.name+"</td>" +
                    "<td>"+employeeType.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Employee Type Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    $scope.loadingImage=true;
    $scope.getEmployeeType=function () {
        $http({
            method: 'GET',
            url: 'api/EmployeeTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.employeeTypesList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getEmployeeType();
    $scope.addEmployeeModal = function () {
        $("#addEmployeeType").modal("show");
        $scope.employeeType = {};
    }
    $scope.addEmployeeType=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var employeeType=$scope.employeeType;
        employeeType['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/EmployeeTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":employeeType
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $("#addEmployeeType").modal("hide");
            $("#addEMPTypeSuccess").modal("show");
            setTimeout(function(){$('#addEMPTypeSuccess').modal('hide')}, 3000);
            $scope.getEmployeeType();
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
                $timeout(function(){
                    $scope.errorMessage=false;
                }, 3000);
                //console.log(' $scope.errorMessageData:' + JSON.stringify($scope.errorMessageData));
            }
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.editEmployeeTypeModal=function (editData) {
        $scope.loadingImage=false;
        $scope.editEmployeeTypeData=angular.copy(editData);
        $("#editEmployeeType").modal('show');
    }
    $scope.editEmployeeTypeButton=function () {
        var editEmployeeTypeData= $scope.editEmployeeTypeData;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editEmployeeTypeData.lastEditPerson=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/EmployeeTypes/'+$scope.editEmployeeTypeData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editEmployeeTypeData
        }).success(function (response, data) {
            $scope.loadingImage=false;
            //console.log("filter Schemes "+ JSON.stringify(response));
            $("#editEmployeeType").modal("hide");
            $("#editEMPTypeSuccess").modal("show");
            setTimeout(function(){$('#editEMPTypeSuccess').modal('hide')}, 3000);
            $scope.getEmployeeType();
        }).error(function (response, data) {
            console.log("failure");
        })
    }

}]);
/*, DTOptionsBuilder, DTColumnBuilder*/

app.controller('employeeDepartmentController', ['userAdminstration', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("employeeDepartmentController");

     $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #userAdminLi").addClass("active");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .employeeDepartment ").addClass("active1");
             $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .employeeDepartment ").addClass("menu-open");
             $(".sidebar-menu #scheme #userAdminiProSetUp .employeeDepartment  ").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu li .collapse").removeClass("in");
         });


    		var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'workflow'}, function (response) {
//    		alert(JSON.stringify(response));
    		   if(!response){
    			window.location.href = "#/noAccessPage";
    		   }
    		});

    /*function WithBootstrapOptionsCtrl(DTOptionsBuilder, DTColumnBuilder) {
        var vm = this;
        vm.dtOptions = DTOptionsBuilder
            // .fromSource('data.json')
            // .withDOM('&lt;\'row\'&lt;\'col-xs-6\'l&gt;&lt;\'col-xs-6\'f&gt;r&gt;t&lt;\'row\'&lt;\'col-xs-6\'i&gt;&lt;\'col-xs-6\'p&gt;&gt;')
            // Add Bootstrap compatibility
            .withBootstrap()
            .withBootstrapOptions({
                TableTools: {
                    classes: {
                        container: 'btn-group',
                        buttons: {
                            normal: 'btn btn-danger'
                        }
                    }
                },
                ColVis: {
                    classes: {
                        masterButton: 'btn btn-primary'
                    }
                },
                pagination: {
                    classes: {
                        ul: 'pagination pagination-sm'
                    }
                }
            })

            // Add ColVis compatibility
            .withColVis()

            // Add Table tools compatibility
            .withTableTools('vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf')
            .withTableToolsButtons([
                'copy',
                'print', {
                    'sExtends': 'collection',
                    'sButtonText': 'Save',
                    'aButtons': ['csv', 'xls', 'pdf']
                }
            ]);
        vm.dtColumns = [
            DTColumnBuilder.newColumn('name').withTitle('Department Name').withClass('text-danger'),
            DTColumnBuilder.newColumn('status').withTitle('Status'),
            DTColumnBuilder.newColumn('action').withTitle('Action')
        ];
    }*/

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    /*$scope.dtColumns = [
        DTColumnBuilder.newColumn('name').withHtml("<span lang=en>").withTitle('Department Name').withClass('text-danger'),
        DTColumnBuilder.newColumn('status').withTitle('Status'),
        DTColumnBuilder.newColumn('action').withTitle('Action')
    ];*/


    $scope.editDepartment = function(department) {
        $("#editDepartment").modal("show");
        /*$scope.editCharterArea = true;
         $scope.createCharterArea = false;*/
        $scope.editDepartmentData=angular.copy(department);
    }

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/EmployeeDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.addDepartmentModal = function() {
        $("#addDepartment").modal("show");
        $scope.department = {};
        $scope.errorMessageData = '';
        $scope.errorMessage=false;
    }
    $scope.addDepartment=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var department=$scope.department;
        department['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/EmployeeDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":department
        }).success(function (response) {
            console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $("#addDepartment").modal("hide");
            $("#addDeptSuccess").modal("show");
            setTimeout(function(){$('#addDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
            }
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);

        });
    }

    $scope.editDepartmentButton=function () {
        var editCharterDetails= $scope.editDepartmentData;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/EmployeeDepartments/'+$scope.editDepartmentData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editCharterDetails
        }).success(function (response, data) {
            //console.log("filter Schemes "+ JSON.stringify(response));
            $scope.loadingImage=false;
            $("#editDepartment").modal("hide");
            $("#editDeptSuccess").modal("show");
            setTimeout(function(){$('#editDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response, data) {
            if(response.error.details.messages.name) {
                $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                $scope.errorUpdateMessage=true;
            }
            $timeout(function(){
                $scope.errorUpdateMessage=false;
            }, 3000);
            console.log("failure");
        })
    }

    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('workFlowListController', ['userAdminstration', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("workFlowListController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #userAdminLi").addClass("active");
            $(".sidebar-menu #userAdminLi.treeview #workflowLi").addClass("active");
            $(".sidebar-menu #userAdminLi.treeview #workflowLi.active #workflowSetup").addClass("menu-open");
            $(".sidebar-menu #userAdminiProSetUp #workflowLi.active #workflowSetup li.workFlowList ").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #userAdminiProSetUp #workflowLi.active #workflowSetup li.workFlowList  .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    		var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'workflow'}, function (response) {
//    		alert(JSON.stringify(response));
    		   if(!response){
    			window.location.href = "#/noAccessPage";
    		   }
    		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.workflowList!=null &&  $scope.workflowList.length>0){

            $("<tr>" +
                "<th>Workflow Id</th>" +
                "<th>Workflow Name</th>" +
                "<th>Purpose</th>" +
                "<th>Max Levels</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.workflowList.length;i++){
                var workflow=$scope.workflowList[i];

                $("<tr>" +
                    "<td>"+workflow.workflowId+"</td>" +
                    "<td>"+workflow.name+"</td>" +
                    "<td>"+workflow.purpose+"</td>" +
                    "<td>"+workflow.maxLevel+"</td>" +
                    "<td>"+workflow.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Workflow Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

    $scope.loadingImage=true;

    $scope.getWorkflow = function () {
        $http({
            method: 'GET',
            url: 'api/Workflows',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.workflowList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.workflow = {};

    $scope.addWorkFlowModal = function() {
        $("#addWorkFlow").modal("show");
        $scope.workflow = {};
        $scope.workCreationError = false;
        $scope.createWorkError = '';
    };
    var addWorkflowSubmit = true;
    $scope.addWorkflow = function () {
        if(addWorkflowSubmit) {
            addWorkflowSubmit = false;
        $scope.workCreationError = false;
        $scope.createWorkError = '';
        if ($scope.workflow.workflowId.length <= 40) {
            if ($scope.workflow.name.length <= 40) {
                if ($scope.workflow.purpose) {
                    if ($scope.workflow.maxLevel) {
                        if ($scope.workflow.status) {

                            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                            console.log("loginPersonDetails:::" + JSON.stringify(loginPersonDetails));

                            var workflow = $scope.workflow;
                            workflow['createdPerson'] = loginPersonDetails.name;
                            $http({
                                method: 'POST',
                                url: 'api/Workflows',
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                "data": workflow
                            }).success(function (response) {
                                $scope.loadingImage=false;
                                //console.log('Users Response :' + JSON.stringify(response));
                                $scope.workFlowId = response.workflowId;
                                console.log('$scope.workFlowId :' + JSON.stringify($scope.workFlowId));
                                addWorkflowSubmit = true;
                                $("#addWorkFlow").modal("hide");
                                $("#createWorkFlowSuccess").modal("show");
                                setTimeout(function(){$('#createWorkFlowSuccess').modal('hide')}, 3000);
                                $scope.getWorkflow();
                            }).error(function (response) {
                                addWorkflowSubmit = true;
                                console.log('Error Response :' + JSON.stringify(response));
                                $scope.workCreationError = true;
                                if(response.error){
                                    if(response.error.message) {
                                        $scope.createWorkError = response.error.message;
                                    }
                                }
                                $timeout(function(){
                                    $scope.workCreationError=false;
                                }, 3000);
                            });
                        } else {
                            addWorkflowSubmit = true;
                            $scope.workCreationError = true;
                            $scope.createWorkError = 'Please Enter the Status';
                            $timeout(function(){
                                $scope.workCreationError=false;
                            }, 3000);
                        }
                    } else {
                        addWorkflowSubmit = true;
                        $scope.workCreationError = true;
                        $scope.createWorkError = 'Please Enter the Max level';
                        $timeout(function(){
                            $scope.workCreationError=false;
                        }, 3000);
                    }
                }
                else {
                    addWorkflowSubmit = true;
                    $scope.workCreationError = true;
                    $scope.createWorkError = 'Please Enter the Purpose';
                    $timeout(function(){
                        $scope.workCreationError=false;
                    }, 3000);
                }
            }
            else {
                addWorkflowSubmit = true;
                $scope.workCreationError = true;
                $scope.createWorkError = 'Please Enter the WorkFlow Name - Max length is 40';
                $timeout(function(){
                    $scope.workCreationError=false;
                }, 3000);
            }
        }
        else {
            addWorkflowSubmit = true;
            $scope.workCreationError = true;
            $scope.createWorkError = 'Please Enter the WorkFlow Id - Max length is 40';
            $timeout(function(){
                $scope.workCreationError=false;
            }, 3000);
        }
    }

    }
    $scope.editWorkflowData={};
    $scope.editWorkflow = function (department) {
        $scope.loadingImage=false;
        $("#editWorkflow").modal("show");
        /*$scope.editCharterArea = true;
         $scope.createCharterArea = false;*/
        $scope.editWorkflowData = angular.copy(department);
        //console.log("Edit Work FLow response"+JSON.stringify($scope.editWorkflowData))
    }


    $scope.editWorkflowButton=function () {
        $scope.workUpdationError = false;
        $scope.updateWorkError = '';



        if ($scope.editWorkflowData.name.length <= 40) {
            if ($scope.editWorkflowData.purpose) {
                if ($scope.editWorkflowData.maxLevel) {
                    if ($scope.editWorkflowData.status) {

                        var editWorkflowData= $scope.editWorkflowData;

                        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                        console.log("loginPersonDetails :::" +JSON.stringify(loginPersonDetails));

                        editWorkflowData['lastEditPerson']=loginPersonDetails.name;
                        $http({
                            "method": "PUT",
                            "url": 'api/Workflows/'+$scope.editWorkflowData.id,
                            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                            "data": editWorkflowData
                        }).success(function (response, data) {
                            $scope.loadingImage=false;
                            //console.log("filter Schemes "+ JSON.stringify(response));
                            $("#editWorkflow").modal("hide");
                            $("#editWorkFlowSuccess").modal("show");
                            setTimeout(function(){$('#editWorkFlowSuccess').modal('hide')}, 3000);
                            $scope.getWorkflow();
                        }).error(function (response, data) {
                            console.log("failure");
                        })
                    } else {
                        $scope.workUpdationError = true;
                        $scope.updateWorkError = 'Please Enter the  Status';
                    }
                } else {
                    $scope.workUpdationError = true;
                    $scope.updateWorkError = 'Please Enter the Maxlevels';
                }
            }
            else {
                $scope.workUpdationError = true;
                $scope.updateWorkError = 'Please Enter the Purpose';
            }
        }
        else {
            $scope.workUpdationError = true;
            $scope.updateWorkError = 'Please Enter the WorkFlow Name';
        }
    }

    $scope.getWorkflow();
}]);

app.controller('workFlowLevelController', ['userAdminstration', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("workFlowLevelController");

    $(document).ready(function () {
                $('html,body').scrollTop(0);
                $(".sidebar-menu li ul > li").removeClass('active1');
                $('[data-toggle="tooltip"]').tooltip();
                $(".sidebar-menu #userAdminLi").addClass("active");
                $(".sidebar-menu #userAdminLi.treeview #workflowLi").addClass("active");
                $(".sidebar-menu #userAdminLi.treeview #workflowLi.active #workflowSetup").addClass("menu-open");
                $(".sidebar-menu #userAdminiProSetUp #workflowLi.active #workflowSetup li.workFlowLevel").addClass("active1").siblings('.active1').removeClass('active1');
                $(".sidebar-menu #userAdminiProSetUp #workflowLi.active #workflowSetup li.workFlowLevel .collapse").addClass("in");
                $(".sidebar-menu li .collapse").removeClass("in");
            });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'workflowLevels'}, function (response) {
//    		alert(JSON.stringify(response));
    		   if(!response){
    			window.location.href = "#/noAccessPage";
    		   }
    		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.workflowLevelList!=null &&  $scope.workflowLevelList.length>0){

            $("<tr>" +
                "<th>Workflow Id</th>" +
                "<th>Level NO</th>" +
                "<th>Level Id</th>" +
                "<th>Level or Role Description</th>" +
                "<th>Comment</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.workflowLevelList.length;i++){
                var workflowLevel=$scope.workflowLevelList[i];

                $("<tr>" +
                    "<td>"+workflowLevel.workflowId+"</td>" +
                    "<td>"+workflowLevel.levelNo+"</td>" +
                    "<td>"+workflowLevel.levelId+"</td>" +
                    "<td>"+workflowLevel.roleDescription+"</td>" +
                    "<td>"+workflowLevel.commnet+"</td>" +
                    "<td>"+workflowLevel.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Workflow Level Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    $scope.loadingImage=true;
    $scope.editWorkflowLevelModal = function (department) {
        $scope.loadingImage=false;
        $("#editWorkflowLevel").modal("show");
        /*$scope.editCharterArea = true;
         $scope.createCharterArea = false;*/
        $scope.editWorkflowLevel = angular.copy(department);
        $scope.levelCount($scope.editWorkflowLevel.workflowId);
    }
    $scope.getWorkflow = function () {
        $http({
            method: 'GET',
            url: 'api/Workflows',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            console.log('Users Response :' + JSON.stringify(response));
            $scope.workflowList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getWorkflow();
    $scope.getWorkflowLevel = function () {
        $http({
            method: 'GET',
            url: 'api/WorkflowLevels',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.workflowLevelList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.levelList = [];
    $scope.levelCount = function (workflowId) {
        //  //alert(workflowId);
        $scope.levelList = [];
        if ($scope.workflowList != undefined && $scope.workflowList != null && $scope.workflowList.length > 0) {
            for (var i = 0; i < $scope.workflowList.length; i++) {
                if ($scope.workflowList[i].workflowId == workflowId) {
                    //   //alert('found');
                    var maximumLevel = parseInt($scope.workflowList[i].maxLevel);
                    for (var j = 1; j <= maximumLevel; j++) {
                        var level = {
                            'no': j
                        }
                        $scope.levelList.push(level);
                    }
                }
            }
        }
    }
    $scope.workflowLevel = {};
    $scope.addWorkFlowLevelModal = function() {
     $("#addWorkFlowLevel").modal("show");
        $scope.workflowLevel = {};
    }
    $scope.addWorkflowLevels = function () {
        $scope.loadingImage=false;
        $scope.levelCreationError = false;
        $scope.createLevelError = '';
        if ($scope.workflowLevel.workflowId) {
            if ($scope.workflowLevel.levelNo) {
                if ($scope.workflowLevel.status) {
                    if ($scope.workflowLevel.levelId) {
                        if ($scope.workflowLevel.roleDescription) {
                            if ($scope.workflowLevel.commnet) {
                                //var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                                var workflowLevel = $scope.workflowLevel;
                                workflowLevel['createdPerson'] = loginPersonDetails.name;
                                $http({
                                    method: 'POST',
                                    url: 'api/WorkflowLevels',
                                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                    "data": workflowLevel
                                }).success(function (response) {
                                    console.log('Users Response :' + JSON.stringify(response));
                                    $scope.workFlowLevelId = response.levelId;
                                    console.log('$scope.workFlowId :' + JSON.stringify($scope.workFlowLevelId));
                                    $("#addWorkFlowLevel").modal("hide");
                                    $("#addWorkFlowLevelSuccess").modal("show");
                                    setTimeout(function(){$('#addWorkFlowLevelSuccess').modal('hide')}, 3000);
                                    $scope.getWorkflowLevel();
                                    $scope.loadingImage=false;
                                }).error(function (response) {
                                    console.log('Error Response :' + JSON.stringify(response));
                                    if(response.error){
                                        if(response.error.message){
                                            $scope.levelCreationError = true;
                                            $scope.createLevelError = response.error.message;
                                            $timeout(function(){
                                                $scope.levelCreationError=false;
                                            }, 3000);
                                        }
                                    }
                                });
                            } else {
                                $scope.levelCreationError = true;
                                $scope.createLevelError = 'Please Enter The Comment ';
                                $timeout(function(){
                                    $scope.levelCreationError=false;
                                }, 3000);
                            }

                        } else {
                            $scope.levelCreationError = false;
                            $scope.createLevelError = 'Please Enter The Level or Role Description ';
                            $timeout(function(){
                                $scope.levelCreationError=false;
                            }, 3000);
                        }

                    } else {
                        $scope.levelCreationError = false;
                        $scope.createLevelError = 'Please Enter The Level Id ';
                        $timeout(function(){
                            $scope.levelCreationError=false;
                        }, 3000);
                    }

                } else {
                    $scope.levelCreationError = false;
                    $scope.createLevelError = 'Please Select The Status ';
                    $timeout(function(){
                        $scope.levelCreationError=false;
                    }, 3000);
                }
            } else {
                $scope.levelCreationError = false;
                $scope.createLevelError = 'Please Select The Level No ';
                $timeout(function(){
                    $scope.levelCreationError=false;
                }, 3000);
            }

        }else
        {
            $scope.levelCreationError = false;
            $scope.createLevelError = 'Please Enter The WorkFlow Id ';
            $timeout(function(){
                $scope.levelCreationError=false;
            }, 3000);
        }

    }

    $scope.editWorkflowLevelButton  =function () {
        var editWorkflowLevel= $scope.editWorkflowLevel;
        //var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editWorkflowLevel['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/WorkflowLevels/'+$scope.editWorkflowLevel.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editWorkflowLevel
        }).success(function (response, data) {
            //console.log("filter Schemes "+ JSON.stringify(response));
            $("#editWorkflowLevel").modal("hide");
            $("#editWorkFlowLevelSuccess").modal("show");
            setTimeout(function(){$('#editWorkFlowLevelSuccess').modal('hide')}, 3000);
            $scope.loadingImage=false;
            $scope.getWorkflowLevel();
        }).error(function (response, data) {
            console.log("failure");
        })
    }

    $scope.getWorkflowLevel();
}]);

app.controller('workFlowWithEmployeeController', ['userAdminstration', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("workFlowWithEmployeeController");

    $(document).ready(function () {
                    $('html,body').scrollTop(0);
                    $(".sidebar-menu li ul > li").removeClass('active1');
                    $('[data-toggle="tooltip"]').tooltip();
                    $(".sidebar-menu #userAdminLi").addClass("active");
                    $(".sidebar-menu #userAdminLi.treeview #workflowLi").addClass("active");
                    $(".sidebar-menu #userAdminLi.treeview #workflowLi.active #workflowSetup").addClass("menu-open");
                    $(".sidebar-menu #userAdminiProSetUp #workflowLi.active #workflowSetup li.workFlowEmployee").addClass("active1").siblings('.active1').removeClass('active1');
                    $(".sidebar-menu #userAdminiProSetUp #workflowLi.active #workflowSetup li.workFlowEmployee .collapse").addClass("in");
                    $(".sidebar-menu li .collapse").removeClass("in");
                });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'workflowEmployee'}, function (response) {
//        		alert(JSON.stringify(response));
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.workflowEmployeeList!=null &&  $scope.workflowEmployeeList.length>0){

            $("<tr>" +
                "<th>Workflow Id</th>" +
                "<th>Level Id</th>" +
                "<th>Employee Id</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.workflowEmployeeList.length;i++){
                var workflowEEmployee=$scope.workflowEmployeeList[i];

                $("<tr>" +
                    "<td>"+workflowEEmployee.workflowId+"</td>" +
                    "<td>"+workflowEEmployee.levelId+"</td>" +
                    "<td>"+workflowEEmployee.employeeData+"</td>" +
                    "<td>"+workflowEEmployee.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Workflow Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    $scope.loadingImage=true;

    $scope.somdata=[];

    $scope.example15settings = {enableSearch: true};
    /*$scope.editWorkflowEmployeeModal = function(workflowEEmployee) {
        //alert(workflowEEmployee);
        $scope.loadingImage=false;
        $("#editWorkflowEmployee").modal("show");
        /!*$scope.editCharterArea = true;
         $scope.createCharterArea = false;
        $scope.levelCount($scope.editWorkflowLevel.workflowId);*!/
        $scope.editWorkflowLevel=angular.copy(workflowEEmployee);
        console.log("editWorkflowLevel "+ JSON.stringify($scope.editWorkflowLevel));
        //$scope.levelCount($scope.editWorkflowLevel.workflowId);
    }
*/
    $scope.getWorkflow=function () {
        $http({
            method: 'GET',
            url: 'api/Workflows',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.workflowList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.employeeData=[];
    $scope.getEmployees=function () {
        $http({
            method: 'GET',
            url: 'api/Employees',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('Users Response............ :' + JSON.stringify(response));

            $scope.employeeList = response;

            $scope.getWorkflowEmployees();
            if($scope.employeeList!=undefined && $scope.employeeList!=null && $scope.employeeList.length>0){
                for(var i=0;i<$scope.employeeList.length;i++){
                    var data={
                        label: $scope.employeeList[i].employeeId+'-'+$scope.employeeList[i].name,
                        id: $scope.employeeList[i].employeeId
                    }
                    //  var daata={id: 1, label: "David"}
                    $scope.employeeData.push(data);
                }
            }
            $scope.loadingImage=false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    //  $scope.workflowEmployee.employees=[];
    $scope.getWorkflowLevel=function () {
        $http({
            method: 'GET',
            url: 'api/WorkflowLevels',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.workflowLevelList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.getWorkflowLevel();
    $scope.getEmployees();
    $scope.getWorkflow();

    $scope.schemesInfo=function (data) {
        if(data=='schemeManagement') {
            $http({
                method: 'GET',
                url: 'api/Schemes',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                //console.log('GetModules Response :' + JSON.stringify(response[1]));
                $scope.schemeList = response;
                $scope.loadingImage=false;
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }

    $scope.getWorkflowEmployees=function () {
        $http({
            method: 'GET',
            url: 'api/WorkflowEmployees',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('workflowEmployeeList :' + JSON.stringify(response));
            $scope.workflowEmployeeList = response;

            if($scope.workflowEmployeeList!=undefined && $scope.workflowEmployeeList!=null && $scope.workflowEmployeeList.length>0){
                for(var i=0;i<$scope.workflowEmployeeList.length;i++){
                    var employeeId=$scope.workflowEmployeeList[i].employees;
                    var employeeData=[];
                    if(employeeId!=undefined && employeeId!=null && employeeId.length>0){
                        for(var j=0;j<employeeId.length;j++){
                            if($scope.employeeList!=undefined && $scope.employeeList!=null && $scope.employeeList.length>0){
                                for(var x=0;x<$scope.employeeList.length;x++){
                                    if(employeeId[j]==$scope.employeeList[x].employeeId){
                                        var showEmployee=employeeId[j]+'-'+$scope.employeeList[x].name;
                                        employeeData.push(showEmployee);
                                        //console.log('data is'+JSON.stringify(employeeData));
                                        break;
                                    }
                                }
                            }
                            //console.log('data is '+JSON.stringify(employeeData));
                            $scope.workflowEmployeeList[i]['employeeData']=employeeData;
                        }
                    }

                }


            }

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.levelList=[];
    $scope.levelId=function (workflowData) {
        $scope.loadingImage=false;
        //var workflow=JSON.parse(workflowData);
        /*$scope.workflowEmployee['workFlowId']=workflow.workFlowId;
         $scope.workflowEmployee.maxLevel=workflow.maxLevel;*/
        // //alert(workflowId);
        $scope.levelList=[];
        if($scope.workflowLevelList!=undefined && $scope.workflowLevelList!=null && $scope.workflowLevelList.length>0){
            for(var i=0;i<$scope.workflowLevelList.length;i++){
                if($scope.workflowLevelList[i].workflowId==workflowData){
                    //  //alert('found');
                    var level={
                        'no':$scope.workflowLevelList[i].levelId
                    }
                    /*$scope.levelList.push(level);*/
                    $scope.levelList.push($scope.workflowLevelList[i]);
                }
            }
        }
    }

    $scope.addWorkFlowLevelModal = function() {
        $("#addWorkFlowLevel").modal("show");
        $scope.workflowEmployee = {};
        $scope.customFilter='';
        $scope.somdata = [];
        $scope.customFilter='';
        $scope.workflowEmpEditErrorMessage = '';
        $scope.workflowEmpEditError = false;
    }


    $scope.addWorkflowEmployee=function () {
        $scope.workflowEmpEditErrorMessage = '';
        $scope.workflowEmpEditError = false;
        $scope.loadingImage=false;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var workflowEmployee=$scope.workflowEmployee;
        // alert(JSON.stringify($scope.workflowEmployee));

        if( $scope.workflowList !=undefined &&  $scope.workflowList !=null &&  $scope.workflowList.length>0 ){
            for( var i=0;i<$scope.workflowList.length;i++ ){
                if($scope.workflowList[i].workflowId==workflowEmployee.workflowId){
                    workflowEmployee['maxLevels']=$scope.workflowList[i].maxLevel;
                }
            }
        }

        if($scope.workflowLevelList!=undefined && $scope.workflowLevelList!=null && $scope.workflowLevelList.length>0){
            for(var i=0;i<$scope.workflowLevelList.length;i++){
                if($scope.workflowLevelList[i].workflowId==workflowEmployee.workflowId && $scope.workflowLevelList[i].levelId==workflowEmployee.levelId){
                    //alert(JSON.stringify($scope.workflowLevelList[i]));
                    workflowEmployee['levelNo']=$scope.workflowLevelList[i].levelNo;
                }
            }
        }

        workflowEmployee['createdPerson']=loginPersonDetails.name;
        var someData=[];

        if($scope.somdata!=null && $scope.somdata.length>0){
            for(var i=0;i<$scope.somdata.length;i++){
                someData.push($scope.somdata[i].id);
            }
        }

        workflowEmployee['employees']=someData;

          console.log("workflowEmployee:::" +JSON.stringify(workflowEmployee));


        if(workflowEmployee.workflowId && workflowEmployee.workflowId != '') {
            if(workflowEmployee.levelId && workflowEmployee.levelId != '') {
                if(workflowEmployee.status && workflowEmployee.status != '') {
                    if (workflowEmployee.employees.length > 0) {
                        $http({
                         method: 'POST',
                         url: 'api/WorkflowEmployees/',
                         headers: {"Content-Type": "application/json", "Accept": "application/json"},
                         "data":workflowEmployee
                         }).success(function (response) {
                         //console.log('Users Response :' + JSON.stringify(response));
                         /*$scope.workflowEmployee = {};*/

                         $("#addWorkFlowLevel").modal("hide");
                         $('#addWorkflowSuccess').modal('show');
                         setTimeout(function(){$('#addWorkflowSuccess').modal('hide')}, 3000);
                         $scope.loadingImage=false;
                         $scope.getWorkflowEmployees();
                         }).error(function (response) {
                         console.log('Error Response :' + JSON.stringify(response));
                         });
                    } else {
                        $scope.workflowEmpEditErrorMessage = 'Please select Employee(s)';
                        $scope.workflowEmpEditError = true;
                    }
                }else{
                    $scope.workflowEmpEditErrorMessage = 'Please Select Status';
                    $scope.workflowEmpEditError = true;
                }
            }else{
                $scope.workflowEmpEditErrorMessage = 'Please Select LevelId';
                $scope.workflowEmpEditError = true;
            }
        }else{
            $scope.workflowEmpEditErrorMessage = 'Please Select WorkflowId';
            $scope.workflowEmpEditError = true;
        }
    };

    $scope.editSelectedEmployee=[];
    $scope.workflowEmpEditErrorMessage = '';
    $scope.workflowEmpEditError = false;
    $scope.editWorkflowEmployeeModal = function(workflowEEmployee){
        $("#editWorkflowEmployee").modal("show");
        //console.log('Edit wf:'+JSON.stringify(workflowEEmployee));
        $scope.editworkFlowEmp = angular.copy(workflowEEmployee);
        $scope.editCharterArea = true;
        $scope.createCharterArea = false;
        $scope.editSelectedEmployee=[];
        $scope.workflowEmpEditErrorMessage = '';
        $scope.workflowEmpEditError = false;

        var selectedEmployess=workflowEEmployee.employees;
        if(selectedEmployess!=undefined && selectedEmployess!=null && selectedEmployess.length>0){
            for(var i=0;i<selectedEmployess.length;i++){
                var data={
                    id:selectedEmployess[i]
                };
                $scope.editSelectedEmployee.push(data);
            }
        }
        //$scope.levelCount($scope.editWorkflowLevel.workflowId);
    };

    $scope.editWorkflowLevelButton  =function () {

        if($scope.editworkFlowEmp.status.length > 0) {
            if ($scope.editSelectedEmployee) {
                if ($scope.editSelectedEmployee.length > 0) {
                    $scope.editworkFlowEmp.employees = [];
                    for (var i = 0; i < $scope.editSelectedEmployee.length; i++) {
                        $scope.editworkFlowEmp.employees.push($scope.editSelectedEmployee[i].id);
                    }
                    delete $scope.editworkFlowEmp.employeeData;
                    //console.log(JSON.stringify($scope.editworkFlowEmp));
                    var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                    $scope.editworkFlowEmp['lastEditPerson'] = loginPersonDetails.name;
                    $http({
                        "method": "PUT",
                        "url": 'api/WorkflowEmployees/' + $scope.editworkFlowEmp.id,
                        "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                        "data": $scope.editworkFlowEmp
                    }).success(function (response, data) {
                        //console.log("filter Schemes "+ JSON.stringify(response));
                        $("#editWorkflowEmployee").modal("hide");
                        $("#editWorkflowEMPSuccess").modal("show");
                        setTimeout(function(){$('#editWorkflowEMPSuccess').modal('hide')}, 3000);
                        $scope.getWorkflowLevel();
                        $scope.getWorkflowEmployees();
                        $scope.loadingImage = false;
                    }).error(function (response, data) {
                        console.log("failure");
                    })
                } else {
                    $scope.workflowEmpEditErrorMessage = 'Please select atleast one employee';
                    $scope.workflowEmpEditError = true;
                }
            } else {
                $scope.workflowEmpEditErrorMessage = 'Please select atleast one employee';
                $scope.workflowEmpEditError = true;
            }
        } else {
            $scope.workflowEmpEditErrorMessage = 'Please select the Status';
            $scope.workflowEmpEditError = true;
        }
    };

    $scope.getWorkflowLevel();



}]);

app.controller('workFlowWithFormsController', ['userAdminstration', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("workFlowWithFormsController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #userAdminLi").addClass("active");
        $(".sidebar-menu #userAdminLi.treeview #workflowLi").addClass("active");
        $(".sidebar-menu #userAdminLi.treeview #workflowLi.active #workflowSetup").addClass("menu-open");
        $(".sidebar-menu #userAdminiProSetUp #workflowLi.active #workflowSetup li.workFlowForms").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #userAdminiProSetUp #workflowLi.active #workflowSetup li.workFlowForms .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'workflowForms'}, function (response) {
//            		alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.workflowEmployeeList!=null &&  $scope.workflowEmployeeList.length>0){

            $("<tr>" +
                "<th>Workflow Id</th>" +
                "<th>Module Name</th>" +
                "<th>Form Details</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.workflowEmployeeList.length;i++){
                var workflowForm=$scope.workflowEmployeeList[i];

                $("<tr>" +
                    "<td>"+workflowForm.workflowId+"</td>" +
                    "<td>"+workflowForm.formDetails+"</td>" +
                    "<td>"+workflowForm.schemeUniqueId+"</td>" +
                    "<td>"+workflowForm.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }

    $scope.loadingImage=true;
    $scope.somdata=[];

    $scope.example15settings = {enableSearch: true};
    $scope.editWorkflowEmployeeModal = function(workflowForm) {
        $scope.loadingImage=false;
        $scope.workflowFormError = false;
        $scope.workflowFormErrorMessage = '';
        $("#editWorkflowEmployee").modal("show");
        //alert(workflowForm);
        /*$scope.editCharterArea = true;
         $scope.createCharterArea = false;*/
        $scope.editWorkflowEmp=angular.copy(workflowForm);
        $rootScope.formEditSchemeUniqueId = $scope.editWorkflowEmp.schemeUniqueId;
        console.log(JSON.stringify($scope.editWorkflowEmp));
        $scope.schemesInfo($scope.editWorkflowEmp.formDetails,'edit');
        //$scope.levelCount($scope.editWorkflowEmp.workflowId);
    };
    $scope.getWorkflow=function () {
        $http({
            method: 'GET',
            url: 'api/Workflows',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.workflowList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    //$scope.employeeData=[];
    /* $scope.getEmployees=function () {
     $http({
     method: 'GET',
     url: 'api/Employees',
     headers: {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response) {
     //console.log('Users Response :' + JSON.stringify(response));

     $scope.employeeList = response;
     if($scope.employeeList!=undefined && $scope.employeeList!=null && $scope.employeeList.length>0){
     for(var i=0;i<$scope.employeeList.length;i++){
     var data={
     label: $scope.employeeList[i].employeeId+'-'+$scope.employeeList[i].name,
     id: $scope.employeeList[i].employeeId
     }
     //  var daata={id: 1, label: "David"}
     $scope.employeeData.push(data);
     }
     }
     }).error(function (response) {
     console.log('Error Response :' + JSON.stringify(response));
     });
     }*/
    //  $scope.workflowEmployee.employees=[];
    /*$scope.getWorkflowLevel=function () {
     $http({
     method: 'GET',
     url: 'api/WorkflowLevels',
     headers: {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response) {
     //console.log('Users Response :' + JSON.stringify(response));
     $scope.workflowLevelList = response;
     }).error(function (response) {
     console.log('Error Response :' + JSON.stringify(response));
     });
     }*/

    // $scope.getWorkflowLevel();
    // $scope.getEmployees();
    $scope.getWorkflow();

    $scope.schemeDataShow=false;
    $scope.projectWardShow=false;

    $scope.schemesInfo=function (data, check) {

        console.log('Data...'+data);
        if(check == 'edit'){
            $scope.editWorkflowEmp.schemeUniqueId = '';
        }else {
            $scope.workflowEmployee.schemeUniqueId = '';
        }
        $scope.projectWardShow=false;
        $scope.landAndAsset=false;
        $scope.schemeDataShow=false;
        $scope.userAdminShow=false;
        if(data=='schemeManagement') {
            $http({
                method: 'GET',
                url: 'api/Schemes',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.schemeDataShow=true;
                //console.log('GetModules Response :' + JSON.stringify(response[1]));
                $scope.schemeList = response;
                $scope.loadingImage=false;
                $scope.userAdminShow=false;
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
       else if(data=='projectWardWork'){
            $scope.schemeDataShow=false;
            $scope.projectWardShow=true;
            $scope.userAdminShow=false;
        }
        else if(data=='landAndAsset'){
            $scope.landAndAsset=true;
            $scope.schemeDataShow=false;
            $scope.projectWardShow=false;
            $scope.userAdminShow=false;
        }else if(data=='userAdministration'){
            $scope.landAndAsset=false;
            $scope.schemeDataShow=false;
            $scope.projectWardShow=false;
            $scope.userAdminShow=true;
        }
    }
    $scope.getWorkflowForms=function () {
        $http({
            method: 'GET',
            url: 'api/WorkflowForms',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.workflowEmployeeList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getWorkflowForms();
    $scope.addWorkFlowLevelModal = function() {
        $scope.workflowEmployee = {};
        $scope.projectWardShow=false;
        $scope.landAndAsset=false;
        $scope.schemeDataShow=false;
        $scope.userAdminShow=false;
        $scope.workflowFormError = false;
        $scope.workflowFormErrorMessage = '';
        $("#addWorkFlowLevel").modal("show");

    }

    var workflowFormSubmit = true;
    $scope.addWorkflowForms=function () {
        if(workflowFormSubmit) {
            workflowFormSubmit = false;
            $scope.workflowFormError = false;
            $scope.workflowFormErrorMessage = '';
            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            var workflowEmployee = $scope.workflowEmployee;
            workflowEmployee['createdPerson'] = loginPersonDetails.name;
            workflowEmployee['createdPersonId'] = loginPersonDetails.id;
//            alert("WF Forms:" + JSON.stringify(workflowEmployee));
            if (workflowEmployee.workflowId && workflowEmployee.workflowId != '') {
                if (workflowEmployee.formDetails && workflowEmployee.formDetails != '') {
                    if (workflowEmployee.schemeUniqueId && workflowEmployee.schemeUniqueId != '') {
                        if (workflowEmployee.status && workflowEmployee.status != '') {
                            $http({
                                method: 'POST',
                                url: 'api/WorkflowForms/',
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                "data": workflowEmployee
                            }).success(function (response) {
                                if(response.error){
                                    if(response.error.message){
                                        $scope.workflowFormError = true;
                                        $scope.workflowFormErrorMessage = response.error.message;
                                    }
                                }else{
                                    console.log('Users Response :' + JSON.stringify(response));
                                    /*$scope.workflowEmployee = {};*/
                                    workflowFormSubmit = true;
                                    $("#addWorkFlowLevel").modal("hide");
                                    $("#addworkFlowFormSuccess").modal("show");
                                    setTimeout(function () {
                                        $('#addworkFlowFormSuccess').modal('hide')
                                    }, 3000);
                                    $scope.loadingImage = false;
                                    $scope.getWorkflowForms();
                                }

                            }).error(function (response) {
                                workflowFormSubmit = true;
                                console.log('Error Response :' + JSON.stringify(response));
                                if(response.error){
                                    if(response.error.message){
                                        $scope.workflowFormError = true;
                                        $scope.workflowFormErrorMessage = response.error.message;
                                    }
                                }
                            });
                        } else {
                            workflowFormSubmit = true;
                            $scope.workflowFormError = true;
                            $scope.workflowFormErrorMessage = 'Please Select Status';
                        }
                    } else {
                        workflowFormSubmit = true;
                        $scope.workflowFormError = true;
                        $scope.workflowFormErrorMessage = 'Please Select Scheme';
                    }
                } else {
                    workflowFormSubmit = true;
                    $scope.workflowFormError = true;
                    $scope.workflowFormErrorMessage = 'Please Select Form Data';
                }
            } else {
                workflowFormSubmit = true;
                $scope.workflowFormError = true;
                $scope.workflowFormErrorMessage = 'Please Select Workflow Id';
            }
        }
    };

    var editWorkflowFormcheck = true;
    $scope.editWorkflowForms  =function () {
        console.log(JSON.stringify($scope.editWorkflowEmp));
        //console.log(JSON.stringify($rootScope.formEditSchemeUniqueId));
        $scope.editWorkflowEmp.schemeUniqueId = $rootScope.formEditSchemeUniqueId;
        if(editWorkflowFormcheck) {
            editWorkflowFormcheck = false;
            $scope.workflowFormError = false;
            $scope.workflowFormErrorMessage = '';
            var editWorkflowLevel = $scope.editWorkflowEmp;
            //var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            editWorkflowLevel['lastEditPerson'] = loginPersonDetails.name;
            console.log(JSON.stringify(editWorkflowLevel));
            if (editWorkflowLevel.workflowId && editWorkflowLevel.workflowId != '') {
                if (editWorkflowLevel.formDetails && editWorkflowLevel.formDetails != '') {
                    if (editWorkflowLevel.schemeUniqueId && editWorkflowLevel.schemeUniqueId != '') {
                        if (editWorkflowLevel.status && editWorkflowLevel.status != '') {
                            $http({
                                "method": "PUT",
                                "url": 'api/WorkflowForms/' + editWorkflowLevel.id,
                                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                                "data": editWorkflowLevel
                            }).success(function (response, data) {
                                editWorkflowFormcheck = true;
                                //console.log("filter Schemes "+ JSON.stringify(response));
                                $("#editWorkflowEmployee").modal("hide");
                                $("#editworkFlowFormSuccess").modal("show");
                                setTimeout(function () {
                                    $('#editworkFlowFormSuccess').modal('hide')
                                }, 3000);
                                $scope.getWorkflowForms();
                                $scope.loadingImage = false;
                            }).error(function (response, data) {
                                editWorkflowFormcheck = true;
                                console.log("failure");
                                if(response.error){
                                    if(response.error.message){
                                        $scope.workflowFormError = true;
                                        $scope.workflowFormErrorMessage = response.error.message;
                                    }
                                }
                            })
                        }else {
                            workflowFormSubmit = true;
                            $scope.workflowFormError = true;
                            $scope.workflowFormErrorMessage = 'Please Select Status';
                        }
                    }else {
                        editWorkflowFormcheck = true;
                        $scope.workflowFormError = true;
                        $scope.workflowFormErrorMessage = 'Please Select Scheme';
                    }
                }else {
                    editWorkflowFormcheck = true;
                    $scope.workflowFormError = true;
                    $scope.workflowFormErrorMessage = 'Please Select Form Data';
                }
            }else {
                editWorkflowFormcheck = true;
                $scope.workflowFormError = true;
                $scope.workflowFormErrorMessage = 'Please Select WorkflowId';
            }
        }
    };

    //   $scope.getWorkflowLevel();
}]);

app.controller('rolemodelController', ['userAdminstration', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(userAdminstration,$http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
     console.log("rolemodelController");

      $(document).ready(function () {
              $('html,body').scrollTop(0);
              $(".sidebar-menu li ul > li").removeClass('active1');
              $('[data-toggle="tooltip"]').tooltip();
              $(".sidebar-menu #userAdminLi").addClass("active");
              $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .role ").addClass("active1");
              $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .role ").addClass("menu-open");
              $(".sidebar-menu #scheme #userAdminiProSetUp .role  ").addClass("active1").siblings('.active1').removeClass('active1');
              $(".sidebar-menu li .collapse").removeClass("in");
          });

     var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'role'}, function (response) {
//         		alert(JSON.stringify(response));
         		   if(!response){
         			window.location.href = "#/noAccessPage";
         		   }
         		});

     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
         .withOption("sPaginationType", "bootstrap")
         .withOption("retrieve", true)
         .withOption('stateSave', true)
         .withPaginationType('simple_numbers')
         .withOption('order', [0, 'ASC']);

     $scope.exportToExcel=function(tableId){
         if( $scope.rolemodelDetails.formDetails!=null &&  $scope.rolemodelDetails.formDetails.length>0){

             $("<tr>" +
                 "<th>typeValue</th>" +
                 "<th>name</th>" +
                 "</tr>").appendTo("table"+tableId);

             for(var i=0;i<$scope.rolemodelDetails.formDetails.length;i++){
                 var role=$scope.rolemodelDetails.formDetails[i];

                 $("<tr>" +
                     "<td>"+role.typeValue+"</td>" +
                     "<td>"+role.name+"</td>" +
                     "</tr>").appendTo("table"+tableId);
             }
             var exportHref = Excel.tableToExcel(tableId, 'Role Management Details ');
             $timeout(function () {
                 location.href = exportHref;
             }, 100);
         } else {
             $("#emptyDataTable").modal("show");
             setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
         }
     }

     $scope.loadingImage=true;
     $scope.getRolemodel = function(){
         $http({
             method: 'GET',
             url: 'api/RoleModels',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.rolemodelList = response;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }
     $scope.getRolemodel();
     $scope.roleDetails=false

     $scope.selectRoleDetails=function(roleId){
    //alert(roleId);

         $http({
             method: 'GET',
             url: 'api/RoleModels/'+roleId,
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.roleDetails=true;
             $scope.rolemodelDetails = response;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }

     $scope.checkAll = function() {
         if ($scope.selectedAll) {
             $scope.selectedAll = true;
         } else {
             $scope.selectedAll = false;
         }
         angular.forEach($scope.rolemodelList.formsList, function(user) {
             user.select = $scope.selectAll;
         });
     };
     $scope.fromView=function(form){
         if(form.view){
             var status=true;
             angular.forEach($scope.rolemodelList.formsList, function(user) {
                 if(!user.view){
                     status=false;
                 }
             });
             if(status){
                 $scope.viewAll = true;
             }else{
                 $scope.viewAll = false;
             }
             //$scope.viewAll = true;
         }else{
             $scope.viewAll = false;
         }

     }
     $scope.formEdit=function(form){
         if(form.edit){
             var status=true;
             angular.forEach($scope.rolemodelList.formsList, function(user) {
                 if(!user.edit){
                     status=false;
                 }
             });
             if(status){
                 $scope.editAll = true;
             }else{
                 $scope.editAll = false;
             }
             //$scope.viewAll = true;
         }else{
             $scope.editAll = false;
         }

     }

     $scope.checkViewAll = function() {
         alert($scope.viewAll);
         if (!$scope.viewAll) {
             $scope.viewAll = false;
         } else {
             $scope.viewAll = true;
         }
         if( $scope.viewAll){
             angular.forEach($scope.rolemodelList.formsList, function(user) {
                 user.view = $scope.viewAll;
             });
         }else if(! $scope.viewAll){
             angular.forEach($scope.rolemodelList.formsList, function(user) {
                 user.view = $scope.viewAll;
                 user.edit = $scope.viewAll;
                 $scope.editAll = $scope.viewAll;
             });
         }

     };
     $scope.checkEditAll = function() {
         if ($scope.editAll) {
             $scope.editAll = true;
         } else {
             $scope.editAll = false;
         }
         angular.forEach($scope.rolemodelList.formsList, function(user) {
             //  user.view = $scope.editAll;
             user.edit = $scope.editAll;
             //   $scope.viewAll =  $scope.editAll;
         });
     };



     $scope.updateRole=function(){
        //alert('hai');

         $http({
             method: 'PUT',
             url: 'api/RoleModels/'+$scope.rolemodelDetails.id,
             headers: {"Content-Type": "application/json", "Accept": "application/json"},
             data: $scope.rolemodelDetails
         }).success(function (response) {
           $("#editRoleSuccess").modal("show");
             setTimeout(function(){$('#editRoleSuccess').modal('hide')}, 3000);

         }).error(function(data){
             console.log('Error:'+JSON.stringify(data));
         });

     }

 /*    $scope.selectedForms=[];
     $scope.selectModule=function(selectedModule){
         $scope.loadingImage=false;
         $scope.selectedForms=[];
         if($scope.formsList!=undefined && $scope.formsList!=null && $scope.formsList.length>0){
             for(var i=0;i<$scope.formsList.length;i++){
                 if(selectedModule==$scope.formsList[i].typeValue){
                     $scope.selectedForms.push($scope.formsList[i]);
                 }
             }
         }
     }

     $scope.getFormList=function(){

         $http({
             method: 'GET',
             url: 'api/FormLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.formsList = response;
             if($scope.formsList!=undefined && $scope.formsList!=null && $scope.formsList.length>0){
                 $scope.moduleList=[
                     {
                         "typeName": "Scheme",
                         "typeValue": "scheme"
                     }, {
                         "typeName": "Project/Ward Works",
                         "typeValue": "project"
                     }, {
                         "typeName": "Configuration Data",
                         "typeValue": "configurationData"
                     },{
                         "typeName": "User Administration",
                         "typeValue": "userAdministration"
                     },{
                         "typeName": "Legal Management",
                         "typeValue": "legalManagement"
                     },{
                         "typeName": "Land And Asset Management",
                         "typeValue": "landAndAssetManagement"
                     }
                 ]

             }

         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }

     $scope.getFormList();
     $scope.errorInEmployeeSelection=false;

     $scope.role={}
     $scope.addWorkFlowModal = function() {
         $("#addWorkFlow").modal("show");
         $scope.selectedEmployee = {};
         $scope.selectedEmployee = [];
         $scope.role = {};
     }
     $scope.addRole = function(){
         $scope.loadingImage=false;
         var existingEmployee=[];
         var employeeString='';
         var statusOfemployee=true;
         if($scope.roleList!=undefined && $scope.roleList!=null && $scope.roleList.length>0 ){
             for(var x=0;x<$scope.roleList.length;x++){
                 if($scope.roleList[x].selectFormList==$scope.role.selectFormList){
                     if($scope.selectedEmployee!=undefined && $scope.selectedEmployee!=null && $scope.selectedEmployee.length>0){
                         for(var i=0;i<$scope.selectedEmployee.length;i++){
                             if($scope.roleList[x].employees!=undefined && $scope.roleList[x].employees!=null && $scope.roleList[x].employees.length>0){
                                 for(var j=0;j<$scope.roleList[x].employees.length;j++){
                                     if($scope.selectedEmployee[i].id==$scope.roleList[x].employees[j]){
                                         existingEmployee.push($scope.roleList[x].employees[j]);
                                         employeeString=employeeString+$scope.roleList[x].employees[i]+', ';
                                        // alert(employeeString);
                                         statusOfemployee=false;
                                     }else{

                                     }
                                 }
                             }
                         }
                     }

                 }

             }

         }
         // alert(statusOfemployee);

         console.log("role response"+JSON.stringify($scope.role));
         if(statusOfemployee) {
             var employeeData = [];
             if ($scope.selectedEmployee != undefined && $scope.selectedEmployee != null && $scope.selectedEmployee.length > 0) {
                 for (var i = 0; i < $scope.selectedEmployee.length; i++) {
                     employeeData.push($scope.selectedEmployee[i].id);
                 }
             }
             $scope.role.employees = employeeData;
             if ($scope.role.name.length < 30) {
                 //if ($scope.role.selectFormList) {
                 //    if ($scope.role.employees) {

                 if ($scope.role.selectAccessLevel.length > 0) {
                     if ($scope.role.status.length > 0) {
                         if($scope.selectedEmployee != undefined && $scope.selectedEmployee != null && $scope.selectedEmployee.length > 0) {

                             $scope.role.type = 'employee';
                             //   alert(JSON.stringify($scope.role.employees));
                             $http({
                                 method: 'POST',
                                 url: 'api/RoleModels',
                                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                 "data": $scope.role
                             }).success(function (response) {
                                 $scope.loadingImage = false;
                                 /!*$scope.role = {};*!/
                                 $("#addWorkFlow").modal("hide");
                                 $("#addRoleSuccess").modal("show");
                                 setTimeout(function(){$('#addRoleSuccess').modal('hide')}, 3000);
                                 $scope.getRole();
                             }).error(function (response) {
                                 if (response.error.details.messages.name) {
                                     $scope.errorMessageData = response.error.details.messages.name[0];
                                     $scope.errorMessage = true;
                                 }
                                 $timeout(function(){
                                     $scope.errorMessage=false;
                                 }, 3000);
                                 console.log('Error Response :' + JSON.stringify(response));
                             });
                         }else{
                             $scope.errorInEmployeeSelection=true;
                             $scope.errorMessageEmployeeSelection='Please Employee Id Selection..';
                             $timeout(function(){
                                 $scope.errorInEmployeeSelection=false;
                                 console.log("should change");
                             }, 3000);
                         }
                     }
                     else{
                         $scope.errorInEmployeeSelection=true;
                         $scope.errorMessageEmployeeSelection='Please Select the Status';
                         $timeout(function(){
                             $scope.errorInEmployeeSelection=false;
                             console.log("should change");
                         }, 3000);
                     }
                 }
                 else{
                     $scope.errorInEmployeeSelection=true;
                     $scope.errorMessageEmployeeSelection='Please Select the Access Level';
                     $timeout(function(){
                         $scope.errorInEmployeeSelection=false;
                         console.log("should change");
                     }, 3000);
                 }

                 //}
                 //else{
                 //    $scope.errorInEmployeeSelection=true;
                 //    $scope.errorMessageEmployeeSelection='Please Select the Employee List';
                 //}

                 //}
                 //else{
                 //    $scope.errorInEmployeeSelection=true;
                 //    $scope.errorMessageEmployeeSelection='Please Select the Form List';
                 //}
             }
             else{
                 $scope.errorInEmployeeSelection=true;
                 $scope.errorMessageEmployeeSelection='Enter Role Name in the Correct Format';
                 $timeout(function(){
                     $scope.errorInEmployeeSelection=false;
                     console.log("should change");
                 }, 3000);

             }
         }
         else{
             $scope.errorInEmployeeSelection=true;
             $scope.errorMessageEmployeeSelection='Please remove ' +employeeString + ' from another roles from '+  $scope.role.selectFormList+' form';
             $timeout(function(){
                 $scope.errorInEmployeeSelection=false;
                 console.log("should change");
             }, 3000);

         }
     }
     $scope.getRole = function(){
         $http({
             method: 'GET',
             url: 'api/RoleModels?filter=%7B%22where%22%3A%7B%22type%22%3A%20%22employee%22%7D%7D',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $("#addWorkFlow").modal("hide");
             $scope.roleList=response;

             if($scope.roleList!=undefined && $scope.roleList!=null && $scope.roleList.length>0){
                 for(var i=0;i<$scope.roleList.length;i++){
                     var employeeId=$scope.roleList[i].employees;
                     var employeeData=[];
                     if(employeeId!=undefined && employeeId!=null && employeeId.length>0){
                         for(var j=0;j<employeeId.length;j++){
                             if($scope.employeeList!=undefined && $scope.employeeList!=null && $scope.employeeList.length>0){
                                 for(var x=0;x<$scope.employeeList.length;x++){
                                     if(employeeId[j]==$scope.employeeList[x].employeeId){
                                         var showEmployee=employeeId[j]+'-'+$scope.employeeList[x].name;
                                         employeeData.push(showEmployee);
                                         break;
                                     }
                                 }
                             }
                             //console.log('data is '+JSON.stringify(employeeData));
                             $scope.roleList[i]['employeeData']=employeeData;
                         }
                     }

                 }


             }
         }).error(function (response) {
             alert(JSON.stringify(response))
             console.log('Error Response :' + JSON.stringify(response));
         });
     }


     $scope.editSelectedEmployee=[];

     $scope.editRoleButton=function(){
         $scope.loadingImage=false;
         $scope.errorInEmployeeSelection=false;
         var existingEmployee=[];
         var employeeString='';
         var statusOfemployee=true;

         if($scope.editRoleId.name.length < 30) {
            if($scope.editRoleId.selectFormList.length > 0) {
            if($scope.editRoleId.selectAccessLevel.length > 0) {
            if($scope.editRoleId.status.length > 0) {
                if ($scope.editSelectedEmployee) {
                    if ($scope.editSelectedEmployee.length > 0) {
                        if ($scope.roleList != undefined && $scope.roleList != null && $scope.roleList.length > 0) {

                            for (var x = 0; x < $scope.roleList.length; x++) {
                                //$scope.editSelectedEmployee = null;
                                if ($scope.roleList[x].selectFormList == $scope.editRoleId.selectFormList && $scope.roleList[x].id != $scope.editRoleId.id) {

                                    for (var i = 0; i < $scope.editSelectedEmployee.length; i++) {
                                        if ($scope.roleList[x].employees != undefined && $scope.roleList[x].employees != null && $scope.roleList[x].employees.length > 0) {
                                            for (var j = 0; j < $scope.roleList[x].employees.length; j++) {
                                                if ($scope.editSelectedEmployee[i].id == $scope.roleList[x].employees[j]) {
                                                    existingEmployee.push($scope.roleList[x].employees[j]);
                                                    employeeString = employeeString + $scope.roleList[x].employees[j] + ', ';
                                                    statusOfemployee = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    } else {
                        $scope.errorInEmployeeSelection = true;
                        $scope.errorMessageEmployeeSelection = 'Please select at least one employee';
                    }
                } else {
                    $scope.errorInEmployeeSelection = true;
                    $scope.errorMessageEmployeeSelection = 'Please select at least one employee';
                }
            }else{
                $scope.errorInEmployeeSelection=true;
                $scope.errorMessageEmployeeSelection='Please Select the Status';
            }
            }else{
                $scope.errorInEmployeeSelection=true;
                $scope.errorMessageEmployeeSelection='Please Select the Access Level';
            }
            }else{
                $scope.errorInEmployeeSelection=true;
                $scope.errorMessageEmployeeSelection='Please Select the Form';
            }
         }else {
             $scope.errorInEmployeeSelection = true;
             $scope.errorMessageEmployeeSelection = 'Enter Role Name in the Correct Format';
         }

         if(statusOfemployee) {
             if($scope.errorInEmployeeSelection == false){
                 $scope.editRoleId.employees = [];
                 for(var i=0; i<$scope.editSelectedEmployee.length; i++){
                     $scope.editRoleId.employees.push($scope.editSelectedEmployee[i].id);
                 }
                 delete $scope.editRoleId.employeeData;
                 $http({
                     method: 'PUT',
                     url: 'api/RoleModels/'+$scope.editRoleId.id,
                     headers: {"Content-Type": "application/json", "Accept": "application/json"},
                     data: $scope.editRoleId
                 }).success(function (response) {
                     //console.log('Response:'+JSON.stringify($scope.editRoleId));
                     $scope.getRole();
                     $("#editRole").modal("hide");
                     $("#editRoleSuccess").modal("show");
                     setTimeout(function(){$('#editRoleSuccess').modal('hide')}, 3000);
                 }).error(function(data){
                    console.log('Error:'+JSON.stringify(data));
                 });
             }
         } else{
             $scope.errorInEmployeeSelection=true;
             $scope.errorMessageEmployeeSelection='Please remove '+ employeeString +' from another roles from '+ $scope.editRoleId.selectFormList+' form';

         }


     }

     $scope.editRole = function(role) {
         $scope.loadingImage=false;
         $scope.errorInEmployeeSelection=false;
         $scope.errorMessageEmployeeSelection= '';
         $scope.editSelectedEmployee=[];
         $scope.editRoleId = angular.copy(role);
         //alert(role.employees);

         var selectedEmployess=role.employees;
         if(selectedEmployess!=undefined && selectedEmployess!=null && selectedEmployess.length>0){
             for(var i=0;i<selectedEmployess.length;i++){
                 var data={
                     id:selectedEmployess[i]
                 };
                 $scope.editSelectedEmployee.push(data);
             }
         }


         /!*     var employeeData=[];
          if(employeeId!=undefined && employeeId!=null && employeeId.length>0){
          for(var j=0;j<employeeId.length;j++){
          if($scope.employeeList!=undefined && $scope.employeeList!=null && $scope.employeeList.length>0){
          for(var x=0;x<$scope.employeeList.length;x++){
          if(employeeId[j]==$scope.employeeList[x].employeeId){
          var showEmployee=employeeId[j]+'-'+$scope.employeeList[x].name;
          employeeData.push(showEmployee);
          break;
          }
          }
          }
          console.log('data is '+JSON.stringify(employeeData));
          $scope.roleList[i]['employeeData']=employeeData;
          }
          }*!/

         $("#editRole").modal("show");
     }*/

 }]);

app.controller('createRoleController', ['userAdminstration', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(userAdminstration,$http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
     console.log("rolemodelController");

     $(document).ready(function () {
                   $('html,body').scrollTop(0);
                   $(".sidebar-menu li ul > li").removeClass('active1');
                   $('[data-toggle="tooltip"]').tooltip();
                   $(".sidebar-menu #userAdminLi").addClass("active");
                   $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .role ").addClass("active1");
                   $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .role ").addClass("menu-open");
                   $(".sidebar-menu #scheme #userAdminiProSetUp .role  ").addClass("active1").siblings('.active1').removeClass('active1');
                   $(".sidebar-menu li .collapse").removeClass("in");
               });

     var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
              		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'role'}, function (response) {
     //         		alert(JSON.stringify(response));
              		   if(!response){
              			window.location.href = "#/noAccessPage";
              		   }
              		});

     $scope.checkAll = function() {
         if ($scope.selectedAll) {
             $scope.selectedAll = true;
         } else {
             $scope.selectedAll = false;
         }
         angular.forEach($scope.formsList, function(user) {
             user.select = $scope.selectAll;
         });
     };
     $scope.fromView=function(form){
         if(form.view){
             var status=true;
             angular.forEach($scope.formsList, function(user) {
                 if(!user.view){
                     status=false;
                 }
             });
             if(status){
                 $scope.viewAll = true;
             }else{
                 $scope.viewAll = false;
             }
             //$scope.viewAll = true;
         }else{
             $scope.viewAll = false;
         }

     }
     $scope.formEdit=function(form){
         if(form.edit){
             var status=true;
             angular.forEach($scope.formsList, function(user) {
                 if(!user.edit){
                     status=false;
                 }
             });
             if(status){
                 $scope.editAll = true;
             }else{
                 $scope.editAll = false;
             }
             //$scope.viewAll = true;
         }else{
             $scope.editAll = false;
         }

     }

     $scope.checkViewAll = function() {
         if ($scope.viewAll) {
             $scope.viewAll = true;
         } else {
             $scope.viewAll = false;
         }
         if( $scope.viewAll){
             angular.forEach($scope.formsList, function(user) {
                 user.view = $scope.viewAll;
             });
         }else if(! $scope.viewAll){
             angular.forEach($scope.formsList, function(user) {
                 user.view = $scope.viewAll;
                 user.edit = $scope.viewAll;
                 $scope.editAll = $scope.viewAll;
             });
         }

     };
     $scope.checkEditAll = function() {
         if ($scope.editAll) {
             $scope.editAll = true;
         } else {
             $scope.editAll = false;
         }
         angular.forEach($scope.formsList, function(user) {
           //  user.view = $scope.editAll;
             user.edit = $scope.editAll;
          //   $scope.viewAll =  $scope.editAll;
         });
     };



     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
         .withOption("sPaginationType", "bootstrap")
         .withOption("retrieve", true)
         .withOption('stateSave', true)
         .withPaginationType('simple_numbers')
         .withOption('order', [0, 'ASC']);

     $scope.exportToExcel=function(tableId){
         if( $scope.roleList!=null &&  $scope.roleList.length>0){

             $("<tr>" +
                 "<th>Role Name</th>" +
                 "<th>Employee Id</th>" +
                 "<th>Form Name</th>" +
                 "<th>Access Level</th>" +
                 "<th>Status</th>" +
                 "</tr>").appendTo("table"+tableId);

             for(var i=0;i<$scope.roleList.length;i++){
                 var role=$scope.roleList[i];

                 $("<tr>" +
                     "<td>"+role.name+"</td>" +
                     "<td>"+role.employeeData+"</td>" +
                     "<td>"+role.selectFormList+"</td>" +
                     "<td>"+role.selectAccessLevel+"</td>" +
                     "<td>"+role.status+"</td>" +
                     "</tr>").appendTo("table"+tableId);
             }
             var exportHref = Excel.tableToExcel(tableId, 'Role Management Details ');
             $timeout(function () {
                 location.href = exportHref;
             }, 100);
         } else {
             $("#emptyDataTable").modal("show");
             setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
         }
     }

     $scope.loadingImage=true;
   /*  $scope.getRolemodel = function(){
         $http({
             method: 'GET',
             url: 'api/RoleModels',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.rolemodelList = response;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }
$scope.getRolemodel();*/
     $scope.getFormList=function(){

         $http({
             method: 'GET',
             url: 'api/FormLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.formsList = response;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }

     $scope.getFormList();


     $scope.role={}


     $scope.addRole=function()  {

         var selectFormStatus=false;
         var formDetails=[];
         angular.forEach($scope.formsList, function(user) {
             //  user.view = $scope.editAll;
             if(user.edit ){
                 selectFormStatus=true;
             }
             else if(user. view){
                 selectFormStatus=true;
             }
             var objectDetails={
                 "name": user.name,
                 "typeName": user.typeName,
                 "typeValue": user.typeValue,
                 "value": user. value,
                 "view":user. view,
                 "edit":user.edit
             }
             formDetails.push(objectDetails);
         });

         if(selectFormStatus){
                $scope.role['formDetails']=formDetails;
             console.log('details are'+JSON.stringify($scope.role));
             $http({
                 method: 'POST',
                 url: 'api/RoleModels',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": $scope.role
             }).success(function (response) {
                            $scope.response=response;
                 $('#addRoleSuccess').modal('show');
             }).error(function (response) {
                 if (response.error.details.messages.name) {
                     $scope.errorMessageData = response.error.details.messages.name[0];
                     $scope.errorMessage = true;
                 }
                 $timeout(function(){
                     $scope.errorMessage=false;
                 }, 3000);
                 console.log('Error Response :' + JSON.stringify(response));
             });
         }else{
             alert('please select any of from from list');
         }

     }
     $scope.addRoleEmployees= function () {
         $('#addRoleSuccess').modal('hide');
         location.href = '#/role';
         $window.location.reload();
     }

     $scope.editSelectedEmployee=[];

 }]);

app.controller('rolemodelEmployeesController', ['userAdminstration', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(userAdminstration, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
     console.log("rolemodelController");

      $(document).ready(function () {
              $('html,body').scrollTop(0);
              $(".sidebar-menu li ul > li").removeClass('active1');
              $('[data-toggle="tooltip"]').tooltip();
              $(".sidebar-menu #userAdminLi").addClass("active");
              $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .roleEmployee ").addClass("active1");
//              $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .roleEmployee ").addClass("menu-open");
              $(".sidebar-menu #scheme #userAdminiProSetUp .roleEmployee  ").addClass("active1").siblings('.active1').removeClass('active1');
              $(".sidebar-menu li .collapse").removeClass("in");
          });

     var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
              		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'role'}, function (response) {
     //         		alert(JSON.stringify(response));
              		   if(!response){
              			window.location.href = "#/noAccessPage";
              		   }
              		});

     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
         .withOption("sPaginationType", "bootstrap")
         .withOption("retrieve", true)
         .withOption('stateSave', true)
         .withPaginationType('simple_numbers')
         .withOption('order', [0, 'ASC']);

     $scope.exportToExcel=function(tableId){
         if( $scope.roleList!=null &&  $scope.roleList.length>0){

             $("<tr>" +
                 "<th>Role Name</th>" +
                 "<th>Employee Id</th>" +
                 "<th>Form Name</th>" +
                 "<th>Access Level</th>" +
                 "<th>Status</th>" +
                 "</tr>").appendTo("table"+tableId);

             for(var i=0;i<$scope.roleList.length;i++){
                 var role=$scope.roleList[i];

                 $("<tr>" +
                     "<td>"+role.name+"</td>" +
                     "<td>"+role.employeeData+"</td>" +
                     "<td>"+role.selectFormList+"</td>" +
                     "<td>"+role.selectAccessLevel+"</td>" +
                     "<td>"+role.status+"</td>" +
                     "</tr>").appendTo("table"+tableId);
             }
             var exportHref = Excel.tableToExcel(tableId, 'Role Management Details ');
             $timeout(function () {
                 location.href = exportHref;
             }, 100);
         } else {
             $("#emptyDataTable").modal("show");
             setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
         }
     }

     $scope.loadingImage=true;
     $scope.getRolemodel = function(){
         $http({
             method: 'GET',
             url: 'api/RoleModels',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.rolemodelList = response;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }
     $scope.getRolemodel();
     $scope.roleDetails=false

     $scope.selectRoleDetails=function(roleId){
    //alert(roleId);

         $http({
             method: 'GET',
             url: 'api/RoleEmployees?filter=%7B%22where%22%3A%7B%22roleId%22%3A%20%22'+roleId+'%22%7D%7D',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.roleDetails=true;
             $scope.rolemodelDetails = response[0];
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }

     $scope.checkAll = function() {
         if ($scope.selectedAll) {
             $scope.selectedAll = true;
         } else {
             $scope.selectedAll = false;
         }
         angular.forEach($scope.rolemodelList.formsList, function(user) {
             user.select = $scope.selectAll;
         });
     };
     $scope.fromView=function(form){
         if(form.view){
             var status=true;
             angular.forEach($scope.rolemodelList.formsList, function(user) {
                 if(!user.view){
                     status=false;
                 }
             });
             if(status){
                 $scope.viewAll = true;
             }else{
                 $scope.viewAll = false;
             }
             //$scope.viewAll = true;
         }else{
             $scope.viewAll = false;
         }

     }
     $scope.formEdit=function(form){
         if(form.edit){
             var status=true;
             angular.forEach($scope.rolemodelList.formsList, function(user) {
                 if(!user.edit){
                     status=false;
                 }
             });
             if(status){
                 $scope.editAll = true;
             }else{
                 $scope.editAll = false;
             }
             //$scope.viewAll = true;
         }else{
             $scope.editAll = false;
         }

     }

     $scope.checkViewAll = function() {
         //alert($scope.viewAll);
         if (!$scope.viewAll) {
             $scope.viewAll = false;
         } else {
             $scope.viewAll = true;
         }
         if( $scope.viewAll){
             angular.forEach($scope.rolemodelList.formsList, function(user) {
                 user.view = $scope.viewAll;
             });
         }else if(! $scope.viewAll){
             angular.forEach($scope.rolemodelList.formsList, function(user) {
                 user.view = $scope.viewAll;
                 user.edit = $scope.viewAll;
                 $scope.editAll = $scope.viewAll;
             });
         }

     };
     $scope.checkEditAll = function() {
         if ($scope.editAll) {
             $scope.editAll = true;
         } else {
             $scope.editAll = false;
         }
         angular.forEach($scope.rolemodelList.formsList, function(user) {
             //  user.view = $scope.editAll;
             user.edit = $scope.editAll;
             //   $scope.viewAll =  $scope.editAll;
         });
     };



     $scope.updateRole=function(){
        //alert('hai');

         $http({
             method: 'PUT',
             url: 'api/RoleEmployees/'+$scope.rolemodelDetails.id,
             headers: {"Content-Type": "application/json", "Accept": "application/json"},
             data: $scope.rolemodelDetails
         }).success(function (response) {
           $("#editRoleSuccess").modal("show");
             setTimeout(function(){$('#editRoleSuccess').modal('hide')}, 3000);

         }).error(function(data){
             console.log('Error:'+JSON.stringify(data));
         });

     }

 /*    $scope.selectedForms=[];
     $scope.selectModule=function(selectedModule){
         $scope.loadingImage=false;
         $scope.selectedForms=[];
         if($scope.formsList!=undefined && $scope.formsList!=null && $scope.formsList.length>0){
             for(var i=0;i<$scope.formsList.length;i++){
                 if(selectedModule==$scope.formsList[i].typeValue){
                     $scope.selectedForms.push($scope.formsList[i]);
                 }
             }
         }
     }

     $scope.getFormList=function(){

         $http({
             method: 'GET',
             url: 'api/FormLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.formsList = response;
             if($scope.formsList!=undefined && $scope.formsList!=null && $scope.formsList.length>0){
                 $scope.moduleList=[
                     {
                         "typeName": "Scheme",
                         "typeValue": "scheme"
                     }, {
                         "typeName": "Project/Ward Works",
                         "typeValue": "project"
                     }, {
                         "typeName": "Configuration Data",
                         "typeValue": "configurationData"
                     },{
                         "typeName": "User Administration",
                         "typeValue": "userAdministration"
                     },{
                         "typeName": "Legal Management",
                         "typeValue": "legalManagement"
                     },{
                         "typeName": "Land And Asset Management",
                         "typeValue": "landAndAssetManagement"
                     }
                 ]

             }

         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }

     $scope.getFormList();
     $scope.errorInEmployeeSelection=false;

     $scope.role={}
     $scope.addWorkFlowModal = function() {
         $("#addWorkFlow").modal("show");
         $scope.selectedEmployee = {};
         $scope.selectedEmployee = [];
         $scope.role = {};
     }
     $scope.addRole = function(){
         $scope.loadingImage=false;
         var existingEmployee=[];
         var employeeString='';
         var statusOfemployee=true;
         if($scope.roleList!=undefined && $scope.roleList!=null && $scope.roleList.length>0 ){
             for(var x=0;x<$scope.roleList.length;x++){
                 if($scope.roleList[x].selectFormList==$scope.role.selectFormList){
                     if($scope.selectedEmployee!=undefined && $scope.selectedEmployee!=null && $scope.selectedEmployee.length>0){
                         for(var i=0;i<$scope.selectedEmployee.length;i++){
                             if($scope.roleList[x].employees!=undefined && $scope.roleList[x].employees!=null && $scope.roleList[x].employees.length>0){
                                 for(var j=0;j<$scope.roleList[x].employees.length;j++){
                                     if($scope.selectedEmployee[i].id==$scope.roleList[x].employees[j]){
                                         existingEmployee.push($scope.roleList[x].employees[j]);
                                         employeeString=employeeString+$scope.roleList[x].employees[i]+', ';
                                        // alert(employeeString);
                                         statusOfemployee=false;
                                     }else{

                                     }
                                 }
                             }
                         }
                     }

                 }

             }

         }
         // alert(statusOfemployee);

         console.log("role response"+JSON.stringify($scope.role));
         if(statusOfemployee) {
             var employeeData = [];
             if ($scope.selectedEmployee != undefined && $scope.selectedEmployee != null && $scope.selectedEmployee.length > 0) {
                 for (var i = 0; i < $scope.selectedEmployee.length; i++) {
                     employeeData.push($scope.selectedEmployee[i].id);
                 }
             }
             $scope.role.employees = employeeData;
             if ($scope.role.name.length < 30) {
                 //if ($scope.role.selectFormList) {
                 //    if ($scope.role.employees) {

                 if ($scope.role.selectAccessLevel.length > 0) {
                     if ($scope.role.status.length > 0) {
                         if($scope.selectedEmployee != undefined && $scope.selectedEmployee != null && $scope.selectedEmployee.length > 0) {

                             $scope.role.type = 'employee';
                             //   alert(JSON.stringify($scope.role.employees));
                             $http({
                                 method: 'POST',
                                 url: 'api/RoleModels',
                                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                 "data": $scope.role
                             }).success(function (response) {
                                 $scope.loadingImage = false;
                                 /!*$scope.role = {};*!/
                                 $("#addWorkFlow").modal("hide");
                                 $("#addRoleSuccess").modal("show");
                                 setTimeout(function(){$('#addRoleSuccess').modal('hide')}, 3000);
                                 $scope.getRole();
                             }).error(function (response) {
                                 if (response.error.details.messages.name) {
                                     $scope.errorMessageData = response.error.details.messages.name[0];
                                     $scope.errorMessage = true;
                                 }
                                 $timeout(function(){
                                     $scope.errorMessage=false;
                                 }, 3000);
                                 console.log('Error Response :' + JSON.stringify(response));
                             });
                         }else{
                             $scope.errorInEmployeeSelection=true;
                             $scope.errorMessageEmployeeSelection='Please Employee Id Selection..';
                             $timeout(function(){
                                 $scope.errorInEmployeeSelection=false;
                                 console.log("should change");
                             }, 3000);
                         }
                     }
                     else{
                         $scope.errorInEmployeeSelection=true;
                         $scope.errorMessageEmployeeSelection='Please Select the Status';
                         $timeout(function(){
                             $scope.errorInEmployeeSelection=false;
                             console.log("should change");
                         }, 3000);
                     }
                 }
                 else{
                     $scope.errorInEmployeeSelection=true;
                     $scope.errorMessageEmployeeSelection='Please Select the Access Level';
                     $timeout(function(){
                         $scope.errorInEmployeeSelection=false;
                         console.log("should change");
                     }, 3000);
                 }

                 //}
                 //else{
                 //    $scope.errorInEmployeeSelection=true;
                 //    $scope.errorMessageEmployeeSelection='Please Select the Employee List';
                 //}

                 //}
                 //else{
                 //    $scope.errorInEmployeeSelection=true;
                 //    $scope.errorMessageEmployeeSelection='Please Select the Form List';
                 //}
             }
             else{
                 $scope.errorInEmployeeSelection=true;
                 $scope.errorMessageEmployeeSelection='Enter Role Name in the Correct Format';
                 $timeout(function(){
                     $scope.errorInEmployeeSelection=false;
                     console.log("should change");
                 }, 3000);

             }
         }
         else{
             $scope.errorInEmployeeSelection=true;
             $scope.errorMessageEmployeeSelection='Please remove ' +employeeString + ' from another roles from '+  $scope.role.selectFormList+' form';
             $timeout(function(){
                 $scope.errorInEmployeeSelection=false;
                 console.log("should change");
             }, 3000);

         }
     }
     $scope.getRole = function(){
         $http({
             method: 'GET',
             url: 'api/RoleModels?filter=%7B%22where%22%3A%7B%22type%22%3A%20%22employee%22%7D%7D',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $("#addWorkFlow").modal("hide");
             $scope.roleList=response;

             if($scope.roleList!=undefined && $scope.roleList!=null && $scope.roleList.length>0){
                 for(var i=0;i<$scope.roleList.length;i++){
                     var employeeId=$scope.roleList[i].employees;
                     var employeeData=[];
                     if(employeeId!=undefined && employeeId!=null && employeeId.length>0){
                         for(var j=0;j<employeeId.length;j++){
                             if($scope.employeeList!=undefined && $scope.employeeList!=null && $scope.employeeList.length>0){
                                 for(var x=0;x<$scope.employeeList.length;x++){
                                     if(employeeId[j]==$scope.employeeList[x].employeeId){
                                         var showEmployee=employeeId[j]+'-'+$scope.employeeList[x].name;
                                         employeeData.push(showEmployee);
                                         break;
                                     }
                                 }
                             }
                             //console.log('data is '+JSON.stringify(employeeData));
                             $scope.roleList[i]['employeeData']=employeeData;
                         }
                     }

                 }


             }
         }).error(function (response) {
             alert(JSON.stringify(response))
             console.log('Error Response :' + JSON.stringify(response));
         });
     }


     $scope.editSelectedEmployee=[];

     $scope.editRoleButton=function(){
         $scope.loadingImage=false;
         $scope.errorInEmployeeSelection=false;
         var existingEmployee=[];
         var employeeString='';
         var statusOfemployee=true;

         if($scope.editRoleId.name.length < 30) {
            if($scope.editRoleId.selectFormList.length > 0) {
            if($scope.editRoleId.selectAccessLevel.length > 0) {
            if($scope.editRoleId.status.length > 0) {
                if ($scope.editSelectedEmployee) {
                    if ($scope.editSelectedEmployee.length > 0) {
                        if ($scope.roleList != undefined && $scope.roleList != null && $scope.roleList.length > 0) {

                            for (var x = 0; x < $scope.roleList.length; x++) {
                                //$scope.editSelectedEmployee = null;
                                if ($scope.roleList[x].selectFormList == $scope.editRoleId.selectFormList && $scope.roleList[x].id != $scope.editRoleId.id) {

                                    for (var i = 0; i < $scope.editSelectedEmployee.length; i++) {
                                        if ($scope.roleList[x].employees != undefined && $scope.roleList[x].employees != null && $scope.roleList[x].employees.length > 0) {
                                            for (var j = 0; j < $scope.roleList[x].employees.length; j++) {
                                                if ($scope.editSelectedEmployee[i].id == $scope.roleList[x].employees[j]) {
                                                    existingEmployee.push($scope.roleList[x].employees[j]);
                                                    employeeString = employeeString + $scope.roleList[x].employees[j] + ', ';
                                                    statusOfemployee = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    } else {
                        $scope.errorInEmployeeSelection = true;
                        $scope.errorMessageEmployeeSelection = 'Please select at least one employee';
                    }
                } else {
                    $scope.errorInEmployeeSelection = true;
                    $scope.errorMessageEmployeeSelection = 'Please select at least one employee';
                }
            }else{
                $scope.errorInEmployeeSelection=true;
                $scope.errorMessageEmployeeSelection='Please Select the Status';
            }
            }else{
                $scope.errorInEmployeeSelection=true;
                $scope.errorMessageEmployeeSelection='Please Select the Access Level';
            }
            }else{
                $scope.errorInEmployeeSelection=true;
                $scope.errorMessageEmployeeSelection='Please Select the Form';
            }
         }else {
             $scope.errorInEmployeeSelection = true;
             $scope.errorMessageEmployeeSelection = 'Enter Role Name in the Correct Format';
         }

         if(statusOfemployee) {
             if($scope.errorInEmployeeSelection == false){
                 $scope.editRoleId.employees = [];
                 for(var i=0; i<$scope.editSelectedEmployee.length; i++){
                     $scope.editRoleId.employees.push($scope.editSelectedEmployee[i].id);
                 }
                 delete $scope.editRoleId.employeeData;
                 $http({
                     method: 'PUT',
                     url: 'api/RoleModels/'+$scope.editRoleId.id,
                     headers: {"Content-Type": "application/json", "Accept": "application/json"},
                     data: $scope.editRoleId
                 }).success(function (response) {
                     //console.log('Response:'+JSON.stringify($scope.editRoleId));
                     $scope.getRole();
                     $("#editRole").modal("hide");
                     $("#editRoleSuccess").modal("show");
                     setTimeout(function(){$('#editRoleSuccess').modal('hide')}, 3000);
                 }).error(function(data){
                    console.log('Error:'+JSON.stringify(data));
                 });
             }
         } else{
             $scope.errorInEmployeeSelection=true;
             $scope.errorMessageEmployeeSelection='Please remove '+ employeeString +' from another roles from '+ $scope.editRoleId.selectFormList+' form';

         }


     }

     $scope.editRole = function(role) {
         $scope.loadingImage=false;
         $scope.errorInEmployeeSelection=false;
         $scope.errorMessageEmployeeSelection= '';
         $scope.editSelectedEmployee=[];
         $scope.editRoleId = angular.copy(role);
         //alert(role.employees);

         var selectedEmployess=role.employees;
         if(selectedEmployess!=undefined && selectedEmployess!=null && selectedEmployess.length>0){
             for(var i=0;i<selectedEmployess.length;i++){
                 var data={
                     id:selectedEmployess[i]
                 };
                 $scope.editSelectedEmployee.push(data);
             }
         }


         /!*     var employeeData=[];
          if(employeeId!=undefined && employeeId!=null && employeeId.length>0){
          for(var j=0;j<employeeId.length;j++){
          if($scope.employeeList!=undefined && $scope.employeeList!=null && $scope.employeeList.length>0){
          for(var x=0;x<$scope.employeeList.length;x++){
          if(employeeId[j]==$scope.employeeList[x].employeeId){
          var showEmployee=employeeId[j]+'-'+$scope.employeeList[x].name;
          employeeData.push(showEmployee);
          break;
          }
          }
          }
          console.log('data is '+JSON.stringify(employeeData));
          $scope.roleList[i]['employeeData']=employeeData;
          }
          }*!/

         $("#editRole").modal("show");
     }*/

 }]);

app.controller('createRoleEmployeesController', ['userAdminstration', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(userAdminstration,$http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
     console.log("rolemodelController...........");
// debugger;
$(document).ready(function () {
              $('html,body').scrollTop(0);
              $(".sidebar-menu li ul > li").removeClass('active1');
              $('[data-toggle="tooltip"]').tooltip();
              $(".sidebar-menu #userAdminLi").addClass("active");
              $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .roleEmployee ").addClass("active");
//              $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .roleEmployee ").addClass("menu-open");
              $(".sidebar-menu #scheme #userAdminiProSetUp .roleEmployee  ").addClass("active1").siblings('.active1').removeClass('active1');
              $(".sidebar-menu li .collapse").removeClass("in");
          });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         		userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'role'}, function (response) {
//         		alert(JSON.stringify(response));
         		   if(!response){
         			window.location.href = "#/noAccessPage";
         		   }
         		});

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #userAdmini li.role").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #userAdminLi.treeview").addClass("active");
         $(".sidebar-menu #userAdmini li.role .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.checkAll = function() {
         if ($scope.selectedAll) {
             $scope.selectedAll = true;
         } else {
             $scope.selectedAll = false;
         }
         angular.forEach($scope.formsList, function(user) {
             user.select = $scope.selectAll;
         });
     };
     $scope.fromView=function(form){
         if(form.view){
             var status=true;
             angular.forEach($scope.formsList, function(user) {
                 if(!user.view){
                     status=false;
                 }
             });
             if(status){
                 $scope.viewAll = true;
             }else{
                 $scope.viewAll = false;
             }
             //$scope.viewAll = true;
         }else{
             $scope.viewAll = false;
         }

     }
     $scope.formEdit=function(form){
         if(form.edit){
             var status=true;
             angular.forEach($scope.formsList, function(user) {
                 if(!user.edit){
                     status=false;
                 }
             });
             if(status){
                 $scope.editAll = true;
             }else{
                 $scope.editAll = false;
             }
             //$scope.viewAll = true;
         }else{
             $scope.editAll = false;
         }

     }

     $scope.checkViewAll = function() {
         if ($scope.viewAll) {
             $scope.viewAll = true;
         } else {
             $scope.viewAll = false;
         }
         if( $scope.viewAll){
             angular.forEach($scope.formsList, function(user) {
                 user.view = $scope.viewAll;
             });
         }else if(! $scope.viewAll){
             angular.forEach($scope.formsList, function(user) {
                 user.view = $scope.viewAll;
                 user.edit = $scope.viewAll;
                 $scope.editAll = $scope.viewAll;
             });
         }

     };
     $scope.checkEditAll = function() {
         if ($scope.editAll) {
             $scope.editAll = true;
         } else {
             $scope.editAll = false;
         }
         angular.forEach($scope.formsList, function(user) {
           //  user.view = $scope.editAll;
             user.edit = $scope.editAll;
          //   $scope.viewAll =  $scope.editAll;
         });
     };



     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
         .withOption("sPaginationType", "bootstrap")
         .withOption("retrieve", true)
         .withOption('stateSave', true)
         .withPaginationType('simple_numbers')
         .withOption('order', [0, 'ASC']);


     $scope.loadingImage=true;
     $scope.getRolemodel = function(){
         $http({
             method: 'GET',
             url: 'api/RoleModels',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.rolemodelList = response;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }
        $scope.getRolemodel();
     $scope.getFormList=function(){

         $http({
             method: 'GET',
             url: 'api//Employees?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             $scope.formsList = response;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }

     $scope.getFormList();


     $scope.role={}


     $scope.addEmployeesToRole=function()  {

        if($scope.role.roleId != '' && $scope.role.roleId != undefined && $scope.role.roleId != null) {
        if($scope.role.status != '' && $scope.role.status != undefined && $scope.role.status != null) {



         console.log(JSON.stringify($scope.role));
         var selectFormStatus=false;
         var formDetails=[];
         angular.forEach($scope.formsList, function(user) {
             //  user.view = $scope.editAll;
             if(user.edit ){
                 selectFormStatus=true;
             }
             else if(user. view){
                 selectFormStatus=true;
             }
             var objectDetails={
                 "name": user.name,
                 "employeeId": user.employeeId,
                 "department": user.department,
                 "view":user. view
             }
             formDetails.push(objectDetails);
         });

         if(selectFormStatus){
                $scope.role['formDetails']=formDetails;
             console.log('details are'+JSON.stringify($scope.role));
             $http({
                 method: 'POST',
                 url: 'api/RoleEmployees',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": $scope.role
             }).success(function (response) {
                    console.log("response"+ JSON.stringify(response))
                 $('#addRoleSuccess').modal('show');


             }).error(function (response) {
                 if (response.error.details.messages.name) {
                     $scope.errorMessageData = response.error.details.messages.name[0];
                     $scope.errorMessage = true;
                 }
                 $timeout(function(){
                     $scope.errorMessage=false;
                 }, 3000);
                 console.log('Error Response :' + JSON.stringify(response));
             });
         }else{
//             alert('please select any of from from list');
               $scope.errorMessage = true;
                $scope.showError = "Please Select any of from from list";
                $timeout(function () {
                    $scope.errorMessage = false;
                }, 3000);
         }
         } else {
//                              alert("$scope.role.Status")
               $scope.errorMessage = true;
                $scope.showError = "Please Select Status";
                $timeout(function () {
                    $scope.errorMessage = false;
                }, 3000);
         }
         } else {
//                     alert("$scope.role.roleId")
               $scope.errorMessage = true;
                $scope.showError = "Please Select Role";
                $timeout(function () {
                    $scope.errorMessage = false;
                }, 3000);
         }

     }
     $scope.addRoleEmployees= function () {
         $('#addRoleSuccess').modal('hide');
         location.href = '#/roleEmployees';
         $window.location.reload();
     }

     $scope.editSelectedEmployee=[];

 }]);

 /***** User Management ****/
app.controller('workFlowManagementController', function($http, $scope, $window, $location, $rootScope) {
    console.log("workFlowManagementController");


    $scope.dtOptions = { paging: false, searching: false };

    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function() {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top},
                'slow');
        });
        $("#cancel").click(function() {
            $("#scroll2").hide()
            $("#scroll").show();;
        });
    })


    $scope.addTask = function() {
        $scope.addTask = false;
        /*$scope.groups = [
         {
         subTaskName: "Task - ",
         content: "",
         no: 1
         }
         ];*/
        $scope.groups=[];

        var details={
        }
        $scope.groups.push(details);
        $scope.addGroup = function(idx, e) {
            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }
            var newGroup = angular.copy(details);
            newGroup.no = $scope.groups.length + 1;
            $scope.groups.splice(idx + 1, 0, newGroup);
        };
        $scope.removeGroup = function(idx, e) {
            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }
            $scope.groups.splice(idx, 1);
        };
    }

});

app.controller('contractorsListController', ['userAdminstration', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(userAdminstration, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("contractorsListController");

 $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #userAdminLi").addClass("active");
         $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .contractorsList ").addClass("active1");
         $(".sidebar-menu #userAdminLi.treeview #userAdminiProSetUp .contractorsList ").addClass("menu-open");
         $(".sidebar-menu #scheme #userAdminiProSetUp .contractorsList  ").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu li .collapse").removeClass("in");
     });


               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                          userAdminstration.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'contractorsList'}, function (response) {
//                          alert(JSON.stringify(response));
                                if(!response){
                                 window.location.href = "#/noAccessPage";
                                }
                             });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function() {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top},
                'slow');
        });
        $("#cancel").click(function() {
            $("#scroll2").hide()
            $("#scroll").show();;
        });
    })

    $scope.getContractors = function () {

        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

        $http({
            method: 'GET',
            url: 'api/contractors/getDetails?employeeId=%7B%22employeeId%22%3A%22'+loginPersonDetails.employeeId +'%22%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('Citizens Response :' + JSON.stringify(response));
            $scope.contractorList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getContractors();
    $scope.comments;
    $scope.approvalModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#approavlReport").modal("show");
    }
    $scope.rejectModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#rejectReport").modal("show");
    };
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;

    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/contractors/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {

                $scope.getContractors();
                approvalDoubleClick=false;
                $("#approavlReport").modal("hide");


            }).error(function (response, data) {
                console.log("failure");
                approvalDoubleClick=false;
            })

        }



    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/contractors/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                $scope.getContractors();
                $("#rejectReport").modal("hide");
                rejectDoubleClick=false;

            }).error(function (response, data) {
                console.log("failure");
                rejectDoubleClick=false;
            })
        }


    }


}]);
/***** User Management ****/

function checkPhoneNumber(checkData){
    var phoneNumber =/^[7-9]{1}[0-9]{9}$/;
    if(checkData.match(phoneNumber)){
        return true;
    }
    else{
        return false;
    }
}