var HearingURL = 'http://54.149.172.244:3003/admin/#/hearingDetails/';
var HolidayURL = 'http://54.149.172.244:3003/admin/#/holidays/';

var uploadFileURL ='http://54.149.172.244:3004/api/Uploads/dhanbadDb/download/';

angular.module('ui.tinymce', ['ui.calendar'])
    .value('uiTinymceConfig', {})
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
        uiTinymceConfig = uiTinymceConfig || {};
        var generatedIds = 0;
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ngModel) {
                var expression, options, tinyInstance;
                // generate an ID if not present
                if (!attrs.id) {
                    attrs.$set('id', 'uiTinymce' + generatedIds++);
                }
                options = {
                    // Update model when calling setContent (such as from the source editor popup)
                    setup: function(ed) {
                        ed.on('init', function(args) {
                            ngModel.$render();
                        });
                        // Update model on button click
                        ed.on('ExecCommand', function(e) {
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                        // Update model on keypress
                        ed.on('KeyUp', function(e) {
//                            console.log(ed.isDirty());
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                    },
                    mode: 'exact',
                    elements: attrs.id,
                };
                if (attrs.uiTinymce) {
                    expression = scope.$eval(attrs.uiTinymce);
                } else {
                    expression = {};
                }
                angular.extend(options, uiTinymceConfig, expression);
                setTimeout(function() {
                    tinymce.init(options);
                });
                ngModel.$render = function() {
                    if (!tinyInstance) {
                        tinyInstance = tinymce.get(attrs.id);
                    }
                    if (tinyInstance) {
                        tinyInstance.setContent(ngModel.$viewValue || '');
                    }
                };
            }
        };
    }]);

var app = angular.module('legalMgmt', ['ngRoute', 'ui.calendar', 'ngCalendar', 'ngFileUpload', 'datatables', 'angularjs-dropdown-multiselect', 'servicesDetails', 'ui.tinymce', 'datatables.bootstrap']);

app.config(function ($routeProvider) {
    $routeProvider

        .when('/homeBirth', {
            templateUrl: './homeBD.html',
            controller: 'homeBDController'
        }).when('/birthCertificates', {
            templateUrl: './birthCertificates.html',
            controller: 'birthCertificatesController'
        }).when('/deathCertificates', {
            templateUrl: './deathCertificates.html',
            controller: 'deathCertificatesController'
        }).when('/legalMngmt', {
            templateUrl: './legalMngmt.html',
            controller: 'legalMngmtController'
        }).when('/caseType', {
        templateUrl: './caseType.html',
        controller: 'caseTypeController'
    }).when('/master', {
        templateUrl: './masterData.html',
        controller: 'masterController'
    }).when('/advocates', {
        templateUrl: './advocates.html',
        controller: 'advocatesController'
    }).when('/status', {
        templateUrl: './status.html',
        controller: 'statusController'
    }).when('/priority', {
        templateUrl: './priority.html',
        controller: 'priorityController'
    }).when('/acts', {
        templateUrl: './acts.html',
        controller: 'actsController'
    }).when('/stakeHolders', {
        templateUrl: './stakeHolders.html',
        controller: 'stakeHoldersController'
    }).when('/createCase', {
        templateUrl: './createCase.html',
        controller: 'createCaseController'
    }).when('/advocatePayment', {
        templateUrl: './advocatePayment.html',
        controller: 'advocatePaymentController'
    }).when('/hearingDate', {
        templateUrl: './hearingDate.html',
        controller: 'hearingDateController'
    }).when('/caseStakeHolders', {
        templateUrl: './caseStakeHolders.html',
        controller: 'caseStakeHoldersController'
    }).when('/noticeComments', {
        templateUrl: './noticeComments.html',
        controller: 'noticeCommentsController'
    }).when('/case', {
        templateUrl: './case.html',
        controller: 'caseController'
    }).when('/editCase', {
        templateUrl: './editCase.html',
        controller: 'editCaseController'
    }).when('/caseDetails', {
        templateUrl: './caseDetails.html',
        controller: 'caseDetailsController'
    }).when('/advocatePaymentDetails', {
        templateUrl: './advocatePaymentDetails.html',
        controller: 'advocatePaymentDetailsController'
    }).when('/noticeDetails', {
        templateUrl: './noticeDetails.html',
        controller: 'noticeDetailsController'
    }).when('/createPayments', {
        templateUrl: './createPayments.html',
        controller: 'createPaymentsController'
    }).when('/hearingDetails', {
        templateUrl: './hearingDetails.html',
        controller: 'hearingDetailsController'
    }).when('/caseUpload', {
        templateUrl: './caseUpload.html',
        controller: 'caseUploadController'
    }).when('/stakeHolders', {
        templateUrl: './stakeHolders.html',
        controller: 'stakeHoldersController'
    }).when('/writePetition', {
        templateUrl: './writePetition.html',
        controller: 'writePetitionController'
    }).when('/editWritePetition', {
        templateUrl: './editWritePetition.html',
        controller: 'editWritePetitionController'
    }).when('/court', {
        templateUrl: './court.html',
        controller: 'courtController'
    }).when('/createNotice', {
        templateUrl: './createNotice.html',
        controller: 'createNoticeController'
    }).when('/caseDepartment', {
        templateUrl: './caseDepartment.html',
        controller: 'caseDepartmentController'
    }).when('/paymentMode', {
        templateUrl: './paymentMode.html',
        controller: 'paymentModeController'
    }).when('/calendarDockets', {
        templateUrl: './calendar.html',
        controller: 'calendarController'
    }).when('/caseReports', {
        templateUrl: './caseReports.html',
        controller: 'caseReportsController'
    }).when('/createWritePetition', {
        templateUrl: './createWritePetition.html',
        controller: 'createWritePetitionController'
    }).when('/petitionDetails', {
        templateUrl: './petitionDetails.html',
        controller: 'petitionDetailsController'
    }).when('/judgement', {
        templateUrl: './judgement.html',
        controller: 'judgementController'
    }).when('/empanelGroup', {
        templateUrl: './empannelGroup.html',
        controller: 'empannelGroupController'
    }).when('/caseHistory', {
        templateUrl: './caseHistory.html',
        controller: 'caseHistoryController'
    }).when('/hearingCaseDetails', {
        templateUrl: './hearingCaseDetails.html',
        controller: 'hearingCaseDetailsController'
    }).when('/legalEmailTemplate',{
        templateUrl: './legalEmailTemplate.html',
        controller: 'legalEmailTemplateController'

    }).when('/hearingDetails/:id', {
        templateUrl: './hearingDetails.html',
        controller: 'hearingDetailsController'
    }).when('/createHearingDate', {
        templateUrl: './createHearingDate.html',
        controller: 'createHearingDateController'
    }).when('/editHearingDate', {
        templateUrl: './editHearingDate.html',
        controller: 'editHearingDateController'
    }).when('/holidays', {
        templateUrl: './holidays.html',
        controller: 'holidaysController'
    });

});


app.controller('indexController', function ($scope, $rootScope, $window, $location, $http) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
    });
    $rootScope.userLogin = false;

    $scope.reloadRoute = function() {
        $window.location.reload();
    }
    $scope.userName = $window.localStorage.getItem('userName');

    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    if ($scope.userInfo) {
        $rootScope.userLogin = true;
    }

    var roleLogin = JSON.parse($window.localStorage.getItem('roleList'));
    $rootScope.schemeListViewTab = true;
    $rootScope.schemeRequestViewTab = true;
    $rootScope.schemeListView = true;
    $rootScope.schemeListEdit = true;
    $rootScope.schemeRequestViewTab = true;
    $rootScope.schemeRequestView = true;
    $rootScope.schemeRequestEdit = true;
    if (roleLogin) {
        $rootScope.schemeListViewTab = false;
        $rootScope.schemeRequestViewTab = false;
        $rootScope.schemeListView = false;
        $rootScope.schemeListEdit = false;
        if (roleLogin != undefined && roleLogin != null && roleLogin.length > 0) {
            for (var i = 0; i < roleLogin.length; i++) {
                if (roleLogin[i].selectFormList == 'schemeManagement') {
                    $rootScope.schemeListViewTab = true;
                    if (roleLogin[i].selectAccessLevel == 'edit') {
                        $rootScope.schemeListView = true;
                        $rootScope.schemeListEdit = true;
                    }
                    else if (roleLogin[i].selectAccessLevel == 'view') {
                        $rootScope.schemeListView = true;
                    }
                    else if (roleLogin[i].selectAccessLevel == 'all') {
                        $rootScope.schemeListView = true;
                        $rootScope.schemeListEdit = true;
                    }
                } else if (roleLogin[i].selectFormList == 'schemeRequests') {
                    $rootScope.schemeRequestViewTab = true;
                    if (roleLogin[i].selectAccessLevel == 'edit') {
                        $rootScope.schemeRequestView = true;
                        $rootScope.schemeRequestEdit = true;
                    }
                    else if (roleLogin[i].selectAccessLevel == 'view') {
                        $rootScope.schemeListView = true;
                    }
                    else if (roleLogin[i].selectAccessLevel == 'all') {
                        $rootScope.schemeRequestView = true;
                        $rootScope.schemeRequestEdit = true;
                    }
                }
            }
        }

    }

    $scope.logout = function () {
        $window.localStorage.removeItem("userDetails");
        $window.localStorage.removeItem("accessToken");
        $window.localStorage.removeItem("afterLogin");
        $window.localStorage.removeItem("userName");
        $window.location.reload();
        window.location.href = "/admin/login.html";
    };

});

app.controller('homeBDController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$timeout', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope, $timeout, Excel, DTOptionsBuilder, DTColumnBuilder) {



    $scope.selectYear = function (years) {


        // alert("helook,ko,ok"+years)
        if (years == "2015") {
            Highcharts.chart('birth', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Birth and Death Bar Chart'
                },
                xAxis: {
                    categories: [
                        'Coimbatore',
                        'Madurai',
                        'Salem',
                        'Thanjavur',
                        'Tiruchirapalli',
                        'Vellore',
                        'Tirunelveli',
                        'Tiruppur',
                        'Thoothukudi',
                        'Erode'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Birth & Death'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {

                                    //  alert ('Category: '+ this.series.name +', value: '+ this.y + this.category + this.series);
                                    if (this.series.name === "Birth") {
                                        //  alert("test");
                                        if (this.category === "Coimbatore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Madurai") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 17.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Salem") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 16.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 15.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thanjavur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 16.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruchirapalli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 13.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 17.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Vellore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tirunelveli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 19.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 15.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruppur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 15.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 13.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thoothukudi") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Birth',
                                                        y: 17.84
                                                    }, {
                                                        name: 'Death',
                                                        y: 9.85
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Erode") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Birth',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Death',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        }
                                        // end of if
                                    } else if (this.series.name === "Death") {
                                        //  alert("test");
                                        if (this.category === "Coimbatore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 14.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Madurai") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 10.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Salem") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thanjavur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruchirapalli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 13.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.88
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Vellore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 15.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tirunelveli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruppur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 44.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 11.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thoothukudi") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Erode") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        }
                                    }
                                    //end of else if
                                }
                            }
                        }
                    },
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{


                    name: 'Birth',
                    data: [20000, 26000, 22500, 25300, 31500, 28000, 24600, 24600, 22600, 27000]

                }, {
                    name: 'Death',
                    data: [16000, 21000, 19500, 22500, 28500, 26709, 22000, 22000, 19000, 22000]

                }]
            });
        }
        if (years == "2016") {
            Highcharts.chart('birth', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Birth and Death Bar Chart'
                },
                xAxis: {
                    categories: [
                        'Coimbatore',
                        'Madurai',
                        'Salem',
                        'Thanjavur',
                        'Tiruchirapalli',
                        'Vellore',
                        'Tirunelveli',
                        'Tiruppur',
                        'Thoothukudi',
                        'Erode'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Birth & Death'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    if (this.series.name === "Birth") {
                                        //  alert("test");
                                        if (this.category === "Coimbatore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Madurai") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 17.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Salem") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 16.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 15.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thanjavur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 16.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruchirapalli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 13.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 17.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Vellore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tirunelveli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 19.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 15.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruppur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 15.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 13.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thoothukudi") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Birth',
                                                        y: 17.84
                                                    }, {
                                                        name: 'Death',
                                                        y: 9.85
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Erode") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Birth',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Death',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        }
                                        // end of if
                                    } else if (this.series.name === "Death") {
                                        // alert("test");
                                        if (this.category === "Coimbatore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 14.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Madurai") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 10.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Salem") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thanjavur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruchirapalli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 13.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.88
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Vellore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 15.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tirunelveli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruppur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 44.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 11.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thoothukudi") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Erode") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        }
                                    }
                                    //end of else if
                                }
                            }
                        }
                    },
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Birth',
                    data: [28000, 26000, 22500, 31500, 28000, 24600, 24600, 22600, 22700, 28000]

                }, {
                    name: 'Death',
                    data: [22000, 21000, 22500, 28500, 26709, 22000, 22000, 21000, 20000, 25000]

                }]
            });
        }
        if (years == "2017") {
            Highcharts.chart('birth', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Birth and Death Bar Chart'
                },
                xAxis: {
                    categories: [
                        'Coimbatore',
                        'Madurai',
                        'Salem',
                        'Thanjavur',
                        'Tiruchirapalli',
                        'Vellore',
                        'Tirunelveli',
                        'Tiruppur',
                        'Thoothukudi',
                        'Erode'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Birth & Death'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    //debugger
                                    //  alert ('Category: '+ this.series.name +', value: '+ this.y + this.category + this.series);
                                    if (this.series.name === "Birth") {
                                        //  alert("test");
                                        if (this.category === "Coimbatore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Madurai") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 17.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Salem") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 16.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 15.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thanjavur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 16.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruchirapalli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 13.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 17.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Vellore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tirunelveli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 19.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 15.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruppur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 15.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 13.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thoothukudi") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Birth',
                                                        y: 17.84
                                                    }, {
                                                        name: 'Death',
                                                        y: 9.85
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Erode") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Birth',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Birth',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Death',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        }
                                        // end of if
                                    } else if (this.series.name === "Death") {
                                        // alert("test");
                                        if (this.category === "Coimbatore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 14.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Madurai") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 10.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Salem") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thanjavur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruchirapalli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 13.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 14.88
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Vellore") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 15.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 16.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tirunelveli") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 11.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Tiruppur") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 44.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 11.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Thoothukudi") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        } else if (this.category === "Erode") {
                                            Highcharts.chart('birthPie', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                            style: {
                                                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Death',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Male',
                                                        y: 12.84
                                                    }, {
                                                        name: 'Female',
                                                        y: 19.77
                                                    }]
                                                }]
                                            });
                                        }
                                    }
                                    //end of else if
                                }
                            }
                        }
                    },
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Birth',
                    data: [25000, 27000, 21500, 25300, 39500, 29000, 26600, 24800, 28800, 29000]

                }, {
                    name: 'Death',
                    data: [22000, 28000, 15500, 22500, 20500, 28709, 21000, 29000, 17000, 21100]

                }]
            });
        }
    }
    $scope.selectYear("2015");





    Highcharts.chart('birth', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Birth and Death Bar Chart'
        },
        xAxis: {
            categories: [
                'Coimbatore',
                'Madurai',
                'Salem',
                'Thanjavur',
                'Tiruchirapalli',
                'Vellore',
                'Tirunelveli',
                'Tiruppur',
                'Thoothukudi',
                'Erode'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Birth & Death'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            // alert ('Category: '+ this.series.name +', value: '+ this.y + this.category + this.series);
                            if (this.series.name === "Birth" && $scope.options === "2015") {
                                // alert("test");
                                if (this.category === "Coimbatore") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 12.84
                                            }, {
                                                name: 'Female',
                                                y: 16.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Madurai") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 17.84
                                            }, {
                                                name: 'Female',
                                                y: 16.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Salem") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 16.84
                                            }, {
                                                name: 'Female',
                                                y: 15.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Thanjavur") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 16.84
                                            }, {
                                                name: 'Female',
                                                y: 14.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Tiruchirapalli") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 13.84
                                            }, {
                                                name: 'Female',
                                                y: 17.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Vellore") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 11.84
                                            }, {
                                                name: 'Female',
                                                y: 19.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Tirunelveli") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 19.84
                                            }, {
                                                name: 'Female',
                                                y: 15.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Tiruppur") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 15.84
                                            }, {
                                                name: 'Female',
                                                y: 13.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Thoothukudi") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Birth',
                                                y: 17.84
                                            }, {
                                                name: 'Death',
                                                y: 9.85
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Erode") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Birth',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Birth',
                                                y: 12.84
                                            }, {
                                                name: 'Death',
                                                y: 19.77
                                            }]
                                        }]
                                    });
                                }
                                // end of if
                            } else if (this.series.name === "Death") {
                                //alert("test");
                                if (this.category === "Coimbatore") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 14.84
                                            }, {
                                                name: 'Female',
                                                y: 16.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Madurai") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 11.84
                                            }, {
                                                name: 'Female',
                                                y: 10.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Salem") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 11.84
                                            }, {
                                                name: 'Female',
                                                y: 14.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Thanjavur") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 12.84
                                            }, {
                                                name: 'Female',
                                                y: 19.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Tiruchirapalli") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 13.84
                                            }, {
                                                name: 'Female',
                                                y: 14.88
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Vellore") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 15.84
                                            }, {
                                                name: 'Female',
                                                y: 16.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Tirunelveli") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 11.84
                                            }, {
                                                name: 'Female',
                                                y: 19.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Tiruppur") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 44.84
                                            }, {
                                                name: 'Female',
                                                y: 11.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Thoothukudi") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 12.84
                                            }, {
                                                name: 'Female',
                                                y: 19.77
                                            }]
                                        }]
                                    });
                                } else if (this.category === "Erode") {
                                    Highcharts.chart('birthPie', {
                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: null,
                                            plotShadow: false,
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',
                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                }
                                            }
                                        },
                                        series: [{
                                            name: 'Death',
                                            colorByPoint: true,
                                            data: [{
                                                name: 'Male',
                                                y: 12.84
                                            }, {
                                                name: 'Female',
                                                y: 19.77
                                            }]
                                        }]
                                    });
                                }
                            }
                            //end of else if
                        }
                    }
                }
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Birth',
            data: [28000, 26000, 22500, 25300, 31500, 28000, 24600, 24600, 22600, 27000]

        }, {
            name: 'Death',
            data: [22000, 21000, 19500, 22500, 28500, 26709, 22000, 22000, 19000, 22000]

        }]
    });

    


    // $scope.getDeathinfoData = function () {
    //     $http({
    //         "method": "GET",
    //         "url": 'http://54.189.195.233:3000/api/DeathCertificates',
    //         "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    //     }).success(function (response) {
    //         $scope.loadingImage=false;
    //         $rootScope.deathData = response;
    //     }).error(function (response, data) {
    //     });
    // };

    // $scope.getDeathinfoData();

    // $scope.deathMsg = function(data){
    //     alert('Death Certificate Generated for ' + JSON.stringify($rootScope.deathData[data].name));
    // }

}]);

// home BD Controller end

//birth and death controllers

// birth certificates controller
app.controller('birthCertificatesController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$timeout', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope, $timeout, Excel, DTOptionsBuilder, DTColumnBuilder) {
   
    $scope.getBirthinfoData = function () {
        $http({
            "method": "GET",
            "url": 'http://54.189.195.233:3000/api/BirthCertificates',
            "headers": { "Content-Type": "application/json", "Accept": "application/json" }
        }).success(function (response) {
            $scope.loadingImage = false;
            $rootScope.birthData = response;
        }).error(function (response, data) {
        });
    };

    $scope.getBirthinfoData();


    $scope.birthMsg = function (data) {
        alert('Birth Certificate Generated for ' + JSON.stringify($rootScope.birthData[data].name));
    }



}]);


// birth certificates controller end




// death certificates controller
app.controller('deathCertificatesController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$timeout', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope, $timeout, Excel, DTOptionsBuilder, DTColumnBuilder) {

    $scope.getDeathinfoData = function () {
        $http({
            "method": "GET",
            "url": 'http://54.189.195.233:3000/api/DeathCertificates',
            "headers": { "Content-Type": "application/json", "Accept": "application/json" }
        }).success(function (response) {
            $scope.loadingImage = false;
            $rootScope.deathData = response;
        }).error(function (response, data) {
        });
    };

    $scope.getDeathinfoData();

    $scope.deathMsg = function (data) {
        alert('Death Certificate Generated for ' + JSON.stringify($rootScope.deathData[data].name));
    }

}]);
// death certificates controller end





//******************************CaseType Controller************************************
app.controller('caseTypeController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout, Excel, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.caseType").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.caseType .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'caseType'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.loadingImage=true;
    $scope.getCaseTypes = function () {
        $http({
            "method": "GET",
            "url": 'api/CaseTypes',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.caseTypeData = response;
        }).error(function (response, data) {
//            console.log("failure");
        });
    };
    $scope.getCaseTypes();
    $scope.reset = function () {
        $scope.case = angular.copy($scope.master);
        $scope.caseTYpeCreationError = false;
        $scope.createCaseTypeError = '';

    };

    $scope.caseTypeClick=function(){
        $('#CreateCaseTypeModal').modal('show');
        $scope.case = {};
        $scope.caseTYpeCreationError = false;
        $scope.createCaseTypeError = '';

        $scope.caseTYpeUpdationError = false;
        $scope.updateCaseTypeError = '';
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.case = {};
    $scope.caseTypeCheck=true;
    $scope.createCaseType = function () {

        $scope.caseTYpeCreationError = false;
        $scope.createCaseTypeError = '';

        if(  $scope.caseTypeCheck==true) {
            $scope.caseTypeCheck=false;
            if ($scope.case.caseType.match(alphabetsWithSpaces) && $scope.case.caseType) {

                $http({
                    method: 'POST',
                    url: 'api/CaseTypes',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.case
                }).success(function (response) {
                    $scope.caseTypeCheck=true;
                    $scope.loadingImage=false;
                    $rootScope.caseTypeData.push(response);
                    $('#CreateCaseTypeModal').modal('hide');
                    $("#addSuccess").modal("show");
                    setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                    $scope.getCaseTypes();
                }).error(function (response) {
                    if (response.error.details.messages.caseType) {
                        $scope.createCaseTypeError = response.error.details.messages.caseType[0];
                        $scope.caseTYpeCreationError = true;
                        $scope.caseTypeCheck=true;
                    }

                });
            } else {
                $scope.createCaseTypeError = 'Please Enter Alphabetic Characters Only';
                $scope.caseTYpeCreationError = true;
                $scope.caseTypeCheck=true;
            }
        }

    };
    $scope.updateCaseType = {};
    $scope.editCasePopup = function (caseTypeInfo) {
        $scope.updateCaseType=angular.copy(caseTypeInfo);

    };
    $scope.caseTypeUpdateCheck=true;
    $scope.editCaseType = function () {
        console.log("edit case type" +JSON.stringify($scope.updateCaseType))
        $scope.caseTYpeUpdationError = false;
        $scope.updateCaseTypeError = '';
        if($scope.caseTypeUpdateCheck==true) {
            $scope.caseTypeUpdateCheck = false;
            if ($scope.updateCaseType.caseType.match(alphabetsWithSpaces) && $scope.updateCaseType.caseType) {
                $http({
                    method: 'PUT',
                    url: 'api/CaseTypes/' + $scope.updateCaseType.id,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.updateCaseType
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.caseTypeUpdateCheck = true;
                    $('#caseTypeEdit').modal('hide');
                    $("#editSuccess").modal("show");
                    setTimeout(function () {
                        $('#editSuccess').modal('hide')
                    }, 3000);
                    //$scope.updateCaseType = {};
                    $scope.getCaseTypes();
                }).error(function (response) {
                    if (response.error.details.messages.caseType) {
                        $scope.updateCaseTypeError = response.error.details.messages.caseType[0];
                        $scope.caseTYpeUpdationError = true;
                        $scope.caseTypeUpdateCheck = true;
                    }
                    $timeout(function (){
                        $scope.caseTYpeUpdationError = false;
                    },3000)
                });
            } else {
                $scope.updateCaseTypeError = 'Please Enter Alphabetic Characters Only';
                $scope.caseTYpeUpdationError = true;
                $scope.caseTypeUpdateCheck = true;

                $timeout(function (){
                    $scope.caseTYpeUpdationError = false;
                },3000)

            }
        }
        $scope.caseTypeUpdateCheck=true;
    };
    $scope.cancelEdit = function () {
        $scope.getCaseTypes();
        $('#caseTypeEdit').modal('hide');
    };

    $scope.reset();

    /*function capitalizeWord(inStr)
    {
        return inStr.replace(/\w\S*!/g, function(tStr)
        {
            return tStr.charAt(0).toUpperCase() + tStr.substr(1).toLowerCase();
        });
    }*/

    $scope.exportToExcel=function(tableId){
        if( $scope.caseTypeData!=null &&  $scope.caseTypeData.length>0){

            $("<tr>" +
                "<th>Case Type</th>" +
                "<th>Case Type Description</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.caseTypeData.length;i++){
                var paymentData=$scope.caseTypeData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.caseType)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.caseNumber)+"</td>" +



                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'caseType');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);
app.controller('advocatesController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.advocates").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.advocates .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'advocateLists'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    $scope.getAdvocates = function () {
        $http({
            "method": "GET",
            "url": 'api/Advocates',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.advocateData = response;
        }).error(function (response, data) {
        });
    };

    $scope.getAdvocates();
    $scope.reset = function () {
        $scope.advocate = angular.copy($scope.master);
        $scope.advocateCreationError = false;
        $scope.createAdvocateTypeError = '';

    };


    $scope.advocate = {};
    $scope.advocateClick=function(){
        $('#createAdvocateModal').modal('show');
        $scope.advocate = {};
        $scope.advocateCreationError = false;
        $scope.createAdvocateTypeError = '';
    }

    $scope.advCheck=true;
    $scope.createAdvocate = function () {
        $scope.advocateCreationError = false;
        $scope.createAdvocateTypeError = '';

        if($scope.advCheck==true) {
            $scope.advCheck=false;

            if ($scope.advocate.name.match(alphabetsWithSpaces) && $scope.advocate.name) {
                if ($scope.advocate.advId.match(alphaNumeric) && $scope.advocate.advId) {
                    if (checkMailFormat($scope.advocate.email)) {
                        if ($scope.advocate.designation.match(alphabetsWithSpaces) && $scope.advocate.designation) {
                            if ($scope.advocate.phoneNumber.match(phoneNumber) && $scope.advocate.phoneNumber.length == 10) {
                                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                                var advocate = $scope.advocate;

                                advocate['createdPerson']=loginPersonDetails.employeeId;
                                $http({
                                    method: 'POST',
                                    url: 'api/Advocates',
                                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                    "data":advocate
                                }).success(function (response) {
                                    $scope.loadingImage=false;
                                    $scope.advCheck = true;
                                    $rootScope.advocateData.push(response);


                                    $('#createAdvocateModal').modal('hide');
                                    $("#addSuccess").modal("show");
                                    setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                                    $scope.getAdvocates();
                                }).error(function (response) {
                                    if (response.error.details.messages.email) {
                                        $scope.createAdvocateTypeError = response.error.details.messages.email[0];
                                        $scope.advocateCreationError = true;
                                        $scope.advCheck=true;
                                        $timeout(function (){
                                            $scope.advocateCreationError = false;
                                        },3000)
                                    }
                                    else if(response.error.details.messages.advId){
                                        $scope.createAdvocateTypeError = response.error.details.messages.advId[0];
                                        $scope.advocateCreationError = true;
                                        $scope.advCheck=true;
                                        $timeout(function (){
                                            $scope.advocateCreationError = false;
                                        },3000)
                                    }

                                });
                            } else {
                                $scope.createAdvocateTypeError = 'Please Enter the Phone Number in the Correct Format';
                                $scope.advocateCreationError = true;
                                $scope.advCheck=true;
                                $timeout(function (){
                                    $scope.advocateCreationError = false;
                                },3000)

                            }
                        } else {
                            $scope.createAdvocateTypeError = 'Please Enter Designation in the Alphabetic Characters Only';
                            $scope.advocateCreationError = true;
                            $scope.advCheck=true;
                            $timeout(function (){
                                $scope.advocateCreationError = false;
                            },3000)

                        }
                    } else {
                        $scope.createAdvocateTypeError = 'Please enter the Email in the Correct format';
                        $scope.advocateCreationError = true;
                        $scope.advCheck=true;
                        $timeout(function (){
                            $scope.advocateCreationError = false;
                        },3000)

                    }
                }else{
                    $scope.createAdvocateTypeError = 'Please enter the Advocate Id in  Alpha Numaric Only';
                    $scope.advocateCreationError = true;
                    $scope.advCheck=true;
                    $timeout(function (){
                        $scope.advocateCreationError = false;
                    },3000)
                }

            } else {
                $scope.createAdvocateTypeError = 'Please enter the Name in  Alphabetic Characters Only';
                $scope.advocateCreationError = true;
                $scope.advCheck=true;
                $timeout(function (){
                    $scope.advocateCreationError = false;
                },3000)

            }
        }
    };
    $scope.updateAdvocate = {};
    $scope.editAdvocatePopup = function (advocateInfo) {

        $scope.updateAdvocate = advocateInfo;
    };

    $scope.editAdvocate = function () {
        $scope.advocateUpdationError = false;
        $scope.updateAdvocateTypeError = '';
        if ($scope.updateAdvocate.name.match(alphabetsWithSpaces) && $scope.updateAdvocate.name) {
            if ($scope.updateAdvocate.advId.match(alphaNumeric) && $scope.updateAdvocate.advId) {
                if (checkMailFormat($scope.updateAdvocate.email)) {
                    if ($scope.updateAdvocate.designation.match(alphabetsWithSpaces) && $scope.updateAdvocate.designation) {
                        if ($scope.updateAdvocate.phoneNumber.match(phoneNumber) && $scope.updateAdvocate.phoneNumber.length == 10) {
                            $http({
                                method: 'PUT',
                                url: 'api/Advocates/' + $scope.updateAdvocate.id,
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                data: $scope.updateAdvocate
                            }).success(function (response) {
                                $scope.loadingImage=false;
                                //$scope.updateAdvocate = {};
                                $("#editSuccess").modal("show");
                                setTimeout(function () {
                                    $('#editSuccess').modal('hide')
                                }, 3000);
                                $scope.getAdvocates();
                                $('#advocateEdit').modal('hide');
                            }).error(function (response) {
                                if (response.error.details.messages.email) {
                                    $scope.updateAdvocateTypeError = response.error.details.messages.email[0];
                                    $scope.advocateUpdationError = true;
                                }

                                else if (response.error.details.messages.advId) {
                                    $scope.updateAdvocateTypeError = response.error.details.messages.advId[0];
                                    $scope.advocateUpdationError = true;
                                }
                                $timeout(function (){
                                    $scope.advocateUpdationError = false;
                                },3000)
                            });

                        } else {
                            $scope.updateAdvocateTypeError = 'Please Enter the Phone Number in the Correct Format';
                            $scope.advocateUpdationError = true;

                            $timeout(function (){
                                $scope.advocateUpdationError = false;
                            },3000)

                        }
                    } else {
                        $scope.updateAdvocateTypeError = 'Please Enter the Designation in Alphabetic Characters Only';
                        $scope.advocateUpdationError = true;

                        $timeout(function (){
                            $scope.advocateUpdationError = false;
                        },3000)

                    }
                } else {
                    $scope.updateAdvocateTypeError = 'Please enter the Email in the Correct format';
                    $scope.advocateUpdationError = true;

                    $timeout(function (){
                        $scope.advocateUpdationError = false;
                    },3000)

                }
            }else{
                $scope.updateAdvocateTypeError = 'Please enter the Advocate Id in Alpha Numaric Only';
                $scope.advocateUpdationError = true;

                $timeout(function (){
                    $scope.advocateUpdationError = false;
                },3000)
            }
        } else {
            $scope.updateAdvocateTypeError = 'Please enter the Name in Alphabetic Characters Only';
            $scope.advocateUpdationError = true;

            $timeout(function (){
                $scope.advocateUpdationError = false;
            },3000)

        }

    };
    $scope.cancelEdit = function () {
        $scope.getAdvocates();
        $('#advocateEdit').modal('hide');
    };


    $scope.reset();

    $scope.exportToExcel=function(tableId){
        if( $scope.advocateData!=null &&  $scope.advocateData.length>0){

            $("<tr>" +
                "<th>Name</th>" +
                "<th>Advocate Id</th>" +
                "<th>Email</th>" +
                "<th>Designation</th>" +
                "<th>PhoneNumber</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.advocateData.length;i++){
                var paymentData=$scope.advocateData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.name)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.advId)+"</td>" +
                    "<td>"+paymentData.email+"</td>" +
                    "<td>"+capitalizeWord(paymentData.designation)+"</td>" +
                    "<td>"+paymentData.phoneNumber+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'advocates');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
app.controller('statusController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.status").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.status .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'status'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.getStatus = function () {
        $http({
            "method": "GET",
            "url": 'api/Statuses',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.statusData = response;

        }).error(function (response, data) {
        });
    };
    $scope.getStatus();
    $scope.reset = function () {
        $scope.status = angular.copy($scope.master);
        $scope.statusCreationError = false;
    };

    $scope.statusClick=function(){
        $('#createStatusModal').modal('show');
        $scope.status = {};
        $scope.statusCreationError = false;
        $scope.createStatusTypeError = '';
    }
    $scope.status = {};
    $scope.statusCheck=true;
    $scope.createStatus = function () {
        $scope.statusCreationError = false;
        $scope.createStatusTypeError = '';

        if ($scope.statusCheck == true) {
            $scope.statusCheck=false;
            if($scope.status.field1.match(alphabetsWithSpaces) && $scope.status.field1) {

                $http({
                    method: 'POST',
                    url: 'api/Statuses',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.status
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.statusCheck=true;
                    $rootScope.statusData.push(response);
                    $('#createStatusModal').modal('hide');
                    $("#addAssetType").modal("hide");
                    $("#addAssetSuccess").modal("show");
                    setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                    $scope.getStatus();
                }).error(function (response) {
                    if (response.error.details.messages.field1) {
                        $scope.createStatusTypeError = response.error.details.messages.field1[0];
                        $scope.statusCreationError = true;
                        $scope.statusCheck=true;
                    }

                });
            } else {
                $scope.createStatusTypeError = 'Please Enter the Status in the Correct Format';
                $scope.statusCreationError = true;
                $scope.statusCheck=true;
            }
        }
    };
    $scope.updateStatus = {};
    $scope.editStatusPopup = function (statusInfo) {
        $scope.updateStatus = statusInfo;
    };

    $scope.editStatus = function () {
        $scope.statusUpdationError = false;
        $scope.updateStatusTypeError = '';

        if($scope.updateStatus.field1.match(alphabetsWithSpaces) && $scope.updateStatus.field1){

            $http({
                method: 'PUT',
                url: 'api/Statuses/' + $scope.updateStatus.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updateStatus
            }).success(function (response) {
                $scope.loadingImage=false;
                //$scope.updateStatus = {};
                $("#editSuccess").modal("show");
                setTimeout(function () {
                    $('#editSuccess').modal('hide')
                }, 3000);
                $scope.getStatus();
                $('#statusEdit').modal('hide');
            }).error(function (response) {
                if (response.error.details.messages.field1) {
                    $scope.updateStatusTypeError = response.error.details.messages.field1[0];
                    $scope.statusUpdationError = true;
                }
                $timeout(function (){
                    $scope.statusUpdationError = false;
                },3000)

            });
        }else {
            $scope.updateStatusTypeError = 'Please Enter the Status in the Correct Format';
            $scope.statusUpdationError = true;
            $timeout(function (){
                $scope.statusUpdationError = false;
            },3000)

        }
    };
    $scope.cancelEdit = function () {
        $scope.getStatus();
        $('#statusEdit').modal('hide');
    };


    $scope.reset();

    $scope.exportToExcel=function(tableId){
        if( $scope.statusData!=null &&  $scope.statusData.length>0){

            $("<tr>" +
                "<th>Status</th>" +
                "<th>Description</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.statusData.length;i++){
                var paymentData=$scope.statusData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.field1)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.field2)+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'status');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }
}]);
app.controller('priorityController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.priority").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.priority .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'priority'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;

    $scope.getPriority = function () {
        $http({
            "method": "GET",
            "url": 'api/Priorities',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.priorityData = response;

        }).error(function (response, data) {
        });
    };
    $scope.getPriority();
    $scope.reset = function () {
        $scope.priority = angular.copy($scope.master);
        $scope.priorityCreationError = false;
        $scope.priorityDescriptionError = '';
    };

    $scope.priorityClick=function(){
        $('#createPriorityModal').modal('show');
        $scope.priority = {};
        $scope.priorityCreationError = false;
        $scope.priorityDescriptionError = '';
    }
    $scope.priority = {};
    $scope.priorityCheck=true;
    $scope.createPriority = function () {

        $scope.priorityCreationError = false;
        $scope.priorityDescriptionError = '';


        if($scope.priorityCheck==true) {
            $scope.priorityCheck=false;
            if ($scope.priority.priority.match(alphabetsWithSpaces) && $scope.priority.priority) {

                $http({
                    method: 'POST',
                    url: 'api/Priorities',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.priority
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.priorityCheck=true;
                    $rootScope.priorityData.push(response);
                    $('#createPriorityModal').modal('hide');
                    $("#addSuccess").modal("show");
                    setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                    $scope.getPriority();
                }).error(function (response) {
                    if (response.error.details.messages.priority) {
                        $scope.priorityDescriptionError = response.error.details.messages.priority[0];
                        $scope.priorityCreationError = true;
                        $scope.priorityCheck=true;
                    }

                });
            } else {
                $scope.priorityDescriptionError = 'Please Provide The Priority in the Correct Format';
                $scope.priorityCreationError = true;
                $scope.priorityCheck=true;

            }
        }
    };


    $scope.updatePriority = {};
    $scope.editPriorityPopup = function (priorityInfo) {
        $scope.updatePriority = priorityInfo;
    };

    $scope.editPriority = function () {

        if ($scope.updatePriority.priority.match(alphabetsWithSpaces)){
            $http({
                method: 'PUT',
                url: 'api/Priorities/'+$scope.updatePriority.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updatePriority
            }).success(function (response) {
                $scope.loadingImage=false;
                //$scope.updatePriority = {};
                $('#priorityEdit').modal('hide');
                $("#editSuccess").modal("show");
                setTimeout(function () {
                    $('#editSuccess').modal('hide')
                }, 3000);
                $scope.getPriority();
                $scope.priorityCheck=true;
            }).error(function (response) {
                $scope.loadingImage=false;
                if(response.error!=undefined && response.error!=null){
                    if (response.error.details.messages.priority) {
                        $scope.updatePriorityTypeError = response.error.details.messages.priority[0];
                        $scope.priorityUpdationError = true;
                        $scope.priorityCheck=true;
                    }
                    $timeout(function () {
                        $scope.priorityUpdationError = false;
                    }, 3000)
                }
            });
        }else {
            $scope.loadingImage=false;
            $scope.priorityUpdationError = true;
            $scope.updatePriorityTypeError = 'Please Update the Priority in the Correct Format';
            $timeout(function () {
                $scope.priorityUpdationError = false;
            }, 3000);
        }

        $scope.loadingImage=true;
        $scope.priorityUpdationError = false;
        $scope.updatePriorityTypeError = '';
        if($scope.updatePriority) {
            delete  $scope.updatePriority["$$hashKey"]
        }

        /*if ($scope.updatePriority.priority.match(alphabetsWithSpaces) && $scope.updatePriority.priority) {
            $http({
                method: 'PUT',
                url: 'api/Priorities/'+$scope.updatePriority.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updatePriority
            }).success(function (response) {
                $scope.loadingImage=false;
                alert('success');
                //$scope.updatePriority = {};
                $("#editSuccess").modal("show");
                setTimeout(function () {
                    $('#editSuccess').modal('hide')
                }, 3000);
                $scope.getPriority();
                $('#priorityEdit').modal('hide');
                $scope.priorityCheck=true;
            }).error(function (response) {
                alert('error');
                $scope.loadingImage=false;
                if(response.error!=undefined && response.error!=null){
                    if (response.error.details.messages.priority) {
                        $scope.updatePriorityTypeError = response.error.details.messages.priority[0];
                        $scope.priorityUpdationError = true;
                        $scope.priorityCheck=true;
                    }
                    $timeout(function () {
                        $scope.priorityUpdationError = false;
                    }, 3000)
                }
            });
        } else {
            $scope.updatePriorityTypeError = 'Please Update the Priority in the Correct Format';
            $scope.priorityUpdationError = true;
            $scope.priorityCheck=true;
            $timeout(function () {
                $scope.priorityUpdationError = false;
            }, 3000)
        }*/




    };


    $scope.cancelEdit = function () {
        $scope.getPriority();
        $('#priorityEdit').modal('hide');
    };


    $scope.reset();

    $scope.exportToExcel=function(tableId){
        if( $scope.priorityData!=null &&  $scope.priorityData.length>0){

            $("<tr>" +
                "<th>Priority</th>" +
                "<th>Description</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.priorityData.length;i++){
                var paymentData=$scope.priorityData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.priority)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.description)+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'priority');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);
app.controller('actsController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.acts").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.acts .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'acts'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    var alphaNumeric =/^[a-z\d\-_\s]+$/i;
    $scope.getAct = function () {
        $http({
            "method": "GET",
            "url": 'api/Acts',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage= false;
            $rootScope.actData = response;
        }).error(function (response, data) {

        });
    };
    $scope.getAct();
    $scope.reset = function () {
        $scope.act = angular.copy($scope.master);
        $scope.actCreationError = false;
        $scope.createActError = '';
    };

    $scope.actClick=function(){
        $('#createActModal').modal('show');
        $scope.act = {};
        $scope.actCreationError = false;
        $scope.createActError = '';
    }
    $scope.act = {};
    $scope.actCheck=true;
    $scope.createAct = function () {
        $scope.actCreationError = false;
        $scope.createActError = '';

        if($scope.actCheck==true) {
            $scope.actCheck = false;
            if ($scope.act.actNumber.match(alphaNumeric) && $scope.act.actNumber) {
                $http({
                    method: 'POST',
                    url: 'api/Acts',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.act
                }).success(function (response) {
                    $scope.loadingImage= false;
                    $scope.actCheck = true;
                    $rootScope.actData.push(response);
                    //$scope.act = {};
                    $('#createActModal').modal('hide');
                    $("#addSuccess").modal("show");
                    setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                    $scope.getAct();
                }).error(function (response) {
                    if (response.error.details.messages.actNumber) {
                        $scope.createActError = response.error.details.messages.actNumber[0];
                        $scope.actCreationError = true;
                        $scope.actCheck = true;
                    }
                });
            } else {
                $scope.createActError = 'Please Enter the Act Number in Alpha Numaric Format';

                $scope.actCreationError = true;
                $scope.actCheck = true;

            }
        }
    };
    $scope.updateAct = {};
    $scope.editActPopup = function (actInfo) {

        $scope.updateAct = actInfo;
    };
    $scope.editAct = function () {

        $scope.actUpdationError = false;
        $scope.updateActError = '';


        if ($scope.updateAct.actNumber.match(alphaNumeric) && $scope.updateAct.actNumber) {

            $http({
                method: 'PUT',
                url: 'api/Acts/' + $scope.updateAct.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updateAct
            }).success(function (response) {
                $scope.loadingImage= false;
                //$scope.updateAct = {};
                $('#actEdit').modal('hide');
                $("#editSuccess").modal("show");
                setTimeout(function(){$('#editSuccess').modal('hide')}, 3000);
                $scope.getAct();
            }).error(function (response) {

                if (response.error.details.messages.actNumber) {
                    $scope.updateActError = response.error.details.messages.actNumber[0];
                    $scope.actUpdationError = true;
                }
                $timeout(function (){
                    $scope.actUpdationError = false;
                },3000)
            });
        }else{
            $scope.updateActError = 'Please Enter The ActNumber in the Correct Format';
            $scope.actUpdationError = true;
            $timeout(function (){
                $scope.actUpdationError = false;
            },3000)

        }

    };
    $scope.cancelEdit = function () {
        $scope.getAct();
        $('#actEdit').modal('hide');
    };

    $scope.reset();
    $scope.exportToExcel=function(tableId){

        if( $scope.actData!=null &&  $scope.actData.length>0){

            $("<tr>" +
                "<th>Act Number</th>" +
                "<th>Description</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.actData.length;i++){
                var paymentData=$scope.actData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.actNumber)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.viewDetails)+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'acts');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
app.controller('stakeHoldersController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.stakeHolders").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.stakeHolders .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'stakeHolders'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;

    $scope.getStakeHolders = function () {
        $http({
            "method": "GET",
            "url": 'api/StakeHolders',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.stakeHolderData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getStakeHolders();
    $scope.reset = function () {
        $scope.stakeHolder = angular.copy($scope.master);
        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';
    };

    $scope.stakeHolderClick=function(){
        $('#craeteStakeHolderModal').modal('show');
        $scope.stakeHolder = {};
        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';
    }

    $scope.stakeHolder = {};
    $scope.holderCheck=true;

    $scope.createMasterStakeHolders = function () {

        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';

        if($scope.holderCheck==true) {
            $scope.holderCheck = false;
            if ($scope.stakeHolder.name.match(alphabetsWithSpaces) && $scope.stakeHolder.name) {
                if ($scope.stakeHolder.holderId.match(alphaNumeric) && $scope.stakeHolder.holderId) {
                    if ($scope.stakeHolder.department.match(alphabetsWithSpaces) && $scope.stakeHolder.department) {
                        if ($scope.stakeHolder.designation.match(alphabetsWithSpaces) && $scope.stakeHolder.designation) {
                            if (checkMailFormat($scope.stakeHolder.email)) {
                                if ($scope.stakeHolder.phone.match(phoneNumber) && $scope.stakeHolder.phone.length == 10) {

                                    $http({
                                        method: 'POST',
                                        url: 'api/StakeHolders',
                                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                        data: $scope.stakeHolder
                                    }).success(function (response) {
                                        $scope.loadingImage=false;
                                        $scope.holderCheck = true;
                                        $rootScope.stakeHolderData.push(response);
                                        $('#craeteStakeHolderModal').modal('hide');
                                        $("#addSuccess").modal("show");
                                        setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                                        $scope.getStakeHolders();
                                    }).error(function (response) {

                                        if (response.error.details.messages.email) {
                                            $scope.createHolderTypeError = response.error.details.messages.email[0];
                                            $scope.holderCreationError = true;
                                            $scope.holderCheck = true;
                                        }
                                        else if (response.error.details.messages.holderId) {
                                            $scope.createHolderTypeError = response.error.details.messages.holderId[0];
                                            $scope.holderCreationError = true;
                                            $scope.holderCheck = true;
                                        }
                                    });
                                } else {
                                    $scope.createHolderTypeError = 'Please Enter The Phone Number In the Required Format';
                                    $scope.holderCreationError = true;
                                    $scope.holderCheck = true;
                                }
                            } else {
                                $scope.createHolderTypeError = 'Please Enter The Email In the Required Format';
                                $scope.holderCreationError = true;
                                $scope.holderCheck = true;
                            }
                        } else {
                            $scope.createHolderTypeError = 'Please Enter The Designation In the Alphabet';
                            $scope.holderCreationError = true;
                            $scope.holderCheck = true;
                        }
                    } else {
                        $scope.createHolderTypeError = 'Please Enter The Department In the Alphabet';
                        $scope.holderCreationError = true;
                        $scope.holderCheck = true;
                    }
                } else {
                    $scope.createHolderTypeError = 'Please Enter The Stake Holder Id In the Correct Format';
                    $scope.holderCreationError = true;
                    $scope.holderCheck = true;
                }
            } else {
                $scope.createHolderTypeError = 'Please Enter The StakeHolder Name In the Alphabet';
                $scope.holderCreationError = true;
                $scope.holderCheck = true;

            }
        }
    };
    $scope.updateStakeHolder = {};
    $scope.editStakeHolderPopup = function (stakeHolderInfo) {

        $scope.updateStakeHolder = stakeHolderInfo;
    };

    $scope.editStakeHolder = function () {


        $scope.holderUpdationError = false;
        $scope.updateHolderTypeError = '';

        if($scope.updateStakeHolder.name.match(alphabetsWithSpaces) && $scope.updateStakeHolder.name){
            if($scope.updateStakeHolder.department.match(alphabetsWithSpaces) && $scope.updateStakeHolder.department){
                if($scope.updateStakeHolder.designation.match(alphabetsWithSpaces) && $scope.updateStakeHolder.designation){
                    if(checkMailFormat($scope.updateStakeHolder.email)) {
                        if ($scope.updateStakeHolder.phone.match(phoneNumber) && $scope.updateStakeHolder.phone.length == 10) {

                            $http({
                                method: 'PUT',
                                url: 'api/StakeHolders/' + $scope.updateStakeHolder.id,
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                data: $scope.updateStakeHolder
                            }).success(function (response) {

                                $scope.loadingImage=false;
                                //$scope.updateStakeHolder = {};
                                $("#editSuccess").modal("show");
                                setTimeout(function(){$('#editSuccess').modal('hide')}, 3000);
                                $scope.getStakeHolders();
                                $('#stakeHolderEdit').modal('hide');
                            }).error(function (response) {

                                if (response.error.details.messages.email) {
                                    $scope.updateHolderTypeError = response.error.details.messages.email[0];
                                    $scope.holderUpdationError = true;
                                }

                                $timeout(function (){
                                    $scope.holderUpdationError = false;
                                },3000)
                            });
                        }else{
                            $scope.updateHolderTypeError = 'Please Enter The Phone Number In the Required Format';
                            $scope.holderUpdationError = true;
                            $timeout(function (){
                                $scope.holderUpdationError = false;
                            },3000)
                        }
                    }else{
                        $scope.updateHolderTypeError = 'Please Enter The Email In the Required Format';
                        $scope.holderUpdationError = true;
                        $timeout(function (){
                            $scope.holderUpdationError = false;
                        },3000)
                    }
                }else{
                    $scope.updateHolderTypeError = 'Please Enter The Designation In the Required Format';
                    $scope.holderUpdationError = true;
                    $timeout(function (){
                        $scope.holderUpdationError = false;
                    },3000)
                }
            }else{
                $scope.updateHolderTypeError = 'Please Enter The Department In the Required Format';
                $scope.holderUpdationError = true;

                $timeout(function (){
                    $scope.holderUpdationError = false;
                },3000)
            }
        }else{
            $scope.updateHolderTypeError = 'Please Enter The StakeHolder Name In the Required Format';
            $scope.holderUpdationError = true;
            $timeout(function (){
                $scope.holderUpdationError = false;
            },3000)

        }

    };
    $scope.cancelEdit = function () {
        $scope.getStakeHolders();
        $('#stakeHolderEdit').modal('hide');
    };


    $scope.reset();

    $scope.exportToExcel=function(tableId){

        if( $scope.stakeHolderData!=null &&  $scope.stakeHolderData.length>0){

            $("<tr>" +
                "<th>StakeHolder Name</th>" +
                "<th>Department</th>" +
                "<th>Designation</th>" +
                "<th>Email</th>" +
                "<th>Phone</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.stakeHolderData.length;i++){
                var paymentData=$scope.stakeHolderData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.name)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.department)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.designation)+"</td>" +
                    "<td>"+paymentData.email+"</td>" +
                    "<td>"+paymentData.phone+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'stakeHolders');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
app.controller('createCaseController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {

    $scope.uploadFile = uploadFileURL;

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;

    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;

    $scope.docUploadURL = 'apiUploads/dhanbadDb/download/';
    $scope.dtOptions = {paging: false, searching: false};

    $scope.getCaseTypes = function () {
        $http({
            method: 'GET',
            url: 'api/CaseTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.caseTypes = response;
        }).error(function (response) {
        });
    };
    $scope.getCaseTypes();


    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.status = response;
        }).error(function (response) {
        });
    };
    $scope.getStatus();

    $scope.getCourt = function () {
        $http({
            method: 'GET',
            url: 'api/Courts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.courtName = response;
        }).error(function (response) {
        });
    };
    $scope.getCourt();

    $scope.getPriority = function () {
        $http({
            method: 'GET',
            url: 'api/Priorities',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.priority = response;
        }).error(function (response) {
        });
    };
    $scope.getPriority();

    $scope.getActs = function () {
        $http({
            method: 'GET',
            url: 'api/Acts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.acts = response;
        }).error(function (response) {
        });
    };
    $scope.getActs();

    $scope.getDepartment = function () {
        $http({
            method: 'GET',
            url: 'api/caseDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.department = response;
        }).error(function (response) {
        });
    };
    $scope.getDepartment();


    $scope.getStakeHolders = function () {
        $http({
            method: 'GET',
            url: 'api/StakeHolders',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.holders = response;
        }).error(function (response) {
        });
    };
    $scope.getStakeHolders();

    $scope.getEmapnnel = function () {
        $http({
            method: 'GET',
            url: 'api/EmpannelGroups',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.empannels = response;
        }).error(function (response) {
        });
    };
    $scope.getEmapnnel();

    $scope.getAdvocates = function (emp) {
        $http({
            method: 'GET',
            url: 'api/EmpannelGroups?filter={"where":{"groupName":"'+emp+'"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.advocates = response[0].advocateList;

        }).error(function (response) {
        });
    };
    $scope.getCase = function () {
        $http({
            "method": "GET",
            "url": 'api/CreateCases',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.caseData = response;

        }).error(function (response, data) {
        });
    };
    $scope.getCase();


    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;

        $scope.advocates={};
        $scope.CaseCreationError = false;
        $scope.CaseError = '';
    };

    $scope.user = {};
    $scope.createCaseCheck=true;
    $scope.createCase = function () {
        $scope.CaseCreationError = false;
        $scope.CaseError = '';

        if ($scope.createCaseCheck == true) {
            $scope.createCaseCheck = false;
            if (formUploadStatus) {
                $scope.loadingImage=false;

                $scope.advocateData = [];
                for(var i=0;i<$scope.user.advocate.length;i++){
                    $scope.advocateData.push(JSON.parse($scope.user.advocate[i]));
                }
                $scope.user.advocate = [];

                for(var i=0;i<$scope.advocateData.length;i++){
                    $scope.user.advocate.push({
                        name:$scope.advocateData[i].name,
                        email:$scope.advocateData[i].email,
                        advId:$scope.advocateData[i].advId,
                        phoneNumber:$scope.advocateData[i].phoneNumber
                    });
                }
                if ($scope.user.caseDate) {
                    var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                    $scope.user['createdPerson'] = loginPersonDetails.employeeId;
                    $http({
                        method: 'POST',
                        url: 'api/CreateCases',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        data: $scope.user
                    }).success(function (response) {


                        if(response.error){
                            $scope.CaseCreationError = true;
                            $scope.CaseError =response.error.message;
                            $scope.createCaseCheck=true;
                        } else {
                            $scope.loadingImage=false;
                            $scope.createCaseCheck=true;
                            $rootScope.caseData.push(response);
                            $scope.selectCaseNumber = response.caseNumber;
                            $window.localStorage.setItem('caseCreationCheck', $scope.selectCaseNumber);
                            $scope.caseUploads();
                            location.href = '#/case';
                        }


                    }).error(function (response) {
                        console.log('Error Response1 :' + JSON.stringify(response));
                        if(response.error.message){
                            $scope.CaseCreationError = true;
                            $scope.CaseError =response.error.message;
                            $scope.createCaseCheck=true;
                        }

                        $scope.createCaseCheck=true;
                    });
                } else {
                    $scope.CaseCreationError = true;
                    $scope.CaseError = 'Please Select Case Submission Date';
                    $scope.createCaseCheck=true;
                }
            } else {
                $scope.CaseCreationError = true;
                $scope.CaseError = 'Please Upload the files';
                $scope.createCaseCheck=true;
            }
        }
    };
    $scope.docUploadURL = 'apiUploads/dhanbadDb/download/';
    $scope.dtOptions = {paging: false, searching: false};
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.caseList = response;
        }).error(function (response) {
        });


    }
    $scope.getCaseNumber();

    var formIdsArray = [];
    var formUploadStatus = false;
    $scope.disable = true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        formIdsArray = [];
        var fileCount = 0;
        angular.forEach(files, function (file) {
            $scope.disable = true;
            $scope.errorMssg1 = true;
            $scope.fileUploadSuccess = false;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}

                });
                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if(fileCount == files.length){
                $scope.disable = false;
                $scope.errorMssg1 = false;
                $scope.fileUploadSuccess = true;
            }
        });
    };

    $scope.caseUploads = function () {
        if (formUploadStatus) {
            var uploadDetails = $scope.upload;
            uploadDetails.file = $scope.docList;
            var caseNumber = $scope.selectCaseNumber;
            uploadDetails['caseNumber'] = caseNumber;
            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            uploadDetails['createdPerson'] = loginPersonDetails.name;
            uploadDetails['uploadedPersonName'] = loginPersonDetails.name;
            uploadDetails.status = 'active';
            $http({
                method: 'POST',
                url: 'api/CaseUploads',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data": uploadDetails
            }).success(function (response) {
                $scope.loadingImage=false;
                $("#scroll2").hide();
                $("#scroll").show();
                $scope.docList = [];
                $scope.upload = {};
                $scope.getDocuments();

            }).error(function (response) {
            });
        } else {
        }
    }


    $scope.getDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/CaseUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.documentsList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
        });

    }
    $scope.deleteFile = function(fileId, index){
        $scope.fileUploadSuccess = false;
        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
        });
    }



    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
    });
    $scope.reset();
}]);

app.controller('advocatePaymentController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window,Excel,$timeout, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'advocatePayments'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    $scope.getPayments = function () {
        $http({
            "method": "GET",
            "url": 'api/AdvocatePayments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.paymentData = response;
            $scope.paymentGetData = response;
        }).error(function (response, data) {
        });
    };
    $scope.getPayments();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    if($window.localStorage.getItem('paymentCreationCheck')){
        $scope.caseShow=true;
        $scope.caseHide=false;

        $scope.advName= $window.localStorage.getItem('CreationCheck');
        $scope.selectedCaseNumber = $window.localStorage.getItem('paymentCreationCheck')
        $window.localStorage.removeItem('paymentCreationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }


    $scope.payment = {};

    $scope.updatePayment = {};
    $scope.editPaymentPopup = function (paymentInfo) {
        $scope.updatePayment = paymentInfo;
    };

    /*$scope.editPayment = function () {

        $scope.advocateData = [];
        $scope.advocateData.push(JSON.parse($scope.updatePayment.advocateName));

        $scope.updatePayment.advocateName=[];
        for(var i=0;i<$scope.advocateData.length;i++){


            $scope.updatePayment.advocateName.push($scope.advocateData[i].advId);

        }
        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
        $scope.updatePayment['lastEditPerson'] = loginPersonDetails.employeeId;

        $http({
            method: 'PUT',
            url: 'api/AdvocatePayments/' + $scope.updatePayment.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updatePayment
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.advocatName=response.advocateName;
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            $window.location.reload();
        }).error(function (response) {
        });

    };*/

    $scope.editPayment = function () {

        console.log('adv name'+$scope.updatePayment.advocateName);
        $scope.advocateData = [];

        $scope.advocateData.push($scope.updatePayment.advocateName);

        console.log(JSON.stringify($scope.advocateData));


        /*$scope.updatePayment.advocateName = [];

         for (var i = 0; i < $scope.advocateData.length; i++) {
         $scope.updatePayment.advocateName.push($scope.advocateData[i].advId);
         }*/

        $scope.advocateNameTemp = [];

        $scope.advocateNameTemp.push($scope.advocateData[0]);

        $scope.updatePayment.advocateName= $scope.advocateNameTemp;

        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));

        $scope.updatePayment['lastEditPerson'] = loginPersonDetails.employeeId;

        console.log('final obje'+JSON.stringify($scope.updatePayment));
        $http({
            method: 'PUT',
            url: 'api/AdvocatePayments/' + $scope.updatePayment.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updatePayment
        }).success(function (response) {
            $scope.loadingImage = false;
            $scope.advocatName = response.advocateName;
            $scope.selectCaseNumb = response.caseNumber;
            $scope.caseShow = false;
            $scope.caseHide = true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function () {
                $('#addDepartmentSuccess').modal('hide')
            }, 9000);
            $window.location.reload();
        }).error(function (response) {
        });

    };

    $scope.cancelEdit = function () {
        $scope.getPayments();
        $('#paymentEdit').modal('hide');
    };

    $scope.reset();
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
        });
    };
    $scope.getCaseNumber();

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.advocates = response;
        }).error(function (response) {
        });
    };
    $scope.getAdvocates();

    $scope.exportToExcel=function(tableId){
        if( $scope.paymentData!=null &&  $scope.paymentData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Advocate Name</th>" +
                "<th>Amount Paid</th>" +
                "<th>Mode of Payment</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.paymentData.length;i++){
                var paymentData=$scope.paymentData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.advocateName)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.amount)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.mode)+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'advocatePayment ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }


}]);
app.controller('hearingDateController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'hearingDateInfo'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.reloadRoute = function() {
        $window.location.reload();
    }
    $scope.gethearingDate = function () {
        $http({
            "method": "GET",
            "url": 'api/HearingDates',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.hearingData = response;
        }).error(function (response, data) {
        });
    };
    $scope.gethearingDate();

    $scope.hearing = {};



    if($window.localStorage.getItem('hearingCreationCheck')){
        $scope.caseShow=true;
        $scope.caseHide=false;

        $scope.selectedCaseNumber = $window.localStorage.getItem('hearingCreationCheck')
        $window.localStorage.removeItem('hearingCreationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }

    if($window.localStorage.getItem('hearingUpdationCheck')){
        $scope.caseShow=false;
        $scope.caseHide=true;
        $scope.selectedCaseNumb = $window.localStorage.getItem('hearingUpdationCheck')
        $window.localStorage.removeItem('hearingUpdationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }

    $scope.reset = function () {
        $scope.hearing = angular.copy($scope.master);
    };
    $scope.hearing = {};
    $scope.hearingCheck=true;
    $scope.updateHearingDate = {};
    $scope.edithearingPopup = function (hearingInfo) {
        $scope.updateHearingDate = hearingInfo;
        //console.log($scope.advocates.length+'xxxxxxxxxxxxxxxxx'+JSON.stringify($scope.advocates));
        $scope.advcts=[];
        var a={};
        for(var i=0;i<$scope.advocates.length;i++){
            a=$scope.advocates[i];
            $scope.advcts.push({ "name": a.name, "email": a.email, "advId": a.advId, "phoneNumber": a.phoneNumber } );
        }
        //console.log('$scope.advocates  .  length....'+$scope.advcts.length);
    };

    /*holiday check while update start*/
    $http({
        "method": "GET",
        "url": 'api/Holidays',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {

        $scope.loadingImage=false;
        $scope.holidaysData = response;

    }).error(function (response, data) {

    });


    $scope.$watch('updateHearingDate.dateHearing',function(o,n){
        if(o!=undefined){
            console.log(o+'old----new'+n);
            if(o!=n){
                $scope.hearingDateUpdate();
            }
        }
    });


    $scope.hearingDateUpdate=function(){
        //$scope.hearing = {};
        //$scope.hearing.dateHearing;
        //console.log('$rootScope.holidaysData----'+JSON.stringify($scope.holidaysData));
        //console.log('$scope.hearing.dateHearing----'+JSON.stringify($scope.updateHearingDate.dateHearing));
        angular.forEach($scope.holidaysData,function(date,index){
            if(date.festivalDate==$scope.updateHearingDate.dateHearing){
                //Notification.error('The date is a holiday, Please Select Other Date.!');
                $scope.updateMessage='The date is a holiday, Please Select Other Date.!';
                $scope.updateHearingDate.dateHearing='';
                $scope.updateError=true;
                $timeout(function(){
                    $scope.updateError = false;
                },3000);
            }
        });
    }

    /*holiday check while update end*/

    $scope.editHearingDate = function () {
        $scope.advocateData = [];

        console.log('editHearingDate....'+JSON.stringify($scope.updateHearingDate.advocateName));
        for(var i=0;i<$scope.updateHearingDate.advocateName.length;i++){
            $scope.advocateData.push($scope.updateHearingDate.advocateName[i]);
        }

        $scope.updateHearingDate.advocateName = [];

        for(var i=0;i<$scope.advocateData.length;i++){

            $scope.updateHearingDate.advocateName.push({
                name:$scope.advocateData[i].name,
                email:$scope.advocateData[i].email,
                advId:$scope.advocateData[i].advId,
                phoneNumber:$scope.advocateData[i].phoneNumber

            });

        }
        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
        $scope.updateHearingDate['lastEditPerson'] = loginPersonDetails.employeeId;
        $http({
            method: 'PUT',
            url: 'api/HearingDates/' + $scope.updateHearingDate.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateHearingDate
        }).success(function (response) {
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            $('#hearingEdit').modal('hide');
            //$window.location.reload();
            $scope.gethearingDate();
        }).error(function (response) {
        });

    };
    $scope.cancelEdit = function () {
        $scope.gethearingDate();
        $('#hearingEdit').modal('hide');
    };
    $scope.reset();

    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.caseNumber = response;
        }).error(function (response) {
        });
    };
    $scope.getCaseNumber();

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.advocates = response;

        }).error(function (response) {
        });
    };
    $scope.getAdvocates();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.status = response;
        }).error(function (response) {
        });
    };
    $scope.getStatus();
    var formIdsArray = [];
    var formUploadStatus = true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        formIdsArray = [];
        $scope.forms = files;
        var fileCount = 0;
        angular.forEach(files, function (file) {
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });
    };

    $scope.exportToExcel=function(tableId){
        if( $scope.hearingData!=null &&  $scope.hearingData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Advocate Name</th>" +
                "<th>Hearing date</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.hearingData.length;i++){
                var paymentData=$scope.hearingData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.advocateName[0].name)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.dateHearing)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.status)+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'hearingDate ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);

app.controller('caseStakeHoldersController', ['legalManagement', '$http', 'Excel', '$timeout', '$scope', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(legalManagement, $http,Excel, $timeout, $scope, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseStakeHolders").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseStakeHolders .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'caseStakeHolder'}, function (response) {
        if(
            !response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;

    $scope.getcaseStakeHolders = function () {
        $http({
            "method": "GET",
            "url": 'api/CaseStakeHolders',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.caseStakeHolderData = response;

        }).error(function (response, data) {
        });
    };
    $scope.getcaseStakeHolders();

    $scope.reset = function () {
        $scope.caseStakeHolder = angular.copy($scope.master);
    };

    $scope.caseStakeClick=function(){
        $('#createCaseStakeModal').modal('show');
        $scope.caseStakeHolder = {};
        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';
    }
    $scope.caseStakeHolder = {};

    $scope.check=true;


    var acceptDoubleClick=false;
    $scope.createMastercaseStakeHolders = function () {
        //$scope.loadingImage=true;
        $scope.exist = false;
        var data = {};
        for (var x = 0; x < $scope.caseStakeHolderData.length; x++) {
            //$scope.caseStakeHolder.id
            //$scope.caseStakeHolder.stake
            data = $scope.caseStakeHolderData[x];
            //console.log('existing data...' + JSON.stringify(data));
            //console.log('------' + JSON.stringify($scope.caseStakeHolder));
            if (data.caseNumber == $scope.caseStakeHolder.caseId.caseNumber && data.name == $scope.caseStakeHolder.stake.name) {
                $scope.exist = true;
                break;
            }
        }
        console.log('exist.....' + $scope.exist);

        if ($scope.exist) {

            $scope.holderCreationError = true;
            $scope.createHolderTypeError = "Stake Holder Already Exists...";

        }else{
            if (!acceptDoubleClick) {
                acceptDoubleClick = true;
                $scope.holderCreationError = false;
                $scope.createHolderTypeError = '';
                if ($scope.caseStakeHolder.caseId != undefined) {
                    if ($scope.caseStakeHolder.stake != undefined) {
                        if ($scope.check == true) {
                            $scope.check = false;
                            var caseNumber = $scope.caseStakeHolder.caseId.caseNumber;
                            var name = $scope.caseStakeHolder.stake.name;
                            $scope.caseStakeHolder['caseNumber'] = caseNumber;
                            $scope.caseStakeHolder['name'] = name;

                            $http({
                                method: 'POST',
                                url: 'api/CaseStakeHolders',
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                data: $scope.caseStakeHolder
                            }).success(function (response) {

                                $('#createCaseStakeModal').modal('hide');
                                $scope.loadingImage = false;
                                $scope.check = true;
                                $rootScope.caseStakeHolderData.push(response);
                                $scope.selectCaseNumber = response.caseNumber;
                                $scope.caseShow = true;
                                $scope.caseHide = false;
                                acceptDoubleClick = false;
                                $scope.getcaseStakeHolders();

                                /*if (response.error) {
                                    acceptDoubleClick = false;
                                    $scope.createHolderTypeError = response.error.message;
                                    $scope.holderCreationError = true;
                                    $scope.check = true;
                                } else {
                                    $scope.loadingImage = false;
                                    $scope.check = true;
                                    $rootScope.caseStakeHolderData.push(response);
                                    $scope.selectCaseNumber = response.caseNumber;
                                    $scope.caseShow = true;
                                    $scope.caseHide = false;
                                    acceptDoubleClick = false;
                                    $('#createCaseStakeModal').modal('hide');
                                    $('#addDepartmentSuccess').modal('show');
                                    $scope.getcaseStakeHolders();
                                }*/
                            }).error(function (response) {
                                acceptDoubleClick = false;
                                if (response.error.message) {
                                    $scope.createHolderTypeError = response.error.message;
                                    $scope.holderCreationError = true;
                                    $scope.check = true;
                                }
                            });


                        }

                    } else {

                        acceptDoubleClick = false;
                        $scope.holderCreationError = true;
                        $scope.createHolderTypeError = 'Please Select Stake Holder Name';
                        $scope.check = true;

                    }
                } else {
                    acceptDoubleClick = false;
                    $scope.holderCreationError = true;
                    $scope.createHolderTypeError = 'Please Select Case Number';
                    $scope.check = true;
                }

            }
        }
    };
    $scope.updateCaseStakeHolder = {};
    $scope.editCaseStakeHolderPopup = function (caseStakeHolderInfo) {
        $scope.updateCaseStakeHolder = caseStakeHolderInfo;
    };

    $scope.editCaseStakeHolder = function () {
        $scope.holderUpdationError = false;
        $scope.updateHolderTypeError = '';



        var caseNumber = $scope.updateCaseStakeHolder.caseId.caseNumber;
        var name = $scope.updateCaseStakeHolder.stake.name;
        $scope.updateCaseStakeHolder['caseNumber']=caseNumber;
        $scope.updateCaseStakeHolder['name']=name;



        $http({
            method: 'PUT',
            url: 'api/CaseStakeHolders/' + $scope.updateCaseStakeHolder.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateCaseStakeHolder
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            //$('#myModal').modal('hide');
            $('#caseStakeHolderEdit').modal('hide');
        }).error(function (response) {
        });

    };
    $scope.cancelEdit = function () {
        $scope.getcaseStakeHolders();
        $('#caseStakeHolderEdit').modal('hide');
    };


    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
        });
    };
    $scope.getCaseNumber();

    $scope.getStakeHolders = function () {
        $http({
            method: 'GET',
            url: 'api/StakeHolders',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.stakeHolders = response;
        }).error(function (response) {
        });
    };
    $scope.getStakeHolders();

    $scope.getDepartments = function (adv) {
        $http({
            method: 'GET',
            url: 'api/StakeHolders?filter={"where":{"name":"'+adv+'"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.departments = response;
        }).error(function (response) {
        });
    };

    $scope.exportToExcel=function(tableId){
        if( $scope.caseStakeHolderData!=null &&  $scope.caseStakeHolderData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Case StakeHolder name</th>" +
                "<th>Email</th>" +

                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.caseStakeHolderData.length;i++){
                var paymentData=$scope.caseStakeHolderData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.name)+"</td>" +
                    "<td>"+paymentData.email+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'caseStakeHolders ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
app.controller('noticeCommentsController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope,Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'noticeComments'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.getNotice = function () {
        $http({
            "method": "GET",
            "url": 'api/NoticeComments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.noticeData = response;
        }).error(function (response, data) {
        });
    };
    $scope.getNotice();

    if($window.localStorage.getItem('noticeCreationCheck')){
        $scope.caseShow=true;
        $scope.caseHide=false;
        $scope.selectedCaseNumber = $window.localStorage.getItem('noticeCreationCheck')
        $window.localStorage.removeItem('noticeCreationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }


    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };


    $scope.updateNotice = {};
    $scope.editNoticePopup = function (noticeInfo) {
        $scope.updateNotice = noticeInfo;
    };



    $scope.updateNotice.comment=[];

    $scope.addNewChoice = function () {

        $scope.updateNotice.comment.push('');

    };

    $scope.removeChoice = function (z) {

        $scope.updateNotice.comment.splice(z,1);
    };

    $scope.editNotice = function () {

        $http({
            method: 'PUT',
            url: 'api/NoticeComments/' + $scope.updateNotice.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateNotice
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            $('#noticeEdit').modal('hide');
        }).error(function (response) {
        });

    };
    $scope.cancelEdit = function () {
        $scope.getNotice();
        $('#noticeEdit').modal('hide');
    };


    $scope.reset();


    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
        });
    };
    $scope.getCaseNumber();

    $scope.exportToExcel=function(tableId){
        if( $scope.noticeData!=null &&  $scope.noticeData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Notice Number</th>" +
                "</tr>").appendTo("table"+tableId);
            for(var i=0;i<$scope.noticeData.length;i++){
                var paymentData=$scope.noticeData[i];
                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.noticeNumber)+"</td>" +
                    "</tr>").appendTo("table"+tableId);            }
            var exportHref = Excel.tableToExcel(tableId, 'noticeComments ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('caseController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', 'Excel', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, Upload, $timeout,Excel, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    $scope.getCase = function () {
        $http({
            "method": "GET",
            "url": 'api/CreateCases',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $rootScope.CaseData = response;
        }).error(function (response, data) {
        });
    };
    $scope.getCase();


    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.case = {};

    $scope.getEditCase=function (caseInfo) {

        $rootScope.updateCase=caseInfo;
    }
    if($window.localStorage.getItem('caseCreationCheck')){

        $scope.caseShow=true;
        $scope.caseHide=false;

        $scope.selectedCaseNumber = $window.localStorage.getItem('caseCreationCheck')
        $window.localStorage.removeItem('caseCreationCheck')
        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 90000);
        $window.location.reload();
    }

    if($window.localStorage.getItem('caseUpdationCheck')){
        $scope.caseShow=false;
        $scope.caseHide=true;
        $scope.selectedCaseNumb = $window.localStorage.getItem('caseUpdationCheck')
        $window.localStorage.removeItem('caseUpdationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }
    $scope.exportToExcel=function(tableId){
        if( $scope.CaseData!=null &&  $scope.CaseData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Case Type</th>" +
                "<th>Advocate Name</th>" +
                "<th>Status</th>" +
                "<th>Priority</th>" +
                "<th>Act Number</th>" +
                "<th>Case Created date</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.CaseData.length;i++){
                var caseData=$scope.CaseData[i];

                $("<tr>" +
                    "<td>"+capitalizeWord(caseData.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(caseData.caseType)+"</td>" +
                    "<td>"+capitalizeWord(caseData.advocate[0].name)+"</td>" +
                    "<td>"+capitalizeWord(caseData.status)+"</td>" +
                    "<td>"+capitalizeWord(caseData.priority)+"</td>" +
                    "<td>"+capitalizeWord(caseData.act)+"</td>" +
                    "<td>"+capitalizeWord(caseData.createdTime)+"</td>" +

                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'case ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }


}]);

app.controller('caseDetailsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = {paging: false, searching: false};
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.caseList = response;

        }).error(function (response) {
        });


    }
    $scope.getCaseNumber();

    var formIdsArray = [];
    var formUploadStatus = true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        formIdsArray = [];
        $scope.forms = files;

        angular.forEach(files, function (file) {
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });
    };

    $scope.caseUploads = function () {
        if (formUploadStatus) {
            var uploadDetails = $scope.upload;
            uploadDetails.file = $scope.docList;
            var caseNumber = $scope.selectCaseNumber;

            uploadDetails['caseNumber'] = caseNumber;
            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            uploadDetails['createdPerson'] = loginPersonDetails.name;
            uploadDetails['uploadedPersonName'] = loginPersonDetails.name;
            uploadDetails.status = 'active';
            $http({
                method: 'POST',
                url: 'api/CaseUploads',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data": uploadDetails
            }).success(function (response) {
                $("#scroll2").hide();
                $("#scroll").show();
                $scope.upload = {};
                $scope.docList = [];
                $scope.getDocuments();
            }).error(function (response) {
            });
        } else {
            $scope.createScheme();
        }
    }

    $scope.docUploadURL = uploadFileURL;

    $scope.getDocuments = function () {
        $http({
            method: 'GET',
            url: 'api/CaseUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.documentsList = response;

        }).error(function (response) {
        });

    }
    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectCaseDetails = response[0];

        }).error(function (response) {
        });

        $scope.getDocuments();

    }


    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
    })
}]);
app.controller('advocatePaymentDetailsController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'advocatePayments'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.getAdvocate = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $rootScope.advoatesList = response;
        }).error(function (response) {

        });


    }
    $scope.getAdvocate();

    $scope.selectareas = function (name) {
        $scope.selectadvocateName = name;


        $http({
            method: 'GET',
            url: 'api/AdvocatePayments/?filter={"where":{"advocateName":"' + $scope.selectadvocateName + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.selectAdvocateDetails = response;
        }).error(function (response) {

        });
    }


}]);

app.controller('noticeDetailsController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'noticeComments'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    /*print cancel reload*/

    (function () {

        var beforePrint = function () {
            //alert('Functionality to run before printing.');
        };

    var afterPrint = function () {
        //alert('Functionality to run after printing');
        $window.location.reload();
    };

    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');

        mediaQueryList.addListener(function (mql) {
            //alert($(mediaQueryList).html());
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;

    }());
    /*print cancel reload end*/




    $scope.loadingImage=true;

    $scope.generatePDF = function() {
        var printContents = document.getElementById('formConfirmation').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

    }
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/NoticeComments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.noticeList = response;

        }).error(function (response) {

        });
    }
    $scope.getCaseNumber();

    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;

        $http({
            method: 'GET',
            url: 'api/NoticeComments/?filter={"where":{"noticeNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.selectCaseDetails = response[0];

        }).error(function (response) {

        });


    }



}]);
app.controller('hearingDetailsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', '$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,$routeParams, DTOptionsBuilder, DTColumnBuilder) {

    $scope.dtOptions = {paging: false, searching: false};


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'hearingDateInfo'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.deleteFile = function(fileId, index){

        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);

        });
    }


    var formIdsArray = [];
    var formUploadStatus = false;
    $scope.errorMssg1 = false;
    $scope.fileUploadSuccess = false;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {

        formIdsArray = [];

        var fileCount = 0;
        angular.forEach(files, function (file) {
            $scope.errorMssg1 = true;
            $scope.fileUploadSuccess = false;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.errorMssg1 = false;
                $scope.fileUploadSuccess = true;
                $scope.uploadError = false;
                $scope.uploadErrorMessage = '';
            }
        });
    };

    $scope.uploadError = false;
    $scope.uploadErrorMessage = '';
    $scope.caseUploads = function () {

        if (formUploadStatus) {
            if($scope.upload && $scope.upload.comments) {
                var uploadDetails = $scope.upload;
                uploadDetails.file = $scope.docList;
                var caseNumber = $scope.selectCaseNumber;
                uploadDetails['caseNumber'] = caseNumber;
                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                uploadDetails['createdPerson'] = loginPersonDetails.name;
                uploadDetails['uploadedPersonName'] = loginPersonDetails.name;

                uploadDetails.status = 'active';
                $http({
                    method: 'POST',
                    url: 'api/HearingUploads',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": uploadDetails
                }).success(function (response) {

                    $("#scroll2").hide();
                    $("#scroll").show();
                    $scope.errorMssg1 = false;
                    $scope.fileUploadSuccess = false;
                    $scope.uploadError = false;
                    $scope.uploadErrorMessage = '';
                    $scope.upload = {};
                    $scope.docList = [];
                    $scope.getDocuments();
                }).error(function (response) {

                });

            }else {
                $scope.uploadError = true;
                $scope.uploadErrorMessage = 'Please enter the comment';
            }
        } else {
            $scope.uploadError = true;
            $scope.uploadErrorMessage = 'Please attach the files.';

        }
    }
    $scope.docUploadURL = uploadFileURL;

    $scope.getDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/HearingUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.documentsList = response;

        }).error(function (response) {

        });

    }


    $scope.cases= $window.localStorage.getItem('caseNumbb');

    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;
        $scope.eventResponse = JSON.parse($window.localStorage.getItem('events'));

        var url;
        if($routeParams.id){
            url= 'api/HearingDates/?filter={"where":{"id":"' + $routeParams.id + '"}}'
        }else{
            url= 'api/HearingDates/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}'
        }
        $http({
            method: 'GET',
            url: url,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.selectCaseDetails = response[0];
            $window.localStorage.setItem('hearingDetails', $scope.selectCaseDetails);
        }).error(function (response) {

        });

        $scope.getDocuments();
        $scope.selectHearingHistory();
    }


    $scope.selectHearingHistory = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/HearingHistories/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.selectHearingHistoryDetails = response;

        }).error(function (response) {

        });

    }


    $scope.getCaseNumber = function (caseId) {
        var url;
        if(caseId){
            url = 'api/HearingDates/?filter={"where":{"id":"' + caseId + '"}}';
        }else{
            url = 'api/HearingDates';
        }
        $http({
            method: 'GET',
            url: url,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $rootScope.caseList = response;


            if(caseId){
                $scope.selectareas(response[0].caseNumber);
                $scope.caseIdShow =false;
            }
            else{
                $scope.caseIdShow =true;
            }

        }).error(function (response) {

        });


    }
    if($routeParams.id) {
        $scope.getCaseNumber($routeParams.id);
    }else{
        $scope.getCaseNumber();
    }
    $scope.reset = function () {
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;

    };

    $scope.cancel=function(){
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
    }

    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
    });

    $scope.reset();
}]);

app.controller('editCaseController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });


    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    $scope.getCase = function () {
        $http({
            "method": "GET",
            "url": 'api/CreateCases',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $rootScope.CaseData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getCase();


    $scope.getCaseTypes = function () {
        $http({
            method: 'GET',
            url: 'api/CaseTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.caseTypes = response;
        }).error(function (response) {

        });
    };
    $scope.getCaseTypes();

    $scope.getDepartment = function () {
        $http({
            method: 'GET',
            url: 'api/caseDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.department = response;
        }).error(function (response) {

        });
    };
    $scope.getDepartment();


    $scope.getEmapnnel = function () {
        $http({
            method: 'GET',
            url: 'api/EmpannelGroups',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.empannels = response;
            $scope.getAdvocates($rootScope.updateCase.empannel);

            if($rootScope.updateCase) {
                $scope.getAdvocates($rootScope.updateCase.empannel);
            }

        }).error(function (response) {

        });
    };
    $scope.getEmapnnel();

    $scope.getAdvocates = function (emp) {

        $http({
            method: 'GET',
            url: 'api/EmpannelGroups?filter={"where":{"groupName":"'+emp+'"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.advocates = response[0].advocateList;
        }).error(function (response) {

        });
    };

    $scope.getCourt = function () {
        $http({
            method: 'GET',
            url: 'api/Courts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.courtName = response;
        }).error(function (response) {

        });
    };
    $scope.getCourt();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.status = response;
        }).error(function (response) {

        });
    };
    $scope.getStatus();

    $scope.getPriority = function () {
        $http({
            method: 'GET',
            url: 'api/Priorities',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.priority = response;
        }).error(function (response) {

        });
    };
    $scope.getPriority();

    $scope.getActs = function () {
        $http({
            method: 'GET',
            url: 'api/Acts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.acts = response;
        }).error(function (response) {

        });
    };
    $scope.getActs();
    $scope.editCase = function () {



        $scope.advocateData = [];
        for(var i=0;i<$scope.updateCase.advocate.length;i++){

            if((typeof $scope.updateCase.advocate[i]) == 'object'){
                $scope.advocateData.push($scope.updateCase.advocate[i]);
            }else if((typeof $scope.updateCase.advocate[i]) == 'string'){
                $scope.advocateData.push(JSON.parse($scope.updateCase.advocate[i]));
            }

        }
        $scope.updateCase.advocate = [];

        for(var i=0;i<$scope.advocateData.length;i++){

            $scope.updateCase.advocate.push({
                name:$scope.advocateData[i].name,
                email:$scope.advocateData[i].email,
                advId:$scope.advocateData[i].advId,
                phoneNumber:$scope.advocateData[i].phoneNumber
            });

        }

        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
        $scope.updateCase['lastEditPerson'] = loginPersonDetails.employeeId;

        $http({
            method: 'PUT',
            url: 'api/CreateCases/' + $rootScope.updateCase.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $rootScope.updateCase
        }).success(function (response) {

            $scope.loadingImage = false;
            $scope.selectCaseNumb=response.caseNumber;
            $window.localStorage.setItem('caseUpdationCheck', $scope.selectCaseNumb);
            location.href = '#/case';
            $window.location.reload();

        }).error(function (response) {

        });

    };
    $scope.cancelEdit = function () {
        $scope.getCase();
        $('#caseEdit').modal('hide');
    };

    $scope.checkAdvocateSelected = function(advocate){
        var check;
        for(var i=0; i < $rootScope.updateCase.advocate.length; i++) {
            if ($rootScope.updateCase.advocate[i].email == advocate.email) {
                check = true;
                break;
            }
        }

        if(check){
            return true;
        }else{
            return false;
        }
    };

}]);

app.controller('caseUploadController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {


    $scope.uploadFile = uploadFileURL;

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseUpload").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseUpload .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'docUpload'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.dtOptions = {paging: false, searching: false};
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.caseList = response;
        }).error(function (response) {

        });


    }
    $scope.getCaseNumber();



    var formIdsArray = [];
    var formUploadStatus = false;
    $scope.errorMssg1 = false;
    $scope.fileUploadSuccess = false;
    $scope.uploadDocuments = function (files) {

        formIdsArray = [];

        $scope.forms = files;
        var fileCount = 0;

        angular.forEach(files, function (file) {
            $scope.errorMssg1 = true;
            $scope.fileUploadSuccess = false;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.errorMssg1 = false;
                $scope.fileUploadSuccess = true;
                $scope.uploadError = false;
                $scope.uploadErrorMessage = '';
            }
        });
    };

    $scope.uploadError = false;
    $scope.uploadErrorMessage = '';
    $scope.caseUploads = function () {

        if (formUploadStatus) {
            if($scope.upload && $scope.upload.comments) {
                var uploadDetails = $scope.upload;
                uploadDetails.file = $scope.docList;
                var caseNumber = $scope.selectCaseNumber;
                uploadDetails['caseNumber'] = caseNumber;
                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                uploadDetails['createdPerson'] = loginPersonDetails.name;
                uploadDetails['uploadedPersonName'] = loginPersonDetails.name;

                uploadDetails.status = 'active';
                $http({
                    method: 'POST',
                    url: 'api/CaseUploads',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": uploadDetails
                }).success(function (response) {

                    $scope.loadingImage=false;
                    $("#scroll2").hide();
                    $("#scroll").show();
                    $scope.errorMssg1 = false;
                    $scope.fileUploadSuccess = false;
                    $scope.uploadError = false;
                    $scope.uploadErrorMessage = '';
                    $scope.upload = {};
                    $scope.docList = [];
                    $scope.getDocuments();
                }).error(function (response) {

                });

            }else{
                $scope.uploadError = true;
                $scope.uploadErrorMessage = 'Please enter the comment';
            }
        } else {
            $scope.uploadError = true;
            $scope.uploadErrorMessage = 'Please attach the files.';

        }
    }

    $scope.uploadHearingError = false;
    $scope.uploadHearingErrorMessage = '';
    $scope.hearingUploads = function () {

        if (formUploadStatus) {
            if($scope.upload && $scope.upload.comments) {
                var uploadHearingDetails = $scope.upload;
                uploadHearingDetails.file = $scope.docList;
                var caseNumbers = $scope.selectCaseNumber;
                uploadHearingDetails['caseNumber'] = caseNumbers;
                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                uploadHearingDetails['createdPerson'] = loginPersonDetails.name;
                uploadHearingDetails['uploadedPersonName'] = loginPersonDetails.name;

                uploadHearingDetails.status = 'active';
                $http({
                    method: 'POST',
                    url: 'api/HearingUploads',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": uploadHearingDetails
                }).success(function (response) {

                    $scope.loadingImage=false;
                    $("#scroll22").hide();
                    $("#scroll").show();
                    $scope.errorMssg12 = false;
                    $scope.fileUploadSuccesss = false;
                    $scope.uploadHearingError = false;
                    $scope.uploadHearingErrorMessage = '';
                    $scope.upload = {};
                    $scope.docList = [];
                    $scope.getHearingDocs();
                }).error(function (response) {

                });

            }else{
                $scope.uploadHearingError = true;
                $scope.uploadHearingErrorMessage = 'Please enter the comment';
            }
        } else {
            $scope.uploadHearingError = true;
            $scope.uploadHearingErrorMessage = 'Please attach the files.';

        }
    }



    $scope.deleteFile = function(fileId, index){

        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);

        });
    }


    $scope.docUploadURL = uploadFileURL;
    $scope.getDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/CaseUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.documentsList = response;
            $scope.loadingImage=false;

        }).error(function (response) {

        });

    }



    $scope.getHearingDocs = function () {

        $http({
            method: 'GET',
            url: 'api/HearingUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.hearingDocsList = response;
            $scope.loadingImage=false;

        }).error(function (response) {

        });

    }


    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.selectCaseDetails = response[0];

        }).error(function (response) {

        });

        $scope.getDocuments();
        $scope.getHearingDocs();

    }


    $scope.reset = function () {

        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;
    };

    $scope.cancel=function(){
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;
        $("#cancel").click(function () {
            $("#scroll2").hide()

            $("#scroll").show();

            ;
        });
        $("#cancell").click(function () {

            $("#scroll22").hide()

            $("#scroll1").show();
            ;
        });
    }

    $(function () {
        $("#scroll2").hide();
        $("#scroll22").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });

        $("#scroll1").click(function () {
            $("#scroll22").show();
            $("#scroll1").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll22").offset().top
                },
                'slow');
        });

        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
        $("#cancell").click(function () {
            $("#scroll22").hide()
            $("#scroll1").show();
            ;
        });
    })
    $scope.reset();
}]);

/*app.controller('createPaymentsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'paymentMode'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var moneyNumeric= /^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$/;

    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {

        });
    };
    $scope.getCaseNumber();
    $scope.paymentMode=["Cash","Cheque","DD"];


    $scope.reset = function () {
        $scope.payment = angular.copy($scope.master);
    };

    $scope.payment = {};
    $scope.paymentCheck=true;

    $scope.createPayment = function () {
        $scope.createPaymentError=false;
        $scope.createPaymentMessage='';

        $scope.advocateData = [];
        $scope.advocateData.push(JSON.parse($scope.payment.advocateName));
        $scope.payment.advocateName=[];

        for(var i=0;i<$scope.advocateData.length;i++){
            $scope.payment.advocateName.push($scope.advocateData[i].advId);
        }
        $scope.selectCaseNumber=$scope.payment.caseNumber;

        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));


        $scope.payment['createdPerson'] = loginPersonDetails.employeeId;

        if($scope.payment.date !=undefined){
            if($scope.paymentCheck==true) {
                $scope.paymentCheck = false;
                $http({
                    method: 'POST',
                    url: 'api/AdvocatePayments',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.payment
                }).success(function (response) {

                    $scope.loadingImage=false;
                    $scope.paymentCheck == true;
                    $scope.selectAdvocate = response.advocateName;
                    $scope.selectCaseNumber = response.caseNumber;
                    $window.localStorage.setItem('CreationCheck', $scope.selectAdvocate);
                    $window.localStorage.setItem('paymentCreationCheck', $scope.selectCaseNumber);

                    location.href = '#/advocatePayment';
                    $window.location.reload();
                }).error(function (response) {

                    $scope.paymentCheck == true;
                });
            }
        }
        else{

            $scope.createPaymentError=true;
            $scope.createPaymentMessage='Please Select Payment Payment Date';
            $scope.paymentCheck == true;
        }
    };

    $scope.getCaseNumber();

    $scope.modeChange=function (paymentMode) {

        if(paymentMode=="Cheque" || paymentMode=="DD" )
        {
            $scope.onCash=true;
        }
        else {
            $scope.onCash=false;
        }

    }

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.advocates = response;

        }).error(function (response) {

        });
    };
    $scope.getAdvocates();

    $scope.reset();


}]);*/

app.controller('createPaymentsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'paymentMode'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var moneyNumeric= /^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$/;

    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {

        });
    };
    $scope.getCaseNumber();
    $scope.paymentMode=["Cash","Cheque","DD"];


    $scope.reset = function () {
        $scope.payment = angular.copy($scope.master);
    };

    $scope.payment = {};
    $scope.paymentCheck=true;

    $scope.changeAdvocateList= function(caseIndex){
        for(var i=0;i<$scope.caseNumber.length;i++){
            //consloe.log($scope.caseNumber[i]+'--');
            if($scope.caseNumber[i].caseNumber==caseIndex){
                $scope.advList=$scope.caseNumber[i].advocate;
                break;
            }
        }
    };

    $scope.createPayment = function () {
        $scope.createPaymentError=false;
        $scope.createPaymentMessage='';

        $scope.advocateData = [];
        $scope.advocateData.push(JSON.parse($scope.payment.advocateName));
        $scope.payment.advocateName=[];

        for(var i=0;i<$scope.advocateData.length;i++){
            $scope.payment.advocateName.push($scope.advocateData[i].advId);
        }
        $scope.selectCaseNumber=$scope.payment.caseNumber;

        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));


        $scope.payment['createdPerson'] = loginPersonDetails.employeeId;

        if($scope.payment.date !=undefined){
            if($scope.paymentCheck==true) {
                $scope.paymentCheck = false;
                $http({
                    method: 'POST',
                    url: 'api/AdvocatePayments',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.payment
                }).success(function (response) {

                    $scope.loadingImage=false;
                    $scope.paymentCheck == true;
                    $scope.selectAdvocate = response.advocateName;
                    $scope.selectCaseNumber = response.caseNumber;
                    $window.localStorage.setItem('CreationCheck', $scope.selectAdvocate);
                    $window.localStorage.setItem('paymentCreationCheck', $scope.selectCaseNumber);

                    location.href = '#/advocatePayment';
                    $window.location.reload();
                }).error(function (response) {

                    $scope.paymentCheck == true;
                });
            }
        }
        else{

            $scope.createPaymentError=true;
            $scope.createPaymentMessage='Please Select Payment Payment Date';
            $scope.paymentCheck == true;
        }
    };

    $scope.getCaseNumber();

    $scope.modeChange=function (paymentMode) {

        if(paymentMode=="Cheque" || paymentMode=="DD" )
        {
            $scope.onCash=true;
        }
        else {
            $scope.onCash=false;
        }

    }

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.advocates = response;

        }).error(function (response) {

        });
    };
    $scope.getAdvocates();

    $scope.reset();


}]);
app.controller('courtController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder' , function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.court").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.court .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'courtInfo'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.getCourts = function () {
        $http({
            "method": "GET",
            "url": 'api/Courts',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.courtData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getCourts();
    $scope.reset = function () {
        $scope.court = angular.copy($scope.master);
        $scope.courtCreationError = false;
        $scope.createCourtTypeError = '';
    };
    $scope.courtClick=function(){
        $('#createCourtModal').modal('show');
        $scope.court = {};
        $scope.courtCreationError = false;
        $scope.createCourtTypeError = '';
    }
    $scope.court = {};
    $scope.courtCheck=true;
    $scope.createCourts = function () {

        $scope.courtCreationError = false;
        $scope.createCourtTypeError = '';

        if($scope.courtCheck==true) {
            $scope.courtCheck=false;
            if ($scope.court.name.match(alphabetsWithSpaces) && $scope.court.name) {


                $http({
                    method: 'POST',
                    url: 'api/Courts',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.court
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.courtCheck=true;
                    $rootScope.courtData.push(response);
                    $('#createCourtModal').modal('hide');
                    $("#addSuccess").modal("show");
                    setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                    $scope.getCourts();
                }).error(function (response) {

                    if (response.error.details.messages.name) {
                        $scope.createCourtTypeError = response.error.details.messages.name[0];
                        $scope.courtCreationError = true;
                        $scope.courtCheck=true;
                    }

                });
            } else {
                $scope.createCourtTypeError = 'Please Enter the Court Name in the Alphabets';
                $scope.courtCreationError = true;
                $scope.courtCheck=true;

            }
        }
    };
    $scope.updateCourts = {};
    $scope.editCourtPopup = function (courtInfo) {

        $scope.updateCourts = courtInfo;

    };

    $scope.editCourts = function () {
        $scope.courtUpdationError = false;
        $scope.updateCourtTypeError = '';

        if ($scope.updateCourts.name.match(alphabetsWithSpaces)) {

            $http({
                method: 'PUT',
                url: 'api/Courts/' + $scope.updateCourts.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updateCourts
            }).success(function (response) {

                $scope.loadingImage=false;

                $('#caseTypeEdit').modal('hide');
                $("#editSuccess").modal("show");
                setTimeout(function(){$('#editSuccess').modal('hide')}, 3000);
                $scope.getCourts();
            }).error(function (response) {

                if (response.error.details.messages.name) {
                    $scope.updateCourtTypeError = response.error.details.messages.name[0];
                    $scope.courtUpdationError = true;
                }

                $timeout(function (){
                    $scope.courtUpdationError = false;
                },3000)
            });
        } else {
            $scope.updateCourtTypeError = 'Please Enter the Court Name in Alphabets';
            $scope.courtUpdationError = true;
            $timeout(function (){
                $scope.courtUpdationError = false;
            },3000)

        }

    };
    $scope.cancelEdit = function () {
        $scope.getCourts();
        $('#caseTypeEdit').modal('hide');
    };

    $scope.reset();
    $scope.exportToExcel=function(tableId){

        if( $scope.courtData!=null &&  $scope.courtData.length>0){

            $("<tr>" +
                "<th>Court Name</th>" +
                "<th>Court Jurisdiction</th>" +
                "<th>City</th>" +
                "<th>Address</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.courtData.length;i++){
                var paymentData=$scope.courtData[i];
                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.name)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.jurisdiction)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.city)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.address)+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'court');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);

app.controller('createNoticeController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'noticeComments'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

    $scope.getNotice = function () {
        $http({
            "method": "GET",
            "url": 'api/NoticeComments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.noticeData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getNotice();

    $scope.choiceSet = {choices: []};

    $scope.choiceSet.choices = [];
    $scope.addNewChoice = function () {
        $scope.choiceSet.choices.push('');
    };

    $scope.removeChoice = function (z) {

        $scope.choiceSet.choices.splice(z,1);
    };

    $scope.createNotice = function () {
        $scope.createNoticeError= false;
        $scope.createNoticeMessage = '';

        var comment=$scope.choiceSet.choices;

        $scope.notice["comment"]=comment;


        if($scope.choiceSet.choices == ''){
            //alert("Please Enter Comment");
            $scope.createNoticeError= true;
            $scope.createNoticeMessage = 'Please Add a Comment For Notice';
        }else{

            $http({
                method: 'POST',
                url: 'api/NoticeComments',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.notice
            }).success(function (response) {

                $scope.loadingImage=false;
                $rootScope.noticeData.push(response);
                $scope.selectCaseNumber=response.caseNumber;
                $window.localStorage.setItem('noticeCreationCheck',$scope.selectCaseNumber);

                location.href = '#/noticeComments';
            }).error(function (response) {

                if(response.error.details.messages.noticeNumber[0]){
                    $scope.createNoticeError= true;
                    $scope.createNoticeMessage = response.error.details.messages.noticeNumber[0];
                }


            });

        }

    };

    $scope.reset = function () {
        $scope.notice = angular.copy($scope.master);
        $scope.choiceSet.choices = [];

    };
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {

        });
    };
    $scope.getCaseNumber();


    $scope.reset();

}]);

app.controller('caseDepartmentController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.caseDepartment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.caseDepartment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'caseDepartments'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;

    $scope.getDepartments = function () {
        $http({
            "method": "GET",
            "url": 'api/caseDepartments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.caseDepartmentData = response;
        }).error(function (response, data) {

        });
    };
    $scope.getDepartments();

    $scope.reset = function () {
        $scope.caseDepartment = angular.copy($scope.master);
        $scope.caseCreationError = false;
        $scope.createCaseDepartementError = '';
    };

    $scope.caseDepartmentClick=function(){
        $('#createCaseDepartmentModal').modal('show');
        $scope.caseDepartment = {};
        $scope.caseCreationError = false;
        $scope.createCaseDepartementError = '';
    }

    $scope.caseDepartment = {};
    $scope.depCheck=true;
    $scope.createCaseDepartment = function () {

        $scope.caseCreationError = false;
        $scope.createCaseDepartementError = '';

        if($scope.depCheck==true){
            $scope.depCheck=false;
            if ($scope.caseDepartment.name.match(alphabetsWithSpaces) && $scope.caseDepartment.name) {


                $http({
                    method: 'POST',
                    url: 'api/caseDepartments',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.caseDepartment
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.depCheck=true;
                    $rootScope.caseDepartmentData.push(response);
                    $("#addSuccess").modal("show");
                    setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                    $scope.getDepartments();

                    $('#createCaseDepartmentModal').modal('hide');
                }).error(function (response) {

                    if (response.error.details.messages.name) {
                        $scope.createCaseDepartementError = response.error.details.messages.name[0];
                        $scope.caseCreationError = true;
                        $scope.depCheck=true;
                    }
                });
            }

            else {
                $scope.createCaseDepartementError = 'Please Enter the Case Department Name in the Alphabets';
                $scope.caseCreationError = true;
                $scope.depCheck=true;
            }}
    };
    $scope.updateCaseDepartments = {};

    $scope.editCaseDepartmentPopup = function (caseDepartmentInfo) {

        $scope.updateCaseDepartments = caseDepartmentInfo;

    };

    $scope.editCaseDepartments = function () {

        $scope.caseDepartmentUpdationError = false;
        $scope.updateCaseDepartmentError = '';

        if ($scope.updateCaseDepartments.name.match(alphabetsWithSpaces) && $scope.updateCaseDepartments.name) {

            $http({
                method: 'PUT',
                url: 'api/caseDepartments/' + $scope.updateCaseDepartments.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updateCaseDepartments
            }).success(function (response) {

                $scope.loadingImage=false;
                //$scope.updateCaseDepartments = {};
                $('#caseDepartmentEdit').modal('hide');
                $("#editSuccess").modal("show");
                setTimeout(function(){$('#editSuccess').modal('hide')}, 3000);
                $scope.getDepartments();

            }).error(function (response) {

                if (response.error.details.messages.name) {
                    $scope.updateCaseDepartmentError = response.error.details.messages.name[0];
                    $scope.caseDepartmentUpdationError = true;
                }
                $timeout(function (){
                    $scope.caseDepartmentUpdationError = false;
                },3000)
            });
        }
        else {
            $scope.updateCaseDepartmentError = 'Please Edit the Court Name in the Correct Format';
            $scope.caseDepartmentUpdationError = true;
            $timeout(function (){
                $scope.caseDepartmentUpdationError = false;
            },3000)
        }

    };

    $scope.cancelEdit = function () {
        $scope.getDepartments();
        $('#caseDepartmentEdit').modal('hide');
    };
    $scope.reset();

    $scope.exportToExcel=function(tableId){

        if( $scope.caseDepartmentData!=null &&  $scope.caseDepartmentData.length>0){

            $("<tr>" +
                "<th>Case Department Name</th>" +
                "<th>Case Department HeadQuarter</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.caseDepartmentData.length;i++){
                var paymentData=$scope.caseDepartmentData[i];

                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.name)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.headQuarter)+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'caseDepartment');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);

app.controller('writePetitionController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'writePetition'}, function (response) {
        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.getPetition = function () {
        $http({
            "method": "GET",
            "url": 'api/Petitions',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.petitionData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getPetition();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.petition = {};


    $scope.getEditWritePetition=function (petitionInfo) {


        $rootScope.updateWritePetition=petitionInfo;
    }

    if($window.localStorage.getItem('petitionCreationCheck')){
        $scope.caseShow=true;
        $scope.caseHide=false;
        $scope.selectedCaseNumber = $window.localStorage.getItem('petitionCreationCheck')
        $window.localStorage.removeItem('petitionCreationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }

    if($window.localStorage.getItem('petitionUpdationCheck')){
        $scope.caseShow=false;
        $scope.caseHide=true;
        $scope.selectedCaseNumb = $window.localStorage.getItem('petitionUpdationCheck')
        $window.localStorage.removeItem('petitionUpdationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }

    $scope.exportToExcel=function(tableId){

        if( $scope.petitionData!=null &&  $scope.petitionData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Petition Id</th>" +
                "<th>Petition Date</th>" +
                "<th>Petitioner</th>" +
                "<th>Respondent</th>" +

                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.petitionData.length;i++){
                var paymentData=$scope.petitionData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.petitionId)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.petitionDate)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.petitioner)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.respondent)+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'writePetition');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);



app.controller('paymentModeController', function ($http, $scope, Upload, $timeout, $window, $location, $rootScope, $q) {


    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.getPaymentModes = function () {
        $http({
            "method": "GET",
            "url": 'api/PaymentModes',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $rootScope.paymentData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getPaymentModes();
    $scope.reset = function () {
        $scope.payment = angular.copy($scope.master);
    };
    $scope.payment = {};
    $scope.payCheck=true;
    $scope.createPayment = function () {
        $scope.paymentCreationError = false;
        $scope.createPaymentTypeError = '';

        if($scope.payCheck==true){
            $scope.payCheck=false;

            if($scope.payment.name.match(alphabetsWithSpaces) && $scope.payment.name){
                $http({
                    method: 'POST',
                    url: 'api/PaymentModes',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.payment
                }).success(function (response) {
                    $scope.payCheck=true;
                    $rootScope.paymentData.push(response);
                    $scope.payment = {};
                    $('#myModal').modal('hide');
                }).error(function (response) {

                    if (response.error.details.messages.name) {
                        $scope.createPaymentTypeError = response.error.details.messages.name[0];
                        $scope.paymentCreationError = true;
                        $scope.payCheck=true;
                    }

                });
            }else {
                $scope.createPaymentTypeError = 'Please Enter the Payment Mode Name in the Correct Format';
                $scope.paymentCreationError = true;
                $scope.payCheck=true;

            }}

    };
    $scope.updatePayment = {};
    $scope.editPaymentPopup = function (paymentInfo) {

        $scope.updatePayment = paymentInfo;
    };

    $scope.editPayment = function () {
        $scope.paymentUpdationError = false;
        $scope.updatePaymentTypeError = '';

        if($scope.updatePayment.name.match(alphabetsWithSpaces) && $scope.updatePayment.name){


            $http({
                method: 'PUT',
                url: 'api/PaymentModes/' + $scope.updatePayment.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updatePayment
            }).success(function (response) {

                $scope.updatePayment = {};
                $scope.getPaymentModes();
                $('#paymentEdit').modal('hide');
            }).error(function (response) {

                if (response.error.details.messages.name) {
                    $scope.updatePaymentTypeError = response.error.details.messages.name[0];
                    $scope.paymentUpdationError = true;
                }
            });
        }else {
            $scope.updatePaymentTypeError = 'Please Update the Payment Mode in the Correct Format';
            $scope.paymentUpdationError = true;

        }
    };
    $scope.cancelEdit = function () {
        $scope.getPaymentModes();
        $('#paymentEdit').modal('hide');
    };
    $scope.reset();
});

app.controller('caseReportsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', 'Excel', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, Excel, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseReports").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseReports .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'caseReports'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

    $scope.getCourt = function(){
        $http({
            method: 'GET',
            url: 'api/Courts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.courtList = response;

        }).error(function (response) {

        });


    }
    $scope.getCourt();

    $scope.getCaseType = function(){
        $http({
            method: 'GET',
            url: 'api/CaseTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.caseTypeList = response;

        }).error(function (response) {

        });


    }
    $scope.getCaseType();

    $scope.getPriority = function(){
        $http({
            method: 'GET',
            url: 'api/Priorities',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.casePriorityList = response;

        }).error(function (response) {

        });


    }
    $scope.getPriority();

    $scope.getStatus = function(){
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.caseStatusList = response;

        }).error(function (response) {

        });


    }
    $scope.getStatus();

    $scope.getHearing = function(){
        $http({
            method: 'GET',
            url: 'api/HearingDates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;

            $rootScope.caseHearingList = response;
            var mySet = new Set();
            for(var i=0;i<response.length;i++){


                mySet.add(response[i].dateHearing);

            }
            $scope.hearingLists = Array.from(mySet);

        }).error(function (response) {

        });


    }
    $scope.getHearing();

    $scope.getPayment = function(){
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.casePaymentList = response;


        }).error(function (response) {

        });


    }
    $scope.getPayment();

    $scope.names = ["Court Wise Report", "Case Type Wise Report","Case Priority Wise Report","Case own/lost/appeals made Report",
        "Case Hearing Date Wise Report","Payments made to Legal Advisor"];

    $scope.selectareas = function (courtName) {
        $scope.selectCourt = courtName;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"courtName":"' + $scope.selectCourt + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCaseDetails = response;
        }).error(function (response) {

        });
    }

    $scope.selectCaseTypeareas = function (caseType) {
        $scope.selectcaseType = caseType;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"caseType":"' + $scope.selectcaseType + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCaseTypeDetails = response;
        }).error(function (response) {

        });
    }

    $scope.selectCasePriorityareas = function (priority) {
        $scope.selectcasePriority = priority;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"priority":"' + $scope.selectcasePriority + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCasePriorityDetails = response;

        }).error(function (response) {

        });
    }

    $scope.selectCaseStatusareas = function (status) {
        $scope.selectcaseStatus = status;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"status":"' + $scope.selectcaseStatus + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCaseStatusDetails = response;

        }).error(function (response) {

        });
    }

    $scope.selectCaseHearingareas = function (dateHearing) {
        $scope.selectcaseHearing = dateHearing;

        $http({
            method: 'GET',
            url: 'api/HearingDates/?filter={"where":{"dateHearing":"' + $scope.selectcaseHearing + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCaseHearingDetails = response;

        }).error(function (response) {

        });
    }

    $scope.selectCasePaymentareas = function (name) {
        $scope.selectcasePayment = name;

        $http({
            method: 'GET',
            url: 'api/AdvocatePayments/?filter={"where":{"advocateName":"' + $scope.selectcasePayment + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCasePaymentDetails = response;

        }).error(function (response) {

        });
    }


    $scope.modeChange=function (names) {

        if(names=="Court Wise Report" )
        {
            $scope.court=true;
            $scope.case=false;
            $scope.priority=false;
            $scope.status=false;
            $scope.hearing=false;
            $scope.payment=false;

        }
        else if(names=="Case Type Wise Report"){
            $scope.priority=false;
            $scope.case=true;
            $scope.court=false;
            $scope.status=false;
            $scope.hearing=false;
            $scope.payment=false;
        }
        else if(names=="Case Priority Wise Report"){
            $scope.priority=true;
            $scope.case=false;
            $scope.court=false;
            $scope.status=false;
            $scope.hearing=false;
            $scope.payment=false;
        }
        else if(names=="Case own/lost/appeals made Report"){
            $scope.status=true;
            $scope.priority=false;
            $scope.case=false;
            $scope.court=false;
            $scope.hearing=false;
            $scope.payment=false;
        }
        else if(names=="Case Hearing Date Wise Report"){
            $scope.status=false;
            $scope.priority=false;
            $scope.case=false;
            $scope.court=false;
            $scope.hearing=true;
            $scope.payment=false;
        }
        else if(names=="Payments made to Legal Advisor"){
            $scope.payment=true;
            $scope.status=false;
            $scope.priority=false;
            $scope.case=false;
            $scope.court=false;
            $scope.hearing=false;
        }

        else {
            $scope.payment=false;
            $scope.court=false;
            $scope.priority=false;
            $scope.case=false;
            $scope.status=false;
            $scope.hearing=false;
        }

    }


    $scope.exportToExcelCourt=function(tableId){

        if( $scope.selectCaseDetails!=null &&  $scope.selectCaseDetails.length>0){

            $("<tr>" +
                "<th>Case Id</th>" + "<th>Case Date</th>" + "<th>Case Type</th>" + "<th>Priority</th>" + "<th>Status</th>" + "<th>Court Name</th>" +
                "<th>Advocate</th>" + "<th>Case Details</th>" +
                "<th>Act</th>" + "<th>Department</th>" +
                "<th>Empannel</th>" +
                "<th>Created Time</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.selectCaseDetails.length;i++){
                var schemes=$scope.selectCaseDetails[i];

                if(schemes.advocate != null) {
                    $scope.schemeIs= '';
                    for(var j=0; j<schemes.advocate.length;j++) {
                        $scope.schemeIs=$scope.schemeIs + ','+schemes.advocate[j].name;

                    }
                }

                $("<tr>" +
                    "<td>"+capitalizeWord(schemes.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseDate)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseType)+"</td>" +
                    "<td>"+capitalizeWord(schemes.priority)+"</td>" +
                    "<td>"+capitalizeWord(schemes.status)+"</td>" +
                    "<td>"+capitalizeWord(schemes.courtName)+"</td>" +
                    "<td>"+capitalizeWord($scope.schemeIs)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseDetails)+"</td>" +
                    "<td>"+capitalizeWord(schemes.act)+"</td>" +
                    "<td>"+capitalizeWord(schemes.department)+"</td>" +
                    "<td>"+capitalizeWord(schemes.empannel)+"</td>" +
                    "<td>"+capitalizeWord(schemes.createdTime)+"</td>" +
                    "</tr>").appendTo("table"+tableId);

            }
            var exportHref = Excel.tableToExcel(tableId, 'Court Report Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

    $scope.exportToExcelCase=function(tableId){

        if( $scope.selectCaseTypeDetails!=null &&  $scope.selectCaseTypeDetails.length>0){

            $("<tr>" +
                "<th>Case Id</th>" +
                "<th>Case Date</th>" +
                "<th>Case Type</th>" +
                "<th>Priority</th>" +
                "<th>Status</th>" +
                "<th>Court Name</th>" +
                "<th>Advocate</th>" +
                "<th>Act</th>" +
                "<th>Department</th>" +
                "<th>Empannel</th>" +
                "<th>created Time</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.selectCaseTypeDetails.length;i++){
                var schemes=$scope.selectCaseTypeDetails[i];

                if(schemes.advocate != null) {
                    $scope.schemeIs= '';
                    for(var j=0; j<schemes.advocate.length;j++) {
                        $scope.schemeIs=$scope.schemeIs + ','+schemes.advocate[j].name;

                    }
                }

                $("<tr>" +
                    "<td>"+capitalizeWord(schemes.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseDate)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseType)+"</td>" +
                    "<td>"+capitalizeWord(schemes.priority)+"</td>" +
                    "<td>"+capitalizeWord(schemes.status)+"</td>" +
                    "<td>"+capitalizeWord(schemes.courtName)+"</td>" +
                    "<td>"+capitalizeWord($scope.schemeIs)+"</td>" +
                    "<td>"+capitalizeWord(schemes.act)+"</td>" +
                    "<td>"+capitalizeWord(schemes.department)+"</td>" +
                    "<td>"+capitalizeWord(schemes.empannel)+"</td>" +
                    "<td>"+capitalizeWord(schemes.createdTime)+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Case Type Report Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

    $scope.exportToExcelPriority=function(tableId){

        if( $scope.selectCasePriorityDetails!=null &&  $scope.selectCasePriorityDetails.length>0){

            $("<tr>" +
                "<th>Case Id</th>" +
                "<th>Case Date</th>" +
                "<th>Case Type</th>" +
                "<th>Priority</th>" +
                "<th>Status</th>" +
                "<th>Court Name</th>" +
                "<th>advocate</th>" +
                "<th>Case Details</th>" +
                "<th>Act</th>" +
                "<th>Department</th>" +
                "<th>Empannel</th>" +
                "<th>Created Time</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.selectCasePriorityDetails.length;i++){
                var schemes=$scope.selectCasePriorityDetails[i];

                if(schemes.advocate != null) {
                    $scope.schemeIs= '';
                    for(var j=0; j<schemes.advocate.length;j++) {
                        $scope.schemeIs=$scope.schemeIs + ','+schemes.advocate[j].name;
                    }
                }

                $("<tr>" +
                    "<td>"+capitalizeWord(schemes.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseDate)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseType)+"</td>" +
                    "<td>"+capitalizeWord(schemes.priority)+"</td>" +
                    "<td>"+capitalizeWord(schemes.status)+"</td>" +
                    "<td>"+capitalizeWord(schemes.courtName)+"</td>" +
                    "<td>"+capitalizeWord($scope.schemeIs)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseDetails)+"</td>" +
                    "<td>"+capitalizeWord(schemes.act)+"</td>" +
                    "<td>"+capitalizeWord(schemes.department)+"</td>" +
                    "<td>"+capitalizeWord(schemes.empannel)+"</td>" +
                    "<td>"+capitalizeWord(schemes.createdTime)+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Priority Report Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

    $scope.exportToExcelStatus=function(tableId){

        if( $scope.selectCaseStatusDetails!=null &&  $scope.selectCaseStatusDetails.length>0){

            $("<tr>" +
                "<th>Case Id</th>" +
                "<th>Case Date</th>" +
                "<th>Case Type</th>" +
                "<th>Priority</th>" +
                "<th>Status</th>" +
                "<th>Court Name</th>" +
                "<th>Advocate</th>" +
                "<th>Case Details</th>" +
                "<th>Act</th>" +
                "<th>Department</th>" +
                "<th>Empannel</th>" +
                "<th>Created Time</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.selectCaseStatusDetails.length;i++){
                var schemes=$scope.selectCaseStatusDetails[i];

                if(schemes.advocate != null) {
                    $scope.schemeIs= '';
                    for(var j=0; j<schemes.advocate.length;j++) {
                        $scope.schemeIs=$scope.schemeIs + ','+schemes.advocate[j].name;
                    }
                }

                $("<tr>" +
                    "<td>"+capitalizeWord(schemes.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseDate)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseType)+"</td>" +
                    "<td>"+capitalizeWord(schemes.priority)+"</td>" +
                    "<td>"+capitalizeWord(schemes.status)+"</td>" +
                    "<td>"+capitalizeWord(schemes.courtName)+"</td>" +
                    "<td>"+capitalizeWord($scope.schemeIs)+"</td>" +
                    "<td>"+capitalizeWord(schemes.caseDetails)+"</td>" +
                    "<td>"+capitalizeWord(schemes.act)+"</td>" +
                    "<td>"+capitalizeWord(schemes.department)+"</td>" +
                    "<td>"+capitalizeWord(schemes.empannel)+"</td>" +
                    "<td>"+capitalizeWord(schemes.createdTime)+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Case Status Wise Report Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

    $scope.exportToExcelHearing=function(tableId){

        if( $scope.selectCaseHearingDetails!=null &&  $scope.selectCaseHearingDetails.length>0){

            $("<tr>" +
                "<th>Case Id</th>" +
                "<th>Date Hearing</th>" +
                "<th>Status</th>" +
                "<th>Advocate Name</th>" +
                "<th>Comment</th>" +
                "<th>Created Time</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.selectCaseHearingDetails.length;i++){
                var schemes=$scope.selectCaseHearingDetails[i];

                if(schemes.advocateName != null) {
                    $scope.schemeIs= '';
                    for(var j=0; j<schemes.advocateName.length;j++) {
                        $scope.schemeIs=$scope.schemeIs + ','+schemes.advocateName[j].name;
                    }
                }

                $("<tr>" +
                    "<td>"+capitalizeWord(schemes.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(schemes.dateHearing)+"</td>" +
                    "<td>"+capitalizeWord(schemes.status)+"</td>" +
                    "<td>"+capitalizeWord($scope.schemeIs)+"</td>" +
                    "<td>"+capitalizeWord(schemes.comment)+"</td>" +
                    "<td>"+capitalizeWord(schemes.createdTime)+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Hearing Report Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

    $scope.exportToExcelPayment=function(tableId){

        if( $scope.selectCasePaymentDetails!=null &&  $scope.selectCasePaymentDetails.length>0){

            $("<tr>" +
                "<th>Case Id</th>" +
                "<th>Amount</th>" +
                "<th>Mode</th>" +
                "<th>Advocate Name</th>" +
                "<th>Description</th>" +
                "<th>Advocate List</th>" +
                "<th>Created Time</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.selectCasePaymentDetails.length;i++){
                var schemes=$scope.selectCasePaymentDetails[i];

                if(schemes.advocateList != null) {
                    $scope.schemeIs= '';
                    for(var j=0; j<schemes.advocateList.length;j++) {
                        $scope.schemeIs=$scope.schemeIs + ','+schemes.advocateList[j].name;
                    }
                }

                $("<tr>" +
                    "<td>"+capitalizeWord(schemes.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(schemes.amount)+"</td>" +
                    "<td>"+capitalizeWord(schemes.mode)+"</td>" +
                    "<td>"+capitalizeWord(schemes.advocateName)+"</td>" +
                    "<td>"+capitalizeWord(schemes.description)+"</td>" +
                    "<td>"+capitalizeWord($scope.schemeIs)+"</td>" +
                    "<td>"+capitalizeWord(schemes.date)+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Payment Report Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

}]);


app.controller('calendarController', function ($http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,$route,DTOptionsBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview .landCalendar").addClass("active");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.reloadRoute = function() {
        $window.location.reload();
    }

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.changeTo = 'Hungarian';
    $scope.eventSources = [];
    $scope.events=[];

    $scope.$on("$routeChangeSuccess",function(){
        $scope.getHearingDate();
    });

    $scope.getHearingDate = function(){
        $http({
            "method":"GET",
            "url":'api/HearingDates',
            "headers":{"Content-Type":"application/json","Accept":"application/json"}
        }).success(function (response){

            $scope.hearingResponse=response;

            for(var i=0;i<response.length;i++){

                var date=response[i].dateHearing;

                date = date.split('-');
                date = date[1]+"/"+date[0]+"/"+date[2];
                date = new Date(date);
                date.setTime(date.getTime() +19800000);

                $scope.events.push({
                    title:response[i].caseNumber,
                    start:date,
                    url:HearingURL + response[i].id+'?filter={"dateHearing":"'+response[i].dateHearing+'"}'
                });
            }
            //$window.localStorage.setItem('events', JSON.stringify(events));
            $scope.getHolidays();
        }).error(function(response,data){

        });
    };


    $scope.getHolidays = function(){
        $http({
            "method":"GET",
            "url":'api/Holidays',
            "headers":{"Content-Type":"application/json","Accept":"application/json"}
        }).success(function (response){
            $scope.holidayResponse=response;
            for(var i=0;i<response.length;i++){
                var date=response[i].festivalDate;
                date = date.split('-');
                date = date[1]+"/"+date[0]+"/"+date[2];
                date = new Date(date);
                date.setTime(date.getTime() +19800000);
                $scope.events.push({
                    title:response[i].festival,
                    start:date,
                    url:HolidayURL
                });
            }
            //$window.localStorage.setItem('events', JSON.stringify(events));
            render();
        }).error(function(response,data){

        });
    };

  /* if($window.localStorage.getItem('events')) {
        $scope.events = JSON.parse($window.localStorage.getItem('events'));
    }*/

    $scope.addEvent = function() {
        $scope.events.push({
            title: 'Open Sesame',
            start: new Date(y, m, 28),
            end: new Date(y, m, 29),
            className: ['openSesame']
        });
    };

    $scope.remove = function(index) {
        $scope.events.splice(index,1);
    };

    $scope.changeView = function(view,calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };

    var render= function(){
        $scope.renderCalendar = function(calendar) {
            $timeout(function() {
                if(uiCalendarConfig.calendars[calendar]){
                    uiCalendarConfig.calendars[calendar].fullCalendar('render');
                }
            });
        };
    }
    $scope.eventRender = function( event, element, view ) {
        element.attr({'tooltip': event.title,
            'tooltip-append-to-body': true});
        $compile(element)($scope);
    };

    $scope.uiConfig = {
        calendar:{
            height: 450,
            editable: true,
            header:{
                left: 'title',
                center: '',
                right: 'today prev,next'
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
        }
    };
    $scope.changeLang = function() {
        if($scope.changeTo === 'Hungarian'){
            $scope.uiConfig.calendar.dayNames = ["Vas?rnap", "H?tf?", "Kedd", "Szerda", "Cs?t?rt?k", "P?ntek", "Szombat"];
            $scope.uiConfig.calendar.dayNamesShort = ["Vas", "H?t", "Kedd", "Sze", "Cs?t", "P?n", "Szo"];
            $scope.changeTo= 'English';
        } else {
            $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
            $scope.changeTo = 'Hungarian';
        }
    };
    $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
    $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];
});

app.controller('createWritePetitionController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'writePetition'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {

        });
    };
    $scope.getCaseNumber();

    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.petition = {};
    $scope.petitionCheck=true;


    $scope.createPetition = function () {


        $scope.petitionCreationError = false;
        $scope.createPetitionError = '';

        if($scope.user.petitionDate){
            if($scope.petitionCheck==true) {
                $scope.petitionCheck = false;


                $http({
                    method: 'POST',
                    url: 'api/Petitions',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.user
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.petitionCheck=true;
                    $rootScope.petitionData.push(response);
                    $scope.selectCaseNumber = response.caseNumber;
                    $window.localStorage.setItem('petitionCreationCheck', $scope.selectCaseNumber);
                    location.href = '#/writePetition';
                }).error(function (response) {

                    if (response.error.details.messages.petitionId) {
                        $scope.createPetitionError = response.error.details.messages.petitionId[0];
                        $scope.petitionCreationError = true;
                        $scope.petitionCheck=true;
                    }
                });
            }
        }else{

            $scope.createPetitionError = 'Please Select Case Petition Date';
            $scope.petitionCreationError = true;
            $scope.petitionCheck=true;

        }

    };

    $scope.reset();

}]);


app.controller('editWritePetitionController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'writePetition'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {

        });
    };

    $scope.getCaseNumber();

    $scope.editWritePetition = function () {

        $scope.petitionUpdationError = false;
        $scope.updatePetitionError = '';
        if($rootScope.updateWritePetition.petitionDate){

            $http({
                method: 'PUT',
                url: 'api/Petitions/' +  $rootScope.updateWritePetition.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:  $rootScope.updateWritePetition
            }).success(function (response) {
                $scope.loadingImage=false;

                $scope.selectCaseNumb=response.caseNumber;
                $window.localStorage.setItem('petitionUpdationCheck',$scope.selectCaseNumb);
                location.href = '#/writePetition';
                $scope.getCaseNumber();

            }).error(function (response) {


                if (response.error.details.messages.petitionId) {
                    $scope.updatePetitionError = response.error.details.messages.petitionId[0];
                    $scope.petitionUpdationError = true;
                }
            });

        }else{
            $scope.petitionUpdationError = true;
            $scope.updatePetitionError = 'Please Select Case Petition Date';
        }

    };
}]);

app.controller('petitionDetailsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'writePetition'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

    /*print cancel reload*/

    (function () {

        var beforePrint = function () {
            //alert('Functionality to run before printing.');
        };

        var afterPrint = function () {
            //alert('Functionality to run after printing');
            $window.location.reload();
        };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');

            mediaQueryList.addListener(function (mql) {
                //alert($(mediaQueryList).html());
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }

        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;

    }());
    /*print cancel reload end*/

    $scope.generatePDF = function() {
        var printContents = document.getElementById('formConfirmation').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

    }
    $scope.getPetitionId = function () {
        $http({
            method: 'GET',
            url: 'api/Petitions',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.petitionList = response;

        }).error(function (response) {

        });


    }
    $scope.getPetitionId();


    $scope.selectareas = function (petitionId) {
        $scope.selectPetitionId = petitionId;

        $http({
            method: 'GET',
            url: 'api/Petitions/?filter={"where":{"petitionId":"' + $scope.selectPetitionId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.selectPetitionDetails = response[0];

        }).error(function (response) {

        });
    }
}]);

app.controller('empannelGroupController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder' ,function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.empannelGroup").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.empannelGroup .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'empannelGroup'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    var alphaNumeric =/^[a-z\d\-_\s]+$/i;
    $scope.getEmpannel = function () {
        $http({
            "method": "GET",
            "url": 'api/EmpannelGroups',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;

            $rootScope.empannelData = response;
        }).error(function (response, data) {

        });
    };
    $scope.getEmpannel();
    $scope.reset = function () {
        $scope.empannel = angular.copy($scope.master);
        $scope.empannelCreationError = false;
        $scope.createEmpannelError = '';
    };
    $scope.empanelClick=function(){
        $('#createEmpanelModal').modal('show');
        $scope.empannel = {};
        $scope.empannelCreationError = false;
        $scope.createEmpannelError = '';
    }

    $scope.empannel = {};
    $scope.empCheck=true;
    $scope.createEmpannel = function () {
        $scope.advocateData = [];
        for(var i=0;i<$scope.empannel.advocateName.length;i++){
            $scope.advocateData.push(JSON.parse($scope.empannel.advocateName[i]));

        }
        $scope.empannel.advocateName=[];
        for(var i=0;i<$scope.advocateData.length;i++){
            $scope.empannel.advocateName.push($scope.advocateData[i].advId);

        }
        $scope.empannelCreationError = false;
        $scope.createEmpannelError = '';

        if($scope.empCheck==true){
            $scope.empCheck=false;
            if ($scope.empannel.groupName.match(alphabetsWithSpaces) && $scope.empannel.groupName) {

                $http({
                    method: 'POST',
                    url: 'api/EmpannelGroups',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.empannel
                }).success(function (response) {
                    $scope.loadingImage=false;

                    $scope.empCheck=true;
                    $rootScope.empannelData.push(response);
                    //$scope.empannel = {};
                    $('#createEmpanelModal').modal('hide');
                    //$rootScope.empannelData();
                    $("#addSuccess").modal("show");
                    setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                    $scope.getEmpannel();
                    //$window.location.reload();
                }).error(function (response) {

                    if (response.error.details.messages.groupName) {
                        $scope.createEmpannelError = response.error.details.messages.groupName[0];
                        $scope.empannelCreationError = true;
                        $scope.empCheck=true;
                    }
                });
            }else{
                $scope.createEmpannelError = 'Please Enter the Group Name in the Correct Format';
                $scope.empannelCreationError = true;
                $scope.empCheck=true;

            }}
    };
    $scope.updateEmpannel = {};
    $scope.editEmpannelPopup = function (empannelInfo) {

        $scope.updateEmpannel = empannelInfo;

    };
    $scope.editEmpannel = function () {


        //alert($scope.updateEmpannel.advocateName[0]+'----'+$scope.updateEmpannel.advocateName.length);
        $scope.empannelUpdationError = false;
        $scope.updateEmpannelError = '';

        $scope.advocateData = [];
        for(var i=0;i<$scope.updateEmpannel.advocateName.length;i++){
            $scope.advocateData.push(($scope.updateEmpannel.advocateName[i]));

        }
        //$scope.updateEmpannel.advocateName = [];

        /* for(var i=0;i<$scope.advocateData.length;i++){
         $scope.updateEmpannel.advocateName.push($scope.advocateData[i].advId);

         }
         */
        if ($scope.updateEmpannel.groupName.match(alphabetsWithSpaces) && $scope.updateEmpannel.groupName) {

            if($scope.updateEmpannel.advocateList){
                delete  $scope.updateEmpannel.advocateList["$$hashKey"]

            }
            var updateEmpannel={
                "groupName":$scope.updateEmpannel.groupName,
                "advocateName":$scope.updateEmpannel.advocateName
            }

            $http({
                method: 'PUT',
                url: 'api/EmpannelGroups/' + $scope.updateEmpannel.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: updateEmpannel
            }).success(function (response) {
                $scope.loadingImage=false;
                $('#empannelEdit').modal('hide');
                $("#editSuccess").modal("show");
                setTimeout(function () {
                    $('#editSuccess').modal('hide')
                }, 3000);
                $scope.getEmpannel();
                //$rootScope.empannelData();
                $scope.getEmpannel();
            }).error(function (response) {

                if (response.error.details.messages.groupName) {
                    $scope.updateEmpannelError = response.error.details.messages.groupName[0];
                    $scope.empannelUpdationError = true;
                }
                $timeout(function (){
                    $scope.empannelUpdationError = false;
                },3000)
            });
        }else{
            $scope.updateEmpannelError = 'Please Enter The Group Name in the Correct Format';
            $scope.empannelUpdationError = true;
            $timeout(function (){
                $scope.empannelUpdationError = false;
            },3000)
        }

    };
    $scope.cancelEdit = function () {
        $scope.getEmpannel();
        $('#empannelEdit').modal('hide');
    };



    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.advocates = response;
        }).error(function (response) {

        });
    };
    $scope.getAdvocates();


    $scope.reset();

    $scope.exportToExcel=function(tableId){

        if( $scope.empannelData!=null &&  $scope.empannelData.length>0){

            $("<tr>" +
                "<th>Group Name</th>" +
                "<th>Advocate Name</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.empannelData.length;i++){
                var paymentData=$scope.empannelData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.groupName)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.advocateList[0].name)+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'empanelGroup');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);

app.controller('judgementController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$timeout', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.judgement").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.judgement .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'judgement'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.getJudgementDetails = function () {
        $http({
            "method": "GET",
            "url": 'api/Judgements',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.judgementData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getJudgementDetails();

    //check weather date is holiday or not
    $http({
        "method": "GET",
        "url": 'api/Holidays',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {

        $scope.loadingImage=false;
        $scope.holidaysData = response;

    }).error(function (response, data) {

    });


    //for create
    $scope.$watch('judgement.judgementDate',function(o,n){
        if(o!=undefined){
            console.log(o+'old----new'+n);
            if(o!=n){
                $scope.hearingDateClickFoCreate();
            }
        }
    });


    $scope.hearingDateClickFoCreate=function(){
        //$scope.hearing = {};
        //$scope.hearing.dateHearing;
        console.log('$rootScope.holidaysData----'+JSON.stringify($scope.holidaysData));
        console.log('$scope.hearing.dateHearing----'+JSON.stringify($scope.judgement.judgementDate));
        angular.forEach($scope.holidaysData,function(date,index){
            if(date.festivalDate==$scope.judgement.judgementDate){
                //Notification.error('The date is a holiday, Please Select Other Date.!');
                $scope.judgementErrorMessage='The date is a holiday, Please Select Other Date.!';
                $scope.judgement.judgementDate='';
                $scope.judgementError=true;
                $timeout(function(){
                    $scope.judgementError = false;
                },3000);
            }
        });
    }
    

    //for update
    $scope.$watch('updateJudgement.judgementDate',function(o,n){
        if(o!=undefined){
            console.log(o+'old----new'+n);
            if(o!=n){
                $scope.hearingDateClick();
            }
        }
    });


    $scope.hearingDateClick=function(){
        //$scope.hearing = {};
        //$scope.hearing.dateHearing;
        console.log('$rootScope.holidaysData----'+JSON.stringify($scope.holidaysData));
        console.log('$scope.hearing.dateHearing----'+JSON.stringify($scope.updateJudgement.judgementDate));
        angular.forEach($scope.holidaysData,function(date,index){
            if(date.festivalDate==$scope.updateJudgement.judgementDate){
                //Notification.error('The date is a holiday, Please Select Other Date.!');
                $scope.judgementUpdateErrorMessage='The date is a holiday, Please Select Other Date.!';
                $scope.updateJudgement.judgementDate='';
                $scope.judgementUpdateError=true;
                $timeout(function(){
                    $scope.judgementUpdateError = false;
                },3000);
            }
        });
    }



    $scope.reset = function () {
        $scope.judgement = angular.copy($scope.master);
        $scope.judgementErrorMessage="";
        $scope.judgementError=false;
    };
    $scope.judgementClick=function(){
        $('#judgementModal').modal('show');
        $scope.judgement = {};
        $scope.judgementErrorMessage="";
        $scope.judgementError=false;

    }
    $scope.judgement = {};
    $scope.judgementCheck=true;
    $scope.createJudgement = function () {
        $scope.judgementErrorMessage="";
        $scope.judgementError=false;
        $scope.judgementCheck=true;
        if($scope.judgement.caseId !=undefined){
            if($scope.judgement.courts!=undefined){
                if($scope.judgement.statuses!=undefined){
                    if($scope.judgementCheck==true) {
                        $scope.judgementCheck = false;
                        if($scope.judgement.judgementDate){

                            var caseNumber = $scope.judgement.caseId.caseNumber;
                            var status = $scope.judgement.statuses.field1;
                            var court = $scope.judgement.courts.name;
                            $scope.judgement['caseNumber']=caseNumber;
                            $scope.judgement['status']=status;
                            $scope.judgement['court']=court;

                            $http({
                                method: 'POST',
                                url: 'api/Judgements',
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                data: $scope.judgement
                            }).success(function (response) {

                                if(response.error){
                                    $scope.judgementErrorMessage=response.error.message;
                                    $scope.judgementError=true;
                                    $scope.judgementCheck=true;
                                    $scope.judgementCheck=false;
                                } else {

                                    $scope.loadingImage=false;
                                    $rootScope.judgementData.push(response);
                                    $scope.selectCaseNumber = response.caseNumber;
                                    $scope.caseShow = true;
                                    $scope.caseHide = false;
                                    $scope.judgementCheck=false;
                                    $("#addDesignation").modal("hide");
                                    $('#addDepartmentSuccess').modal('show');
                                    setTimeout(function () {
                                        $('#addDepartmentSuccess').modal('hide')
                                    }, 9000);

                                    $('#judgementModal').modal('hide');
                                }

                            }).error(function (response) {

                                if(response.error.message){
                                    $scope.judgementErrorMessage=response.error.message;
                                    $scope.judgementError=true;
                                    $scope.judgementCheck=true;
                                }

                                $scope.judgementCheck=true;
                            });
                        }else{

                            $scope.judgementErrorMessage="Please Select Judgement Date";
                            $scope.judgementError=true;
                            $scope.judgementCheck=true;
                        }}
                }else{

                    $scope.judgementError = true;
                    $scope.judgementErrorMessage = 'Please Select Status';
                    $scope.judgementCheck=true;

                }
            }else{

                $scope.judgementError = true;
                $scope.judgementErrorMessage = 'Please Select Court';
                $scope.judgementCheck=true;

            }

        }else{

            $scope.judgementError = true;
            $scope.judgementErrorMessage = 'Please Select Case Number';
            $scope.judgementCheck=true;

        }
    };
    $scope.updateJudgement = {};
    $scope.editJudgementPopup = function (judgementInfo) {

        $scope.updateJudgement = judgementInfo;
    };

    $scope.editJudgement = function () {
        $scope.judgementUpdateErrorMessage="";
        $scope.judgementUpdateError=false;
        if($scope.updateJudgement.judgementDate){

            var caseNumber = $scope.updateJudgement.caseId.caseNumber;
            var status = $scope.updateJudgement.statuses.field1;
            var court = $scope.updateJudgement.courts.name;
            $scope.updateJudgement['caseNumber']=caseNumber;
            $scope.updateJudgement['status']=status;
            $scope.updateJudgement['court']=court;

            $http({

                method: 'PUT',
                url: 'api/Judgements/' + $scope.updateJudgement.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updateJudgement
            }).success(function (response) {
                $scope.loadingImage=false;
                $scope.selectCaseNumb=response.caseNumber;
                $scope.caseShow=false;
                $scope.caseHide=true;
                $("#addDesignation").modal("hide");
                $('#addDepartmentSuccess').modal('show');
                setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
                $('#judgementEdit').modal('hide');

            }).error(function (response) {

            });
        }else{
            $scope.judgementUpdateErrorMessage="Please select Judgement Date";
            $scope.judgementUpdateError=true;
        }
    };
    $scope.cancelEdit = function () {
        $scope.getJudgementDetails();
        $('#judgementEdit').modal('hide');
    };
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {

        });
    };
    $scope.getCaseNumber();

    $scope.getCourts = function () {
        $http({
            "method": "GET",
            "url": 'api/Courts',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.courtData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getCourts();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.status = response;
        }).error(function (response) {

        });
    };

    $scope.getStatus();

    $scope.reset();

    $scope.exportToExcel=function(tableId){

        if( $scope.judgementData!=null &&  $scope.judgementData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Court Name</th>" +
                "<th>Judgement Date</th>" +
                "<th>Status</th>" +
                "<th>Judgement Details</th>" +

                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.judgementData.length;i++){
                var paymentData=$scope.judgementData[i];


                $("<tr>" +
                    "<td>"+capitalizeWord(paymentData.caseNumber)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.court)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.judgementDate)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.status)+"</td>" +
                    "<td>"+capitalizeWord(paymentData.judgementDetails)+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'judgement');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);


app.controller('caseHistoryController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', '$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder',function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,$routeParams, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var btn = $('#expandAll,#collapseAll').click(function() {
        $(this).hide();
        btn.not(this).show();
    });

    $(function () {
        $('a[data-toggle="collapse"]').on('click',function(){

            var objectID=$(this).attr('href');

            if($(objectID).hasClass('in'))
            {
                $(objectID).collapse('hide');
            }

            else{
                $(objectID).collapse('show');
            }
        });


        $('#expandAll').on('click',function(){
            $('a[data-toggle="collapse"]').each(function(){
                var objectID=$(this).attr('data-target');
                if($(objectID).hasClass('in')===false)
                {
                    $(objectID).collapse('show');
                }
            });
        });

        $('#collapseAll').on('click',function(){
            $('a[data-toggle="collapse"]').each(function(){
                var objectID=$(this).attr('data-target');
                $(objectID).collapse('hide');
            });
        });

    });
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.caseList = response;

        }).error(function (response) {

        });


    }
    $scope.getCaseNumber();
    var formIdsArray = [];
    var formUploadStatus = false;
    $scope.errorMssg1 = false;
    $scope.fileUploadSuccess = false;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {

        formIdsArray = [];

        $scope.errorMssg1 = true;
        $scope.fileUploadSuccess = false;

        var fileCount = 0;

        angular.forEach(files, function (file) {
            $scope.errorMssg1 = true;
            $scope.fileUploadSuccess = false;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.errorMssg1 = false;
                $scope.fileUploadSuccess = true;
                $scope.uploadError = false;
                $scope.uploadErrorMessage = '';
            }
        });
    };

    $scope.reset=function () {
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;

    }
    $scope.cancel=function(){
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").hide();
            ;
        });
    }
    $scope.uploadError = false;
    $scope.uploadErrorMessage = '';
    $scope.caseUploads = function () {

        if (formUploadStatus) {
            if ($scope.upload && $scope.upload.comments) {
                var uploadDetails = $scope.upload;
                uploadDetails.file = $scope.docList;
                var caseNumber = $scope.selectCaseNumber;

                uploadDetails['caseNumber'] = caseNumber;
                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                uploadDetails['createdPerson'] = loginPersonDetails.name;
                uploadDetails['uploadedPersonName'] = loginPersonDetails.name;

                uploadDetails.status = 'active';
                $http({
                    method: 'POST',
                    url: 'api/CaseUploads',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": uploadDetails
                }).success(function (response) {
                    $scope.loadingImage=false;

                    $("#scroll2").hide();
                    $("#scroll").show();
                    $scope.errorMssg1 = false;

                    $scope.uploadError = false;
                    $scope.uploadErrorMessage = '';
                    $scope.upload={};
                    $scope.docList = [];
                    $scope.getDocuments();
                }).error(function (response) {

                });

            }else{
                $scope.uploadError = true;
                $scope.uploadErrorMessage = 'Please enter the comment';
            }
        }else {

            $scope.uploadError = true;
            $scope.uploadErrorMessage = 'Please attach the files.';
        }
    }

    $scope.docUploadURL = uploadFileURL;

    $scope.getDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/CaseUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.documentsList = response;

        }).error(function (response) {

        });

    }

    $scope.getHearingDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/HearingUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.hearingDocumentsList = response;

        }).error(function (response) {

        });

    }


    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.selectCaseDetails = response[0];

        }).error(function (response) {

        });

        $scope.selectareas22(caseNumber);
        $scope.selectHearingDetails(caseNumber);
        $scope.selectHearingHistory(caseNumber);
        $scope.selectJudgementDetails(caseNumber);
        $scope.selectJudgementHistory(caseNumber);
        $scope.getDocuments();
        $scope.getHearingDocuments();



    }


    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });


    })

    $scope.selectareas22 = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/CaseHistories/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.selectCaseHistory = response;

        }).error(function (response) {

        });

        $scope.getDocuments();

    }

    $scope.selectHearingDetails = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/HearingDates/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.selectHearingHistory = response;

        }).error(function (response) {

        });

    }
    $scope.selectHearingHistory = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/HearingHistories/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.selectHearingHistoryDetails = response;

        }).error(function (response) {

        });

    }

    $scope.selectJudgementDetails = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/Judgements/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.selectJudgements = response;

        }).error(function (response) {

        });

    }
    $scope.selectJudgementHistory = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/JudgementHistories/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.selectJudgementHistory = response;

        }).error(function (response) {

        });

    }
    $scope.reset();

}]);

app.controller('hearingCaseDetailsController', function ($http, $scope, Upload, $timeout, $window, $location, $rootScope, $q) {


});

app.controller('legalEmailTemplateController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.legalEmailTemplate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.legalEmailTemplate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'legalEmailTemplate'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.editEmailTemp = function() {

        var editEmailData= $scope.legalType;

        $http({
            "method": "PUT",
            "url": 'api/EmailTempletes/'+$scope.legalType.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editEmailData
        }).success(function (response, data) {
            $scope.loadingImage=false;

            $scope.emailAlert = response;

            $("#registerSuccess").modal("show");
            setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);

        }).error(function (response, data) {

        })
    }
    $scope.getEmail=function () {

        $http({
            method: 'GET',
            url: 'api/EmailTempletes?filter={"where":{"emailType":"legal"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;

            $scope.legalType = response[0];

        }).error(function (response) {

        });
    }
    $scope.getEmail();


}]);

app.controller('createHearingDateController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Upload', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'hearingDateInfo'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.gethearingDate = function () {
        $http({
            "method": "GET",
            "url": 'api/HearingDates',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.hearingData = response;

        }).error(function (response, data) {

        });
    };
    $scope.gethearingDate();



    $scope.hearingDateClick=function(){
        //$scope.hearing = {};
        //$scope.hearing.dateHearing;
        console.log('$rootScope.holidaysData----'+JSON.stringify($scope.holidaysData));
        console.log('$scope.hearing.dateHearing----'+JSON.stringify($scope.hearing.dateHearing));
        angular.forEach($scope.holidaysData,function(date,index){
            if(date.festivalDate==$scope.hearing.dateHearing){
                //Notification.error('The date is a holiday, Please Select Other Date.!');
                $scope.createHearingMessage='The date is a holiday, Please Select Other Date.!';
                $scope.hearing.dateHearing='';
                $scope.hearingError=true;
                $timeout(function(){
                    $scope.hearingError = false;
                },3000);
            }
        });
    }

    /*holidays check while creation */

    $scope.$watch('hearing.dateHearing',function(o,n){
        if(o!=undefined){
            console.log(o+'old----new'+n);
            if(o!=n){
                $scope.hearingDateClick();
            }
        }
    });

    $http({
        "method": "GET",
        "url": 'api/Holidays',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {

        $scope.loadingImage=false;
        $scope.holidaysData = response;

    }).error(function (response, data) {

    });



    /*holidays check while creation end */


    $scope.hearing = {};
    $scope.hearingCheck=true;
    $scope.createHearingDate = function () {
        $scope.createHearingMessage ='';
        $scope.hearingError = false;


        $scope.advocateData = [];

        for(var i=0;i<$scope.hearing.advocateName.length;i++){
            //$scope.advocateData.push(JSON.parse($scope.hearing.advocateName[i]));
            $scope.advocateData.push($scope.hearing.advocateName[i]);
        }
        $scope.hearing.advocateName = [];

        for(var i=0;i<$scope.advocateData.length;i++){

            $scope.hearing.advocateName.push({
                name:$scope.advocateData[i].name,
                email:$scope.advocateData[i].email,
                advId:$scope.advocateData[i].advId,
                phoneNumber:$scope.advocateData[i].phoneNumber
            });


        }

        if($scope.hearingCheck==true) {
            $scope.hearingCheck = false;

            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            $scope.hearing['lastEditPerson'] = loginPersonDetails.employeeId;


            $http({
                method: 'POST',
                url: 'api/HearingDates',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.hearing
            }).success(function (response) {

                if(response.error!=undefined){
                if (response.error.message) {
                    $scope.createHearingMessage = response.error.message;
                    $scope.hearingError = true;

                    $scope.hearingCheck = true;
                }
                }
                else {

                    $scope.loadingImage=false;
                    $rootScope.hearingData.push(response);
                    $scope.hearingCheck = true;
                    $scope.selectCaseNumber = response.caseNumber;
                    $scope.caseShow = true;
                    $scope.caseHide = false;


                    $scope.selectCaseNumber = response.caseNumber;
                    $window.localStorage.setItem('hearingCreationCheck', $scope.selectCaseNumber);
                    location.href = '#/hearingDate';
                }

            }).error(function (response) {

                if(response.error!=undefined) {
                    if (response.error.message) {
                        $scope.createHearingMessage = response.error.message;
                        $scope.hearingError = true;
                        $scope.hearingCheck = true;
                    }
                }

            });
        }

    };

    $scope.reset = function () {
        $scope.hearing = angular.copy($scope.master);
        $scope.createHearingMessage ='';
        $scope.hearingError = false;
    };
    $scope.reset();


    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.caseNumber = response;

        }).error(function (response) {

        });
    };
    $scope.getCaseNumber();

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.advocates = response;

        }).error(function (response) {

        });
    };
    $scope.getAdvocates();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $scope.status = response;
        }).error(function (response) {

        });
    };
    $scope.getStatus();

}]);

app.controller('editHearingDateController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', function(legalManagement, $http, $scope, $window, $location, $rootScope) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'hearingDateInfo'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.gethearingDate = function () {
        $http({
            "method": "GET",
            "url": 'api/HearingDates',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $rootScope.hearingData = response;

        }).error(function (response, data) {

        });
    };
    $scope.gethearingDate();


    $scope.hearingCheck=true;

    $scope.updateHearingDate = {};


    $scope.editHearingDate = function () {

        $scope.advocateData = [];

        for(var i=0;i<$scope.updateHearingDate.advocateName.length;i++){
            $scope.advocateData.push(JSON.parse($scope.updateHearingDate.advocateName[i]));
        }
        $scope.updateHearingDate.advocateName = [];

        for(var i=0;i<$scope.advocateData.length;i++){

            $scope.updateHearingDate.advocateName.push({
                name:$scope.advocateData[i].name,
                email:$scope.advocateData[i].email
            });

        }

        $http({
            method: 'PUT',
            url: 'api/HearingDates/' + $scope.updateHearingDate.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $rootScope.updateHearingDate
        }).success(function (response) {

            $scope.selectCaseNumb=response.caseNumber;

            $scope.selectCaseNumb=response.caseNumber;
            $window.localStorage.setItem('hearingUpdationCheck', $scope.selectCaseNumb);
            location.href = '#/hearingDate';
        }).error(function (response) {

        });

    };
    $scope.cancelEdit = function () {
        $scope.gethearingDate();

    };
    $scope.reset();


    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.caseNumber = response;
        }).error(function (response) {

        });
    };
    $scope.getCaseNumber();

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.advocates = response;

        }).error(function (response) {

        });
    };
    $scope.getAdvocates();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.status = response;
        }).error(function (response) {

        });
    };
    $scope.getStatus();


}]);

app.controller('holidaysController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder' ,function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.holidays").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.holidays .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'holidays'}, function (response) {

        if(!response){
            window.location.href = "#/noAccessPage";
        }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.getHolidays = function () {
        $http({
            "method": "GET",
            "url": 'api/Holidays',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $scope.loadingImage=false;
            $rootScope.holidaysData = response;

        }).error(function (response, data) {

        });
    };
    $scope.getHolidays();
    $scope.reset = function () {
        $scope.holiday = angular.copy($scope.master);
        $scope.holidayCreationError = false;
        $scope.createHolidayError = '';

    };

    $scope.holidaysClick=function(){
        $('#CreateHolidayModel').modal('show');
        $scope.holiday = {};
        $scope.holidayCreationError = false;
        $scope.createHolidayError = '';

        $scope.holidayUpdationError = false;
        $scope.updateHolidayError = '';
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

    $scope.holiday = {};
    $scope.holidayCheck=true;
    $scope.createHoliday = function () {
        $scope.holidayCreationError = false;
        $scope.createHolidayError = '';
        if(  $scope.holidayCheck==true) {
            $scope.holidayCheck=false;
            if ($scope.holiday.festival.match(alphabetsWithSpaces) && $scope.holiday.festival) {


                $http({
                    method: 'POST',
                    url: 'api/Holidays',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.holiday
                }).success(function (response) {

                    $scope.loadingImage=false;
                    $scope.holidayCheck=true;

                    $('#CreateHolidayModel').modal('hide');
                    $("#addSuccess").modal("show");
                    setTimeout(function(){$('#addSuccess').modal('hide')}, 3000);
                    $scope.getHolidays();

                }).error(function (response) {

                    if (response.error.message[0]) {
                        $scope.createHolidayError = response.error.message[0];
                        $scope.holidayCreationError = true;
                        $scope.holidayCheck=true;
                    }


                });
            } else {
                $scope.createHolidayError = 'Please Enter Alphabetic Characters Only';
                $scope.holidayCreationError = true;
                $scope.holidayCheck=true;
            }
        }

    };
    $scope.updateHoliday = {};
    $scope.editHolidayPopup = function (holidayInfo) {

        $scope.updateHoliday=angular.copy(holidayInfo);

    };
    $scope.holidayUpdateCheck=true;

    $scope.editHoliday = function () {
        $scope.holidayUpdationError = false;
        $scope.updateHolidayError = '';


        if($scope.holidayUpdateCheck==true) {
            $scope.holidayUpdateCheck = false;
            if ($scope.updateHoliday.festival.match(alphabetsWithSpaces) && $scope.updateHoliday.festival) {


                $http({
                    method: 'PUT',
                    url: 'api/Holidays/' + $scope.updateHoliday.id,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.updateHoliday
                }).success(function (response) {

                    $scope.loadingImage=false;
                    $scope.holidayUpdateCheck = true;
                    //$scope.updateHoliday = {};
                    $('#holidayEdit').modal('hide');
                    $("#editSuccess").modal("show");
                    setTimeout(function(){$('#editSuccess').modal('hide')}, 3000);
                    $scope.getHolidays();
                }).error(function (response) {

                    $timeout(function (){
                        $scope.holidayUpdationError = false;
                    },3000)
                });
            } else {
                $scope.updateHolidayError = 'Please Enter Alphabetic Characters Only';
                $scope.holidayUpdationError = true;
                $scope.holidayUpdateCheck = true;

                $timeout(function (){
                    $scope.holidayUpdationError = false;
                },3000)

            }

        }
    };
    $scope.cancelEdit = function () {
        $scope.getHolidays();
        $('#holidayEdit').modal('hide');
    };

    $scope.reset();

    $scope.exportToExcel=function(tableId){

        if( $scope.holidaysData!=null &&  $scope.holidaysData.length>0){

            $("<tr>" +
                "<th>Festival Name</th>" +
                "<th>Festival Description</th>" +
                "<th>Festival Date</th>" +
                "</tr>").appendTo("table"+tableId);
            for(var i=0;i<$scope.holidaysData.length;i++){
                var holidays=$scope.holidaysData[i];
                $("<tr>" +
                    "<td>"+capitalizeWord(holidays.festival)+"</td>" +
                    "<td>"+capitalizeWord(holidays.description)+"</td>" +
                    "<td>"+capitalizeWord(holidays.festivalDate)+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'List Of Holidays');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);


function checkMailFormat(checkData) {
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (checkData.match(mailFormat)) {
        return true;
    } else {

        return false;
    }
}

function checkPhoneNumber(checkData) {
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
    if (checkData.match(phoneNumber)) {
        return true;
    }
    else {
        return false;
    }
}

function checkOnlyAlphabets(checkData) {
    var onlyAlphabets = /^[A-Za-z]+$/;
    if (checkData.match(onlyAlphabets)) {
        return true;
    } else {

        return false;
    }
}

function checkAlphaNumeric(checkData) {
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    if (checkData.match(alphaNumeric)) {
        return true;
    } else {
        return false;
    }

}

function capitalizeWord(inStr)
{
    return inStr.replace(/\w\S*/g, function(tStr)
    {
        return tStr.charAt(0).toUpperCase() + tStr.substr(1).toLowerCase();
    });
}



app.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
})