
// var uploadFileURL='http://139.162.27.78:8867/api/Uploads/dhanbadDb/download/';
var uploadFileURL ='http://54.149.172.244:3004/api/Uploads/dhanbadDb/download/';
angular.module('ui.tinymce', ['ui.calendar'])
    .value('uiTinymceConfig', {})
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
        uiTinymceConfig = uiTinymceConfig || {};
        var generatedIds = 0;
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ngModel) {
                var expression, options, tinyInstance;
                // generate an ID if not present
                if (!attrs.id) {
                    attrs.$set('id', 'uiTinymce' + generatedIds++);
                }
                options = {
                    // Update model when calling setContent (such as from the source editor popup)
                    setup: function(ed) {
                        ed.on('init', function(args) {
                            ngModel.$render();
                        });
                        // Update model on button click
                        ed.on('ExecCommand', function(e) {
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                        // Update model on keypress
                        ed.on('KeyUp', function(e) {
                            console.log(ed.isDirty());
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                    },
                    mode: 'exact',
                    elements: attrs.id,
                };
                if (attrs.uiTinymce) {
                    expression = scope.$eval(attrs.uiTinymce);
                } else {
                    expression = {};
                }
                angular.extend(options, uiTinymceConfig, expression);
                setTimeout(function() {
                    tinymce.init(options);
                });
                ngModel.$render = function() {
                    if (!tinyInstance) {
                        tinyInstance = tinymce.get(attrs.id);
                    }
                    if (tinyInstance) {
                        tinyInstance.setContent(ngModel.$viewValue || '');
                    }
                };
            }
        };
    }]);

var app = angular.module('project', ['ngRoute', 'ngCalendar', 'ui.calendar','ngSanitize', 'ui.select', 'servicesDetails','ngFileUpload', 'datatables', 'angularjs-dropdown-multiselect', 'ui.tinymce', 'datatables.bootstrap']);


app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    }
});

app.config(function($routeProvider) {
    $routeProvider.when('/projectCharter', {
        templateUrl: './projectCharter.html',
        controller: 'projectCharterController'
    }).when('/projectPlanDetails', {
        templateUrl: './projectPlanDetails.html',
        controller: 'projectPlanController'
    }).when('/projectPlan', {
        templateUrl: './projectPlan.html',
        controller: 'planController'
    }).when('/noAccessPage', {
      templateUrl: './noAccessPage.html',
      controller: 'noAccessPageController'
    }).when('/billGeneration', {
        templateUrl: './billGeneration.html',
        controller: 'billGenerationController'
    }).when('/fieldVisitReport', {
        templateUrl: './fieldVisitReport.html',
        controller: 'fieldVisitReportController'
    }).when('/measurementBook', {
        templateUrl: './measurementBook.html',
        controller: 'measurementBookController'
    }).when('/paymentOrder', {
        templateUrl: './paymentOrder.html',
        controller: 'paymentOrderController'
    }).when('/projectWorkOrder', {
        templateUrl: './projectWorkOrder.html',
        controller: 'projectWorkOrderController'
    }).when('/headerSchemes', {
        templateUrl: './HeaderScheme.html',
        controller: 'headerSchemesController'
    }).when('/transferFunds', {
        templateUrl: './transferFunds.html',
        controller: 'transferFundsController'
    }).when('/createPlan', {
        templateUrl: './createPlan.html',
        controller: 'createPlanController'
    }).when('/editPlan/:packId', {
        templateUrl: './editPlan.html',
        controller: 'editPlanController'
    }).when('/editPlanWithProjectPlan/:packId', {
          templateUrl: './editPlanWithProjectPlan.html',
          controller: 'editPlanWithProjectPlanController'
      }).when('/viewPlanDetails/:planId', {
        templateUrl: './viewPlanDetails.html',
        controller: 'viewPlanDetailsController'
    }).when('/projectForms', {
        templateUrl: './projectForms.html',
        controller: 'projectFormsController'
    }).when('/projectTask', {
        templateUrl: './projectTasks.html',
        controller: 'projectTaskController'
    }).when('/projectTOR', {
        templateUrl: './projectTOR.html',
        controller: 'projectTORController'
    }).when('/projectTORDetails', {
        templateUrl: './projectTORDetails.html',
        controller: 'projectTORDetailsController'
    }).when('/docUpload', {
        templateUrl: './projectUpload.html',
        controller: 'docUploadController'
    }).when('/comments', {
        templateUrl: './projectComments.html',
        controller: 'commentsController'
    }).when('/projectMileStone', {
        templateUrl: './projectMilestone.html',
        controller: 'projectMilestoneController'
    }).when('/paymentMilestone', {
        templateUrl: './paymentMilestone.html',
        controller: 'paymentMilestoneController'
    }).when('/projectRisks', {
        templateUrl: './projectRisks.html',
        controller: 'projectRisksController'
    }).when('/projectRebaseline', {
        templateUrl: './projectRebaseline.html',
        controller: 'projectRebaselineController'
    }).when('/projectRebaselineDetails', {
        templateUrl: './projectRebaselineDetails.html',
        controller: 'projectRebaselineDetailsController'
    }).when('/projectIssues', {
        templateUrl: './projectIssues.html',
        controller: 'projectIssuesController'
    }).when('/planState', {
        templateUrl: './PlanState.html',
        controller: 'planStateController'
    }).when('/taskTask', {
        templateUrl: './TaskStatus.html',
        controller: 'taskStatusController'
    }).when('/projectClosure', {
        templateUrl: './projectClosure.html',
        controller: 'projectClosureController'
    }).when('/projectRequests', {
        templateUrl: './ApplicationRequests.html',
        controller: 'projectRequestsController'
    }).when('/fieldReports', {
        templateUrl: './fieldReports.html',
        controller: 'fieldReportsController'
    }).when('/formsUpload', {
        templateUrl: './formsUpload.html',
        controller: 'formsUploadController'
    }).when('/viewProjectRequest/:requstId', {
        templateUrl: './viewProjectRequest.html',
        controller: 'viewProjectRequestController',
    }).when('/budgetPlans', {
        templateUrl: './budgetPlans.html',
        controller: 'budgetPlanController'
    }).when('/editBudgetDetails/:budgetId', {
        templateUrl: './editBudgetDetails.html',
        controller: 'editBudgetDetailsController'
    }).when('/departmentWiseBudgets', {
        templateUrl: './departmentWiseBudgets.html',
        controller: 'departmentWiseBudgetsController',
    }).when('/planLayout', {
        templateUrl: './planLayout.html',
        controller: 'planLayoutController'
    }).when('/projectWardworksEmailTemplete', {
        templateUrl: './projectWardworksEmailTemplete.html',
        controller: 'projectWardworksEmailTempleteController'
    }).when('/projectDelivarables', {
        templateUrl: './projectDelivarables.html',
        controller: 'projectDelivarablesController'
    }).when('/documentType', {
        templateUrl: './documentType.html',
        controller: 'documentTypeController'
    }).when('/nocRequests', {
        templateUrl: './nocRequests.html',
        controller: 'nocRequestsController'
    }).when('/nocFiledVisit', {
        templateUrl: './nocFieldReports.html',
        controller: 'nocFiledVisitController'
    }).when('/nocReport', {
        templateUrl: './nocReports.html',
        controller: 'nocReportController'
    }).when('/planDepartment', {
              templateUrl: './planDepartment.html',
              controller: 'planDepartmentController'
    }).when('/budgetUtil', {
        templateUrl: './budgetUtlization.html',
        controller: 'budgetUtilizationController'
    }).when('/deptOver', {
        templateUrl: './departmentoverSpendReport.html',
        controller: 'deptOverspendedController'
    }).when('/pperformance', {
        templateUrl: './pperformanceReport.html',
        controller: 'projectPerformanceController'
    }).when('/weeklyppr', {
      templateUrl: './weeklyProjectReport.html',
      controller: 'weeklyProjectPerfoController'
  }).when('/weeklypprDetails/:planId', {
      templateUrl: './weeklyProjectDetailReport.html',
      controller: 'weeklyProjectDetailsController'
  }).when('/rebaselineReport', {
        templateUrl: './rebaselineReport.html',
        controller: 'rebaselineReportController'
    }).when('/billMISDetails', {
        templateUrl: './billMISDetails.html',
        controller: 'billMISDetailsController'
    }).when('/paymentMISDetails', {
        templateUrl: './paymentMISDetails.html',
        controller: 'paymentMISDetailsController'
    }).when('/measurementMISDetails', {
          templateUrl: './measurementMISDetails.html',
          controller: 'measurementMISDetailsController'
      }).when('/paymentMISView/:requstId', {
          templateUrl: './paymentMISView.html',
          controller: 'paymentMISViewController'
      }).when('/billMISView/:requstId/:subtask', {
          templateUrl: './billMISView.html',
          controller: 'billMISViewController'
      }).when('/deptWiseBudgetMIS', {
          templateUrl: './deptWiseBudgetMIS.html',
          controller: 'deptWiseBudgetMISController'
      }).when('/deptMISView/:requstId/:subtask', {
          templateUrl: './deptMISView.html',
          controller: 'deptMISViewController'
      }).when('/departmentMIS', {
          templateUrl: './departmentMIS.html',
          controller: 'departmentMISController'
      }).when('/applicationRequesAgingtMIS', {
          templateUrl: './applicationRequesAgingtMIS.html',
          controller: 'applicationRequesAgingtMISController'
      }).when('/appRequestAgingDays/:requstId', {
          templateUrl: './appRequestAgingDays.html',
          controller: 'appRequestAgingDaysController'
      }).when('/applicationRequestMIS', {
          templateUrl: './applicationRequestMIS.html',
          controller: 'applicationRequestMISController'
      }).when('/applicationRequestViewMIS/:requstId', {
          templateUrl: './applicationRequestViewMIS.html',
          controller: 'applicationRequestViewMISController'
      }).when('/NOCRequestMIS', {
          templateUrl: './NOCRequestMIS.html',
          controller: 'NOCRequestMISController'
      }).when('/NOCRequestView/:requstId', {
          templateUrl: './NOCRequestView.html',
          controller: 'NOCRequestViewController'
      }).when('/NOCRequestAgingMIS', {
          templateUrl: './NOCRequestAgingMIS.html',
          controller: 'NOCRequestAgingMISController'
      }).when('/NOCRequestAgingDays/:requstId', {
          templateUrl: './NOCRequestAgingDays.html',
          controller: 'NOCRequestAgingDaysController'
      }).when('/ulb', {
        templateUrl: './ulb.html',
        controller: 'ulbController'
    });
});

app.controller('noAccessPageController', function($http, $scope, $window, $location, $rootScope) {

});

app.controller('projectWardworksEmailTempleteController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', function(projectWardWorks, $http, $scope, $window, $location, $rootScope) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro.active #masterDataProSetUp").addClass("menu-open");
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.projectWardworksEmailTemplete").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.projectWardworksEmailTemplete .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectWardworksEmailTemplete'}, function (response) {
    	//alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.loadingImage=true;

    $scope.getEmail = function () {
        $http({
            method: 'GET',
            url: 'api/EmailTempletes/?filter={"where":{"emailType":"' + "projectWardWorks" + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.email = response[0];
        }).error(function (response) {
        });
    }
    $scope.getEmail();

    $scope.getProjectDetailsForNotification = function() {
        var editEmailData= $scope.email;
        editEmailData.lastEditPerson=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/EmailTempletes/'+$scope.email.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editEmailData
        }).success(function (response, data) {
            $scope.loadingImage=false;
            $scope.emailAlert = response;
            $scope.getEmail();
            $("#registerSuccess").modal("show");
            setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.getEmail();


}]);

app.controller('projectRequestsController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope','Excel','$timeout','DTOptionsBuilder', 'DTColumnBuilder',  function(projectWardWorks,$http, $scope, $window, $location, $rootScope,Excel,$timeout,DTOptionsBuilder, DTColumnBuilder) {
  console.log("projectRequestsController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #requestsPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #requestsPro.active #appRequestSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.projectRequests").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.projectRequests .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.viewRequestDiv=true;
    $scope.viewRequestDivBack=true;
    $scope.requestDivBack=false;
    $scope.requestDiv=false;
    $scope.editRequestDivBack=false;
    $scope.editRequestDiv=false;
    var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectRequests'}, function (response) {
    	//alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });


    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;
$scope.getRequest = function(){

 $http({
        "method": "GET",
       // "url": 'api/ProjectRequests/getDetails?employeeId=%7B%22employeeId%22%3A%22'+employeeId+'%22%7D',
       "url": 'http://54.189.195.233:3000/api/ProjectRequests?filter={"where":{"status": {"neq":"Completed"}}}',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $scope.requestLists = response;
        $scope.loadingImage=false;
    }).error(function (response, data) {
        console.log("failure");
    })

}

$scope.getRequest();


   $scope.editAssetType = function(assetType) {
                  $("#editAssetTypeModel").modal("show");
                  $scope.errorMessage=false;
                  /*$scope.editCharterArea = true;
                   $scope.createCharterArea = false;*/
                  $scope.editAssetTypeData=angular.copy(assetType);
         }

   var editRiskGroupFormSubmit = true;
   $scope.editAssetRiskGroupButton=function () {
                   var editAssetTypeData= $scope.editAssetTypeData;
                   var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                   editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
                   editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
                   if(editRiskGroupFormSubmit){
                       editRiskGroupFormSubmit = false;
                       $http({
                           "method": "PUT",
                           "url": 'http://54.189.195.233:3000/api/ProjectRequests/'+$scope.editAssetTypeData.id+'',
                           "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                           "data": editAssetTypeData
                       }).success(function (response, data) {
                           //console.log("filter Schemes "+ JSON.stringify(response));
                           //$scope.loadingImage=true;
                           $("#editAssetTypeModel").modal("hide");
                           $("#editAssetSuccess").modal("show");
                           setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                           $scope.getRequest();
                           //$window.location.reload();
                       }).error(function (response, data) {

                       });
                   }
                   editRiskGroupFormSubmit = true;
               }

/*    $scope.changeStatus=function (id) {
        if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
            for(var i=0;i<$scope.requestLists.length;i++){
                if(id==$scope.requestLists[i].id){
                    $scope.editRequestDetails=angular.copy($scope.requestLists[i]) ;
                    $scope.viewRequestDiv=false;
                    $scope.viewRequestDivBack=false;
                    $scope.editRequestDivBack=true;
                    $scope.editRequestDiv=true;
                    $scope.requestDivBack=false;
                    $scope.requestDiv=false;
                }
            }
        }
    }
    $scope.clickToBack = function(){
        $window.location.reload();
    }
    $scope.viewStatus=function (id) {
        if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
            for(var i=0;i<$scope.requestLists.length;i++){
                if(id==$scope.requestLists[i].id){
                    $scope.viewRequestDetails=angular.copy($scope.requestLists[i]) ;
                    $scope.viewRequestDiv=false;
                    $scope.viewRequestDivBack=false;
                    $scope.editRequestDivBack=false;
                    $scope.editRequestDiv=false;
                    $scope.requestDivBack=true;
                    $scope.requestDiv=true;
                }
            }
        }
    }
    $scope.acceptStatus = function (request) {

        var updateDetails = {
            'email':request.email,
            "acceptLevel":request.acceptLevel,
            "acceptStatus": "Yes",
            "requestStatus": "Approval",
            "comment": request.comment,
            "requestId":request.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };

        $scope.approvalScheme = true;

        $http({
            "method": "POST",
            "url": 'api/ProjectRequests/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            console.log("filter Schemes "+ JSON.stringify(response));
            $scope.requestLists = JSON.stringify(response);
            $window.localStorage.setItem('requestFlag',true);
            $("#acceptStatus").modal("show");
            *//*setTimeout(function(){$('#acceptStatus').modal('hide')}, 3000);*//*
            *//*$window.location.reload();*//*
            //$window.localStorage.getItem('requestTab');

        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.acceptStatusModal = function() {
        $window.location.reload();
    }
    $scope.rejectStatusModal = function() {
        $window.location.reload();
    }
    $scope.rejectStatus = function (request) {
        var updateDetails = {
            'email':request.email,
            "acceptLevel":request.acceptLevel,
            "acceptStatus": "No",
            "requestStatus": "Rejected",
            "comment": request.comment,
            "requestId":request.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };
        $http({
            "method": "POST",
            "url": 'api/ProjectRequests/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            $scope.requestLists = response.data;
            console.log("success", JSON.stringify(response.data));
            $window.localStorage.setItem('requestFlag',true);
            $("#rejectStatus").modal("show");
        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }*/

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

            $("<tr>" +
                "<th>Name</th>" +
                "<th>title</th>" +
                "<th>requestStatus</th>" +
                "<th>acceptStatus</th>" +
                "<th>personName</th>" +
                "<th>email</th>" +
                "<th>mobile</th>" +
                "<th>title</th>" +
                "<th>requested</th>" +
                "<th>createdTime</th>" +
                "<th>description</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.requestLists.length;i++){
                var request=$scope.requestLists[i];

                $("<tr>" +
                    "<td>"+request.applicationId+"</td>" +
                    "<td>"+request.title+"</td>" +
                    "<td>"+request.requestStatus+"</td>" +
                    "<td>"+request.acceptStatus+"</td>" +
                    "<td>"+request.personName+"</td>" +
                    "<td>"+request.email+"</td>" +
                    "<td>"+request.mobile+"</td>" +
                    "<td>"+request.title+"</td>" +
                    "<td>"+request.requested+"</td>" +
                    "<td>"+request.createdTime+"</td>" +
                    "<td>"+request.description+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Lists of Schemes ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);

app.controller('viewProjectRequestController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, $routeParams) {
    console.log("viewProjectRequestController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".navbar-nav > li.projectWorks").addClass("active").siblings('.active').removeClass('active');
        $(".navbar-nav li.projectWorks li.requests li.projectRequests").addClass("active1").siblings('.active1').removeClass('active1');
        $(".navbar-nav li.projectWorks > .collapse").addClass("in");
        $(".navbar-nav li.requests .collapse").addClass("in");
    });

    var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectRequests'}, function (response) {
    	//alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.uploadURL=uploadFileURL;
    $scope.requestDetails=$routeParams.requstId;
    $http({
        "method": "GET",
        "url": 'api/ProjectRequests/'+$scope.requestDetails,
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $scope.requestLists = response;
    }).error(function (response, data) {

    })

    var acceptDoubleClick=false;
    $scope.acceptStatus = function (request) {
        if(!acceptDoubleClick){
            acceptDoubleClick=true;
            var updateDetails = {
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment": request.comment
            };

            $scope.approvalScheme = true;
            $http({
                "method": "PUT",
                "url": 'api/ProjectRequests/'+ request.id,
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                $scope.requestLists = response;
                acceptDoubleClick=false;
                location.href = '#/projectRequests';
                $window.localStorage.setItem('requestFlag',true);
            }).error(function (response, data) {
                console.log("failure");
                acceptDoubleClick=false;
            })
        }

    }
    var  rejectDoubleClick=false;
    $scope.rejectStatus = function (request) {
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment": request.comment
            };
            $http({
                "method": "PUT",
                "url": 'api/ProjectRequests/'+ request.id,
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                $scope.requestLists = response;
                location.href = '#/projectRequests';
                rejectDoubleClick=false;
                $window.localStorage.setItem('requestFlag',true);

            }).error(function (response, data) {
                console.log("failure");
                rejectDoubleClick=false;
            })
        }

    }

}]);

app.controller('projectCharterController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope','Excel','$timeout','DTOptionsBuilder', 'DTColumnBuilder',  function(projectWardWorks,$http, $scope, $window, $location, $rootScope,Excel,$timeout,DTOptionsBuilder, DTColumnBuilder) {
console.log("projectCharterController.........");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #proWorksLi").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
            $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectCharterArea ").addClass("active").siblings('.active').removeClass('active');
            $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectCharterArea .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectCharter'}, function (response) {
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'DESC']);

    $scope.loadingImage=true;
    $scope.showCharter = function() {
        $scope.createCharterArea = true;
        $scope.editCharterArea = false;
    }
    $scope.editCharterButton = function(charter) {
        //console.log("edit charter details"+ JSON.stringify(charter));
        $("#editCharter").modal("show");
        $scope.editCharterArea = true;
        $scope.createCharterArea = false;
        $scope.editCharterData=angular.copy(charter);
    }
    $scope.getcharterDesc = function(name, desc){
//    alert(name);
//    alert(desc);
        $scope.charterName = name;
        $scope.charterDesc = desc;
        $("#showDesc").modal("show");
    }
    $scope.createCharterModal = function() {
        $("#createCharter").modal("show");
        $scope.charter = {};
    }
    var doubleClick=false;
    $scope.createCharter=function () {


        var charterDetails=$scope.charter;
        //console.log('login person details'+loginPersonDetails.name);
        charterDetails['createdPerson']=loginPersonDetails.name;
        console.log("Charter Data:"+JSON.stringify(charterDetails));
        if(!doubleClick){
            doubleClick=true;
            $http({
                method: 'POST',
                url: 'api/ProjectCharters',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":charterDetails
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                //  //alert('successfully created charter');
                $scope.loadingImage=false;
                $scope.getCharterDetails();
                $("#createCharter").modal("hide");
                $("#addCharterSuccess").modal("show");
                doubleClick=false;
                setTimeout(function(){$('#addCharterSuccess').modal('hide')}, 3000);
            }).error(function (response) {
                doubleClick=false;
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }
    var editDoubleClick=false;
    $scope.editCharter=function () {
        if(!editDoubleClick){
            editDoubleClick=true;

        }
        var editCharterDetails= $scope.editCharterData;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            method: 'PUT',
            url: 'api/ProjectCharters/'+$scope.editCharterData.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":editCharterDetails
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            ////alert('successfully edit charter');
            $scope.loadingImage=false;
            $scope.getCharterDetails();
            $("#editCharter").modal("hide");
            $("#editCharterSuccess").modal("show");
            setTimeout(function(){$('#editCharterSuccess').modal('hide')}, 3000);
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getCharterDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectCharters',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Charter Response :' + JSON.stringify(response));
            $scope.charterList=response;
            $scope.loadingImage=false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

        ////alert('get charter list');
    };
    $scope.getCharterDetails();

    projectWardWorks.getTasksDetails({}, function (response) {
        $scope.taskStateList = response;
    });
    $scope.exportToExcel=function(tabelId){
        if( $scope.charterList!=null &&  $scope.charterList.length>0){
            $("<tr>" +
                "<th>Charter Name</th>" +
                "<th>Charter Description</th>" +
                "<th>Charter Goal</th>" +
                "<th>Charter Objectives</th>" +
                "<th>Businees OutComes</th>" +
                "<th>InScope or OutScope</th>" +
                "<th>Plan Id</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tabelId);
            for(var i=0;i<$scope.charterList.length;i++){
                var charter=$scope.charterList[i];
                $("<tr>" +
                    "<td>"+charter.name+"</td>" +
                    "<td>"+charter.description+"</td>" +
                    "<td>"+charter.goal+"</td>" +
                    "<td>"+charter.objectives+"</td>" +
                    "<td>"+charter.businessOutcomes+"</td>" +
                    "<td>"+charter.inScope+"</td>" +
                    "<td>"+charter.projectId+"</td>" +
                    "<td>"+charter.status+"</td>" +
                    "</tr>").appendTo("table"+tabelId);
            }
            var exportHref=Excel.tableToExcel(tabelId,'ProjectCharter');
            $timeout(function(){location.href=exportHref;},100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('planStateController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope','Excel','$timeout','DTOptionsBuilder', 'DTColumnBuilder',  function(projectWardWorks,$http, $scope, $window, $location, $rootScope,Excel,$timeout,DTOptionsBuilder, DTColumnBuilder) {
    console.log("planStateController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro.active #masterDataProSetUp").addClass("menu-open");
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.planState").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.planState .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'planState'}, function (response) {
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.editPlanState = function(department) {
        $("#editPlanStateModel").modal("show");
        $scope.editCharterArea = true;
        $scope.createCharterArea = false;
        $scope.editPlanStateData=angular.copy(department);
    }

    $scope.getPlanState=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectStates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.planStateList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
        });
    }
    $scope.addPlanStateModal = function() {
        $("#addPlanStateModel").modal("show");
        $scope.planState = {};
        $scope.errorMessage=false;
    }

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    var addDoubleClick=false;
    $scope.addPlanState=function () {
        if(!addDoubleClick){
            addDoubleClick=true;
            var planState=$scope.planState;
            planState['createdPerson']=loginPersonDetails.name;
            $http({
                method: 'POST',
                url: 'api/ProjectStates',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":planState
            }).success(function (response) {
                $scope.loadingImage=false;
                $("#addPlanStateModel").modal("hide");
                $("#addPlanStateSuccess").modal("show");
                addDoubleClick=false;
                setTimeout(function(){$('#addPlanStateSuccess').modal('hide')}, 3000);
                $scope.getPlanState();
            }).error(function (response) {
                if(response.error.details.messages.name) {
                    $scope.errorMessageData=response.error.details.messages.name[0];
                    $scope.errorMessage=true;
                    addDoubleClick=false;
                    $timeout(function(){
                        $scope.errorMessage=false;
                    }, 3000);
                }
            });
        }

    }

    var editDoubleClick=false;
    $scope.editPlanStateButton=function () {
        if(!editDoubleClick){
            editDoubleClick=true;
            $scope.editPlanStateData['lastEditPerson']=loginPersonDetails.name;
            $http({
                "method": "PUT",
                "url": 'api/ProjectStates/'+   $scope.editPlanStateData.id,
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data":  $scope.editPlanStateData
            }).success(function (response, data) {
                $scope.loadingImage=false;
                $("#editPlanStateModel").modal("hide");
                $("#editPlanStateSuccess").modal("show");
                editDoubleClick=false;
                setTimeout(function(){$('#editPlanStateSuccess').modal('hide')}, 3000);
                $scope.getPlanState();
            }).error(function (response, data) {
                if(response.error.details.messages.name) {
                    $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                    $scope.errorUpdateMessage=true;
                    editDoubleClick=false;
                    $timeout(function(){
                        $scope.errorUpdateMessage=false;
                    }, 3000);
                }
            })


        }

    }
    $scope.getPlanState();

    $scope.exportToExcel=function(tableId){
        if( $scope.planStateList!=null &&  $scope.planStateList.length>0){

            $("<tr>" +
                "<th>Plan Status</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.planStateList.length;i++){
                var planStatus=$scope.planStateList[i];

                $("<tr>" +
                    "<td>"+planStatus.name+"</td>" +
                    "<td>"+planStatus.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Plan State Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }
}]);

app.controller('taskStatusController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("taskStatusController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".navbar-nav > li.projectWorks").addClass("active").siblings('.active').removeClass('active');
        $(".navbar-nav li.projectWorks li.masterData li.taskStatus").addClass("active1").siblings('.active1').removeClass('active1');
        $(".navbar-nav li.projectWorks li.masterData .nav-list").addClass("active2").siblings('.active2').removeClass('active2');
        $(".navbar-nav li.projectWorks > .collapse").addClass("in");
        $(".navbar-nav li.masterData .collapse").addClass("in");
        /*$(".navbar-nav li .collapse").removeClass("in");*/
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'taskStatus'}, function (response) {
//	    alert(JSON.stringify(response));
            if(!response){
                window.location.href = "#/noAccessPage";
            }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.edittaskSatus = function(taskStatus) {
        $("#editTaskStatusModel").modal("show");
        $scope.editCharterArea = true;
        $scope.createCharterArea = false;
        $scope.editTaskSatus=angular.copy(taskStatus);
    }

    $scope.getTaskStatus=function () {
        $http({
            method: 'GET',
            url: 'api/TaskStatuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.taskStatustList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
        });
    }
    $scope.addTaskStatusModal = function() {
        $("#addTaskStatus").modal("show");
        $scope.taskStatus = {};
        $scope.errorUpdateMessage=true;
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    var addDoubleClick=false;
    $scope.addTaskStatus=function () {
        if(!addDoubleClick){
            addDoubleClick=true;
            var taskStatus=$scope.taskStatus;
            taskStatus['createdPerson']=loginPersonDetails.name;
            $http({
                method: 'POST',
                url: 'api/TaskStatuses',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":taskStatus
            }).success(function (response) {
                $scope.loadingImage=false;
                $("#addTaskStatus").modal("hide");
                $("#addTaskStateSuccess").modal("show");
                addDoubleClick=false;
                setTimeout(function(){$('#addTaskStateSuccess').modal('hide')}, 3000);
                $scope.getTaskStatus();
            }).error(function (response) {
                if(response.error.details.messages.name) {
                    $scope.errorMessageData=response.error.details.messages.name[0];
                    $scope.errorMessage=true;
                    addDoubleClick=false;
                    $timeout(function(){
                        $scope.errorMessage=false;
                    }, 3000);
                }
            });


        }

    }

    var editDoubleClick=false;
    $scope.editTaskStatusButton=function () {
        if(!editDoubleClick){
            editDoubleClick=true;
            $scope.editTaskSatus['lastEditPerson']=loginPersonDetails.name;
            $http({
                "method": "PUT",
                "url": 'api/TaskStatuses/'+   $scope.editTaskSatus.id,
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data":  $scope.editTaskSatus
            }).success(function (response, data) {
                $scope.loadingImage=false;
                $("#editTaskStatusModel").modal("hide");
                $("#editTaskStateSuccess").modal("show");
                editDoubleClick=false;
                setTimeout(function(){$('#editTaskStateSuccess').modal('hide')}, 3000);
                $scope.getTaskStatus();
            }).error(function (response, data) {
                if(response.error.details.messages.name) {
                    $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                    $scope.errorUpdateMessage=true;
                    editDoubleClick=false;
                    $timeout(function(){
                        $scope.errorMessage=false;
                    }, 3000);
                }
            });
        }

    }

    $scope.getTaskStatus();

    $scope.exportToExcel=function(tableId){
        if( $scope.taskStatustList!=null &&  $scope.taskStatustList.length>0){

            $("<tr>" +
                "<th>Task Status</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.taskStatustList.length;i++){
                var taskStatus=$scope.taskStatustList[i];

                $("<tr>" +
                    "<td>"+taskStatus.name+"</td>" +
                    "<td>"+taskStatus.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Task Status Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('projectPlanController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #proWorksLi").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
            $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanDetails").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanDetails .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPlan'}, function (response) {
//	alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });


    $scope.getPlanDetails=function () {

        var employeeId=loginPersonDetails.employeeId;
        var employeeName=loginPersonDetails.name;
        $http({
            method: 'GET',
            url: 'api/ProjectPlans/getDetailsForWorkFlow?employeeId='+employeeId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
        console.log("data..........." + JSON.stringify(response));
            $scope.planList=response;
        }).error(function (response) {
        });
    }
    $scope.getPlanDetails();

    $scope.exportToExcel=function(tableId){
        if( $scope.planList!=null &&  $scope.planList.length>0){

            $("<tr>" +
                "<th>PlanId</th>" +
                "<th>Name</th>" +
                "<th>Department</th>" +
                "<th>Start Date</th>" +
                "<th>End Date</th>" +
                "<th>Financial Year</th>" +
                "<th>Task Status</th>" +
                "<th>Plan Status</th>" +
                "<th>Estimated Cost</th>" +
                "<th>Utilized Cost</th>" +
                "<th>Completed(%)</th>" +
                "<th>Village</th>" +
                "<th>Mandal</th>" +
                "<th>City</th>" +
                "<th>Address</th>" +
                "<th>Created Person</th>" +
                "<th>Created Time</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.planList.length;i++){
                var plan=$scope.planList[i];

                $("<tr>" +
                    "<td>"+plan.planId+"</td>" +
                    "<td>"+plan.name+"</td>" +
                    "<td>"+plan.department+"</td>" +
                    "<td>"+plan.startDate+"</td>" +
                    "<td>"+plan.endDate+"</td>" +
                    "<td>"+plan.financialYear+"</td>" +
                    "<td>"+plan.taskStatus+"</td>" +
                    "<td>"+plan.palnStatus+"</td>" +
                    "<td>"+plan.estimationCost+"</td>" +
                    "<td>"+plan.utilizedCost+"</td>" +
                    "<td>"+plan.completePercentage+"</td>" +
                    "<td>"+plan.village+"</td>" +
                    "<td>"+plan.mandal+"</td>" +
                    "<td>"+plan.city+"</td>" +
                    "<td>"+plan.address+"</td>" +
                    "<td>"+plan.createdTime+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("Project plan Lists");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

    $scope.viewPlanDetails = function(planDetails) {
        $scope.planDetailsView = planDetails;
        $("#planDetailsView").modal("show");
    }

    $scope.comments;
    $scope.approvalModal=function(planData){
        $scope.planData=angular.copy(planData);
        $("#approavlReport").modal("show");
    }
    $scope.rejectModal=function(planData){
        $scope.planData=angular.copy(planData);
        $("#rejectReport").modal("show");
    }

    var employeeId=loginPersonDetails.employeeId;
    var employeeName=loginPersonDetails.name;

    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.planData.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment":  $scope.comments.comment,
                "planId":$scope.planData.planId,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectPlans/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                response.error.message!=null && response.error.message!=''){
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getPlanDetails();
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                }
            }).error(function (response, data) {
                approvalDoubleClick=false;
            })

        }
    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.planData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.planData.planId,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectPlans/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getPlanDetails();
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                }

            }).error(function (response, data) {
                console.log("failure");
                rejectDoubleClick=false;
            })
        }
    }

$scope.editPlanPopUp = function(plan) {
        $scope.planId = plan.id;
        $("#planDetailsView").modal("hide");
        location.href = '#/editPlan/'+$scope.planId;
        $window.location.reload();

    }
}]);

app.controller('planController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
                $('html,body').scrollTop(0);
                $(".sidebar-menu li ul > li").removeClass('active1');
                $('[data-toggle="tooltip"]').tooltip();
                $(".sidebar-menu #proWorksLi").addClass("active");
                $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
                $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
                $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate").addClass("active1").siblings('.active1').removeClass('active1');
                $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate .collapse").addClass("in");
                $(".sidebar-menu li .collapse").removeClass("in");
            });




    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    projectWardWorks.getPlanDetails({}, function (response) {
        $scope.planList=response;
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPlan'}, function (response) {
//	alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });


    $scope.exportToExcel=function(tableId){
        if( $scope.planList!=null &&  $scope.planList.length>0){

            $("<tr>" +
                "<th>PlanId</th>" +
                "<th>Name</th>" +
                "<th>Department</th>" +
                "<th>Start Date</th>" +
                "<th>End Date</th>" +
                "<th>Financial Year</th>" +
                "<th>Task Status</th>" +
                "<th>Plan Status</th>" +
                "<th>Estimated Cost</th>" +
                "<th>Utilized Cost</th>" +
                "<th>Completed(%)</th>" +
                "<th>Village</th>" +
                "<th>Mandal</th>" +
                "<th>City</th>" +
                "<th>Address</th>" +
                "<th>Created Person</th>" +
                "<th>Created Time</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.planList.length;i++){
                var plan=$scope.planList[i];

                $("<tr>" +
                    "<td>"+plan.planId+"</td>" +
                    "<td>"+plan.name+"</td>" +
                    "<td>"+plan.department+"</td>" +
                    "<td>"+plan.startDate+"</td>" +
                    "<td>"+plan.endDate+"</td>" +
                    "<td>"+plan.financialYear+"</td>" +
                    "<td>"+plan.taskStatus+"</td>" +
                    "<td>"+plan.palnStatus+"</td>" +
                    "<td>"+plan.estimationCost+"</td>" +
                    "<td>"+plan.utilizedCost+"</td>" +
                    "<td>"+plan.completePercentage+"</td>" +
                    "<td>"+plan.village+"</td>" +
                    "<td>"+plan.mandal+"</td>" +
                    "<td>"+plan.city+"</td>" +
                    "<td>"+plan.address+"</td>" +
                    "<td>"+plan.createdTime+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("Project plan Lists");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);

app.controller('createPlanController', ['projectWardWorks','$http', '$scope', '$timeout','$window', '$location', '$rootScope', function(projectWardWorks,$http, $scope, $timeout, $window, $location, $rootScope) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPlan'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    projectWardWorks.getPlanDepartment({}, function (response) {
        $scope.departmentList=response;
    });
     projectWardWorks.getCharterDetails({}, function (response) {
         $scope.charterList=response;
         if($scope.charterList.length>0){
             $scope.charterLength=false;
         }
     });
    projectWardWorks.getProjectStatus({}, function (response) {
        $scope.planStateList = response;
        $scope.planStateCopy =response;
    });
    projectWardWorks.getTasksDetails({}, function (response) {
        $scope.taskStateList = response;
    });

    $scope.checkTaxDue=function(){
        $scope.planStatusView=true;
        if($scope.plan.taxDueStatus=='yes'){
            if($scope.planStateList!=undefined && $scope.planStateList!=null && $scope.planStateList.length>0){
                for(var i=0;i<$scope.planStateList.length;i++){
                    if($scope.planStateList[i].name=='draft'){
                        var draftData=angular.copy($scope.planStateList[i]);
                        $scope.planStateList=[];
                        $scope.planStateList.push(draftData)
                        break;
                    }
                }
            }
        }else{
            $scope.planStateList=[];
            $scope.planStateList=$scope.planStateCopy;
        }
    }
    $scope.getULB=function () {
        $http({
            method: 'GET',
            url: 'api/ULBs/?filter={"where":{"status":"Active"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.ulbList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.getPlanDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.planList=response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getPlanDetails();
    $scope.getULB();
    var palnStatus;
    $scope.onChangePlaneId=function(){
        $http({
            method: 'GET',
            url: 'api/ProjectPlans?filter={"where":{"planId":"'+$scope.plan.planId+'"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            if(response.length>0){
                $scope.planErrorMessage="Plan Id already in use,please choose different plan id.";
                $scope.planId=true;
                palnStatus=false;
            }else {
                $scope.planId=false;
                palnStatus=true;
            }
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.plan;
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    var createPlanSubmit = true;
    $scope.createPlan=function () {
        if(createPlanSubmit) {
            createPlanSubmit = false;
            if($scope.plan.startDate){
                if($scope.plan.endDate){
                    if($scope.plan.palnStatusId){
                        var planDetails=$scope.plan;
                        planDetails['completePercentage']=document.getElementById('rangeDanger').value;
                        planDetails['createdPerson']=loginPersonDetails.name;
                        planDetails['createdPersonId']=loginPersonDetails.employeeId;
                        if(palnStatus) {
                            $scope.planId=false;
                            $http({
                                method: 'POST',
                                url: 'api/ProjectPlans',
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                "data": planDetails
                            }).success(function (response) {
                                $("#addPlanSuccess").modal("show");
                            }).error(function (response) {
                            });
                     }else {
                        $scope.planErrorMessage="Please select plan status";
                        $scope.planId=true;
                        createPlanSubmit = true;
                    }

                    }else {
                        $scope.planErrorMessage="Please select plan Id";
                        $scope.planId=true;
                        createPlanSubmit = true;
                    }

                }else{
                    $scope.errorMessageData="Please Select End Date";
                    $scope.errorMessage=true;
                    $timeout(function(){
                        $scope.errorMessage=false;
                    }, 3000);
                    createPlanSubmit = true;
                }


            }else{
                $scope.errorMessageData="Please Select Start Date";
                $scope.errorMessage=true;
                $timeout(function(){
                    $scope.errorMessage=false;
                }, 3000);
                createPlanSubmit = true;
            }

        }

    }
    $scope.addPlanSuccessRedirect = function() {
        $("#addPlanSuccess").modal("hide");
        location.href = '#/projectPlanDetails';
        $window.location.reload();
    }
}]);

app.controller('editPlanController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope','$routeParams',function(projectWardWorks,$http, $scope, $window, $location, $rootScope,$routeParams) {

     $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlan .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPlan'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.pack=$routeParams.packId;
    $http({
        method: 'GET',
        url: 'api/ProjectPlans/'+$scope.pack,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        $scope.editPlanDetails = response;
    }).error(function (response) {
    });
    var editDoubleClick=false;
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

    $scope.editPlan=function(id){
        $scope.packIdName = id;
        if(!editDoubleClick){
            editDoubleClick=true;
                var editCharterDetails=$scope.editPlanDetails;
                editCharterDetails['lastEditPerson']=loginPersonDetails.name;
                editCharterDetails['lastEditPersonId']=loginPersonDetails.employeeId;
                delete editCharterDetails.workflowData;

                $http({
                    method: 'POST',
                    url: 'api/ProjectPlans/updateContent',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data:editCharterDetails
                }).success(function (response) {
                    $scope.getPlanDetails();
                    $('#editPlanSuccess').modal('show');
                    editDoubleClick=false;
                }).error(function (response) {
                    editDoubleClick=false;
                });
        }
    }
    $scope.editPlanSuccess = function () {
        $('#editPlanSuccess').modal('hide');
        location.href = '#/projectPlanDetails';
        $window.location.reload();
    }
    $scope.getPlanDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.planList=response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    projectWardWorks.getPlanDepartment({}, function (response) {
        $scope.departmentList=response;
    });
    projectWardWorks.getProjectStatus({}, function (response) {
        $scope.planStateList = response;
    });
    projectWardWorks.getTasksDetails({}, function (response) {
        $scope.taskStateList = response;
    });
}]);

app.controller('editPlanWithProjectPlanController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope','$routeParams',function(projectWardWorks,$http, $scope, $window, $location, $rootScope,$routeParams) {
     console.log("editPlanWithProjectPlanController");

     $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    $scope.pack=$routeParams.packId;
    console.log("........ "+JSON.stringify($scope.pack));
    //alert($scope.pack);
    $http({
        method: 'GET',
        url: 'api/ProjectPlans/?filter={"where":{"planId":"' + $scope.pack  + '"}}',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        $scope.editPlanDetails = response[0];
        console.log("response......... "+ JSON.stringify(response));
    }).error(function (response) {
        console.log("response"+ JSON.stringify(response));
    });

    $scope.getULB=function () {
        $http({
            method: 'GET',
            url: 'api/ULBs/?filter={"where":{"status":"Active"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('ULB :' + JSON.stringify(response));
            $scope.ulbList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getULB();

    projectWardWorks.getTasksDetails({}, function (response) {
            $scope.taskStateList = response;
        });

        $scope.checkTaxDue=function(){
                $scope.planStatusView=true;
                if($scope.editPlanDetails.taxDueStatus=='yes'){
                    if($scope.planStateList!=undefined && $scope.planStateList!=null && $scope.planStateList.length>0){
                        for(var i=0;i<$scope.planStateList.length;i++){
                            if($scope.planStateList[i].name=='draft'){
                                var draftData=angular.copy($scope.planStateList[i]);
                                $scope.planStateList=[];
                                $scope.planStateList.push(draftData)
                                break;
                            }
                        }
                    }
                }else{
                    $scope.planStateList=[];
                    $scope.planStateList=$scope.planStateCopy;
                }
            }

//            $scope.checkTaxDue();

    var editDoubleClick=false;
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPlan'}, function (response) {
    //    	alert(JSON.stringify(response));
               if(!response){
                window.location.href = "#/noAccessPage";
               }
            });
    $scope.editPlan=function(id){
        $scope.packIdName = id;
        if(!editDoubleClick){
            editDoubleClick=true;
                var editCharterDetails=$scope.editPlanDetails;
                editCharterDetails['lastEditPerson']=loginPersonDetails.name;
                editCharterDetails['lastEditPersonId']=loginPersonDetails.employeeId;
                delete editCharterDetails.workflowData;
                $http({
                    method: 'PUT',
                    url: 'api/ProjectPlans/'+$scope.packIdName,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data:editCharterDetails
                }).success(function (response) {
                console.log("edit...."+JSON.stringify(response));
                    $scope.getPlanDetails();
                    $('#editPlanSuccess').modal('show');
                    editDoubleClick=false;
                }).error(function (response) {
                    editDoubleClick=false;
                });
        }
    }
    $scope.editPlanWithProjectSuccess = function () {
        $('#editPlanSuccess').modal('hide');
        location.href = '#/projectPlan';
        $window.location.reload();
    }
    $scope.getPlanDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.planList=response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    projectWardWorks.getPlanDepartment({}, function (response) {
        $scope.departmentList=response;
    });
    projectWardWorks.getProjectStatus({}, function (response) {
        $scope.planStateList = response;
    });
    projectWardWorks.getTasksDetails({}, function (response) {
        $scope.taskStateList = response;
    });
}]);

app.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
})

app.controller('viewPlanDetailsController', ['projectWardWorks', '$http','Excel','$timeout', '$scope', '$window', '$location', '$rootScope', '$routeParams', function(projectWardWorks, $http,Excel,$timeout, $scope, $window, $location, $rootScope, $routeParams) {
   $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectPlanCreate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPlan'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.exportToExcel=function(tableId){ // ex: '#my-table'
        var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
        $timeout(function(){location.href=exportHref;},100); // trigger download
    }

    var btn = $('#expandAll,#collapseAll').click(function() { // bind click handler to both button
        $(this).hide(); // hide the clicked button
        btn.not(this).show(); // show the another button which is hidden
    });

    $(function () {
        $('a[data-toggle="collapse"]').on('click',function(){

            var objectID=$(this).attr('href');

            if($(objectID).hasClass('in'))
            {
                $(objectID).collapse('hide');
            }

            else{
                $(objectID).collapse('show');
            }
        });

        $('#expandAll').on('click',function(){
            $('div[data-toggle="collapse"]').each(function(){
                var objectID=$(this).attr('data-target');
                if($(objectID).hasClass('in')===false)
                {
                    $(objectID).collapse('show');
                }
            });
        });

        $('#collapseAll').on('click',function(){
            $('div[data-toggle="collapse"]').each(function(){
                var objectID=$(this).attr('data-target');
                $(objectID).collapse('hide');
            });
        });
    });

    $scope.viewPlanDetails=$routeParams.planId;

    $scope.emailPropertyDept = function() {
        $("#emailproperyDeptModal").modal("show");
        setTimeout(function(){$('#emailproperyDeptModal').modal('hide')}, 3000);
    }

    $scope.dtOptions = { paging: false, searching: false };
    var data={
        'planId':$scope.viewPlanDetails
    }
    $scope.docUploadURL = uploadFileURL;
    $scope.viwePlanDetailsFunction= function () {
    $http({
        "method": "GET",
        "url": 'api/ProjectPlans/getDetails?planId=%7B%22planId%22%3A%20%22'+$scope.viewPlanDetails+'%22%7D',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        console.log("response......."+ JSON.stringify(response));
        $scope.viewProjectPlanDetails = response;
    }).error(function (response, data) {
    });

    }
    $scope.viwePlanDetailsFunction();
    $scope.taskComments = function (name,subTask) {
        $scope.taskCommentsName = name;
        $scope.taskCommentsData = subTask;
        $scope.taskcomment = true;
        $("#taskCommentData").modal("show");
    }
    $scope.commentdata = function(comment) {
        $scope.fcommentdata = comment;
        $("#fcommentData").modal("show");
        $scope.fcomment = true;
    }
    $scope.fileUploadComment = function(document) {
        $scope.fUploads = true;
        $scope.fUploaddata = document;
        $("#fUploadsData").modal("show");
    }
    $scope.riskComments = function(description) {
        $scope.riskCommentsArea = true;
        $scope.riskCommentsData = description;
        $("#riskCommentsAreaData").modal("show");
    }
    $scope.riskPrimaryInterest = function(intrestData) {
        $scope.riskCommentsArea = false;
        $scope.riskInterestData = intrestData;
        $("#riskCommentsAreaData").modal("show");
    }
    $scope.trasferFundsDesc = function(transferFunds) {
        $scope.trasferDescArea = true;
        $scope.trasferDescData = transferFunds;
        $("#trasferDescAreaData").modal("show");
    }

}]);

app.controller('billGenerationController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {
       $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.billGeneration").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.billGeneration .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
       	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectBillGeneration'}, function (response) {
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

       $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
           .withOption("sPaginationType", "bootstrap")
           .withOption("retrieve", true)
           .withOption('stateSave', true)
           .withPaginationType('simple_numbers')
           .withOption('order', [0, 'ASC']);

       $scope.exportToExcel=function(tableId){

               if( $scope.selectProjectDetails!=null) {
                   var schemes = $scope.selectProjectDetails;
                   $("<tr>" +
                       "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Plan Id</th>" +
                       "<td>" + schemes.planId + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Plan Name</th>" +
                       "<td>" + schemes.name + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Financial Year</th>" +
                       "<td>" + schemes.financialYear + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Start Date</th>" +
                       "<td>" + schemes.startDate + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>End Date</th>" +
                       "<td>" + schemes.endDate + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Estimation Cost</th>" +
                       "<td>" + schemes.estimationCost + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Utilized Cost</th>" +
                       "<td>" + schemes.utilizedCost + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Task Status</th>" +
                       "<td>" + schemes.taskStatus + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Plan Status</th>" +
                       "<td>" + schemes.palnStatus + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>% Complete</th>" +
                       "<td>" + schemes.completePercentage + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Department</th>" +
                       "<td>" + schemes.department + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "<th>Plan Description</th>" +
                       "<td>" + schemes.description + "</td>" +
                       "</tr>").appendTo("table" + tableId);
                   $("<tr>" +
                       "</tr>").appendTo("table" + tableId);

               }

               if( $scope.subTaskDetails!=null) {
                               var schemes = $scope.subTaskDetails;
                               $("<tr>" +
                                   "<th colspan='2' style='background-color: #ebebeb;'><h3>Project Task Details</h3></th>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Header Name</th>" +
                                   "<td>" + schemes.headerName + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>SubTask Name</th>" +
                                   "<td>" + schemes.subTaskName + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Start Date</th>" +
                                   "<td>" + schemes.startDate + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>End Date</th>" +
                                   "<td>" + schemes.endDate + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Complete Percentage</th>" +
                                   "<td>" + schemes.completePercenatge + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Status</th>" +
                                   "<td>" + schemes.status + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Financial Year</th>" +
                                   "<td>" + schemes.financialYear + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Estimated Cost</th>" +
                                   "<td>" + schemes.estimatedCost + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Utilized Cost</th>" +
                                   "<td>" + schemes.utilizedCost + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Duration</th>" +
                                   "<td>" + schemes.duration + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Resource Id</th>" +
                                   "<td>" + schemes.resourceId + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>Resource</th>" +
                                   "<td>" + schemes.resource + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "<th>SubTask Comment</th>" +
                                   "<td>" + schemes.subTaskComment + "</td>" +
                                   "</tr>").appendTo("table" + tableId);
                               $("<tr>" +
                                   "</tr>").appendTo("table" + tableId);

                           }

               var exportHref = Excel.tableToExcel(tableId, 'Measurement Book Details');
               $timeout(function () {
                   location.href = exportHref;
               }, 100);
           }

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
       var filedVisitEditDoubleClick=false;
    projectWardWorks.getPlanDetails({}, function (response) {
        $rootScope.charterList=response;
    });

    $scope.selectPlanId = function(projectId){
        $scope.subTaskList=[];
        $scope.selectProjectId = projectId;
        $scope.showAction = true;
           projectWardWorks.getSelectedPlan(projectId, function (response) {
               $scope.selectProjectDetails=response[0];
               $scope.subtaskList = true;
                $scope.billGeneration = false;
           });
           getProjectTaskDetails();
       }


    function getProjectTaskDetails(){
        $http({
            method: 'GET',
            url: 'api/ProjectTORs/getSubTaskDetails?planId='+$scope.selectProjectId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.subTaskList=response;
            //taskChecks($scope.subTaskList);
        }).error(function (response) {
        });
    }
    function getContractors(){
        $http({
            method: 'GET',
            url: 'api/Employees?filter=%7B%22where%22%3A%7B%22department%22%3A%20%22consultant%22%7D%7D' ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log("response"+JSON.stringify(response));
            $scope.contratorList=response;
        }).error(function (response) {
        });
    }
    getContractors();
    $scope.selectBillInformation=function(selectedTask){
        $scope.selectedTaskData=selectedTask;
        if($scope.subTaskList!=undefined && $scope.subTaskList!=null && $scope.subTaskList.length>0){
            for(var i=0;i<$scope.subTaskList.length;i++){
                if($scope.subTaskList[i].subTaskName==selectedTask){
                    $scope.subTaskDetails=$scope.subTaskList[i];
                    break;
                }
            }
        }
        $scope.billGeneration = true;
        getBillDetails();
    }
    $scope.billData={}
    $scope.billData.rows=[];
    $scope.createBillGeneration = function() {
        $scope.getRefernceDoc();
               $scope.billData = {};
               $("#createBillGeneration").modal("show");
               $scope.billData.rows = [  {
                   "name": "",
                   "billDescription": "",
                   "billUnits": "",
                   "billRate": "",
                   "billMeasurement": "",
                   "billAmount": ""
               }];
           }
    $scope.getRefernceDoc=function () {
        $http({
            method: 'GET',
            url: 'api/AppConfigs',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            console.log('Users Response.... :' + JSON.stringify(response));
            $scope.appConfig = response[0];
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.docUploadURL = uploadFileURL;
    $scope.addRow = function() {
               $scope.billData.rows.push(
                    {
                        "name": "",
                        "billDescription": "",
                        "billUnits": "",
                        "billRate": "",
                        "billMeasurement": "",
                        "billAmount": ""
                        });
           }

        $scope.addEditRow = function() {
            $scope.editDetails.rows.push({
                        "name": "",
                        "billDescription": "",
                        "billUnits": "",
                        "billRate": "",
                        "billMeasurement": "",
                        "billAmount": ""
            });
            if($scope.editDetails.rows.length>1) {
                $scope.editDetails.estimatedTotalAmount=0;
                for(var i=0; i<$scope.editDetails.rows.length; i++) {
                    $scope.editDetails.estimatedTotalAmount += parseInt($scope.editDetails.rows[i].rowAmount);
                }
            }
        }
    $scope.removeGroup = function(idx,e) {
               if (e) {
                     e.preventDefault();
                     e.stopPropagation();
                 }
                 $scope.billData.rows.splice(idx, 1);

                 if($scope.billData.rows.length<=0) {
                   $scope.addWorkArticle = false;

                   }

                $scope.billData.totalAmount=0;
                if($scope.billData.rows!=null && $scope.billData.rows.length>0){
                    for(var count=0;count<$scope.billData.rows.length;count++){
                        $scope.billData.totalAmount+=parseInt($scope.billData.rows[count].billAmount);
                    }
                }
           }


    $scope.removeGroupEdit = function(idx,e) {
            if (e) {
                  e.preventDefault();
                  e.stopPropagation();
              }
              $scope.editDetails.rows.splice(idx, 1);

              if($scope.editDetails.rows.length<=0) {
                $scope.addWorkArticle = false;

                }
        $scope.editDetails.totalAmount=0;
        if($scope.editDetails.rows!=null && $scope.editDetails.rows.length>0){
            for(var count=0;count<$scope.editDetails.rows.length;count++){
                $scope.editDetails.totalAmount+=parseInt($scope.editDetails.rows[count].rowAmount);
            }
        }
        }

    $scope.billDetailsModal = function(billData) {
//        console.log("responce"+JSON.stringify(billData));
        $("#viewBillDetails").modal("show");
        $scope.viewBillDetails = billData;
    }
    $scope.editBillGeneration = function(editBillGeneration) {
        $("#viewBillDetails").modal("hide");
        $("#editBillGeneration").modal("show");
        $scope.editDetails=angular.copy(editBillGeneration);

    }


    $scope.billRate=function(indexPosition){
        $scope.billData.totalAmount=0;
        if($scope.billData.rows!=null && $scope.billData.rows.length>0){
        /*if($scope.billData.rows[indexPosition].billRate==undefined || $scope.billData.rows[indexPosition].billRate==null || $scope.billData.rows[indexPosition].billRate==''){
            $scope.billData.rows[indexPosition].billRate=0;
        }
            */
            if($scope.billData.rows[indexPosition].billRate!=undefined && $scope.billData.rows[indexPosition].billRate!=null

                && $scope.billData.rows[indexPosition].billRate!='' && $scope.billData.rows[indexPosition].billMeasurement!=undefined && $scope.billData.rows[indexPosition].billMeasurement!=null && $scope.billData.rows[indexPosition].billMeasurement!='') {
            $scope.billData.rows[indexPosition].billAmount=(parseInt($scope.billData.rows[indexPosition].billMeasurement)*($scope.billData.rows[indexPosition].billRate));
            $scope.billData.totalAmount=0;
            if($scope.billData.rows!=null && $scope.billData.rows.length>0){
                for(var count=0;count<$scope.billData.rows.length;count++){
                    $scope.billData.totalAmount+=parseInt($scope.billData.rows[count].billAmount);
                }
            }
        }else{
                $scope.billData.rows[indexPosition].billAmount=0;
            }

    }
    }


$scope.billEditRate=function(indexPosition){
        $scope.editDetails.totalAmount=0;
        if($scope.editDetails.rows!=null && $scope.editDetails.rows.length>0){
        if($scope.editDetails.rows[indexPosition].billRate!=undefined && $scope.editDetails.rows[indexPosition].billRate!=null && $scope.editDetails.rows[indexPosition].billRate!=''
            && $scope.editDetails.rows[indexPosition].billMeasurement!=undefined && $scope.editDetails.rows[indexPosition].billMeasurement!=null && $scope.editDetails.rows[indexPosition].billMeasurement!='') {
            $scope.editDetails.rows[indexPosition].billAmount=(parseInt($scope.editDetails.rows[indexPosition].billMeasurement)*($scope.editDetails.rows[indexPosition].billRate));
            $scope.editDetails.totalAmount=0;
            if($scope.editDetails.rows!=null && $scope.editDetails.rows.length>0){
                for(var count=0;count<$scope.editDetails.rows.length;count++){
                    $scope.editDetails.totalAmount+=parseInt($scope.editDetails.rows[count].billAmount);
                }
            }
        }
    }
                //$scope.billData.totalAmount+=parseInt($scope.billData.rows[count].billAmount);

        }
       var addBillDataDoubleClick=false;
       $scope.addBillDetails=function () {
               if(!addBillDataDoubleClick){
                   addBillDataDoubleClick=true;
                   var billDetails=$scope.billData;
                   billDetails['planId']=$scope.selectProjectId;
                   billDetails['subtask']=$scope.selectedTaskData;
                   billDetails['createdPerson']=loginPersonDetails.name;
                   billDetails['createdPersonId']=loginPersonDetails.employeeId;
                   $http({
                       method: 'POST',
                       url: 'api/BillGenerations',
                       headers: {"Content-Type": "application/json", "Accept": "application/json"},
                       "data":billDetails
                   }).success(function (response) {
//                   console.log("responce"+ JSON.stringify(response));

                       getBillDetails();
                       $("#createBillGeneration").modal("hide");
                       $("#editBillReportSuccess").modal("show");
                       addBillDataDoubleClick=false;
                       setTimeout(function(){$('#editBillReportSuccess').modal('hide')}, 3000);
                   }).error(function (response) {
                       addBillDataDoubleClick=false;
                   });
               }
       }
    function getBillDetails() {
        $http({
            method: 'GET',
            url: 'api/BillGenerations/getDetails?employeeId='+loginPersonDetails.employeeId+'&planId='+ $scope.selectProjectId +'&subTask='+$scope.selectedTaskData,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.billList=response;
            console.log("bill details-----"+JSON.stringify(response));
        }).error(function (response) {
        });
    }
         $scope.comments;
       $scope.approvalModal=function(billdata){
           $scope.billInfo=angular.copy(billdata);
           $("#approavlReport").modal("show");
           $scope.comments = '';
       }
       $scope.rejectModal=function(billdata){
           $scope.billInfo=angular.copy(billdata);
           $("#rejectReport").modal("show");
           $scope.comments = '';
       }
      var employeeId=loginPersonDetails.employeeId;
       var employeeName=loginPersonDetails.name;
       var approvalDoubleClick=false;
       $scope.approvalData=function(){
           if(!approvalDoubleClick){
               if(parseInt($scope.comments.approvedAmount)<=$scope.billInfo.totalAmount){
                   approvalDoubleClick=true;
                   var updateDetails = {
                       "acceptLevel":$scope.billInfo.acceptLevel,
                       "acceptStatus": "Yes",
                       "requestStatus": "Approval",
                       "comment":  $scope.comments.comment,
                       "requestId":$scope.billInfo.id,
                       "approvedAmount":$scope.comments.approvedAmount,
                       "employeeId":employeeId,
                       "employeeName":employeeName
                   };
                   $http({
                       "method": "POST",
                       "url": 'api/BillGenerations/updateDetails',
                       "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                       "data": updateDetails
                   }).success(function (response, data) {
                       if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                           response.error.message!=null && response.error.message!=''){
                           approvalDoubleClick=false;
                           $("#approavlReport").modal("hide");
                           $("#errorMessageDisplay").modal("show");
                           setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                       }else{
                            getBillDetails();
                           approvalDoubleClick=false;
                           $("#approavlReport").modal("hide");
                       }
                   }).error(function (response, data) {
                       approvalDoubleClick=false;
                   })
               }else{
//                   alert('please enter less amount');
                                     $scope.errorMessage=true;
                                $timeout(function(){
                                     $scope.errorMessage=false;
                                 }, 3000);
               }
           }
       }
       var rejectDoubleClick=false;
       $scope.rejectData=function(){
           if(!rejectDoubleClick){
               rejectDoubleClick=true;
               var updateDetails = {
                   "acceptLevel":$scope.billInfo.acceptLevel,
                   "acceptStatus": "No",
                   "requestStatus": "Rejected",
                   "comment":  $scope.comments.comment,
                   "requestId":$scope.billInfo.id,
                   "employeeId":employeeId,
                   "employeeName":employeeName
               };
               $http({
                   "method": "POST",
                   "url": 'api/BillGenerations/updateDetails',
                   "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                   "data": updateDetails
               }).success(function (response, data) {
                   if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                       response.error.message!=null && response.error.message!=''){
                       $("#rejectReport").modal("hide");
                       rejectDoubleClick=false;
                       $("#errorMessageDisplay").modal("show");
                       setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                   }else{
                       $scope.getFieldVisitReport();
                       $("#rejectReport").modal("hide");
                       rejectDoubleClick=false;
                   }
               }).error(function (response, data) {
                   rejectDoubleClick=false;
               })
           }
       }
    
    $scope.editBillDetails= function (billData) {
        $scope.editDetails=angular.copy(billData);
        $("#editBillGeneration").modal("show");
    }
    var billEditDoubleClick=false;
    $scope.editBillData=function(){
        if(!billEditDoubleClick){
            billEditDoubleClick=true;
            var editCharterDetails=$scope.editDetails;
            editCharterDetails['lastEditPerson']=loginPersonDetails.name;
            editCharterDetails['lastEditPersonId']=loginPersonDetails.employeeId;
            $http({
                method: 'POST',
                url: 'api/BillGenerations/updateContent',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:editCharterDetails
            }).success(function (response) {
//                console.log("responce"+JSON.stringify(response))
                getBillDetails();
                $("#editBillGeneration").modal("hide");
                billEditDoubleClick=false;
            }).error(function (response) {
                billEditDoubleClick=false;
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }

   }]);

app.controller('measurementBookController',['projectWardWorks','$http', '$scope','Upload', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, Upload,$window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {
   $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.measurementBook").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.measurementBook .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectMeasurementBook'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        if( $scope.subTaskDetails!=null) {
                        var schemes = $scope.subTaskDetails;
                        $("<tr>" +
                            "<th colspan='2' style='background-color: #ebebeb;'><h3>Project Task Details</h3></th>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Header Name</th>" +
                            "<td>" + schemes.headerName + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>SubTask Name</th>" +
                            "<td>" + schemes.subTaskName + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Start Date</th>" +
                            "<td>" + schemes.startDate + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>End Date</th>" +
                            "<td>" + schemes.endDate + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Complete Percentage</th>" +
                            "<td>" + schemes.completePercenatge + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Status</th>" +
                            "<td>" + schemes.status + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Financial Year</th>" +
                            "<td>" + schemes.financialYear + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Estimated Cost</th>" +
                            "<td>" + schemes.estimatedCost + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Utilized Cost</th>" +
                            "<td>" + schemes.utilizedCost + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Duration</th>" +
                            "<td>" + schemes.duration + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Resource Id</th>" +
                            "<td>" + schemes.resourceId + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>Resource</th>" +
                            "<td>" + schemes.resource + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "<th>SubTask Comment</th>" +
                            "<td>" + schemes.subTaskComment + "</td>" +
                            "</tr>").appendTo("table" + tableId);
                        $("<tr>" +
                            "</tr>").appendTo("table" + tableId);

                    }

        if( $scope.subTaskLists!=null &&  $scope.subTaskLists.length>0){

        $("<tr>" +
            "<th colspan='17' style='background-color: #ebebeb;'><h3>Project Measurement Details</h3></th>" +
            "</tr>").appendTo("table" + tableId);

                    $("<tr>" +
                        "<th>Measurement Id</th>" +
                        "<th>Contractor Name</th>" +
                        "<th>Allotted Work</th>" +
                        "<th>Allotted Work<table><tr>" +
                        "<th>Length</th>" +
                        "<th>Breadth</th>" +
                        "<th>Height</th>" +
                        "</tr></table>" +
                        "<th>Completed Work<table><tr>" +
                        "<th>Length</th>" +
                        "<th>Breadth</th>" +
                        "<th>Height</th>" +
                        "</tr></table>" +
                        "<th>Amount Work</th>" +
                        "<th>Completed Work Amount</th>" +
                        "<th>Advance Pay</th>" +
                        "<th>TDS</th>" +
                        "<th>Security Deposit</th>" +
                        "<th>Date Completion</th>" +
                        "<th>SanAuthority Date</th>" +
                        "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.subTaskLists.length;i++){
                var subTask=$scope.subTaskLists[i];

                        $("<tr>" +
                            "<td>"+subTask.measurementId+"</td>" +
                            "<td>"+subTask.contractorName+"</td>" +
                            "<td>"+subTask.allottedWork+"</td>" +
                            "<td><table><tr><td>"+subTask.allottedWorkLength+"</td>" +
                            "<td>"+subTask.allottedWorkBreadth+"</td>" +
                            "<td>"+subTask.allottedWorkHeight+"</td></tr></table></td>" +
                            "<td><table><tr><td>"+subTask.compltedDateLength+"</td>" +
                            "<td>"+subTask.compltedDateBreadth+"</td>" +
                            "<td>"+subTask.compltedDateHeight+"</td></tr></table></td>" +
                            "<td>"+subTask.amountWork+"</td>" +
                            "<td>"+subTask.completedWorkAmount+"</td>" +
                            "<td>"+subTask.advacePay+"</td>" +
                            "<td>"+subTask.tds+"</td>" +
                            "<td>"+subTask.SecurityDeposite+"</td>" +
                            "<td>"+subTask.dateCompletion+"</td>" +
                            "<td>"+subTask.sanAuthority+"</td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                }

        var exportHref = Excel.tableToExcel(tableId, 'Measurement Book Details');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.loadingImage=true;
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
        });
    }
    function getProjectTaskDetails(){
        $scope.subTaskList = [];
        $http({
            method: 'GET',
            url: 'api/ProjectTORs/getSubTaskDetails?planId='+$scope.selectProjectId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log("sub lists"+ JSON.stringify(response))
            $scope.subTaskList=response;
            //taskChecks($scope.subTaskList);
        }).error(function (response) {
        });
    }
    $scope.getCharterDetails();

    function getContractors(){
            $http({
                method: 'GET',
                url: 'api/Employees?filter=%7B%22where%22%3A%7B%22department%22%3A%20%22consultant%22%7D%7D' ,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
//                console.log("response"+JSON.stringify(response));
                $scope.contratorList=response;
            }).error(function (response) {
            });
        }
        getContractors();

    $scope.editMeasurement = function(details) {
        $("#editMeasurementBook").modal("show");
        $scope.editMeasureBook = angular.copy(details);

    }

    $scope.getRefernceDoc=function () {
        $http({
            method: 'GET',
            url: 'api/AppConfigs',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            console.log('Users Response.... :' + JSON.stringify(response));
            $scope.appConfig = response[0];
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.docUploadURL = uploadFileURL;


    $scope.editBook = function(viewMeasurementDetails) {

        $("#viewMeasurementBook").modal("hide");
        $("#editMeasurementBook").modal("show");
        $scope.editMeasureBook = angular.copy(viewMeasurementDetails);
    }
    var editMeasureBookClick=false;
    $scope.editMeasureBookSubmit=function () {

        if(!editMeasureBookClick){
            editMeasureBookClick=true;
            var editMeasureBookDetails= $scope.editMeasureBook;
            editMeasureBookDetails['lastEditPerson']=loginPersonDetails.name;
            editMeasureBookDetails['lastEditPersonId']=loginPersonDetails.employeeId;
            $http({
                method: 'PUT',
                url: 'api/ProjectMeasurements/'+$scope.editMeasureBook.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:editMeasureBookDetails
            }).success(function (response) {
//                console.log(' Response :' + JSON.stringify(response));
//                getTORReport($scope.torId);
                $("#editMeasurementBook").modal("hide");
                editMeasureBookClick=false;
            $scope.measurementTaskDetails();
            }).error(function (response) {
                editMeasureBookClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }

    $scope.viewMeasurement = function(details) {
        $("#viewMeasurementBook").modal("show");
        $scope.viewMeasurementDetails = details;
    }

    $scope.torReport={};
    $scope.addMeasurementBookModal=function(){
        $scope.getRefernceDoc();
        $('#createMeasurementBook').modal('show');
        $scope.measurementData={};
    }
    var addMeasurementBookClick=false;
    $scope.addMeasurementBook= function () {
            if(!addMeasurementBookClick){
                addMeasurementBookClick=true;
                var measurementDetails=$scope.measurementData;
                measurementDetails['planId']=$scope.selectProjectId;
                measurementDetails['subTaskName']=$scope.selectedTaskData;
                measurementDetails['createdPerson']=loginPersonDetails.name;
                measurementDetails['createdPersonId']=loginPersonDetails.employeeId;

                $http({
                    method: 'POST',
                    url: 'api/ProjectMeasurements',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":measurementDetails
                }).success(function (response) {
//                    console.log("responce"+ JSON.stringify(response));
                    $("#createMeasurementBook").modal("hide");
                    $("#creatMeasurementBookSuccess").modal("show");
                    addMeasurementBookClick=false;
//                    getTORReport($scope.torId);
                    setTimeout(function(){$('#creatMeasurementBookSuccess').modal('hide')}, 3000);
            $scope.measurementTaskDetails();
                }).error(function (response) {
                    addMeasurementBookClick=false;
                });
            }

    }
    $scope.selectPlanId = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
            $scope.taskDetails=true;
        });
        $scope.subtaskList = true;
        $scope.subTaskList = [];
        getProjectTaskDetails();
        $scope.measurementTaskDetails();
    }

        $scope.selectMeasurement=function(selectedTask){
            $scope.selectedTaskData=selectedTask;
            $scope.planId =$scope.selectProjectId;
            if($scope.subTaskList!=undefined && $scope.subTaskList!=null && $scope.subTaskList.length>0){
                for(var i=0;i<$scope.subTaskList.length;i++){
                    if($scope.subTaskList[i].subTaskName==selectedTask){
                        $scope.subTaskDetails=$scope.subTaskList[i];
//                        console.log("response" + JSON.stringify($scope.subTaskDetails))
                        break;
                    }
                }
            }
            $scope.subtaskDetails = true;
            $scope.measurementTaskDetails();
        }
$scope.reset = function() {
    $scope.measurementData = {};
    $scope.measurementData = [];
}
        $scope.measurementTaskDetails = function() {
            $scope.planId =$scope.selectProjectId;
            $scope.selectedData =$scope.selectedTaskData;
                $http({
                       method: 'GET',
                       url: 'api/ProjectMeasurements/?filter={"where":{"and":[{"planId":"' + $scope.planId  + '"},{"subTaskName":"' + $scope.selectedData  + '"}]}}',
                       headers: {"Content-Type": "application/json", "Accept": "application/json"},
                   }).success(function (response) {
                       console.log("responce"+ JSON.stringify(response));
                       $scope.subTaskLists = response;
                   }).error(function (response) {
//                       console.log("errors"+ JSON.stringify(response));
                   });
        }

}]);

app.controller('paymentOrderController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li .collapse").removeClass("in");
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.paymentOrder").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.paymentOrder .collapse").addClass("in");

    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPaymentOrder'}, function (response) {
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

           if( $scope.selectProjectDetails!=null) {
               var schemes = $scope.selectProjectDetails;
               $("<tr>" +
                   "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Plan Id</th>" +
                   "<td>" + schemes.planId + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Plan Name</th>" +
                   "<td>" + schemes.name + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Financial Year</th>" +
                   "<td>" + schemes.financialYear + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Start Date</th>" +
                   "<td>" + schemes.startDate + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>End Date</th>" +
                   "<td>" + schemes.endDate + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Estimation Cost</th>" +
                   "<td>" + schemes.estimationCost + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Utilized Cost</th>" +
                   "<td>" + schemes.utilizedCost + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Task Status</th>" +
                   "<td>" + schemes.taskStatus + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Plan Status</th>" +
                   "<td>" + schemes.palnStatus + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>% Complete</th>" +
                   "<td>" + schemes.completePercentage + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Department</th>" +
                   "<td>" + schemes.department + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "<th>Plan Description</th>" +
                   "<td>" + schemes.description + "</td>" +
                   "</tr>").appendTo("table" + tableId);
               $("<tr>" +
                   "</tr>").appendTo("table" + tableId);

           }

            if( $scope.paymentOrderList!=null &&  $scope.paymentOrderList.length>0){

                                   $("<tr>" +
                                       "<th colspan='13' style='background-color: #ebebeb;'><h3>Payment Order Details</h3></th>" +
                                       "</tr>").appendTo("table" + tableId);

                                $("<tr>" +
                                    "<th>Bill Id</th>" +
                                    "<th>Estimated Amount</th>" +
                                    "<th>Final Status</th>" +
                                    "<th>Created Person</th>" +
                                    "<th>Created Time</th>" +
                                    "<th colspan='5' style='background-color: #ccc;'>Info"+
                                    "<table><tr><th>Article</th><th>Quntity</th><th>rate</th><th>Units</th><th>Row Amount</th></tr></th></tr></table></th>" +
                                    "</tr>").appendTo("table"+tableId);

                                for(var i=0;i<$scope.paymentOrderList.length;i++){
                                    var schemes=$scope.paymentOrderList[i];

                                    $("<tr>" +
                                    "<td rowspan='"+(schemes.rows.length+1)+"'>"+schemes.billId+"</td>"+
                                                    "<td rowspan='"+(schemes.rows.length+1)+"'>"+schemes.estimatedAmount+"</td>" +
                                                    "<td rowspan='"+(schemes.rows.length+1)+"'>"+schemes.finalStatus+"</td>" +
                                                    "<td rowspan='"+(schemes.rows.length+1)+"'>"+schemes.createdPerson+"</td>" +
                                                    "<td rowspan='"+(schemes.rows.length+1)+"'>"+schemes.createdTime+"</td>" +
                                    "<td>").appendTo("table"+tableId);
                                    if(schemes.rows != null) {

                                                $scope.schemeIs= [];
                                                for(var j=0; j<schemes.rows.length;j++) {
                                                        $scope.schemeIs.push(schemes.rows[j]);
    //                                                                                            alert(schemes.rows.length);

                                                    $("<tr><td>"+$scope.schemeIs[j].article+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].quantity+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].rate+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].units+"</td>" +
                                                    "<td>"+$scope.schemeIs[j].rowAmount+"</td>" +
                                                    "</tr>").appendTo("table"+tableId);

                                                    }}
                                                    $("</td>"+
                                                    "</tr>").appendTo("table"+tableId);
                                   }
                                var exportHref = Excel.tableToExcel(tableId, 'Payment Order Details ');
                                $timeout(function () { location.href = exportHref; }, 100);
                            } else {
                                $("#emptyDataTable").modal("show");
                                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                            }

           var exportHref = Excel.tableToExcel(tableId, 'Payment Order Details');
           $timeout(function () {
               location.href = exportHref;
           }, 100);
       }

    $scope.reset = function() {
        $scope.paymentOrder = {};
    }
    var paymentOrderEditDoubleClick=false;
    $scope.editPaymentOrderDetails=function(paymentOrder){
        projectWardWorks.getBillInfomation($scope.selectProjectId, function (billData) {
            $scope.billDataList=billData;
            $scope.editPaymentOrderVisit = angular.copy(paymentOrder);
            $("#editPaymentOrderReport").modal("show");
        });
    }

    $scope.payOrderDetailsModal = function(paymentOrder) {
        $("#viewPayOrderDetails").modal("show");
        $scope.paymentOrderDetails = paymentOrder;
//        console.log("response"+JSON.stringify(paymentOrder));
    }

    $scope.editPayOrder = function(paymentOrder) {
        $("#viewPayOrderDetails").modal("hide");
            $("#editPaymentOrderReport").modal("show");
            $scope.editPaymentOrderVisit = angular.copy(paymentOrder);
    }

    $scope.editPaymentOrderReport=function () {
        if(!paymentOrderEditDoubleClick){
            paymentOrderEditDoubleClick=true;
            var editPaymentDetails=$scope.editPaymentOrderVisit;
            editPaymentDetails['lastEditPerson']=loginPersonDetails.name;
            editPaymentDetails['lastEditPersonId']=loginPersonDetails.employeeId;
            $http({
                method: 'POST',
                url: 'api/ProjectPaymentOrders/updateContent',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:editPaymentDetails
            }).success(function (response) {
                $scope.getPaymentOrderReport();
                $("#editPaymentOrderReport").modal("hide");
                paymentOrderEditDoubleClick=false;
            }).error(function (response) {
                paymentOrderEditDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }
    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
        });
        $scope.getPaymentOrderReport();
    }
    projectWardWorks.getPlanDetails({}, function (response) {
        $rootScope.charterList=response;
    });
    $scope.paymentOrder = {};
    $scope.createPaymentOrder = function() {
        $("#createPaymentOrderReport").modal("show");
        projectWardWorks.getBillInfomation($scope.selectProjectId, function (billData) {
           $scope.billDataList=billData;
        });
        $scope.paymentOrder.rows = [];
        $scope.paymentOrder.rows.push({"article": "", "quantity": "", "rate": "", "units": "", "rowAmount": ""});
    }
    $scope.addRow = function() {
        $scope.paymentOrder.rows.push({"article": "", "quantity": "", "rate": "", "units": "", "rowAmount": ""});
        if($scope.paymentOrder.rows.length>1) {
            $scope.paymentOrder.estimatedTotalAmount=0;
            for(var i=0; i<$scope.paymentOrder.rows.length; i++) {
                $scope.paymentOrder.estimatedTotalAmount += parseInt($scope.paymentOrder.rows[i].rowAmount);
            }
        }
    }
    $scope.addRowEdit = function() {
            $scope.editPaymentOrderVisit.rows.push({"article": "", "quantity": "", "rate": "", "units": "", "rowAmount": ""});
            if($scope.editPaymentOrderVisit.rows.length>1) {
                $scope.editPaymentOrderVisit.estimatedTotalAmount=0;
                for(var i=0; i<$scope.editPaymentOrderVisit.rows.length; i++) {
                    $scope.editPaymentOrderVisit.estimatedTotalAmount += parseInt($scope.editPaymentOrderVisit.rows[i].rowAmount);
                }
            }
        }
    $scope.removeGroup = function(idx,e) {
        if (e) {
              e.preventDefault();
              e.stopPropagation();
          }
          $scope.paymentOrder.rows.splice(idx, 1);

          if($scope.paymentOrder.rows.length<=0) {
            $scope.addWorkArticle = false;

            }
        $scope.paymentOrder.estimatedAmount=0;
        if($scope.paymentOrder.rows!=null && $scope.paymentOrder.rows.length>0){
            for(var count=0;count<$scope.paymentOrder.rows.length;count++){
                $scope.paymentOrder.estimatedAmount+=parseInt($scope.paymentOrder.rows[count].rowAmount);
            }
        }
    }

    $scope.removeGroupEdit = function(idx,e) {
            if (e) {
                  e.preventDefault();
                  e.stopPropagation();
              }
              $scope.editPaymentOrderVisit.rows.splice(idx, 1);

              if($scope.editPaymentOrderVisit.rows.length<=0) {
                $scope.addWorkArticle = false;

                }
        $scope.editPaymentOrderVisit.estimatedAmount=0;
        if($scope.editPaymentOrderVisit.rows!=null && $scope.editPaymentOrderVisit.rows.length>0){
            for(var count=0;count<$scope.editPaymentOrderVisit.rows.length;count++){
                $scope.editPaymentOrderVisit.estimatedAmount+=parseInt($scope.editPaymentOrderVisit.rows[count].rowAmount);
            }
        }
        }

    var addOrderPaymentDoubleClick=false;
    $scope.orderRate= function (indexPosition) {
        if($scope.paymentOrder.rows!=null && $scope.paymentOrder.rows.length>0){
            if($scope.paymentOrder.rows[indexPosition].rate!=undefined && $scope.paymentOrder.rows[indexPosition].rate!= null && $scope.paymentOrder.rows[indexPosition].rate!=''
                && $scope.paymentOrder.rows[indexPosition].units!=undefined && $scope.paymentOrder.rows[indexPosition].units!=null && $scope.paymentOrder.rows[indexPosition].units!='') {
                $scope.paymentOrder.rows[indexPosition].rowAmount=(parseInt($scope.paymentOrder.rows[indexPosition].rate)*($scope.paymentOrder.rows[indexPosition].units));
                $scope.paymentOrder.estimatedAmount=0;
                if($scope.paymentOrder.rows!=null && $scope.paymentOrder.rows.length>0){
                    for(var count=0;count<$scope.paymentOrder.rows.length;count++){
                        $scope.paymentOrder.estimatedAmount+=parseInt($scope.paymentOrder.rows[count].rowAmount);
                    }
                }
            }else{
                $scope.paymentOrder.rows[indexPosition].rowAmount=0;
            }
        }
    };
    $scope.editOrderRate= function (indexPosition) {
        if($scope.editPaymentOrderVisit.rows!=null && $scope.editPaymentOrderVisit.rows.length>0){
            if($scope.editPaymentOrderVisit.rows[indexPosition].rate!=undefined && $scope.editPaymentOrderVisit.rows[indexPosition].rate!= null && $scope.editPaymentOrderVisit.rows[indexPosition].rate!=''
                && $scope.editPaymentOrderVisit.rows[indexPosition].units!=undefined && $scope.editPaymentOrderVisit.rows[indexPosition].units!=null && $scope.editPaymentOrderVisit.rows[indexPosition].units!='') {
                $scope.editPaymentOrderVisit.rows[indexPosition].rowAmount=(parseInt($scope.editPaymentOrderVisit.rows[indexPosition].rate)*($scope.editPaymentOrderVisit.rows[indexPosition].units));
                $scope.editPaymentOrderVisit.estimatedAmount=0;
                if($scope.editPaymentOrderVisit.rows!=null && $scope.editPaymentOrderVisit.rows.length>0){
                    for(var count=0;count<$scope.editPaymentOrderVisit.rows.length;count++){
                        $scope.editPaymentOrderVisit.estimatedAmount+=parseInt($scope.editPaymentOrderVisit.rows[count].rowAmount);
                    }
                }
            }else{
                $scope.editPaymentOrderVisit.rows[indexPosition].rowAmount=0;
            }
        }
    }
    $scope.addPaymentOrderReport=function () {
            if(!addOrderPaymentDoubleClick){
                addOrderPaymentDoubleClick=true;
                var planId=$scope.selectProjectId;
                var orderDataDetails=$scope.paymentOrder;
                orderDataDetails['planId']=planId;
                orderDataDetails['createdPerson']=loginPersonDetails.name;
                orderDataDetails['createdPersonId']=loginPersonDetails.employeeId;
                orderDataDetails['visitorName']=loginPersonDetails.name;
                $http({
                    method: 'POST',
                    url: 'api/ProjectPaymentOrders',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":orderDataDetails
                }).success(function (response) {
                    $scope.getPaymentOrderReport();
                    $("#createPaymentOrderReport").modal("hide");
                    $("#editFieldVisitReportSuccess").modal("show");
                    addOrderPaymentDoubleClick=false;
                    setTimeout(function(){$('#editFieldVisitReportSuccess').modal('hide')}, 3000);
                }).error(function (response) {
                    addOrderPaymentDoubleClick=false;
                });
            }
    }
    $scope.comments;
    $scope.approvalModal=function(orderData){
        $scope.orderEditData=angular.copy(orderData);
        $("#approavlReport").modal("show");
        $scope.comments = '';
    }
    $scope.rejectModal=function(orderData){
        $scope.orderEditData=angular.copy(orderData);
        $("#rejectReport").modal("show");
        $scope.comments = '';
    }
    var employeeId=loginPersonDetails.employeeId;
    var employeeName=loginPersonDetails.name;
    var approvalDoubleClick=false;
    $scope.approvalData=function() {
        if (!approvalDoubleClick) {
            if (parseInt($scope.comments.approvedAmount) <= $scope.orderEditData.estimatedAmount) {
                approvalDoubleClick = true;
                var updateDetails = {
                    "acceptLevel": $scope.orderEditData.acceptLevel,
                    "acceptStatus": "Yes",
                    "requestStatus": "Approval",
                    "comment": $scope.comments.comment,
                    "approvedAmount": $scope.comments.approvedAmount,
                    "requestId": $scope.orderEditData.id,
                    "employeeId": employeeId,
                    "employeeName": employeeName
                };
                $http({
                    "method": "POST",
                    "url": 'api/ProjectPaymentOrders/updateDetails',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": updateDetails
                }).success(function (response, data) {
                    if (response.error != undefined && response.error != null && response.error.message != undefined &&
                        response.error.message != null && response.error.message != '') {
                        approvalDoubleClick = false;
//                    console.log("responce"+ JSON.stringify(responce));
                        $("#approavlReport").modal("hide");
                        $("#errorMessageDisplay").modal("show");
                        setTimeout(function () {
                            $('#errorMessageDisplay').modal('hide')
                        }, 3000);
                    } else {
                        $scope.getPaymentOrderReport();
                        approvalDoubleClick = false;
                        $("#approavlReport").modal("hide");
                    }
                }).error(function (response, data) {
                    approvalDoubleClick = false;
                })
            } else {
//                alert('please Enter Less Amount');
                        $scope.acceptError=true;
                    $timeout(function(){
                        $scope.acceptError=false;
                    }, 3000);
            }
        }
    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.orderEditData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.orderEditData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectPaymentOrders/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getPaymentOrderReport();
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                }
            }).error(function (response, data) {
                rejectDoubleClick=false;
            })
        }
    }
    $scope.getPaymentOrderReport=function () {
        var employeeId=loginPersonDetails.employeeId;
        var employeeName=loginPersonDetails.name;
        $http({
            method: 'GET',
            url: 'api/ProjectPaymentOrders/getDetails?employeeId='+employeeId+'&planId='+ $scope.selectProjectId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log("response"+JSON.stringify(response));
            $scope.paymentOrderList=response;
        }).error(function (response) {
        });
    }
}]);

app.controller('fieldVisitReportController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.fieldVisitReport").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.fieldVisitReport .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectFieldVisit'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

                       if( $scope.selectProjectDetails!=null) {
                           var schemes = $scope.selectProjectDetails;
                           $("<tr>" +
                               "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Plan Id</th>" +
                               "<td>" + schemes.planId + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Plan Name</th>" +
                               "<td>" + schemes.name + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Financial Year</th>" +
                               "<td>" + schemes.financialYear + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Start Date</th>" +
                               "<td>" + schemes.startDate + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>End Date</th>" +
                               "<td>" + schemes.endDate + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Estimation Cost</th>" +
                               "<td>" + schemes.estimationCost + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Utilized Cost</th>" +
                               "<td>" + schemes.utilizedCost + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Task Status</th>" +
                               "<td>" + schemes.taskStatus + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Plan Status</th>" +
                               "<td>" + schemes.palnStatus + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>% Complete</th>" +
                               "<td>" + schemes.completePercentage + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Department</th>" +
                               "<td>" + schemes.department + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Plan Description</th>" +
                               "<td>" + schemes.description + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "</tr>").appendTo("table" + tableId);

                       }

                       if( $scope.filedVisitList!=null &&  $scope.filedVisitList.length>0){


                                       $("<tr>" +
                                           "<th colspan='8' style='background-color: #ebebeb;'><h3>Field Visit Details</h3></th>" +
                                           "</tr>").appendTo("table" + tableId);

                                $("<tr>" +
                                    "<th>Measurement ID</th>" +
                                    "<th>Visited Date</th>" +
                                    "<th>SubTask Name</th>" +
                                    "<th>Comment</th>" +
                                    "<th>Created Person</th>" +
                                    "<th>Visitor Name</th>" +
                                    "<th>Final Status</th>" +
                                    "<th>Created Time</th>" +
                                    "</tr>").appendTo("table"+tableId);

                                for(var i=0;i<$scope.filedVisitList.length;i++){
                                    var schemes=$scope.filedVisitList[i];

                                    $("<tr>" +
                                    "<td>"+schemes.measurementID+"</td>"+
                                    "<td>"+schemes.visitedDate+"</td>"+
                                    "<td>"+schemes.subTaskName+"</td>"+
                                    "<td>"+schemes.comment+"</td>"+
                                    "<td>"+schemes.createdPerson+"</td>"+
                                    "<td>"+schemes.visitorName+"</td>"+
                                    "<td>"+schemes.finalStatus+"</td>"+
                                    "<td>"+schemes.createdTime+"</td>"+
                                                    "</tr>").appendTo("table"+tableId);
                                   }
                                var exportHref = Excel.tableToExcel(tableId, 'Payment Order Details ');
                                $timeout(function () { location.href = exportHref; }, 100);
                            } else {
                                $("#emptyDataTable").modal("show");
                                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                            }

                       var exportHref = Excel.tableToExcel(tableId, 'Field Visit Report Details');
                       $timeout(function () {
                           location.href = exportHref;
                       }, 100);
                   }

    $scope.editFieldVisitors = function(fieldVisit) {
        $("#fieldVisitReportEdit").modal("show");
        $scope.editFieldVisit = angular.copy(fieldVisit);
    }

    $scope.fieldDetailsModel = function(fieldVisit) {
         console.log("responce"+ JSON.stringify(fieldVisit));
        $("#viewFieldsDetails").modal("show");
        $scope.fieldDetails = fieldVisit;
    }

    $scope.editFieldVisitData = function(fieldDetails) {
        $("#viewFieldsDetails").modal("hide");
        $("#fieldVisitReportEdit").modal("show");
        $scope.editFieldVisit = angular.copy(fieldDetails);
    }

    $scope.reset = function() {
        $scope.fieldVisitData = {};
        $scope.fieldVisitData = angular.copy($scope.master);
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    var filedVisitEditDoubleClick=false;
    $scope.fieldVisitReportEdit=function () {
        if(!filedVisitEditDoubleClick){
            filedVisitEditDoubleClick=true;
            var editCharterDetails=$scope.editFieldVisit;
            editCharterDetails['lastEditPerson']=loginPersonDetails.name;
            editCharterDetails['lastEditPersonId']=loginPersonDetails.employeeId;
//            console.log("editCharterDetails"+editCharterDetails);
            $http({
                method: 'POST',
                url: 'api/FieldVisits/updateContent',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:editCharterDetails
            }).success(function (response) {
                $scope.getFieldVisitReport();
                $("#fieldVisitReportEdit").modal("hide");
                filedVisitEditDoubleClick=false;
            }).error(function (response) {
                filedVisitEditDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }
    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
        });
        getProjectTaskDetails();
        $scope.getFieldVisitReport();
    }

        function getProjectTaskDetails(){
            $scope.subTaskList = [];
            $http({
                method: 'GET',
                url: 'api/ProjectTORs/getSubTaskDetails?planId='+$scope.selectProjectId ,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
//               alert(JSON.stringify(response))
                $scope.subTaskList=response;
                //taskChecks($scope.subTaskList);
            }).error(function (response) {
            });
        }

        $scope.selectSubTask=function(selectedTask){
            $scope.selectedTaskData=selectedTask;
            $scope.planId =$scope.selectProjectId;
            $scope.subTaskName = true;
            $scope.mesID = true;
            if($scope.subTaskList!=undefined && $scope.subTaskList!=null && $scope.subTaskList.length>0){
                for(var i=0;i<$scope.subTaskList.length;i++){
                    if($scope.subTaskList[i].subTaskName==selectedTask){
                        $scope.subTaskDetails=$scope.subTaskList[i];
                        break;
                    }
                }
            }
            getMeasurementDetails();
        }

    $scope.createFieldVisitReport = function() {
        $("#createFieldVisitReport").modal("show");
        $scope.fieldVisitData = {};
        $scope.mesID = false;    }

        function getMeasurementDetails(){
            $scope.planId = $scope.selectProjectId;
            $scope.selectedData = $scope.selectedTaskData;
            $http({
                method: 'GET',
                 url: 'api/ProjectMeasurements/?filter={"where":{"and":[{"planId":"' + $scope.planId  + '"},{"subTaskName":"' + $scope.selectedData  + '"}]}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                console.log(JSON.stringify(response))
                $scope.measurementDetails=response;
                //taskChecks($scope.subTaskList);
            }).error(function (response) {
            });
        }




    projectWardWorks.getPlanDetails({}, function (response) {
        $rootScope.charterList=response;
    });
    var addFiledVisitDoubleClick=false;
    $scope.addFieldVisitReport=function () {
        if($scope.fieldVisitData.visitedDate){
            if(!addFiledVisitDoubleClick){
                addFiledVisitDoubleClick=true;
                var planId=$scope.selectProjectId;
                var filedDetails=$scope.fieldVisitData;
                filedDetails['planId']=planId;
                filedDetails['subTaskName']=$scope.selectedTaskData;
//                filedDetails['measurementId']=$scope.measurementId;
                filedDetails['createdPerson']=loginPersonDetails.name;
                filedDetails['createdPersonId']=loginPersonDetails.employeeId;
                filedDetails['visitorName']=loginPersonDetails.name;
                $http({
                    method: 'POST',
                    url: 'api/FieldVisits',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":filedDetails
                }).success(function (response) {
                    console.log("responce.........." + JSON.stringify(response))
                    $scope.getFieldVisitReport();
                    $("#createFieldVisitReport").modal("hide");
                    $("#editFieldVisitReportSuccess").modal("show");
                    addFiledVisitDoubleClick=false;
                    setTimeout(function(){$('#editFieldVisitReportSuccess').modal('hide')}, 3000);
                }).error(function (response) {
                    addFiledVisitDoubleClick=false;
                });
            }

        } else{
            alert("Please Select Visit Date");
        }
    }
    $scope.comments;
    $scope.approvalModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#approavlReport").modal("show");
        $scope.comments = '';
    }
    $scope.rejectModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#rejectReport").modal("show");
        $scope.comments = '';
    }
    var employeeId=loginPersonDetails.employeeId;
    var employeeName=loginPersonDetails.name;
    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/FieldVisits/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
//            console.log("response"+ JSON.stringify(response));
                console.log("response"+ JSON.stringify(updateDetails));
                    $("#approavlReportStatus").modal("show");
                    $scope.new = updateDetails;
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getFieldVisitReport();
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                }
            }).error(function (response, data) {
                approvalDoubleClick=false;
            })
        }
    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/FieldVisits/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getFieldVisitReport();
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                }
            }).error(function (response, data) {
                rejectDoubleClick=false;
            })
        }
    }
    $scope.getFieldVisitReport=function () {
        var employeeId=loginPersonDetails.employeeId;
        var employeeName=loginPersonDetails.name;
        $http({
            method: 'GET',
            url: 'api/FieldVisits/getDetails?employeeId='+employeeId+'&planId='+ $scope.selectProjectId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.filedVisitList=response;
        }).error(function (response) {
        });
    }
}]);

app.controller('projectWorkOrderController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectWorkOrder").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectWorkOrder .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });
//
//    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
//    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectFieldVisit'}, function (response) {
//           if(!response){
//            window.location.href = "#/noAccessPage";
//           }
//        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

                       if( $scope.selectProjectDetails!=null) {
                           var schemes = $scope.selectProjectDetails;
                           $("<tr>" +
                               "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Plan Id</th>" +
                               "<td>" + schemes.planId + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Plan Name</th>" +
                               "<td>" + schemes.name + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Financial Year</th>" +
                               "<td>" + schemes.financialYear + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Start Date</th>" +
                               "<td>" + schemes.startDate + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>End Date</th>" +
                               "<td>" + schemes.endDate + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Estimation Cost</th>" +
                               "<td>" + schemes.estimationCost + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Utilized Cost</th>" +
                               "<td>" + schemes.utilizedCost + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Task Status</th>" +
                               "<td>" + schemes.taskStatus + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Plan Status</th>" +
                               "<td>" + schemes.palnStatus + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>% Complete</th>" +
                               "<td>" + schemes.completePercentage + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Department</th>" +
                               "<td>" + schemes.department + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "<th>Plan Description</th>" +
                               "<td>" + schemes.description + "</td>" +
                               "</tr>").appendTo("table" + tableId);
                           $("<tr>" +
                               "</tr>").appendTo("table" + tableId);

                       }

                       if( $scope.filedVisitList!=null &&  $scope.filedVisitList.length>0){


                                                          $("<tr>" +
                                                              "<th colspan='11' style='background-color: #ebebeb;'><h3>Work Order Details</h3></th>" +
                                                              "</tr>").appendTo("table" + tableId);

                                                   $("<tr>" +
                                                       "<th>Employee Id</th>" +
                                                       "<th>Employee Name</th>" +
                                                       "<th>Start Date</th>" +
                                                       "<th>End Date</th>" +
                                                       "<th>Estimated Cost</th>" +
                                                       "<th>Support Emp</th>" +
                                                       "<th>Main Emp</th>" +
                                                       "<th>Visitor Name</th>" +
                                                       "<th>Final Status</th>" +
                                                       "<th>Created Person</th>" +
                                                       "<th>Created Time</th>" +
                                                       "</tr>").appendTo("table"+tableId);

                                                   for(var i=0;i<$scope.filedVisitList.length;i++){
                                                       var schemes=$scope.filedVisitList[i];

                                                       $("<tr>" +
                                                       "<td>"+schemes.employeeId+"</td>"+
                                                       "<td>"+schemes.employeeName+"</td>"+
                                                       "<td>"+schemes.startDate+"</td>"+
                                                       "<td>"+schemes.EndDate+"</td>"+
                                                       "<td>"+schemes.EstimatedCost+"</td>"+
                                                       "<td>"+schemes.supportEmp+"</td>"+
                                                       "<td>"+schemes.mainEmp+"</td>"+
                                                       "<td>"+schemes.visitorName+"</td>"+
                                                       "<td>"+schemes.finalStatus+"</td>"+
                                                       "<td>"+schemes.createdPerson+"</td>"+
                                                       "<td>"+schemes.createdTime+"</td>"+
                                                       "</tr>").appendTo("table"+tableId);
                                                      }
                                                   var exportHref = Excel.tableToExcel(tableId, 'Payment Order Details ');
                                                   $timeout(function () { location.href = exportHref; }, 100);
                                               } else {
                                                   $("#emptyDataTable").modal("show");
                                                   setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                                               }

                       var exportHref = Excel.tableToExcel(tableId, 'Project Work Order Details');
                       $timeout(function () {
                           location.href = exportHref;
                       }, 100);
                   }

    function getContractors(){
            $http({
                method: 'GET',
                url: 'api/Employees?filter=%7B%22where%22%3A%7B%22department%22%3A%20%22consultant%22%7D%7D' ,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                console.log("response"+JSON.stringify(response));
                $scope.contratorList=response;
            }).error(function (response) {
            });
        }
        getContractors();

    $scope.editFieldVisitors = function(fieldVisit) {
        $("#fieldVisitReportEdit").modal("show");
        $scope.editWorkOrder= angular.copy(fieldVisit);
    }

    $scope.workOrderDetails = function(fieldVisit) {
        $scope.orderDetails = fieldVisit;
//        console.log(JSON.stringify($scope.orderDetails))
        $("#viewWorkOrderDetails").modal("show");
    }

    $scope.editWorkOrderData = function(orderDetails) {
        $("#viewWorkOrderDetails").modal("hide");
        $("#fieldVisitReportEdit").modal("show");
        $scope.editWorkOrder= angular.copy(orderDetails);

    }

    $scope.reset = function() {
        $scope.fieldVisitData = {};
    }

    $scope.projectWorkModal = function(fieldVisit) {
            $("#addProjectWorkOrder").modal("show");
            $scope.projectOrder = {};
        }
    $scope.getEmployeeType=function () {
        $http({
            method: 'GET',
            url: 'api/Employees?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.employeeList = response;
            $scope.employeeList1 = response;
        }).error(function (response) {
        });
    };
    $scope.getEmployeeType();

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    var filedVisitEditDoubleClick=false;
    $scope.fieldVisitReportEdit=function () {
        if(!filedVisitEditDoubleClick){
            filedVisitEditDoubleClick=true;
            var editCharterDetails=$scope.editWorkOrder;
            editCharterDetails['lastEditPerson']=loginPersonDetails.name;
            editCharterDetails['lastEditPersonId']=loginPersonDetails.employeeId;
            $http({
                method: 'POST',
                url: 'api/ProjectWorkOrders/updateContent',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:editCharterDetails
            }).success(function (response) {
                $scope.getFieldVisitReport();
                $("#fieldVisitReportEdit").modal("hide");
                filedVisitEditDoubleClick=false;
            }).error(function (response) {
                filedVisitEditDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }
    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
        });
        $scope.getFieldVisitReport();
    }
    projectWardWorks.getPlanDetails({}, function (response) {
        $rootScope.charterList=response;
    });
    var addprojectWorkOrderDoubleClick=false;
    $scope.addProjectWorkOrderButton=function () {

    if($scope.projectOrder.employeeId != undefined && $scope.projectOrder.employeeId != null && $scope.projectOrder.employeeId != '') {
                if($scope.projectOrder.startDate != undefined && $scope.projectOrder.startDate != null && $scope.projectOrder.startDate != '') {
                    if($scope.projectOrder.EndDate != undefined && $scope.projectOrder.EndDate != null && $scope.projectOrder.EndDate != '') {
                        if($scope.projectOrder.EstimatedCost != undefined && $scope.projectOrder.EstimatedCost != null && $scope.projectOrder.EstimatedCost != '') {
                            if($scope.projectOrder.supportEmp != undefined && $scope.projectOrder.supportEmp != null && $scope.projectOrder.supportEmp != '') {
                                if($scope.projectOrder.mainEmp != undefined && $scope.projectOrder.mainEmp != null && $scope.projectOrder.mainEmp != '') {

                 if(!addprojectWorkOrderDoubleClick){
                     addprojectWorkOrderDoubleClick=true;
                    var planId=$scope.selectProjectId;
                    var projectOrderDetails=$scope.projectOrder;
                    projectOrderDetails['planId']=planId;
                    projectOrderDetails['createdPerson']=loginPersonDetails.name;
                    projectOrderDetails['createdPersonId']=loginPersonDetails.employeeId;
                    projectOrderDetails['visitorName']=loginPersonDetails.name;
//                     console.log(JSON.stringify(projectOrderDetails));
                    $http({
                        method: 'POST',
                        url: 'api/ProjectWorkOrders',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        "data":projectOrderDetails
                    }).success(function (response) {
//                        console.log("Success....."+JSON.stringify(response));
                    //    $scope.getFieldVisitReport();
                        $("#addProjectWorkOrder").modal("hide");
                        $("#addProjectWorkOrderSuccess").modal("show");
                        addprojectWorkOrderDoubleClick=false;
                        setTimeout(function(){$('#addProjectWorkOrderSuccess').modal('hide')}, 3000);
                        $scope.getFieldVisitReport();
                    }).error(function (response) {
                        addprojectWorkOrderDoubleClick=false;
                    });
                }
                                } else {
//                                    alert("mainEmp");
                                    $scope.createEmpError = 'Select Main Employee';
                                    $scope.empCreationError = true;
                                    $timeout(function(){
                                        $scope.empCreationError=false;
                                    }, 3000);
                                }
                            } else {
//                                alert("supportEmp");
                                    $scope.createEmpError = 'Select Support Employee';
                                    $scope.empCreationError = true;
                                    $timeout(function(){
                                        $scope.empCreationError=false;
                                    }, 3000);
                            }
                        } else {
//                            alert("estimated cost");
                                    $scope.createEmpError = 'Select Estimated Cost';
                                    $scope.empCreationError = true;
                                    $timeout(function(){
                                        $scope.empCreationError=false;
                                    }, 3000);
                        }
                    } else {
//                        alert("enddate");
                                    $scope.createEmpError = 'Select End Date';
                                    $scope.empCreationError = true;
                                    $timeout(function(){
                                        $scope.empCreationError=false;
                                    }, 3000);
                    }
                } else {
//                    alert("startDate");
                                    $scope.createEmpError = 'Select Start Date';
                                    $scope.empCreationError = true;
                                    $timeout(function(){
                                        $scope.empCreationError=false;
                                    }, 3000);
                }
            } else {
//                alert("name");
                                    $scope.createEmpError = 'Select Name';
                                    $scope.empCreationError = true;
                                    $timeout(function(){
                                        $scope.empCreationError=false;
                                    }, 3000);
            }


    }

    $scope.reset = function() {
        $scope.projectOrder = {};
    }

    $scope.comments;
    $scope.approvalModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $scope.comments = '';

        $("#approavlReport").modal("show");
    }
    $scope.rejectModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $scope.comments = '';
        $("#rejectReport").modal("show");
    }
    var employeeId=loginPersonDetails.employeeId;
    var employeeName=loginPersonDetails.name;
    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectWorkOrders/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getFieldVisitReport();
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                }
            }).error(function (response, data) {
                approvalDoubleClick=false;
            })
        }
    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectWorkOrders/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getFieldVisitReport();
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                }
            }).error(function (response, data) {
                rejectDoubleClick=false;
            })
        }
    }
    $scope.getFieldVisitReport=function () {
        var employeeId=loginPersonDetails.employeeId;
        var employeeName=loginPersonDetails.name;
        $http({
            method: 'GET',
            url: 'api/ProjectWorkOrders/getDetails?employeeId='+employeeId+'&planId='+ $scope.selectProjectId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.filedVisitList=response;
        }).error(function (response) {
        });
    }
}]);

app.controller('fieldReportsController', function($http, $scope,Upload, $timeout, $window, $location, $rootScope, $routeParams) {
//    console.log("fieldReportsController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #requestsPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #requestsPro.active #appRequestSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.fieldReports").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.fieldReports .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    $scope.getCharterDetails = function () {
        $http({
            method: 'GET',
            //url: 'api/ProjectRequests',
            url: 'http://54.189.195.233:3000/api/ProjectRequests?filter={"where":{"status": "Completed"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Field visit report :' + JSON.stringify(response));
            $scope.requestLists = response;
            //    //alert('successfully created charter');
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getCharterDetails();

//    $scope.docUploadURL = uploadFileURL;

/*    $scope.selectAppId = function (appId) {
        $scope.selectRequestId = appId;
        ////alert($scope.selectRequestId);

        $http({
            method: 'GET',
            url: 'api/ProjectRequests/?filter={"where":{"applicationId":"' + $scope.selectRequestId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Field visit report...... :' + JSON.stringify(response));
            $scope.selectRequestDetails = response[0];
            $scope.showAction = true;
            // $scope.selectProjectDetails = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
        $scope.visitReport();
    }


    $scope.createAppReport = function () {
        var applicationId = $scope.selectRequestId;
        var appVisitData = $scope.appVisitData;
        appVisitData['applicationId'] = applicationId;
        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
        appVisitData['createdPerson'] = loginPersonDetails.name;
        appVisitData['visitorName'] = loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/ApplicationFieldReports',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: appVisitData
        }).success(function (response) {
            $scope.appVisitData = {};
            //console.log('Users Response :' + JSON.stringify(response));
            //$scope.appVisitData = response;
            $scope.visitReport();
            $("#createAppReport").modal("hide");
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.visitReport = function () {
        $http({
            method: 'GET',
            url: 'api/ApplicationFieldReports/?filter={"where":{"applicationId":"' + $scope.selectRequestId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Field report... :' + JSON.stringify(response));
            $scope.appReportDisplay=response;
            // $scope.selectProjectDetails = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.editFieldVisitors = function (fieldVisit) {
        $("#editAppReport").modal("show");
        $scope.editReportData = angular.copy(fieldVisit);
    }

    $scope.editAppReport=function () {
        var editCommentDetails= $scope.editReportData;

        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        //editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            method: 'PUT',
            url: 'api/ApplicationFieldReports/'+$scope.editReportData.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":editCommentDetails
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            ////alert('successfully edit charter');
            $scope.getCharterDetails();
            $("#editAppReport").modal("hide");
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.getCharterDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ApplicationFieldReports',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.appReportDisplay=response;
            //    //alert('successfully created charter');
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });

        ////alert('get charter list');
    }
    $scope.getCharterDetails();*/

});

app.controller('projectTaskController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {
   $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectTask").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectTask .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectTask'}, function (response) {
//        	alert(JSON.stringify(response));
               if(!response){
                window.location.href = "#/noAccessPage";
               }
            });

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        $("<tr>" +
            "<th colspan='9' style='background-color: #ebebeb;'><h3>Header Schemes Details</h3></th>" +
            "</tr>").appendTo("table" + tableId);

        if( $scope.selectProjectTaskDetails!=null &&  $scope.selectProjectTaskDetails.length>0){

            for(var i=0;i<$scope.selectProjectTaskDetails.length;i++){
                var task=$scope.selectProjectTaskDetails[i];

                $("<tr>" +
                    "<th colspan='9' style='background-color: #a9a9a9;'>"+task.name+"</th>" +
                    "</tr>").appendTo("table"+tableId);

                if(task.taskList!=null && task.taskList.length>0){
                    $("<tr>" +
                        "<th>SubTask Name</th>" +
                        "<th>Financial Year</th>" +
                        "<th>Start Date</th>" +
                        "<th>Duration</th>" +
                        "<th>End Date</th>" +
                        "<th>Estimation Cost</th>" +
                        "<th>Utilized Cost</th>" +
                        "<th>Status</th>" +
                        "<th>% Complete</th>" +
                        "</tr>").appendTo("table"+tableId);

                    for(var i=0;i<task.taskList.length;i++){
                        var subTask=task.taskList[i];

                        $("<tr>" +
                            "<td>"+subTask.subTaskName+"</td>" +
                            "<td>"+subTask.financialYear+"</td>" +
                            "<td>"+subTask.startDate+"</td>" +
                            "<td>"+subTask.duration+"</td>" +
                            "<td>"+subTask.endDate+"</td>" +
                            "<td>"+subTask.estimatedCost+"</td>" +
                            "<td>"+subTask.utilizedCost+"</td>" +
                            "<td>"+subTask.status+"</td>" +
                            "<td>"+subTask.completePercenatge+"</td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                }

                $("<tr>" +
                    "<td colspan='3'></td>" +
                    "</tr>").appendTo("table"+tableId);


            }


        }

        var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }

    $scope.loadingImage=true;
    $scope.editHeaderButton = function(header) {
        $("#editHeader").modal("show");
        $scope.editCharterArea = true;
        $scope.createCharterArea = false;
        $scope.editHeaderata=angular.copy(header);
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.createHeader=function () {
        var headerDetails=$scope.header;
        headerDetails['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/ProjectHeaders',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":headerDetails
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.getHeaderDetails();
            $("#createCharter").modal("hide");
        }).error(function (response) {
        });
    }
    $scope.editHeader=function () {
        var editHeaderDetails= $scope.editHeaderData;
        editHeaderDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            method: 'PUT',
            url: 'api/ProjectHeaders/'+$scope.editCharterData.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":editHeaderDetails
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.getHeaderDetails();
            $("#editCharter").modal("hide");
        }).error(function (response) {
        });
    }
    $scope.subTaskDetails=false;
    $scope.addSubTask=function(){
        $scope.subTaskDetails=true;
    }
    $scope.getTaskDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectTasks?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectProjectTaskDetails=response;
//            console.log("$scope.selectProjectTaskDetails..."+JSON.stringify($scope.selectProjectTaskDetails));
            $scope.loadingImage=false;
        }).error(function (response) {
        });
    }
    $scope.getEndDate = function(durationDate, indexValue){
        var duration = durationDate;
        var indexValue1 = indexValue;
        var newDate = new Date();
        if(duration && duration != ''){
            if($scope.groups[indexValue1].startDate != ''){
                var startDateArray = $scope.groups[indexValue1].startDate.split('-');
                var finalDate=startDateArray[1]+'/'+startDateArray[0]+'/'+startDateArray[2];
                var startDate = new Date(finalDate);
                var endDate = new Date(startDate.setTime( startDate.getTime() + Number(duration) * 86400000 ));
                var month = endDate.getMonth() + 1; //months from 1-12
                var day = endDate.getDate();
                var year = endDate.getFullYear();
                $scope.groups[indexValue1].endDate = endDate.getDate() +'-'+(endDate.getMonth()+1)+'-'+endDate.getFullYear();
            }
        }
    };
    $scope.getHeaderDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectHeaders?filter={"where":{"status":"' + "Active" + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.headerList=response;
        }).error(function (response) {
        });
    }
    $scope.getHeaderDetails();
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
        $rootScope.charterList=response;
    });
}
    $scope.getCharterDetails();
    $scope.groups={};
    $scope.addTask=true;
    $scope.showAddTasks = function() {
        $scope.groups = {}
    }
    var createNewTaskDoubleClick=false;
    $scope.createNewTask=function () {
        var headerData=JSON.parse($scope.header.selectedHeader);
        var status=true;
        if($scope.groups != null && $scope.groups.length >0){
            for(var i=0; i<$scope.groups.length; i++){
                if($scope.groups[i].startDate == undefined || $scope.groups[i].startDate == null || $scope.groups[i].startDate == '') {
                    status=false;
                    break;
                }
            }
        }
        $scope.dateCreationError = false;
        $scope.dateError = '';
        if(status) {
            var createTaskDetails = {
                "taskList": $scope.groups,
                "name": headerData.name,
                "headerId": headerData.id,
                "planId": $scope.selectProjectDetails.planId,
                "planName": $scope.selectProjectDetails.name
            };
            $scope.createTaskDetailsView = createTaskDetails;
            $scope.createTaskDetailstaskList = createTaskDetails.taskList;
            createTaskDetails['createdPerson'] = loginPersonDetails.name;
            if(!createNewTaskDoubleClick){
                createNewTaskDoubleClick=true;
                $http({
                    method: 'POST',
                    url: 'api/ProjectTasks',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": createTaskDetails
                }).success(function (response) {
                    $("#scroll2").hide();
                    $("#scroll").show();
                    $scope.getTaskDetails();
                    $("#addProjectTaskSuccess").modal("show");
                    setTimeout(function(){$('#addProjectTaskSuccess').modal('hide')}, 3000);
                    $scope.loadingImage = false;
                    $scope.header={};
                    $scope.addTask=true;
                    createNewTaskDoubleClick=false;
                }).error(function (response) {
                    createNewTaskDoubleClick=false;
                    $scope.header={};

                    if(response.error.message=='Inserted successfully') {
                        $scope.addTask=true;
                        $("#scroll2").hide();
                        $("#scroll").show();
                        $scope.getTaskDetails();
                        $("#addProjectTaskSuccess").modal("show");
                        setTimeout(function(){$('#addProjectTaskSuccess').modal('hide')}, 3000);
                        $scope.loadingImage = false;
                    }else  if(response.error.message=='Duplicate name in Subtask') {
                        $scope.addTask=false;
                       alert('please enter different subtask name');
                       /* $("#scroll2").hide();
                        $("#scroll").show();
                        $scope.getTaskDetails();
                        $("#addProjectTaskSuccess").modal("show");
                        setTimeout(function(){$('#addProjectTaskSuccess').modal('hide')}, 3000);
                        $scope.loadingImage = false;*/
                    }
                });
            }
        }
        else {
            $scope.dateError = 'Please Choose Start Date';
            $scope.dateCreationError = true;
        }
    }
    $scope.selectPlanId = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
        });
        $scope.getTaskDetails();
    }

    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function() {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top},
                'slow');
        });
        $("#cancel").click(function() {
            $("#scroll2").hide();
            $("#scroll").show();
            $("body, html").animate({scrollTop: 0}, 'slow');
        });
    })

    $scope.editHeaderScheme = function(task, subTask) {
        $("#editHeaderSchemeDetails").modal("show");
        $scope.editHeaderSchemeDetails1 = angular.copy(task);
        $scope.editHeaderSchemeDetails = angular.copy(subTask);
        $scope.editSubTaskName = $scope.editHeaderSchemeDetails.subTaskName;
    }

    $scope.viewHeaderScheme = function(view) {
        $("#viewSubtaskDetails").modal("show");
//           console.log(JSON.stringify(view));
           $scope.viewSubtaskDetailsList = view;
    }

    $scope.submitHeaderSchemeDetails = function() {
        var editHeaderSchemeDetails2 = $scope.editHeaderSchemeDetails1;
        var editHeaderSchemeLists = $scope.editHeaderSchemeDetails;
          editHeaderSchemeLists['lastEditPerson']=loginPersonDetails.name;
        var projectTask = [];
          for(var i=0;i<editHeaderSchemeDetails2.taskList.length;i++ ){
            if($scope.editSubTaskName == editHeaderSchemeDetails2.taskList[i].subTaskName ){
                projectTask.push(editHeaderSchemeLists);
            } else {
                projectTask.push(editHeaderSchemeDetails2.taskList[i]);
            }
        }
        $http({
            "method": "PUT",
            "url": 'api/ProjectTasks/'+$scope.editHeaderSchemeDetails1.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": {taskList:projectTask}
        }).success(function (response, data) {
            $("#editHeaderSchemeDetails").modal("hide");
            $("#editProjectTaskSuccess").modal("show");
            setTimeout(function(){$('#editProjectTaskSuccess').modal('hide')}, 3000);
            $scope.getCharterDetails();
            $scope.getTaskDetails();
        }).error(function (response, data) {
        })
    }

    $scope.getEmployees = function() {
        $http({
            "method": "GET",
            "url": 'api/Employees/',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response, data) {
            $scope.employeesListName = response;
        }).error(function (response, data) {
//            console.log("failure");
        })
    }
    $scope.getEmployees();
    $scope.header={};
    $scope.addTaskGroup = function() {
        var headerSelectData=$scope.header.selectedHeader;
        var test=document.getElementById('selectHeader');
        $scope.caseTYpeCreationError = false;
        $scope.createCaseTypeError = '';
        if (test.value) {
            $scope.addTask = false;
            $scope.groups = [
                {
                    subTaskName: "Task - ",
                    content: "",
                    no: 1
                }
            ];
            $scope.groups=[];
            var details={
                "subTaskName":"",
                "subTaskComment":"",
                "startDate":"",
                "endDate":"",
                "completePercenatge":"",
                "status":"",
                "financialYear":"",
                "estimatedCost":"",
                "utilizedCost":"",
            }
            $scope.groups.push(details);
            $scope.addGroup = function(idx, e) {
                if (e) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                var details={
                    "subTaskName":"",
                    "subTaskComment":"",
                    "startDate":"",
                    "endDate":"",
                    "completePercenatge":"",
                    "status":"",
                    "financialYear":"",
                    "estimatedCost":"",
                    "utilizedCost":"",
                }
                var newGroup = angular.copy(details);
                newGroup.no = $scope.groups.length + 1;
                $scope.groups.splice(idx + 1, 0, newGroup);
            };
            $scope.removeGroup = function(idx, e) {
                if (e) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                $scope.groups.splice(idx, 1);
            };
        }
        else {
            $scope.createCaseTypeError = 'Please Select Header Name';
            $scope.caseTYpeCreationError = true;

        }
    };
    $(document).on('click', '.panel-heading span.clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    });
}]);

app.controller('projectTORController',['projectWardWorks','$http', '$scope','Upload', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, Upload,$window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {

   $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectTOR").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectTOR .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectTOR'}, function (response) {
//        	alert(JSON.stringify(response));
               if(!response){
                window.location.href = "#/noAccessPage";
               }
            });

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        $("<tr>" +
            "<th colspan='9' style='background-color: #ebebeb;'><h3>Header Schemes Details</h3></th>" +
            "</tr>").appendTo("table" + tableId);

        if( $scope.selectProjectTaskDetails!=null &&  $scope.selectProjectTaskDetails.length>0){

            for(var i=0;i<$scope.selectProjectTaskDetails.length;i++){
                var task=$scope.selectProjectTaskDetails[i];

                $("<tr>" +
                    "<th colspan='9' style='background-color: #a9a9a9;'>"+task.name+"</th>" +
                    "</tr>").appendTo("table"+tableId);

                if(task.taskList!=null && task.taskList.length>0){
                    $("<tr>" +
                        "<th>SubTask Name</th>" +
                        "<th>Financial Year</th>" +
                        "<th>Start Date</th>" +
                        "<th>Duration</th>" +
                        "<th>End Date</th>" +
                        "<th>Estimation Cost</th>" +
                        "<th>Utilized Cost</th>" +
                        "<th>Status</th>" +
                        "<th>% Complete</th>" +
                        "</tr>").appendTo("table"+tableId);

                    for(var i=0;i<task.taskList.length;i++){
                        var subTask=task.taskList[i];

                        $("<tr>" +
                            "<td>"+subTask.subTaskName+"</td>" +
                            "<td>"+subTask.financialYear+"</td>" +
                            "<td>"+subTask.startDate+"</td>" +
                            "<td>"+subTask.duration+"</td>" +
                            "<td>"+subTask.endDate+"</td>" +
                            "<td>"+subTask.estimatedCost+"</td>" +
                            "<td>"+subTask.utilizedCost+"</td>" +
                            "<td>"+subTask.status+"</td>" +
                            "<td>"+subTask.completePercenatge+"</td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                }

                $("<tr>" +
                    "<td colspan='3'></td>" +
                    "</tr>").appendTo("table"+tableId);


            }


        }

        var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }

    $scope.loadingImage=true;
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
        });
    }
    $scope.getCharterDetails();
    $scope.docUploadURL=uploadFileURL;
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.selectedTask= function (indexNumber) {
        $scope.createTor=angular.copy($scope.subTaskList[indexNumber]);
        $scope.createTor.indexId=indexNumber;
    }

    $scope.deleteFile = function(fileId, index){
        $scope.docList.splice(index, 1);
       /* $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
        });*/
    };

    $scope.createTORDetails= function () {
        var torDetails=$scope.createTor;
        torDetails['documents']= $scope.docList;
        torDetails['createdPerson']= loginPersonDetails.name;
        torDetails['planId']= $scope.selectProjectId;
        torDetails['createdPersonId']= loginPersonDetails.employeeId;
           $http({
         "method": "POST",
         "url": 'api/ProjectTORs',
         "headers": {"Content-Type": "application/json", "Accept": "application/json"},
         "data": torDetails
         }).success(function (response) {
               $scope.getProjectTORDetails();
               $("#createProjectTOR").modal("hide");
               $("#createProjectTorReportSuccess").modal("show");
                setTimeout(function(){$('#createProjectTorReportSuccess').modal('hide')}, 3000);
         }).error(function (response) {
         });
    }
    $scope.editTORDetails= function(editDetails) {
        $scope.editTOR=editDetails;
        $scope.docList= angular.copy($scope.editTOR.documents);

        $("#editProjectTORModal").modal("show");
        projectWardWorks.getEmployeeDetails({}, function (response) {
                    $rootScope.employeeList=response;
                });

    }
    $scope.editTORDetailsData= function () {
        if ($scope.docList.length > 0) {
        var details=$scope.editTOR;
        details.documents= $scope.docList;
        details.createdPerson=loginPersonDetails.name;
        details.createdPersonId=loginPersonDetails.employeeId;
        $http({
            "method": "PUT",
            "url": 'api/ProjectTORs/'+details.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": details
        }).success(function (response, data) {
            $("#editProjectTORModal").modal("hide");
            $("#editProjectTorReportSuccess").modal("show");
            setTimeout(function(){$('#editProjectTorReportSuccess').modal('hide')}, 3000);
            $scope.getCharterDetails();
        }).error(function (response, data) {
        })
        } else {

                $scope.errorMessage = true;
                  $scope.showError = 'Please Upload CV';
                  $timeout(function(){
                      $scope.errorMessage=false;
                  }, 3000);
           }
    }


    var formIdsArray=[];
    var formUploadStatus=true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        formIdsArray=[];
        angular.forEach(files, function(file) {
            if(file.type == "application/pdf" ) {
                formUploadStatus = false;
                var fsize=0;
                                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                                   if (file.size == 0){
                                   fsize='0 Byte';
                                   }else{
                                     var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                                                 fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
                                   }
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'filename': response.data.filename,
                            'size': fsize
                        }
                        formIdsArray.push(response.data);
                        $scope.docList.push(fileDetails);
//                        fileDetails.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
                $scope.fileUpload = true;
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });
    };
    $scope.createTor={};
    $scope.addTOR=function(){
        projectWardWorks.getEmployeeDetails({}, function (response) {
            $rootScope.employeeList=response;
            console.log("response"+ JSON.stringify(response))
        });
        $("#createProjectTOR").modal("show");
        $scope.createTor={};
        $scope.createTor.scheduleList=[];
        $scope.createTor.scheduleList.push({'name':'','startDate':'','endDate':''});
        $scope.docList = [];
    }
    $scope.addRow=function(){
        $scope.createTor.scheduleList.push({'name':'','startDate':'','endDate':''});
    }
    $scope.editRow=function(){
            $scope.editTOR.scheduleList.push({'name':'','startDate':'','endDate':''});
        }
    $scope.removeRow=function(indexPosition){
        if($scope.createTor.scheduleList.length>1){
                $scope.createTor.scheduleList.splice(indexPosition, 1);

        }
    }

    $scope.reset = function() {
        $scope.createTor={};
        $scope.createTor.scheduleList=[];
        $scope.createTor.scheduleList.push({'name':'','startDate':'','endDate':''});
    }

    $scope.removeEditRow=function(indexPosition){
            if($scope.editTOR.scheduleList.length>1){
                    $scope.editTOR.scheduleList.splice(indexPosition, 1);

            }
        }

    $scope.selectPlanId = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
        });
        $scope.getProjectTORDetails();
    }
    $scope.getProjectTORDetails= function () {
        $http({
            method: 'GET',
            url: 'api/ProjectTORs?filter=%7B%22where%22%3A%7B%22planId%22%3A%20%22'+$scope.selectProjectId+'%22%7D%7D' ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.torDetailsList=response;
        }).error(function (response) {
        });
    }
}]);

app.controller('projectTORDetailsController',['projectWardWorks','$http', '$scope','Upload', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, Upload,$window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {
   $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectTORDetails").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectTORDetails .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectTOR'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        $("<tr>" +
            "<th colspan='9' style='background-color: #ebebeb;'><h3>Header Schemes Details</h3></th>" +
            "</tr>").appendTo("table" + tableId);

        if( $scope.selectProjectTaskDetails!=null &&  $scope.selectProjectTaskDetails.length>0){

            for(var i=0;i<$scope.selectProjectTaskDetails.length;i++){
                var task=$scope.selectProjectTaskDetails[i];

                $("<tr>" +
                    "<th colspan='9' style='background-color: #a9a9a9;'>"+task.name+"</th>" +
                    "</tr>").appendTo("table"+tableId);

                if(task.taskList!=null && task.taskList.length>0){
                    $("<tr>" +
                        "<th>SubTask Name</th>" +
                        "<th>Financial Year</th>" +
                        "<th>Start Date</th>" +
                        "<th>Duration</th>" +
                        "<th>End Date</th>" +
                        "<th>Estimation Cost</th>" +
                        "<th>Utilized Cost</th>" +
                        "<th>Status</th>" +
                        "<th>% Complete</th>" +
                        "</tr>").appendTo("table"+tableId);

                    for(var i=0;i<task.taskList.length;i++){
                        var subTask=task.taskList[i];

                        $("<tr>" +
                            "<td>"+subTask.subTaskName+"</td>" +
                            "<td>"+subTask.financialYear+"</td>" +
                            "<td>"+subTask.startDate+"</td>" +
                            "<td>"+subTask.duration+"</td>" +
                            "<td>"+subTask.endDate+"</td>" +
                            "<td>"+subTask.estimatedCost+"</td>" +
                            "<td>"+subTask.utilizedCost+"</td>" +
                            "<td>"+subTask.status+"</td>" +
                            "<td>"+subTask.completePercenatge+"</td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                }

                $("<tr>" +
                    "<td colspan='3'></td>" +
                    "</tr>").appendTo("table"+tableId);


            }


        }

        var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }
    $scope.loadingImage=true;
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
        });
    }
    $scope.getCharterDetails();
    $scope.docUploadURL=uploadFileURL;
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.selectTOR=function(torId){
        $scope.torId=torId;
        if($rootScope.torDetailsList!=undefined && $rootScope.torDetailsList!=null && $rootScope.torDetailsList.length>0){
            for(var i=0;i<$rootScope.torDetailsList.length;i++){
                if($rootScope.torDetailsList[i].id==torId){
                    $scope.taskDetails=$rootScope.torDetailsList[i];
                    break;
                }
            }
        }
        getTORReport(torId);
    }
    $scope.editTORVisitors=function(editTor){
        $scope.editTOR=editTor;
        $('#editProjectTORReportModal').modal('show');
    }
    $scope.approvalModal= function (torData) {
        $scope.torRerportData=torData;
        $('#approavlReport').modal('show');
        $scope.comments = '';
    }
    $scope.rejectModal= function (torData) {
        $scope.rejectModal=torData;
        $('#rejectReport').modal('show');
        $scope.comments = '';
    }
    var torEditDoubleClick=false;
    $scope.torEditReportEdit=function () {
        if(!torEditDoubleClick){
            torEditDoubleClick=true;
            var editCharterDetails=$scope.editTOR;
            editCharterDetails['lastEditPerson']=loginPersonDetails.name;
            editCharterDetails['lastEditPersonId']=loginPersonDetails.employeeId;
            $http({
                method: 'POST',
                url: 'api/ProjectTORDetails/updateContent',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:editCharterDetails
            }).success(function (response) {
                getTORReport($scope.torId);
                $("#editProjectTORReportModal").modal("hide");
                torEditDoubleClick=false;
            }).error(function (response) {
                torEditDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }
    $scope.comments;
    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.torRerportData.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment":  $scope.comments.comment,
                "requestId":$scope.torRerportData.id,
                "employeeId":loginPersonDetails.employeeId,
                "employeeName":loginPersonDetails.employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectTORDetails/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
//            console.log("response.........."+ JSON.stringify(response));
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    getTORReport($scope.torId);
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                }
                //getTORReport($scope.torId);
                //approvalDoubleClick=false;
                //$("#approavlReport").modal("hide");
            }).error(function (response, data) {
                approvalDoubleClick=false;
            })
        }
    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.torRerportData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.torRerportData.id,
                "employeeId":loginPersonDetails.employeeId,
                "employeeName":loginPersonDetails.employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectTORDetails/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    //getTORReport($scope.torId);
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    getTORReport($scope.torId);
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                }
            }).error(function (response, data) {
                rejectDoubleClick=false;
            })
        }
    }
    function getTORReport(torId){
        $http({
            method: 'GET',
            url: 'api/ProjectTORDetails/getDetails?employeeId='+loginPersonDetails.employeeId+'&planId='+torId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.torReportList=response;
        }).error(function (response) {
        });
    }
    $scope.torReport={};
    $scope.addTORReportData=function(){
        $('#createTORReport').modal('show');
        $scope.torReport={};
    }
    var torReportDoubleClick=false;
    $scope.addTORReport= function () {
        if($scope.torReport.reviewDate){
            if(!torReportDoubleClick){
                torReportDoubleClick=true;
                var torId=$scope.torId;
                var filedDetails=$scope.torReport;
                filedDetails['torId']=torId;
                filedDetails['createdPerson']=loginPersonDetails.name;
                filedDetails['createdPersonId']=loginPersonDetails.employeeId;
                filedDetails['visitorName']=loginPersonDetails.name;
                $http({
                    method: 'POST',
                    url: 'api/ProjectTORDetails',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":filedDetails
                }).success(function (response) {
                    $("#createTORReport").modal("hide");
                    $("#editProjectTorReportSuccess").modal("show");
                    torReportDoubleClick=false;
                    getTORReport($scope.torId);
                    setTimeout(function(){$('#editProjectTorReportSuccess').modal('hide')}, 3000);
                }).error(function (response) {
                    torReportDoubleClick=false;
                });
            }
        } else{
            alert("Please Select Review Date");
        }
    }
    $scope.selectPlanId = function(projectId){
        $scope.selectProjectId = projectId;

            $scope.selectProjectDetails=true;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
        });
        $scope.getProjectTORDetails();
    }
    $scope.getProjectTORDetails= function () {
        $http({
            method: 'GET',
            url: 'api/ProjectTORs?filter=%7B%22where%22%3A%7B%22planId%22%3A%20%22'+$scope.selectProjectId+'%22%7D%7D' ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.torDetailsList=response;
        }).error(function (response) {
        });
    }
}]);

app.controller('headerSchemesController', ['projectWardWorks', '$http', '$scope', '$timeout', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' , function(projectWardWorks, $http, $scope, $timeout, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro.active #masterDataProSetUp").addClass("menu-open");
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.headScheme").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.headScheme .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'headOrScheme'}, function (response) {
    	//alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.headerList!=null &&  $scope.headerList.length>0){

            $("<tr>" +
                "<th>Head/Scheme Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.headerList.length;i++){
                var header=$scope.headerList[i];

                $("<tr>" +
                    "<td>"+header.name+"</td>" +
                    "<td>"+header.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Head/Schemes Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }
    $scope.editHeaderSchemeButton = function(header) {
        $("#editHeader").modal("show");
        $scope.editCharterArea = true;
        $scope.createCharterArea = false;
        $scope.editHeaderData=angular.copy(header);
    }
    $scope.createHeaderModal = function() {
        $("#createHeader").modal("show");
        $scope.header = {};
        $scope.errorMessage=false;
    }
    var createHeaderDoubleClick=false;
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.createHeader=function () {
        if(!createHeaderDoubleClick){
            createHeaderDoubleClick=true;
            var headerDetails=$scope.header;
            $scope.headerDetailsNew = headerDetails;
            headerDetails['createdPerson']=loginPersonDetails.id;
            $http({
                method: 'POST',
                url: 'api/ProjectHeaders',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":headerDetails
            }).success(function (response) {
                $scope.getHeaderDetails();
                $("#createHeader").modal("hide");
                $("#createHeadSuccess").modal("show");
                createHeaderDoubleClick=false;
                setTimeout(function(){$('#createHeadSuccess').modal('hide')}, 3000);
            }).error(function (response) {
                createHeaderDoubleClick=false;
                if(response.error.details.messages.name) {
                    $scope.errorMessageData=response.error.details.messages.name[0];
                    $scope.errorMessage=true;
                    $timeout(function(){
                        $scope.errorMessage=false;
                    }, 3000);
                }
            });
        }
    }
    var editHeaderDoubleClick=false;
    $scope.editHeader=function () {
        if(!editHeaderDoubleClick){
            editHeaderDoubleClick=true;
            var editHeaderDetails= $scope.editHeaderData;
            editHeaderDetails['lastEditPerson']=loginPersonDetails.id;
            $http({
                method: 'PUT',
                url: 'api/ProjectHeaders/'+$scope.editHeaderData.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":editHeaderDetails
            }).success(function (response) {
                $scope.getHeaderDetails();
                editHeaderDoubleClick=false;
                $("#editHeader").modal("hide");
                $("#editHeadSuccess").modal("show");
                setTimeout(function(){$('#editHeadSuccess').modal('hide')}, 3000);
            }).error(function (response) {
                editHeaderDoubleClick=false;
                if(response.error.details.messages.name) {
                    $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                    $scope.errorUpdateMessage=true;
                    $timeout(function(){
                        $scope.errorUpdateMessage=false;
                    }, 3000);
                }
            });
        }
    }
    $scope.getHeaderDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectHeaders',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.headerList=response;
        }).error(function (response) {
        });
    }
    $scope.getHeaderDetails();
    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function() {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top},
                'slow');
        });
        $("#cancel").click(function() {
            $("#scroll2").hide()
            $("#scroll").show();;
        });
    })
    $scope.editHeaderScheme = function(task, subTask) {
        $("#editHeaderSchemeDetails").modal("show");
        $scope.editHeaderSchemeDetails = angular.copy(task, subTask);
    }

}]);

app.controller('transferFundsController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder ) {
   $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.transferFunds").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.transferFunds .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectTransferFounds'}, function (response) {
//	alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = { paging: false, searching: false };
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
        });
    }
    $scope.reset = function() {
        $scope.found = {};
        $scope.availableAmount = '';
    }
    $scope.getSubTaskList=function (data) {
        var details=JSON.parse(data);
        $scope.subTaskData=details.taskList;
    }
    $scope.getSecondSubTaskList=function (data) {
        var details=JSON.parse(data);
        $scope.secondSubTaskData=details.taskList;
    }
    $scope.getAvailableAmount=function (subTask) {
        var details=JSON.parse(subTask);
        $scope.availableAmount=parseInt(details.estimatedCost) - parseInt(details.utilizedCost)
    }
    var addTranseferDoubleClick=false;
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.addFundTransfer=function () {
//        console.log(JSON.stringify($scope.found));
        $scope.found.selectedFirstHeader = JSON.parse($scope.found.selectedFirstHeader);
        $scope.found.selectedSecondHeader = JSON.parse($scope.found.selectedSecondHeader);
        $scope.found.subFirstTaskDetails = JSON.parse($scope.found.subFirstTaskDetails);
        $scope.found.secondSubTaskDetails = JSON.parse($scope.found.secondSubTaskDetails);

        var foundDetails = {
            "fromHeader":$scope.found.selectedFirstHeader.name,
            "fromHeaderId":$scope.found.selectedFirstHeader.headerId,
            "toHeader":$scope.found.selectedSecondHeader.name,
            "toHeaderId":$scope.found.selectedSecondHeader.headerId,
            "fromSubTask":$scope.found.subFirstTaskDetails.subTaskName,
            "toSubTask":$scope.found.secondSubTaskDetails.subTaskName,
            "transferAmount":$scope.found.transferAmount,
            "planId": $scope.selectProjectId
        };

        foundDetails['createdPerson']=loginPersonDetails.name;
        foundDetails['createdPersonId']=loginPersonDetails.employeeId;
        foundDetails['visitorName']=loginPersonDetails.name;

        if(!addTranseferDoubleClick){
            addTranseferDoubleClick=true;
            $http({
                method: 'POST',
                url: 'api/TransferFunds',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":foundDetails
            }).success(function (response) {
                $("#scroll2").hide();
                $("#scroll").show();
                $scope.getFundsDetails();
                $("#createTransferFund").modal("hide");
                addTranseferDoubleClick=false;
            }).error(function (response) {
                addTranseferDoubleClick=false;
            });
        }

    }
    $scope.getTaskDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectTasks/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectProjectTaskDetails=response;
            $scope.loadingImage=false;
        }).error(function (response) {
        });
    }
    $scope.getFundsDetails=function () {
        var employeeId=loginPersonDetails.employeeId;
        var employeeName=loginPersonDetails.name;
        $http({
            method: 'GET',
            url: 'api/TransferFunds/getDetails?employeeId='+employeeId+'&planId='+ $scope.selectProjectId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.fundDetailsList=response;
            $scope.loadingImage=false;
        }).error(function (response) {
        });
    }
    $scope.comments;
    $scope.approvalModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#approavlReport").modal("show");
    }
    $scope.rejectModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#rejectReport").modal("show");
    }
    var employeeId=loginPersonDetails.employeeId;
    var employeeName=loginPersonDetails.name;
    var approvalTransferDoubleClick=false;
    $scope.approvalData=function(){
        var updateDetails = {
            "acceptLevel":$scope.fieldData.acceptLevel,
            "acceptStatus": "Yes",
            "requestStatus": "Approval",
            "comment":  $scope.comments.comment,
            "requestId":$scope.fieldData.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };
        if(!approvalTransferDoubleClick){
            approvalTransferDoubleClick=true;
            $http({
                "method": "POST",
                "url": 'api/TransferFunds/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    approvalTransferDoubleClick=false;
                    $("#approavlReport").modal("hide");
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getFundsDetails();
                    approvalTransferDoubleClick=false;
                    $("#approavlReport").modal("hide");
                }

            }).error(function (response, data) {
                approvalTransferDoubleClick=false;
            })
        }
    }
    var rejectTransferDoubleClick=false;
    $scope.rejectData=function(){
        var updateDetails = {
            "acceptLevel":$scope.fieldData.acceptLevel,
            "acceptStatus": "No",
            "requestStatus": "Rejected",
            "comment":  $scope.comments.comment,
            "requestId":$scope.fieldData.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };
        if(!rejectTransferDoubleClick){
            rejectTransferDoubleClick=true;
            $http({
                "method": "POST",
                "url": 'api/TransferFunds/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    $scope.getFundsDetails();
                    $("#rejectReport").modal("hide");
                    rejectTransferDoubleClick=false;
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getFundsDetails();
                    $("#rejectReport").modal("hide");
                    rejectTransferDoubleClick=false;
                }

                $scope.getFundsDetails();
                $("#rejectReport").modal("hide");
                rejectTransferDoubleClick=false;

            }).error(function (response, data) {
//                console.log("failure");
                rejectTransferDoubleClick=false;
            })
        }
    }
    $scope.getCharterDetails();
    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
        });
        $scope.getTaskDetails();
        $scope.getFundsDetails();
    }
    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function() {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top},
                'slow');
        });
        $("#cancel").click(function() {
            $("#scroll2").hide()
            $("#scroll").show();;
        });
    })
    $scope.editHeaderScheme = function() {
        $("#editHeaderSchemeDetails").modal("show");
    }
}]);

app.controller('docUploadController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder','Upload', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder,Upload ) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.docUpload").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.docUpload .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectUploads'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }
        if ($scope.documentsList != null && $scope.documentsList.length > 0) {

            $("<tr>" +
                "<th colspan='4' style='background-color: #ebebeb;'><h3>Doc Upload Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Name</th>" +
                "<th>Attached Files</th>" +
                "<th>Document Type</th>" +
                "<th>comments</th>" +
                "</tr>").appendTo("table" + tableId);

            for (var i = 0; i < $scope.documentsList.length; i++) {
                var schemes = $scope.documentsList[i];

                $("<tr>" +
                    "<td>" + schemes.uploadedPersonName + "</td>" +
                    "<td>" + schemes.file.filename + "</td>" +
                    "<td>" + schemes.documentType + "</td>" +
                    "<td>" + schemes.comments + "</td>" +
                    "</tr>").appendTo("table" + tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Document Upload Details');
        $timeout(function () {
            location.href = exportHref;
        }, 100);

    };
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
        });
    }

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.getCharterDetails();
    $scope.addFilePopup = function(){
        $scope.docList = [];
        $scope.upload = {};
    };
    $scope.deleteFile = function(fileId, index){
        $scope.docList.splice(index, 1);
       /* $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

        });*/
    };

    var formIdsArray=[];
    var formUploadStatus=true;
    $scope.docList = [];

    $scope.uploadDocuments = function (files) {
//        var count=1;
        formIdsArray=[];
//        $scope.docList = [];
//        var fileCount = 0;
          angular.forEach(files, function(file) {
                           /* $scope.uploadFormNotif = false;
                      $scope.disable = true;
                        $scope.errorMssg = true;*/
                        $scope.uploadFormNotif = false;
//            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;

                var fsize=0;
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                   if (file.size == 0){
                   fsize='0 Byte';
                   }else{
                     var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                                 fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
                   }

                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'filename': response.data.filename,
                           'size': fsize
                        }
                        formIdsArray.push(response.data);
                        $scope.docList.push(fileDetails);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
                $scope.fileUpload = true;
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
            /*if (fileCount == files.length) {
                            $scope.uploadFormNotif = true;
                            $scope.disable = false;
                            $scope.errorMssg = false;
                            $scope.formNotSelected = '';
                        }*/
        });
    };
    /*$scope.selectAll = function () {
        angular.forEach($scope.docList.file, function (obj) {
            obj.visible = true;
        });
      };*/

    var uploadDoubleClick=false;
    $scope.projectUploads= function () {
        if(formUploadStatus){
            if( $scope.docList.length>0){
                if($scope.upload.date!=undefined && $scope.upload.date!=null && $scope.upload.date!=''){
                    var uploadDetails=$scope.upload;
                    uploadDetails.file=$scope.docList;
                    var planId=$scope.selectProjectId;
                    uploadDetails['planId']=planId;
                    uploadDetails['createdPerson']=loginPersonDetails.name;
                    uploadDetails['type']='upload';
                    uploadDetails['uploadedPersonName']=loginPersonDetails.name;
                    uploadDetails.status='active';
                    $scope.fileUpload = true;
                    if(!uploadDoubleClick){
                        uploadDoubleClick=true;
                        $http({
                            method: 'POST',
                            url: 'api/ProjectUploads',
                            headers: {"Content-Type": "application/json", "Accept": "application/json"},
                            "data":uploadDetails
                        }).success(function (response) {
//                            console.log("response"+JSON.stringify(response));
                            $("#scroll2").hide();
                            $("#scroll").show();
                            uploadDoubleClick=false;
                            $("#addUploadFile").modal("hide");
                            $scope.docList = [];
                            $scope.getDocuments();
                        }).error(function (response) {
                            uploadDoubleClick=false;
                        });
                    }
                }else{
                    alert('please select Date');
                }

            }else{
                alert('please select uploads');
            }
        }else{
            $scope.projectUploads();
        }
    }
    $scope.docUploadURL = uploadFileURL;
    $scope.getDocuments=function () {
        var employeeId=loginPersonDetails.employeeId;
        var employeeName=loginPersonDetails.name;
        $http({
            method: 'GET',
            url: 'api/ProjectUploads?filter={"where":{"type":"upload","planId":"'+$scope.selectProjectId+'"}}' ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
        console.log("new......." + JSON.stringify(response))
            $scope.documentsList=response;
        }).error(function (response) {
        });
    }
    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
        });
        $scope.getDocuments();
    }
    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function() {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top},
                'slow');
        });
        $("#cancel").click(function() {
            $("#scroll2").hide()
            $("#scroll").show();;
        });
    })
    $scope.editUploadFile = function(data) {
        $scope.editUploaded=angular.copy(data);
//        alert(JSON.stringify($scope.editUploaded))
        $scope.oldDocList = $scope.editUploaded.file;
        $("#UploadFile").modal("show");
        $scope.docList = [];
    }
    $scope.reset = function() {
        $scope.upload = {};
        $scope.docList = [];
    }
    $http({
        method: 'GET',
        url: 'api/DocumentTypes/?filter={"where":{"status":"Active"}}',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        console.log("response.........."+JSON.stringify(response))
        $scope.documentTypeList = response;
    }).error(function (response) {
    });
    var uploadDoubleClick=false;
    $scope.editUploadedFiles = function() {
        var editUploadedFiles12 = $scope.editUploaded;
//        alert(JSON.stringify(editUploadedFiles12));
        if($scope.editUploaded.file.length > 0){
            for(var i=0; i< $scope.docList.length; i++){
                $scope.editUploaded.file.push($scope.docList[i]);
            }
        }else{
            $scope.editUploaded.file = $scope.docList;
        }
        if(!uploadDoubleClick){
            uploadDoubleClick=true;
            $http({
                method: 'PUT',
                url: 'api/ProjectUploads/'+$scope.editUploaded.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":$scope.editUploaded
            }).success(function (response) {
                $scope.getDocuments();
                uploadDoubleClick=false;
                $("#UploadFile").modal("hide");
            }).error(function (response) {
                uploadDoubleClick=false;
            });
        }
    }
    $scope.deleteFile = function(fileId, index){
       $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
            $scope.oldDocList.splice(index, 1);
        });
    };
}]);

app.controller('commentsController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.comments").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.comments .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectComments'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.exportToExcel=function(tableId){
        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);
        }

        if( $scope.commentList!=null &&  $scope.commentList.length>0){
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Comment Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Visitor Name</th>" +
                "<th>Comment</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.commentList.length;i++){
                var schemes=$scope.commentList[i];

                $("<tr>" +
                    "<td>"+schemes.visitorName+"</td>" +
                    "<td>"+schemes.comment+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Project Comments');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
        });
    }
    $scope.getCharterDetails();
    $scope.reset = function() {
        $scope.commentDetails = {};
    }
    $scope.getCommentDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectComments/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.commentList=response;
        }).error(function (response) {
        });
    }
    $scope.history= function (commentDetails) {
        $scope.historyDetails=commentDetails;
        $("#historyModel").modal("show");
    }
    var addCommnetDoubleClick=false;
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.addComment=function (planId) {
        var planId=$scope.commentDetails;
        var commentDetails=$scope.commentDetails;
        commentDetails['createdPerson']=loginPersonDetails.name;
        commentDetails['visitorName']=loginPersonDetails.name;
        commentDetails['planId']= $scope.selectProjectId;
        if(!addCommnetDoubleClick){
            addCommnetDoubleClick=true;
            $http({
                method: 'POST',
                url: 'api/ProjectComments',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":commentDetails
            }).success(function (response) {
                addCommnetDoubleClick=false;
                $scope.commentDetails = {};
                $scope.getCommentDetails();
                $("#addComment").modal("hide");
                $("#editProjectCommentSuccess").modal("show");
                setTimeout(function(){$('#editProjectCommentSuccess').modal('hide')}, 3000);
            }).error(function (response) {
                addCommnetDoubleClick=false;
            });
        }
    }
    $scope.selectProjectId;
    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
        });
        $scope.getCommentDetails();
    }
    $scope.editComment = function(commentDetails) {
        $("#editCommentModal").modal("show");
        $scope.editCommentData=angular.copy(commentDetails);
    }
    $scope.editCommentDetails=function () {
        var editCharterDetails= $rootScope.editCommentData;
    }
    var editCommentDoubleClick=false;
    $scope.editCommentDetails=function () {
        var editCommentDetails= $scope.editCommentData;
        editCommentDetails['lastEditPerson']=loginPersonDetails.name;
        if(!editCommentDoubleClick){
            editCommentDoubleClick=true;
            $http({
                method: 'PUT',
                url: 'api/ProjectComments/'+$scope.editCommentData.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":editCommentDetails
            }).success(function (response) {
                  $scope.getCommentDetails();
                editCommentDoubleClick=false;
                $("#editCommentModal").modal("hide");
            }).error(function (response) {
                editCommentDoubleClick=false;
            });
        }
    }
}]);

app.controller('projectMilestoneController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("projectMilestoneController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectMilestone").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectMilestone .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectMilestones'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.loadingImage=true;
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);
        }

        if( $scope.paymentMilestoneList!=null &&  $scope.paymentMilestoneList.length>0){

            $("<tr>" +
                "<th colspan='5' style='background-color: #ebebeb;'><h3>Project Milestone Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Project Milestone Name</th>" +
                "<th>Description</th>" +
                "<th>Expected Date</th>" +
                "<th>Status</th>" +
                "<th>Comment</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.paymentMilestoneList.length;i++){
                var payment=$scope.paymentMilestoneList[i];

                $("<tr>" +
                    "<td>"+payment.milestoneName+"</td>" +
                    "<td>"+payment.description+"</td>" +
                    "<td>"+payment.expectedDate+"</td>" +
                    "<td>"+payment.status+"</td>" +
                    "<td>"+payment.comment+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
            $scope.loadingImage=false;
        });
    }
    $scope.getCharterDetails();
    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
        });
        $scope.getProjectMilestoneDetails();
    }
    var createPaymentDoubleClick=false;
    $scope.createPaymentMilestone=function () {
        if($scope.paymentMilestone.expectedDate){
            var paymentMilestoneData=$scope.paymentMilestone;
            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            paymentMilestoneData['createdPerson']=loginPersonDetails.name;
            paymentMilestoneData['createdPersonId']=loginPersonDetails.employeeId;
            var planId=$scope.selectProjectId;
            paymentMilestoneData['planId']=planId;
            if(!createPaymentDoubleClick){
                createPaymentDoubleClick=true;
                $http({
                    method: 'POST',
                    url: 'api/ProjectMilestones',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":paymentMilestoneData
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.getProjectMilestoneDetails();
                    $scope.paymentMilestone={};
                    createPaymentDoubleClick=false;
                    $("#createProjectMilestone").modal("hide");
                    $("#createProjectMilestoneSuccess").modal("show");
                    setTimeout(function(){$('#createProjectMilestoneSuccess').modal('hide')}, 3000);
                }).error(function (response) {
                    createPaymentDoubleClick=false;
                });
            }
        }else{
            alert("Please Select Expected Date");
        }

    }
    $scope.comments;
    $scope.approvalModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#approavlReport").modal("show");
        $scope.comments='';
    }
    $scope.rejectModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#rejectReport").modal("show");
        $scope.comments='';
    }
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;

    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectMilestones/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    $scope.getProjectMilestoneDetails();
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getProjectMilestoneDetails();
                    approvalDoubleClick=false;
                    $("#approavlReport").modal("hide");
                }
            }).error(function (response, data) {
//                console.log("failure");
                approvalDoubleClick=false;
            })
        }
    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectMilestones/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    $scope.getProjectMilestoneDetails();
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getProjectMilestoneDetails();
                    $("#rejectReport").modal("hide");
                    rejectDoubleClick=false;
                }

            }).error(function (response, data) {
//                console.log("failure");
                rejectDoubleClick=false;
            })
        }
    }

    $scope.getProjectMilestoneDetails=function () {
        $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
        var employeeId=$scope.userInfo.employeeId;
        var employeeName=$scope.userInfo.name;
        $http({
            method: 'GET',
            url: 'api/ProjectMilestones/getDetails?employeeId='+employeeId+'&planId='+ $scope.selectProjectId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.paymentMilestoneList=response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.editProjectMilestone = function(paymentMilestone) {
        $("#editProjectMilestone").modal("show");
        $("#viewProjectMilestone").modal("hide");
        $scope.editMilestoneId = angular.copy(paymentMilestone);
    }
    $scope.viewProjectMilestone = function(paymentMilestone) {
        $("#viewProjectMilestone").modal("show");
//        console.log("response"+ JSON.stringify(paymentMilestone));
        $scope.viewMilestoneId = angular.copy(paymentMilestone);
    }
    var editPaymentDoubleClick=false;
    $scope.editProjectMilestoneDetails=function () {
        var editMilestoneDetails= $scope.editMilestoneId;
        if(!editPaymentDoubleClick){
            editPaymentDoubleClick=true;
            $http({
                method: 'POST',
                url: 'api/ProjectMilestones/updateContent',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":editMilestoneDetails
            }).success(function (response) {
                $scope.getProjectMilestoneDetails();
                $scope.loadingImage=false;
                editPaymentDoubleClick=false;
                $("#editProjectMilestone").modal("hide");
                $("#editProjectMilestoneSuccess").modal("show");
                setTimeout(function(){$('#editProjectMilestoneSuccess').modal('hide')}, 3000)
            }).error(function (response) {
                editPaymentDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }
}])

app.controller('paymentMilestoneController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("paymentMilestoneController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.paymentMilestone").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.paymentMilestone .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPaymentMilestones'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        if( $scope.paymentMilestoneList!=null &&  $scope.paymentMilestoneList.length>0){
            $("<tr>" +
                "<th colspan='5' style='background-color: #ebebeb;'><h3>Payment Milestone Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Milestone Name</th>" +
                "<th>Expected Date</th>" +
                "<th>Status</th>" +
                "<th>Description</th>" +
                "<th>Comment</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.paymentMilestoneList.length;i++){
                var schemes=$scope.paymentMilestoneList[i];

                $("<tr>" +
                    "<td>"+schemes.milestoneName+"</td>" +
                    "<td>"+schemes.expectedDate+"</td>" +
                    "<td>"+schemes.status+"</td>" +
                    "<td>"+schemes.description+"</td>" +
                    "<td>"+schemes.comment+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Payment Milestone Details');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }


    $scope.getCharterDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans/getProjectDetails',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getCharterDetails :' + JSON.stringify(response));
            $rootScope.charterList=response;
            $scope.loadingImage=false;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
        ////alert('get charter list');
    }
    $scope.getCharterDetails();

    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;

        $http({
            method: 'GET',
            url: 'api/ProjectPlans/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Field visit report :' + JSON.stringify(response));
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.getPaymentMilestoneDetails();
            // $scope.selectProjectDetails = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.createProMilestoneModal = function() {
        $("#createProjectMilestone").modal("show");
        $scope.paymentMilestone = {};
    }

    var createMilestoneDoubleClick=false;
    $scope.createPaymentMilestone=function () {
        if($scope.paymentMilestone.expectedDate){

            var paymentMilestoneData=$scope.paymentMilestone;
            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            paymentMilestoneData['createdPerson']=loginPersonDetails.name;
            var planId=$scope.selectProjectId;
            paymentMilestoneData['planId']=planId;
            if(!createMilestoneDoubleClick){
                createMilestoneDoubleClick=true;
                $http({
                    method: 'POST',
                    url: 'api/PaymentMilestones',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":paymentMilestoneData
                }).success(function (response) {
                    //console.log('Users Response :' + JSON.stringify(response));
                    //  //alert('successfully created charter');
                    $scope.getPaymentMilestoneDetails();
                    $("#createProjectMilestone").modal("hide");
                    $("#createPaymentMilestoneSuccess").modal("show");
                    createMilestoneDoubleClick=false;
                    setTimeout(function(){$('#createPaymentMilestoneSuccess').modal('hide')}, 3000);
                }).error(function (response) {
                    createMilestoneDoubleClick=false;
//                    console.log('Error Response :' + JSON.stringify(response));
                });
            }


        }else{
            alert("Please Select Expected Date");
        }

    }


    $scope.getPaymentMilestoneDetails=function () {

        $http({
            method: 'GET',
            url: 'api/PaymentMilestones/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getPaymentMilestoneDetails :' + JSON.stringify(response));
            $scope.paymentMilestoneList=response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    $scope.editProjectMilestone = function(paymentMilestone) {
        $("#editProjectMilestone").modal("show");
        $("#viewProjectMilestone").modal("hide");
        $scope.editMilestoneId = angular.copy(paymentMilestone);
    }
    $scope.viewPaymentMilestone = function(paymentMilestone) {
        $("#viewProjectMilestone").modal("show");
        $scope.viewMilestoneId = angular.copy(paymentMilestone);
//        console.log("response"+ JSON.stringify(paymentMilestone))
    }
    var editMilestoneDoubleClick=false;

    $scope.editProjectMilestoneDetails=function () {
        var editMilestoneDetails= $scope.editMilestoneId;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        //editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        if(!editMilestoneDoubleClick){
            editMilestoneDoubleClick=true;
            $http({
                method: 'PUT',
                url: 'api/PaymentMilestones/'+$scope.editMilestoneId.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":editMilestoneDetails
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                ////alert('successfully edit charter');
                $scope.getMilestoneDetails();
                editMilestoneDoubleClick=false;
                $("#editProjectMilestone").modal("hide");
                $("#editPaymentMilestoneSuccess").modal("show");
                setTimeout(function(){$('#editPaymentMilestoneSuccess').modal('hide')}, 3000);
            }).error(function (response) {
                editMilestoneDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }

    }

    $scope.getMilestoneDetails=function () {
        $http({
            method: 'GET',
            url: 'api/PaymentMilestones',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.paymentMilestoneList=response;
            //    //alert('successfully created charter');
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });

        ////alert('get charter list');
    }
    $scope.getCharterDetails();

}]);

app.controller('projectRisksController',['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectRisks").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectRisks .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectRisks'}, function (response) {
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }



        if( $scope.projectRiskList!=null &&  $scope.projectRiskList.length>0){

            $("<tr>" +
                "<th colspan='6' style='background-color: #ebebeb;'><h3>Project Risks Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Open Date</th>" +
                "<th>Issue Description</th>" +
                "<th>Category</th>" +
                "<th>Priority</th>" +
                "<th>Status</th>" +
                "<th>Office Of Primary Interest</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.projectRiskList.length;i++){
                var projectRisk=$scope.projectRiskList[i];

                $("<tr>" +
                    "<td>"+projectRisk.issueOpenDate+"</td>" +
                    "<td>"+projectRisk.description+"</td>" +
                    "<td>"+projectRisk.category+"</td>" +
                    "<td>"+projectRisk.priority+"</td>" +
                    "<td>"+projectRisk.status+"</td>" +
                    "<td>"+projectRisk.primaryInterest+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Project Risks Details');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }

    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
        });
    }
    $scope.getCharterDetails();

    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
        });
        $scope.getProjectRisksDetails();
    }
    var createProjectRisksSubmit = true;
    $scope.createProjectRisks=function () {
        if($scope.projectRisk.issueOpenDate){
            if($scope.projectRisk.issueResolutiondDate){
                var projectRiskData=$scope.projectRisk;
                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                projectRiskData['createdPerson']=loginPersonDetails.name;
                var planId=$scope.selectProjectId;
                projectRiskData['planId']=planId;
                if(createProjectRisksSubmit) {
                    createProjectRisksSubmit = false;
                    $http({
                        method: 'POST',
                        url: 'api/ProjectRisks',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        "data":projectRiskData
                    }).success(function (response) {
                        //console.log('Users Response :' + JSON.stringify(response));
                        //  //alert('successfully created charter');
                        $scope.getProjectRisksDetails();
                        $("#createProjectRisks").modal("hide");
                        $("#createProjectRiskSuccess").modal("show");
                        createProjectRisksSubmit = true;
                        setTimeout(function(){$('#createProjectRiskSuccess').modal('hide')}, 3000);

                    }).error(function (response) {
                        createProjectRisksSubmit = true;
//                        console.log('Error Response :' + JSON.stringify(response));
                    });
                }
            }else{
                createProjectRisksSubmit = true;
                $scope.errorMessageData="Please Select Expected Resolution Date";
                $scope.errorMessage=true;
                $timeout(function(){
                    $scope.errorMessage=false;
                }, 3000);
            }
        }else{
            createProjectRisksSubmit = true;
            $scope.errorMessageData="Please Select Open date";
            $scope.errorMessage=true;
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);
        }
    }
    $scope.createRisk=function(){
        $scope.projectRisk={};
    }

    $scope.getProjectRisksDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectRisks/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getProjectRisksDetails :' + JSON.stringify(response));
            $scope.projectRiskList=response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.editProjectRisk = function(projectRisk) {
        $("#editProjectRisks").modal("show");
        $("#viewProjectRisks").modal("hide");
        $("#viewProjectRisks").modal("hide");
        $scope.editProjectRisks = angular.copy(projectRisk);
    }
    $scope.viewProjectRisk = function(projectRisk) {
        $("#viewProjectRisks").modal("show");
        $scope.viewProjectRisks = angular.copy(projectRisk);
    }
    var editRiskDoubleClick=false;
    $scope.viewRiskDesc = function(riskDesc) {
        $scope.riskDescShow = riskDesc;
        $("#showRiskDesc").modal("show");
    }
    $scope.editProjectRisksDetails = function() {
        var editProjectRisksDetails = $scope.editProjectRisks;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        if(!editRiskDoubleClick){
            editRiskDoubleClick=true;
            $http({
                method: 'PUT',
                url: 'api/ProjectRisks/'+$scope.editProjectRisks.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":editProjectRisksDetails
            }).success(function (response) {
                editRiskDoubleClick=false;
                $scope.getProjectRisksDetails();
                $("#editProjectRisks").modal("hide");
                $("#editProjectRiskSuccess").modal("show");
                setTimeout(function(){$('#editProjectRiskSuccess').modal('hide')}, 3000);
            }).error(function (response) {
                editRiskDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }

    }
    $http({
        method: 'GET',
        url: 'api/Employees/?filter={"where":{"status":"Active"}}',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        //console.log('Employee List :' + JSON.stringify(response));
        $scope.employeeListForRisks = response;
    }).error(function (response) {
//        console.log('Error Response :' + JSON.stringify(response));
    });


}]);

app.controller('projectRebaselineController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
     $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectRebaseline1").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectRebaseline1 .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");

        $("#scroll").click(function() {
            document.getElementById('welcomeDiv').style.display = "block";
            $('html, body').animate({
                scrollTop: $("#scroll2").offset().top
            }, 2000);
        });

        $("#addbaseLine").click(function() {
            document.getElementById('rebaseLineOptions').style.display = "block";
            $scope.addRisks();
            $scope.addIssues();
            $('html, body').animate({
                scrollTop: $("#rebaseLineScroll").offset().top
            }, 2000);
        });

    });


var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectRebaseLine'}, function (response) {
//	alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }



        if( $scope.projectRiskList!=null &&  $scope.projectRiskList.length>0){

            $("<tr>" +
                "<th colspan='6' style='background-color: #ebebeb;'><h3>Project Risks Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Open Date</th>" +
                "<th>Issue Description</th>" +
                "<th>Category</th>" +
                "<th>Priority</th>" +
                "<th>Status</th>" +
                "<th>Office Of Primary Interest</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.projectRiskList.length;i++){
                var projectRisk=$scope.projectRiskList[i];

                $("<tr>" +
                    "<td>"+projectRisk.issueOpenDate+"</td>" +
                    "<td>"+projectRisk.description+"</td>" +
                    "<td>"+projectRisk.category+"</td>" +
                    "<td>"+projectRisk.priority+"</td>" +
                    "<td>"+projectRisk.status+"</td>" +
                    "<td>"+projectRisk.primaryInterest+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Project Risks Details');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }
    $scope.getCharterDetails=function () {
        projectWardWorks.getPlanDetails({}, function (response) {
            $rootScope.charterList=response;
        });
    }
    $scope.getCharterDetails();
    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;
        projectWardWorks.getSelectedPlan(projectId, function (response) {
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.loadingImage=false;
        });
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    var createProjectClosureStatus = true;
    $scope.createProjectRebaseLine=function () {
        $scope.selectedIssues=[];
        $scope.selectedRisks=[];
        if($scope.projectIssuesList!=undefined && $scope.projectIssuesList!=null && $scope.projectIssuesList.length>0 ){
            for(var i=0;i<$scope.projectIssuesList.length;i++){
                if($scope.projectIssuesList[i].selected){
                    $scope.selectedIssues.push($scope.projectIssuesList[i].id);
                }
            }
        }
        if($scope.projectRiskList!=undefined && $scope.projectRiskList!=null && $scope.projectRiskList.length>0 ){
            for(var i=0;i<$scope.projectRiskList.length;i++){
                if($scope.projectRiskList[i].selected){
                    $scope.selectedRisks.push($scope.projectRiskList[i].id);
                }
            }
        }
        if(createProjectClosureStatus){
            var details={
                'planId':$scope.selectProjectId,
                'comment':$scope.rebaseLine.comment,
                'risk':$scope.selectedRisks,
                'issue':$scope.selectedIssues,
                'createdPersonId':loginPersonDetails.employeeId,
                'employeeName':loginPersonDetails.name
            }
            createProjectClosureStatus=false;
            $http({
                method: 'POST',
                url: 'api/ProjectRebaselines',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:details
            }).success(function (response) {
                $("#createProjectRiskSuccess").modal("show");
                createProjectClosureStatus = true;
                setTimeout(function(){$('#createProjectRiskSuccess').modal('hide')}, 3000);
                location.reload();
            }).error(function (response) {
                createProjectClosureStatus = true;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    }
    $scope.projectIssuesList=[];
    $scope.projectRiskList=[];
    $scope.addRisks=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectRisks/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.projectRiskList=response;
            if($scope.projectRiskList!=undefined && $scope.projectRiskList!=null && $scope.projectRiskList.length>0 ){
                for(var i=0;i<$scope.projectRiskList.length;i++){
                    $scope.projectRiskList[i].selected=false;
                }
            }
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.addIssues=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectIssues/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.projectIssuesList=response;
            if($scope.projectIssuesList!=undefined && $scope.projectIssuesList!=null && $scope.projectIssuesList.length>0 ){
                for(var i=0;i<$scope.projectIssuesList.length;i++){
                    $scope.projectIssuesList[i].selected=false;
                }
            }
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
        $scope.selection = [];
    $scope.selectedFruits = function selectedFruits() {
        return filterFilter($scope.projectIssuesList, { selected: true });
    };
    $scope.$watch('projectIssuesList|filter:{selected:true}', function (nv) {
        $scope.selection = nv.map(function (fruit) {
            return fruit.name;
        });
    }, true);
    $scope.selectionRisks = [];
    $scope.selectedRisks = function selectedFruits() {
        return filterFilter($scope.projectRiskList, { selected: true });
    };
    $scope.$watch('projectRiskList|filter:{selected:true}', function (nv) {
        $scope.selectionRisks = nv.map(function (fruit) {
            return fruit.name;
        });
    }, true);
}]);

app.controller('projectRebaselineDetailsController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectRebaseline").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectRebaseline.collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
            $("#scroll").click(function() {
            document.getElementById('welcomeDiv').style.display = "block";
            $('html, body').animate({
                scrollTop: $("#scroll2").offset().top
            }, 2000);
        });
        $("#addbaseLine").click(function() {
            document.getElementById('rebaseLineOptions').style.display = "block";
            $scope.addRisks();
            $scope.addIssues();
            $('html, body').animate({
                scrollTop: $("#rebaseLineScroll").offset().top
            }, 2000);
        });
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectRebaseLine'}, function (response) {
        if(!response){
        window.location.href = "#/noAccessPage";
       }
    });
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }



        if( $scope.projectRiskList!=null &&  $scope.projectRiskList.length>0){

            $("<tr>" +
                "<th colspan='6' style='background-color: #ebebeb;'><h3>Project Risks Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Open Date</th>" +
                "<th>Issue Description</th>" +
                "<th>Category</th>" +
                "<th>Priority</th>" +
                "<th>Status</th>" +
                "<th>Office Of Primary Interest</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.projectRiskList.length;i++){
                var projectRisk=$scope.projectRiskList[i];

                $("<tr>" +
                    "<td>"+projectRisk.issueOpenDate+"</td>" +
                    "<td>"+projectRisk.description+"</td>" +
                    "<td>"+projectRisk.category+"</td>" +
                    "<td>"+projectRisk.priority+"</td>" +
                    "<td>"+projectRisk.status+"</td>" +
                    "<td>"+projectRisk.primaryInterest+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Project Risks Details');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    var createProjectClosureStatus = true;
    $scope.viewProjectRebaseLine = function(projectRisk) {
        $("#viewProjectRisks").modal("show");
        $scope.projectRebaseLine = angular.copy(projectRisk);
    }
    $scope.getPlanDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectRebaselines/getDetails?employeeId='+loginPersonDetails.employeeId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.rebaseLineDetails=response;
        }).error(function (response) {
        });
    }
    $scope.getPlanDetails();
    $scope.comments;
    $scope.approvalModal=function(planData){
        $scope.planData=angular.copy(planData);
        $("#approavlReport").modal("show");
    }
    $scope.rejectModal=function(planData){
        $scope.planData=angular.copy(planData);
        $("#rejectReport").modal("show");
    }
    var employeeId=loginPersonDetails.employeeId;
    var employeeName=loginPersonDetails.name;
    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.planData.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment":  $scope.comments.comment,
                "planId":$scope.planData.planId,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectRebaselines/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                $scope.getPlanDetails();
                approvalDoubleClick=false;
                $("#approavlReport").modal("hide");
            }).error(function (response, data) {
                approvalDoubleClick=false;
            })
        }
    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.planId,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/ProjectRebaselines/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                $scope.getPlanDetails();
                $("#rejectReport").modal("hide");
                rejectDoubleClick=false;
            }).error(function (response, data) {
//                console.log("failure");
                rejectDoubleClick=false;
            })
        }


    }
}]);

app.controller('projectIssuesController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("projectIssuesController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #proWorksLi").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
            $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectIssues").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectIssues .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectIssues'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        if( $scope.projectRiskList!=null &&  $scope.projectRiskList.length>0){

            $("<tr>" +
                "<th colspan='6' style='background-color: #ebebeb;'><h3>Project Risks Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Open Date</th>" +
                "<th>Issue Description</th>" +
                "<th>Category</th>" +
                "<th>Priority</th>" +
                "<th>Status</th>" +
                "<th>Office Of Primary Interest</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.projectRiskList.length;i++){
                var projectRisk=$scope.projectRiskList[i];

                $("<tr>" +
                    "<td>"+projectRisk.issueOpenDate+"</td>" +
                    "<td>"+projectRisk.description+"</td>" +
                    "<td>"+projectRisk.category+"</td>" +
                    "<td>"+projectRisk.priority+"</td>" +
                    "<td>"+projectRisk.status+"</td>" +
                    "<td>"+projectRisk.primaryInterest+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Project Risks Details');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }
    $scope.viewIssueDesc = function(issueDesc) {
        $scope.viewIssueDescp = issueDesc;
        $("#showIssueDesc").modal("show");
    }
    $scope.getCharterDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans/getProjectDetails',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getCharterDetails :' + JSON.stringify(response));
            $rootScope.charterList=response;
            //    //alert('successfully created charter');
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
        ////alert('get charter list');
    }
    $scope.getCharterDetails();

    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;

        $http({
            method: 'GET',
            url: 'api/ProjectPlans/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Field visit report :' + JSON.stringify(response));
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.getProjectRisksDetails();
            // $scope.selectProjectDetails = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    var createProjectRisksSubmit = true;
    $scope.createProjectRisks=function () {

        if($scope.projectRisk.issueOpenDate){

            if($scope.projectRisk.issueResolutiondDate){

                var projectRiskData=$scope.projectRisk;
                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                projectRiskData['createdPerson']=loginPersonDetails.name;
                var planId=$scope.selectProjectId;
                projectRiskData['planId']=planId;
                if(createProjectRisksSubmit) {
                    createProjectRisksSubmit = false;
                    $http({
                        method: 'POST',
                        url: 'api/ProjectIssues',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        "data":projectRiskData
                    }).success(function (response) {
                        //console.log('Users Response :' + JSON.stringify(response));
                        //  //alert('successfully created charter');
                        $scope.getProjectRisksDetails();
                        $("#createProjectRisks").modal("hide");
                        $("#createProjectRiskSuccess").modal("show");
                        createProjectRisksSubmit = true;
                        setTimeout(function(){$('#createProjectRiskSuccess').modal('hide')}, 3000);

                    }).error(function (response) {
                        createProjectRisksSubmit = true;
//                        console.log('Error Response :' + JSON.stringify(response));
                    });
                }


            }else{
                createProjectRisksSubmit = true;
                $scope.errorMessageData="Please Select Expected Resolution Date";
                $scope.errorMessage=true;
                $timeout(function(){
                    $scope.errorMessage=false;
                }, 3000);
            }
        }else{
            createProjectRisksSubmit = true;
            $scope.errorMessageData="Please Select Open date";
            $scope.errorMessage=true;
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);
        }



    }

    $scope.createRisk=function(){
        $scope.projectRisk={};
    }

    $scope.getProjectRisksDetails=function () {

        $http({
            method: 'GET',
            url: 'api/ProjectIssues/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getProjectRisksDetails :' + JSON.stringify(response));
            $scope.projectRiskList=response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    $scope.editProjectRisk = function(projectRisk) {
        $("#editProjectRisks").modal("show");
        $("#viewProjectRisks").modal("hide");
        $("#viewProjectRisks").modal("hide");
        $scope.editProjectRisks = angular.copy(projectRisk);
    }

    $scope.viewProjectRisk = function(projectRisk) {
        $("#viewProjectRisks").modal("show");
        $scope.viewProjectRisks = angular.copy(projectRisk);
    }

    var editRiskDoubleClick=false;

    $scope.editProjectRisksDetails = function() {
        var editProjectRisksDetails = $scope.editProjectRisks;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        //editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        if(!editRiskDoubleClick){
            editRiskDoubleClick=true;
            $http({
                method: 'PUT',
                url: 'api/ProjectIssues/'+$scope.editProjectRisks.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":editProjectRisksDetails
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                ////alert('successfully edit charter');
                editRiskDoubleClick=false;
                $scope.getProjectRisksDetails();
                $("#editProjectRisks").modal("hide");
                $("#editProjectRiskSuccess").modal("show");
                setTimeout(function(){$('#editProjectRiskSuccess').modal('hide')}, 3000);
            }).error(function (response) {
                editRiskDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }

    }


    $http({
        method: 'GET',
        url: 'api/Employees/?filter={"where":{"status":"Active"}}',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        //console.log('Employee List :' + JSON.stringify(response));
        $scope.employeeListForRisks = response;
    }).error(function (response) {
//        console.log('Error Response :' + JSON.stringify(response));
    });


}]);

app.controller('projectClosureController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("projectClosureController");


    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectClosure").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectClosure .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectClosure'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        $("<tr>" +
            "<th colspan='3' style='background-color: #ebebeb;'><h3>Header Schemes Details</h3></th>" +
            "</tr>").appendTo("table" + tableId);

        if( $scope.selectProjectTaskDetails!=null &&  $scope.selectProjectTaskDetails.length>0){

            for(var i=0;i<$scope.selectProjectTaskDetails.length;i++){
                var task=$scope.selectProjectTaskDetails[i];

                $("<tr>" +
                    "<th colspan='3' style='background-color: #a9a9a9;'>"+task.name+"</th>" +
                    "</tr>").appendTo("table"+tableId);

                if(task.taskList!=null && task.taskList.length>0){
                    $("<tr>" +
                        "<th>SubTask Name</th>" +
                        "<th>Status</th>" +
                        "<th>% Complete</th>" +
                        "</tr>").appendTo("table"+tableId);

                    for(var i=0;i<task.taskList.length;i++){
                        var subTask=task.taskList[i];

                        $("<tr>" +
                            "<td>"+subTask.subTaskName+"</td>" +
                            "<td>"+subTask.status+"</td>" +
                            "<td>"+subTask.completePercenatge+"</td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                }

                $("<tr>" +
                    "<td colspan='3'></td>" +
                    "</tr>").appendTo("table"+tableId);


            }


        }

        var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }
    /*
     $scope.getCharterDetails=function () {
     $http({
     method: 'GET',
     url: 'api/ProjectCharters',
     headers: {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response) {
     console.log('Field visit report :' + JSON.stringify(response));
     $rootScope.charterList=response;
     //    //alert('successfully created charter');
     }).error(function (response) {
     console.log('Error Response :' + JSON.stringify(response));
     });
     }*/

    $scope.getCharterDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans/getProjectDetails',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getCharterDetails :' + JSON.stringify(response));
            $rootScope.charterList=response;
            //    //alert('successfully created charter');
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
        ////alert('get charter list');
    }
    $scope.getCharterDetails();

    $scope.selectPlanId = function(projectId){
        $scope.selectProjectId = projectId;

        $http({
            method: 'GET',
            url: 'api/ProjectPlans/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('Field visit report :' + JSON.stringify(response));
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.getTaskDetails();
            // $scope.selectProjectDetails = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.dtOptions = { paging: false, searching: false };
    $scope.getTaskDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectTasks/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('Field visit report :' + JSON.stringify(response));
            $scope.selectProjectTaskDetails=response;
            $scope.loadingImage=false;
            //   $scope.selectProjectDetails = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }





    $scope.showSection = function() {
        $scope.showSectionArea = true;
    }

    $scope.cancelSection = function () {
        $scope.showSectionArea = false;
    }
    $scope.errorMessage=false;

    $scope.closure={}

    function defaultActiveTab() {
        $('#tab1').addClass('active');
        $('#step1').addClass('active');
        $('#tab2').removeClass('active');
        $('#tab3').removeClass('active');
        $('#step2').removeClass('active');
        $('#complete').removeClass('active');
        document.getElementById('tab2').style.pointerEvents = 'none';
        document.getElementById('tab3').style.pointerEvents = 'none';
    }

    $scope.submitClosureConfirmButton = function() {
        if($scope.closureDetails.comments != undefined && $scope.closureDetails.comments !=null && $scope.closureDetails.comments !='' && $scope.closureDetails.comments.length>0){
            $("#submitClosureMessage").modal("show");
            $scope.closure={};
            $scope.closure.departmentId = '';

            defaultActiveTab();
        }else{
            $scope.errorMessage=true;
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);
        }
    }
    var closureDoubleClick=false;
    $scope.submitClosureComment=function () {
        ////alert('hai');
          var projectClosure=$scope.closure;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectClosure['createdPerson']=loginPersonDetails.name;
        projectClosure['planId']=$scope.selectProjectId;
        projectClosure['commnets']=$scope.closureDetails.comments;;
//        console.log('closure details'+JSON.stringify(projectClosure));
        if(!closureDoubleClick){
         closureDoubleClick=true;
         $http({
         method: 'POST',
         url: 'api/ProjectClosures',
         headers: {"Content-Type": "application/json", "Accept": "application/json"},
         "data":projectClosure
         }).success(function (response) {
         //console.log('Users Response :' + JSON.stringify(response));
         //  //alert('successfully created charter');
         //$scope.getProjectRisksDetails();
         $("#submitClosureMessage").modal("hide");
         $scope.showSectionArea=false;
         closureDoubleClick=false;
//             console.log('asset Details are'+JSON.stringify(response));
             if(response!=null && response.assetNumber!=undefined && response.assetNumber!=null){
                $scope.assetdetails=response;
//                    console.log("----------------------------------"+JSON.stringify($scope.assetdetails));
                 $("#showDesc").modal("show");
             }else{

             }

         }).error(function (response) {
         closureDoubleClick=false;
//         console.log('Error Response :' + JSON.stringify(response));
         });
         }

    }
    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {


            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        })

        document.getElementById('test').addEventListener('click', function() {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);
        }, false);

        document.getElementById('test').addEventListener('click', function() {
            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        }, false);

        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           // alert('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDepartments();

    function doSomethingElse() {
        $('#tab3').addClass('active');
        $('#complete').addClass('active');
        $('#tab2').removeClass('active');
        $('#tab1').removeClass('active');
        $('#step1').removeClass('active');
        $('#step2').removeClass('active');
        $('.nav.nav-tabs li').removeClass("active");
        document.getElementById('tab2').style.pointerEvents = 'none';
        document.getElementById('tab1').style.pointerEvents = 'none';
    }
    var closureDoubleClick=false;
    $scope.createProClosure=function () {

        if($scope.closure.departmentId != '' && $scope.closure.departmentId != undefined && $scope.closure.departmentId != null &&  $scope.closure.departmentId.length>0) {
            if($scope.closure.assetName != '' && $scope.closure.assetName != undefined && $scope.closure.assetName != null) {
                if($scope.closure.landLocality != '' && $scope.closure.landLocality != undefined && $scope.closure.landLocality != null) {
                    if($scope.closure.landDetails != '' && $scope.closure.landDetails != undefined && $scope.closure.landDetails != null &&  $scope.closure.landDetails.length>0) {
                        if($scope.closure.areaInUnits != '' && $scope.closure.areaInUnits != undefined && $scope.closure.areaInUnits != null) {
                            doSomethingElse();
                        } else {
                            $scope.errorMessage = true;
                            $scope.errorMessageData = 'Please Enter the Area In Units';
                            $timeout(function(){ $scope.errorMessage=false; }, 3000);
                        }
                    } else {
                        $scope.errorMessage = true;
                        $scope.errorMessageData = 'Please Select the Land Details';
                        $timeout(function(){ $scope.errorMessage=false; }, 3000);
                    }
                } else {
                    $scope.errorMessage = true;
                    $scope.errorMessageData = 'Please Enter the Locality';
                    $timeout(function(){ $scope.errorMessage=false; }, 3000);
                }
            } else {
                $scope.errorMessage = true;
                $scope.errorMessageData = 'Please Enter the Asset Name';
                $timeout(function(){ $scope.errorMessage=false; }, 3000);
            }

        } else {
            $scope.errorMessage = true;
            $scope.errorMessageData = 'Please Select the Department';
            $timeout(function(){ $scope.errorMessage=false; }, 3000);
        }
    }


}]);
/*
app.controller('budgetPlanController', function($http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("budgetPlanController.................");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.departmentWiseBudgets").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.departmentWiseBudgets .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.budgetDetails1!=null &&  $scope.budgetDetails1.length>0){

            $("<tr>" +
                "<th>Id</th>" +
                "<th>Total Budget</th>" +
                "<th>Department</th>" +
                "<th>Financial Year</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.budgetDetails1.length;i++){
                var budget=$scope.budgetDetails1[i];

                $("<tr>" +
                    "<td>"+budget.id+"</td>" +
                    "<td>"+budget.totalBudget+"</td>" +
                    "<td>"+budget.department+"</td>" +
                    "<td>"+budget.financialYear+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    $scope.loadingImage=true;


    $http({
        method: 'GET',
        url: 'api/PlanDepartments',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        //console.log('Users Response :' + JSON.stringify(response));
        $scope.departmentList = response;
    }).error(function (response) {
        console.log('Error Response :' + JSON.stringify(response));
    });




    $scope.projectName22222='';

    $scope.selectDepartment = function(deptId){
        $scope.projectName22222 = deptId;
    };



    $scope.department={financialYear : '2016-2017'};
    $scope.selectPlanId = function(financialYear){
        $scope.deptBudgetError = false;
        $scope.deptBudgetErrorMessage = '';
        $scope.departmentWiseBudget = '';
        $scope.selectedItems = [];
        $scope.showBudget = false;
        //console.log('Entered'+financialYear);
        //console.log("RRRR:" +$scope.projectName22222);
        var checkEditBudget = financialYear;
        if(checkEditBudget == 'edit'){
            $scope.projectName22222 = $scope.editBudgetInfo.department;
            financialYear = $scope.editBudgetInfo.financialYear;
        }
        if($scope.projectName22222 && $scope.projectName22222 != '') {
            if(financialYear && financialYear != '') {
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans/?filter={"where":{"departmentId": "' + $scope.projectName22222 + '","financialYear":"' + financialYear + '"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    //alert("222:"+JSON.stringify(response));
                    $scope.loadingImage=false;
                    //console.log('Field visit report :' + JSON.stringify(response));
                    $scope.selectProjectDetails = response;
                    if(response.length>0){
                        $scope.showBudget = true;
                    }
                    //$scope.selectProjectDetails;
                }).error(function (response) {
                    //console.log('Error Response :' + JSON.stringify(response));
                });
            }else{
                alert('Please select financial year');
            }
        }else{
            alert('Please select department');
        }
    };

    $http({
        "method": "GET",
        "url": 'api/Employees/',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        //console.log("employeesListName " + JSON.stringify(response));
        $scope.employeesListName = response;
    }).error(function (response, data) {
        console.log("failure");
    })


    $scope.editBudget=function(budjet){
        $scope.budgetDetails33=budjet;
        $scope.updateBudgetDetails.comment = '';
        $scope.updateBudgetDetails.totalBudget = budjet.totalBudget;
        $scope.updateBudgetDetails.acceptLevel = budjet.acceptLevel;
        //console.log("Budget Edit......:"+JSON.stringify(budjet));
        var id=budjet.id;
        $window.localStorage.setItem('budgetId',id);
        // alert("257:"+id);
    };

    $scope.budgetChange=function (approvedStatus) {
        // alert(approvedStatus)
        // alert(approvedStatus=="Approved")
        //console.log("Budget Status change" + JSON.stringify(approvedStatus));
        if(approvedStatus=="Approved")
        {
            $scope.onBudget=true;
        }
        else {
            $scope.onBudget=false;
        }
    };

    var subPlanDetails = [];
    $scope.selectedItems = [];
    $scope.departmentWiseBudget='';
    $scope.checkList=function(id,estimationCost,data){
        $scope.deptBudgetError = false;
        $scope.deptBudgetErrorMessage = '';
        //console.log("jghj :" + id);
        //console.log("estimationCost :" + estimationCost);
        //console.log("planId :" + JSON.stringify(data));

        var pos = $scope.selectedItems.indexOf(id);

        if (pos == -1) {

            subPlanDetails.push(data)
            //alert("JJJJJ:" + JSON.stringify(subPlanDetails));
            //console.log("RRRR:" + JSON.stringify(subPlanDetails))
            $scope.selectedItems.push(id);
            //console.log("RRRR:" + JSON.stringify(subPlanDetails))

        } else {
            $scope.selectedItems.splice(pos, 1);
        }
        //$rootScope.curryDeatiles.push(id);
        //console.log('items'+id);

        //console.log("items into array"+ $scope.selectedItems);

        var budgetAmount=$scope.selectedItems;

        //console.log("selected items are::" +budgetAmount);
        var totalAmount=0;

        for (var i = 0; i < $scope.selectedItems.length; i++) {
            //console.log("selected items are::" +splittedSelectedItems);
            totalAmount=parseInt(totalAmount)+parseInt($scope.selectedItems[i]);
        }
        $scope.departmentWiseBudget=totalAmount;
        //console.log(totalAmount);
    };


    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    //var employeeName=$scope.userInfo.name;

    $scope.getBudgetPlanDetails = function() {
        $http({
            method: 'GET',
            url: 'api/DepartmentWiseBudgets/getDetails?employeeId=' + employeeId,
            /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets',*!/
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Budget Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.budgetDetails1 = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getBudgetPlanDetails();

    $scope.department={};
    /!*$scope.reset=function () {
     $scope.found={};
     $scope.selectedItems=[];
     $scope.selectProjectDetails=[];
     }*!/

    $scope.deptBudgetError = false;
    $scope.deptBudgetErrorMessage = '';

    var departmentBudgetDoubleClick=false;

    $scope.budgetDetails = function(){
        $scope.deptBudgetError = false;
        $scope.deptBudgetErrorMessage = '';
        // var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        if($scope.departmentWiseBudget != '' && $scope.departmentWiseBudget > 0) {
            var bgd = {
                "departmentId": $scope.projectName22222,
                "financialYear": $scope.department.financialYear,
                "totalBudget": $scope.departmentWiseBudget,
                "selectedItems": $scope.selectedItems,
                "planDetails": subPlanDetails,
                "approvedStatus": "",
                "approvedBudget": "",
                "approvedDate": "",
                "approvedBy": "",
                "planId": "",
                "planName": "",
                "budget": "",
                "status": "",
                "createdPersonId": $scope.userInfo.employeeId
            };

            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            if(!departmentBudgetDoubleClick){
                departmentBudgetDoubleClick=true;
                $http({
                    method: 'POST',
                    url: 'api/DepartmentWiseBudgets',
                    /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets',*!/
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": bgd
                }).success(function (response) {
                    //   alert("sdfd "+JSON.stringify(response));
                    $scope.loadingImage=false;
                    location.href = '#/budgetPlans';
                    // $scope.getBudgetDetails();
                    departmentBudgetDoubleClick=false;
                }).error(function (response) {
                    departmentBudgetDoubleClick=false;
                    console.log('Error Response :' + JSON.stringify(response));
                    if(response.error){
                        if(response.error.message){
                            $scope.deptBudgetError = true;
                            $scope.deptBudgetErrorMessage = response.error.message;
                        }
                    }
                });
            }

        }else{
            $scope.deptBudgetError = true;
            $scope.deptBudgetErrorMessage = 'Please select the plan';
        }
    };

    $scope.updateBudgetDetails = {};

    var editBugetDoubleClick=false;
    $scope.editBudgetDetails=function (updateBudgetDetails, status) {
        /// alert("gjh:"+JSON.stringify(updateBudgetDetails));

        var budgetCheck = true;
        $scope.updateBudgetDetails.approvedDate = new Date();
        $scope.updateBudgetDetails.approvedBy = $scope.userInfo.name;
        $scope.updateBudgetDetails.employeeId = $scope.userInfo.id;
        if(status == 'approve'){
            $scope.updateBudgetDetails.approvedStatus = 'Approved';
            if($scope.updateBudgetDetails.approvedBudget > 0){
                budgetCheck = true;
            }else{
                budgetCheck = false;
                alert('Please enter a valid budget');
            }
        }else{
            $scope.updateBudgetDetails.approvedStatus = 'Rejected';
            $scope.updateBudgetDetails.approvedBudget = '';
        }
        var budgetId= $window.localStorage.getItem('budgetId');
        $scope.updateBudgetDetails.budgetId = budgetId;
        //console.log("352..........."+JSON.stringify($scope.updateBudgetDetails));
        //alert("353"+JSON.stringify($scope.updateBudgetDetails));

        if(budgetCheck == true) {
            if(!editBugetDoubleClick){
                editBugetDoubleClick=true;
                $http({
                    "method": "POST",
                    url: 'api/DepartmentWiseBudgets/updateDetails',
                    /!*"url": 'http://localhost:8866/api/DepartmentWiseBudgets/'+budgetId,*!/
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": $scope.updateBudgetDetails
                }).success(function (response, data) {
                    // console.log("filter Schemes..: " + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $("#editBudgetPlans").modal("hide");
                    $("#rejectBudgetPlans").modal("hide");
                    editBugetDoubleClick=false;
                    $scope.getBudgetPlanDetails();
                }).error(function (response, data) {
                    editBugetDoubleClick=false;
                    console.log("failure" + JSON.stringify(response));
                });
            }

        }
    };


    $scope.budgetDetailsList = function(id){
        //alert(id);
        //var budgetId= $window.localStorage.getItem('budgetId');
        $scope.selectProjectDetails = false;
        $scope.showBudget = false;
        $http({
            method: 'GET',
            url: 'api/DepartmentWiseBudgets/' +id,
            /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets/'+id,*!/
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.editBudgetInfo = response;
            $scope.getBudgetDetailsListData= response.planDetails;
            // console.log("402:"+JSON.stringify($scope.getBudgetDetailsListData));
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    };
    var updateDoubleClick=false;

    $scope.updateBudgetInfo = function(){
        $scope.editBudgetInfo.planDetails = subPlanDetails;
        $scope.editBudgetInfo.selectedItems = $scope.selectedItems;
        $scope.editBudgetInfo.totalBudget = $scope.departmentWiseBudget;
        //console.log('Update budget details:'+JSON.stringify($scope.editBudgetInfo));
        if($scope.editBudgetInfo.totalBudget != '' && $scope.editBudgetInfo.totalBudget > 0) {
            if(!updateDoubleClick){
                updateDoubleClick=true;
                $http({
                    "method": "POST",
                    url: 'api/DepartmentWiseBudgets/updateContent',
                    /!*"url": 'http://localhost:8866/api/DepartmentWiseBudgets/'+budgetId,*!/
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": $scope.editBudgetInfo
                }).success(function (response, data) {
                    //console.log("filter Schemes..: " + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $("#myModal").modal("hide");
                    updateDoubleClick=false;
                    $scope.getBudgetPlanDetails();
                }).error(function (response, data) {
                    updateDoubleClick=false;
                    console.log("failure" + JSON.stringify(response));
                });
            }

        }else{
            alert('Please enter valid budget');
        }
    };
});
*/
app.controller('budgetPlanController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope','Excel','$timeout','DTOptionsBuilder', 'DTColumnBuilder',  function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("budgetPlanController.................");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.departmentWiseBudgets").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.departmentWiseBudgets .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectDepartmentWiseBudget'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.budgetDetails1!=null &&  $scope.budgetDetails1.length>0){

            $("<tr>" +
                "<th>Id</th>" +
                "<th>Total Budget</th>" +
                "<th>Department</th>" +
                "<th>Financial Year</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.budgetDetails1.length;i++){
                var budget=$scope.budgetDetails1[i];

                $("<tr>" +
                    "<td>"+budget.id+"</td>" +
                    "<td>"+budget.totalBudget+"</td>" +
                    "<td>"+budget.department+"</td>" +
                    "<td>"+budget.financialYear+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

$scope.budgetDetailsList = function(id){

var total = 0;
$scope.estimatedCostTotal = 0;
     //alert(id);
     //var budgetId= $window.localStorage.getItem('budgetId');
     $("#myModal").modal("show");
     $scope.selectProjectDetails = false;
     $scope.showBudget = false;
     $http({
         method: 'GET',
         url: 'api/DepartmentWiseBudgets/'+id,
         headers: {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response) {
//         console.log('Users Response :' + JSON.stringify(response));
         $scope.loadingImage=false;
         $scope.editBudgetInfo = response;
         $rootScope.getBudgetDetailsListData= response.planDetails;
         $scope.getBudgetDetailsListData1= response.planData;
//                alert("getBudgetDetailsListData..........   "+JSON.stringify($scope.getBudgetDetailsListData1));
//                alert("length"+$scope.getBudgetDetailsListData1.length);
                    for(var i = 0; i < $scope.getBudgetDetailsListData1.length; i++){
//                        alert("estimation cost "+$scope.getBudgetDetailsListData1[i].estimationCost)
                        total += parseInt($scope.getBudgetDetailsListData1[i].estimationCost);
//                        alert("total-- "+ total);
                        $scope.estimatedCostTotal = total;
                    }
                    return total;
         // console.log("402:"+JSON.stringify($scope.getBudgetDetailsListData));
     }).error(function (response) {
         console.log('Error Response :' + JSON.stringify(response));
     });
 };


    $scope.estimatedCostTotal = function() {

//           return total;
    }

    $scope.loadingImage=true;
    var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));

    $scope.getBudgetPlanDetails = function() {
        $http({
            method: 'GET',
            url: 'api/DepartmentWiseBudgets/getDetails?employeeId=' + loginPersonDetails.employeeId,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        //console.log('Budget Response :' + JSON.stringify(response));
        $scope.loadingImage=false;
        $scope.budgetDetails = response;
    }).error(function (response) {
//        console.log('Error Response :' + JSON.stringify(response));
    });
    };
    $scope.getBudgetPlanDetails();
        
    $scope.approvalModal= function (selectedBudget) {
        $scope.updateBudgetDetails=selectedBudget;
        $("#approvalBudget").modal("show");
    }
    $scope.editBudgetDetails=function () {

        var updateDetails = {
            "approvedBudget":$scope.updateBudgetDetails.approvedBudget,
            "acceptLevel":$scope.updateBudgetDetails.acceptLevel,
            "acceptStatus": "Yes",
            "approvedStatus": "Approved",
            "comment":  $scope.updateBudgetDetails.comment,
            "requestId":$scope.updateBudgetDetails.id,
            "employeeId":loginPersonDetails.employeeId,
            "employeeName":loginPersonDetails.name
        };
       var editBugetDoubleClick=false;
            if(!editBugetDoubleClick){
                editBugetDoubleClick=true;
                $http({
                    "method": "POST",
                    url: 'api/DepartmentWiseBudgets/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                // console.log("filter Schemes..: " + JSON.stringify(response));
                $scope.loadingImage=false;
                $("#approvalBudget").modal("hide");
                $("#rejectBudgetPlans").modal("hide");
                editBugetDoubleClick=false;
                $scope.getBudgetPlanDetails();
            }).error(function (response, data) {
                editBugetDoubleClick=false;
//                console.log("failure" + JSON.stringify(response));
            });
        }


};
    $scope.comments='';
    $scope.rejectModal= function (selectedBudget) {
        $scope.updateBudgetDetails=selectedBudget;
        $("#rejectBudgetPlans").modal("show");
    }
$scope.rejectData=function(){
    var updateDetails = {
        "approvedBudget":0,
        "acceptLevel":$scope.updateBudgetDetails.acceptLevel,
        "acceptStatus": "No",
        "approvedStatus": "Rejected",
        "comment":  $scope.comments.comment,
        "requestId":$scope.updateBudgetDetails.id,
        "employeeId":loginPersonDetails.employeeId,
        "employeeName":loginPersonDetails.name
    };
    var editBugetDoubleClick=false;
    if(!editBugetDoubleClick){
        editBugetDoubleClick=true;
        $http({
            "method": "POST",
            url: 'api/DepartmentWiseBudgets/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            // console.log("filter Schemes..: " + JSON.stringify(response));
            $scope.loadingImage=false;
            $("#rejectBudgetPlans").modal("hide");
            editBugetDoubleClick=false;
            $scope.getBudgetPlanDetails();
        }).error(function (response, data) {
            editBugetDoubleClick=false;
//            console.log("failure" + JSON.stringify(response));
        });
    }
}


/* $http({
     method: 'GET',
     url: 'api/PlanDepartments',
     headers: {"Content-Type": "application/json", "Accept": "application/json"}
 }).success(function (response) {
     //console.log('Users Response :' + JSON.stringify(response));
     $scope.departmentList = response;
 }).error(function (response) {
     console.log('Error Response :' + JSON.stringify(response));
 });


 $scope.projectName22222='';

 $scope.selectDepartment = function(deptId){
     $scope.projectName22222 = deptId;
 };

 $scope.department={financialYear : '2016-2017'};
 $scope.selectPlanId = function(financialYear){

     $scope.deptBudgetError = false;
     $scope.deptBudgetErrorMessage = '';
     $scope.departmentWiseBudget = '';
     $scope.selectedItems = [];
     $scope.showBudget = false;
     //console.log('Entered'+financialYear);
     //console.log("RRRR:" +$scope.projectName22222);
     var checkEditBudget = financialYear;
     if(checkEditBudget == 'edit'){
         $scope.projectName22222 = $scope.editBudgetInfo.department;
         financialYear = $scope.editBudgetInfo.financialYear;
     }
     if($scope.projectName22222 && $scope.projectName22222 != '') {
         if(financialYear && financialYear != '') {
             $http({
                 method: 'GET',
                 url: 'api/ProjectPlans/?filter={"where":{"departmentId": "' + $scope.projectName22222 + '","financialYear":"' + financialYear + '"}}',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //alert("222:"+JSON.stringify(response));
                 $scope.loadingImage=false;

                 //console.log('Field visit report :' + JSON.stringify(response));
                 $scope.selectProjectDetails = response;
                 if(response.length>0){
                     $scope.showBudget = true;
                 }
                 //$scope.selectProjectDetails;
             }).error(function (response) {
                 //console.log('Error Response :' + JSON.stringify(response));
             });
         }else{
             alert('Please select financial year');
         }
     }else{
         alert('Please select department');
     }
 };

 $http({
     "method": "GET",
     "url": 'api/Employees/',
     "headers": {"Content-Type": "application/json", "Accept": "application/json"}
 }).success(function (response, data) {
     //console.log("employeesListName " + JSON.stringify(response));
     $scope.employeesListName = response;
 }).error(function (response, data) {
     console.log("failure");
 })


 $scope.editBudget=function(budjet){
     $scope.budgetDetails33=budjet;
     $scope.updateBudgetDetails.comment = '';
     $scope.updateBudgetDetails.totalBudget = budjet.totalBudget;
     $scope.updateBudgetDetails.acceptLevel = budjet.acceptLevel;
     //console.log("Budget Edit......:"+JSON.stringify(budjet));
     var id=budjet.id;
     $window.localStorage.setItem('budgetId',id);
     // alert("257:"+id);
 };

 $scope.budgetChange=function (approvedStatus) {
     // alert(approvedStatus)
     // alert(approvedStatus=="Approved")
     //console.log("Budget Status change" + JSON.stringify(approvedStatus));
     if(approvedStatus=="Approved")
     {
         $scope.onBudget=true;
     }
     else {
         $scope.onBudget=false;
     }
 };

 var subPlanDetails = [];
 $scope.selectedItems = [];
 $scope.departmentWiseBudget='';
 $scope.checkList=function(id,estimationCost,data){
     $scope.deptBudgetError = false;
     $scope.deptBudgetErrorMessage = '';
     //console.log("jghj :" + id);
     //console.log("estimationCost :" + estimationCost);
     //console.log("planId :" + JSON.stringify(data));

     var pos = $scope.selectedItems.indexOf(id);

     if (pos == -1) {

         subPlanDetails.push(data)
         //alert("JJJJJ:" + JSON.stringify(subPlanDetails));
         //console.log("RRRR:" + JSON.stringify(subPlanDetails))
         $scope.selectedItems.push(id);
         //console.log("RRRR:" + JSON.stringify(subPlanDetails))

     } else {
         $scope.selectedItems.splice(pos, 1);
     }
     //$rootScope.curryDeatiles.push(id);
     //console.log('items'+id);

     //console.log("items into array"+ $scope.selectedItems);

     var budgetAmount=$scope.selectedItems;

     //console.log("selected items are::" +budgetAmount);
     var totalAmount=0;

     for (var i = 0; i < $scope.selectedItems.length; i++) {
         //console.log("selected items are::" +splittedSelectedItems);
         totalAmount=parseInt(totalAmount)+parseInt($scope.selectedItems[i]);
     }
     $scope.departmentWiseBudget=totalAmount;
     //console.log(totalAmount);
 };


 $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
 var employeeId=$scope.userInfo.employeeId;
 //var employeeName=$scope.userInfo.name;

 $scope.getBudgetPlanDetails = function() {
     $http({
         method: 'GET',
         url: 'api/DepartmentWiseBudgets/getDetails?employeeId=' + employeeId,
         /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets',*!/
         headers: {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response) {
         //console.log('Budget Response :' + JSON.stringify(response));
         $scope.loadingImage=false;
         $scope.budgetDetails = response;
     }).error(function (response) {
         console.log('Error Response :' + JSON.stringify(response));
     });
 };
 $scope.getBudgetPlanDetails();

 $scope.department={};
 /!*$scope.reset=function () {
  $scope.found={};
  $scope.selectedItems=[];
  $scope.selectProjectDetails=[];
  }*!/

 $scope.deptBudgetError = false;
 $scope.deptBudgetErrorMessage = '';

 var departmentBudgetDoubleClick=false;

 $scope.budgetDetails = function(){
     $scope.deptBudgetError = false;
     $scope.deptBudgetErrorMessage = '';
     // var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
     if($scope.departmentWiseBudget != '' && $scope.departmentWiseBudget > 0) {
         var bgd = {
             "departmentId": $scope.projectName22222,
             "financialYear": $scope.department.financialYear,
             "totalBudget": $scope.departmentWiseBudget,
             "selectedItems": $scope.selectedItems,
             "planDetails": subPlanDetails,
             "approvedStatus": "",
             "approvedBudget": "",
             "approvedDate": "",
             "approvedBy": "",
             "planId": "",
             "planName": "",
             "budget": "",
             "status": "",
             "createdPersonId": $scope.userInfo.employeeId
         };
         console.log("123:" + JSON.stringify(bgd));
         var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
         if(!departmentBudgetDoubleClick){
             departmentBudgetDoubleClick=true;
             $http({
                 method: 'POST',
                 url: 'api/DepartmentWiseBudgets',
                 /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets',*!/
                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": bgd
             }).success(function (response) {
                 //   alert("sdfd "+JSON.stringify(response));
                 $scope.loadingImage=false;
                 location.href = '#/budgetPlans';
                 // $scope.getBudgetDetails();
                 departmentBudgetDoubleClick=false;
             }).error(function (response) {
                 departmentBudgetDoubleClick=false;
                 console.log('Error Response :' + JSON.stringify(response));
                 if(response.error){
                     if(response.error.message){
                         $scope.deptBudgetError = true;
                         $scope.deptBudgetErrorMessage = response.error.message;
                     }
                 }
             });
         }

     }else{
         $scope.deptBudgetError = true;
         $scope.deptBudgetErrorMessage = 'Please select the plan';
     }
 };

 $scope.updateBudgetDetails = {};

 var editBugetDoubleClick=false;
 $scope.editBudgetDetails=function (updateBudgetDetails, status) {
     /// alert("gjh:"+JSON.stringify(updateBudgetDetails));

     var budgetCheck = true;
     $scope.updateBudgetDetails.approvedDate = new Date();
     $scope.updateBudgetDetails.approvedBy = $scope.userInfo.name;
     $scope.updateBudgetDetails.employeeId = $scope.userInfo.id;
     if(status == 'approve'){
         $scope.updateBudgetDetails.approvedStatus = 'Approved';
         if($scope.updateBudgetDetails.approvedBudget > 0){
             budgetCheck = true;
         }else{
             budgetCheck = false;
             alert('Please enter a valid budget');
         }
     }else{
         $scope.updateBudgetDetails.approvedStatus = 'Rejected';
         $scope.updateBudgetDetails.approvedBudget = '';
     }
     var budgetId= $window.localStorage.getItem('budgetId');
     $scope.updateBudgetDetails.budgetId = budgetId;
     //console.log("352..........."+JSON.stringify($scope.updateBudgetDetails));
     //alert("353"+JSON.stringify($scope.updateBudgetDetails));

     if(budgetCheck == true) {
         if(!editBugetDoubleClick){
             editBugetDoubleClick=true;
             $http({
                 "method": "POST",
                 url: 'api/DepartmentWiseBudgets/updateDetails',
                 /!*"url": 'http://localhost:8866/api/DepartmentWiseBudgets/'+budgetId,*!/
                 "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": $scope.updateBudgetDetails
             }).success(function (response, data) {
                 // console.log("filter Schemes..: " + JSON.stringify(response));
                 $scope.loadingImage=false;
                 $("#editBudgetPlans").modal("hide");
                 $("#rejectBudgetPlans").modal("hide");
                 editBugetDoubleClick=false;
                 $scope.getBudgetPlanDetails();
             }).error(function (response, data) {
                 editBugetDoubleClick=false;
                 console.log("failure" + JSON.stringify(response));
             });
         }

     }
 };


 $scope.budgetDetailsList = function(id){
     //alert(id);
     //var budgetId= $window.localStorage.getItem('budgetId');
     $scope.selectProjectDetails = false;
     $scope.showBudget = false;
     $http({
         method: 'GET',
         url: 'api/DepartmentWiseBudgets/' +id,
         /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets/'+id,*!/
         headers: {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response) {
         //console.log('Users Response :' + JSON.stringify(response));
         $scope.loadingImage=false;
         $scope.editBudgetInfo = response;
         $scope.getBudgetDetailsListData= response.planDetails;
         // console.log("402:"+JSON.stringify($scope.getBudgetDetailsListData));
     }).error(function (response) {
         console.log('Error Response :' + JSON.stringify(response));
     });

 };
 var updateDoubleClick=false;

 $scope.updateBudgetInfo = function(){
     $scope.editBudgetInfo.planDetails = subPlanDetails;
     $scope.editBudgetInfo.selectedItems = $scope.selectedItems;
     $scope.editBudgetInfo.totalBudget = $scope.departmentWiseBudget;
     //console.log('Update budget details:'+JSON.stringify($scope.editBudgetInfo));
     if($scope.editBudgetInfo.totalBudget != '' && $scope.editBudgetInfo.totalBudget > 0) {
         if(!updateDoubleClick){
             updateDoubleClick=true;
             $http({
                 "method": "POST",
                 url: 'api/DepartmentWiseBudgets/updateContent',
                 /!*"url": 'http://localhost:8866/api/DepartmentWiseBudgets/'+budgetId,*!/
                 "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": $scope.editBudgetInfo
             }).success(function (response, data) {
                 //console.log("filter Schemes..: " + JSON.stringify(response));
                 $scope.loadingImage=false;
                 $("#myModal").modal("hide");
                 updateDoubleClick=false;
                 $scope.getBudgetPlanDetails();
             }).error(function (response, data) {
                 updateDoubleClick=false;
                 console.log("failure" + JSON.stringify(response));
             });
         }

     }else{
         alert('Please enter valid budget');
     }
 };*/
}]);

app.controller('editBudgetDetailsController',['projectWardWorks','$http', '$routeParams', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $routeParams, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("editBudgetDetailsController.................");

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectDepartmentWiseBudget'}, function (response) {
//        	alert(JSON.stringify(response));
               if(!response){
                window.location.href = "#/noAccessPage";
               }
            });

    $scope.budgetIdData = $routeParams.budgetId;
    //alert($scope.budgetIdData);

    $scope.loadingImage=true;
    var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));

    $scope.getBudgetPlanDetails = function() {
        $http({
            method: 'GET',
            url: 'api/DepartmentWiseBudgets/'+$scope.budgetIdData,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
       // console.log('Budget Response :' + JSON.stringify(response));
        $scope.loadingImage=false;
        $scope.budgetDetails = response;
            $scope.searchPlans();
    }).error(function (response) {
//        console.log('Error Response :' + JSON.stringify(response));
    });
    };
    $scope.getBudgetPlanDetails();

$scope.getULB=function () {
        $http({
            method: 'GET',
            url: 'api/ULBs?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log("responce"+JSON.stringify(response));
            $scope.ulbList = response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getULB();
    $scope.editBudgetPlan=function(){
                var schemeDetails = $scope.editBudget;
                    $http({
                    "method": "PUT",
                    url: 'api/DepartmentWiseBudgets/'+$scope.budgetIdData,
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": $scope.editBudget
                }).success(function (response, data) {
//                console.log("......"+ JSON.stringify(response));
                    $scope.editBudget = response;
                }).error(function (response, data) {
//                    console.log("failure");
                })
        }

    $scope.searchPlans = function(){
        if($scope.budgetDetails.ulbName!=undefined && $scope.budgetDetails.ulbName!=null && $scope.budgetDetails.ulbName!='' &&
            $scope.budgetDetails.departmentId!=undefined && $scope.budgetDetails.departmentId!= null && $scope.budgetDetails.departmentId!='' &&
            $scope.budgetDetails.financialYear!=undefined && $scope.budgetDetails.financialYear!=null && $scope.budgetDetails.financialYear!=''){
            $http({
                method: 'GET',
                url: 'api/DepartmentWiseBudgets/getBudgetPlans?ulb='+$scope.budgetDetails.ulbName+'&department='+$scope.budgetDetails.departmentId+'&fYear='+$scope.budgetDetails.financialYear,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.selectProjectDetails = response;
                if($scope.selectProjectDetails==null || $scope.selectProjectDetails=='' ){
                    $scope.selectProjectDetails=[];
                }
                if($scope.budgetDetails.planData!=undefined &&$scope.budgetDetails.planData!=null && $scope.budgetDetails.planData.length>0){
                    for(var x=0;x<$scope.budgetDetails.planData.length;x++){
                        $scope.budgetDetails.planData[x]['checkPlan']=true;
                    }

                    $scope.selectProjectDetails.push.apply($scope.selectProjectDetails, $scope.budgetDetails.planData)
                }
            }).error(function (response) {

            });

        }else{
            alert('please select all');
        }
    }
    projectWardWorks.getPlanDepartment({}, function (response) {
            $scope.departmentList=response;
            var allObject={
                'id':"all",
                'name':"All"
            }
            $scope.departmentList.unshift(allObject);
            $scope.loadingImage=false;
        });

    $scope.approvalModal= function (selectedBudget) {
        $scope.updateBudgetDetails=selectedBudget;
        $("#approvalBudget").modal("show");
    }
    $scope.editBudgetDetails=function () {

        var updateDetails = {
            "approvedBudget":$scope.updateBudgetDetails.approvedBudget,
            "acceptLevel":$scope.updateBudgetDetails.acceptLevel,
            "acceptStatus": "Yes",
            "approvedStatus": "Approved",
            "comment":  $scope.updateBudgetDetails.comment,
            "requestId":$scope.updateBudgetDetails.id,
            "employeeId":loginPersonDetails.employeeId,
            "employeeName":loginPersonDetails.name
        };
       var editBugetDoubleClick=false;
            if(!editBugetDoubleClick){
                editBugetDoubleClick=true;
                $http({
                    "method": "POST",
                    url: 'api/DepartmentWiseBudgets/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                // console.log("filter Schemes..: " + JSON.stringify(response));
                $scope.loadingImage=false;
                $("#approvalBudget").modal("hide");
                $("#rejectBudgetPlans").modal("hide");
                editBugetDoubleClick=false;
                $scope.getBudgetPlanDetails();
            }).error(function (response, data) {
                editBugetDoubleClick=false;
//                console.log("failure" + JSON.stringify(response));
            });
        }


};
    $scope.comments='';
    $scope.rejectModal= function (selectedBudget) {
        $scope.updateBudgetDetails=selectedBudget;
        $("#rejectBudgetPlans").modal("show");
    }
$scope.rejectData=function(){
    var updateDetails = {
        "approvedBudget":0,
        "acceptLevel":$scope.updateBudgetDetails.acceptLevel,
        "acceptStatus": "No",
        "approvedStatus": "Rejected",
        "comment":  $scope.comments.comment,
        "requestId":$scope.updateBudgetDetails.id,
        "employeeId":loginPersonDetails.employeeId,
        "employeeName":loginPersonDetails.name
    };
    var editBugetDoubleClick=false;
    if(!editBugetDoubleClick){
        editBugetDoubleClick=true;
        $http({
            "method": "POST",
            url: 'api/DepartmentWiseBudgets/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            // console.log("filter Schemes..: " + JSON.stringify(response));
            $scope.loadingImage=false;
            $("#rejectBudgetPlans").modal("hide");
            editBugetDoubleClick=false;
            $scope.getBudgetPlanDetails();
        }).error(function (response, data) {
            editBugetDoubleClick=false;
//            console.log("failure" + JSON.stringify(response));
        });
    }
}

}]);

app.controller('departmentWiseBudgetsController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("departmentWiseBudgetsController.................");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.departmentWiseBudgets").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.departmentWiseBudgets .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectDepartmentWiseBudget'}, function (response) {
//	alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.budgetDetails1!=null &&  $scope.budgetDetails1.length>0){

            $("<tr>" +
                "<th>Id</th>" +
                "<th>Total Budget</th>" +
                "<th>Department</th>" +
                "<th>Financial Year</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.budgetDetails1.length;i++){
                var budget=$scope.budgetDetails1[i];

                $("<tr>" +
                    "<td>"+budget.id+"</td>" +
                    "<td>"+budget.totalBudget+"</td>" +
                    "<td>"+budget.department+"</td>" +
                    "<td>"+budget.financialYear+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    $scope.loadingImage=true;
    projectWardWorks.getPlanDepartment({}, function (response) {
        $scope.departmentList=response;
        var allObject={
            'id':"all",
            'name':"All"
        }
        $scope.departmentList.unshift(allObject);
        $scope.loadingImage=false;
    });
    $scope.getULB=function () {
        $http({
            method: 'GET',
            url: 'api/ULBs?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.ulbList = response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getULB();
    $scope.selectULB=function(ulbId){
        $scope.projectULB=ulbId;
    }
    $scope.selectDepartment = function(deptId){
        $scope.selectedDeptId = deptId;
    };
    $scope.searchPlans = function(){
        if($scope.projectULB!=undefined && $scope.projectULB!=null && $scope.projectULB!='' &&
            $scope.selectedDeptId!=undefined && $scope.selectedDeptId!= null && $scope.selectedDeptId!='' &&
            $scope.financialYear!=undefined && $scope.financialYear!=null && $scope.financialYear!=''){
            $http({
                method: 'GET',
                url: 'api/DepartmentWiseBudgets/getBudgetPlans?ulb='+$scope.projectULB+'&department='+$scope.selectedDeptId+'&fYear='+$scope.financialYear,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                if(response.length>0){
                    $scope.showBudget = true;
                }
                $scope.selectProjectDetails = response;
            }).error(function (response) {

            });

        }else{
            alert('please select all');
        }
    }
    var subPlanDetails = [];
    $scope.selectedPlanDetails = [];
    $scope.estimatedTotalAmount=0;
    $scope.departmentWiseBudget='';
    $scope.checkList=function(planDetails){
       /* if(planDetails.checkPlan){*/
            $scope.estimatedTotalAmount=parseInt($scope.estimatedTotalAmount)+parseInt(planDetails.estimationCost);
            var details={
                'id':planDetails.id,
                'planId':planDetails.planId
            }
            $scope.selectedPlanDetails.push(details);
       /* }else{
            alert('false');
        }*/

      /*  alert('plandetails'+JSON.stringify(planDetails));
        $scope.deptBudgetError = false;
        $scope.deptBudgetErrorMessage = '';
         var pos = $scope.selectedItems.indexOf(id);
        if (pos == -1) {
            subPlanDetails.push(data)
            $scope.selectedItems.push(id);
        } else {
            $scope.selectedItems.splice(pos, 1);
        }
        var budgetAmount=$scope.selectedItems;
      var totalAmount=0;
        for (var i = 0; i < $scope.selectedItems.length; i++) {
             totalAmount=parseInt(totalAmount)+parseInt($scope.selectedItems[i]);
        }
        $scope.departmentWiseBudget=totalAmount;*/
    };

 var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.budgetDetails = function(){
        $scope.deptBudgetError = false;
        $scope.deptBudgetErrorMessage = '';
        if($scope.estimatedTotalAmount!=undefined && $scope.estimatedTotalAmount!=null && $scope.estimatedTotalAmount!= '' && $scope.estimatedTotalAmount > 0) {
            var bgd = {
                "departmentId": $scope.selectedDeptId,
                "ulbName": $scope.projectULB,
                "financialYear": $scope.financialYear,
                "totalBudget": $scope.estimatedTotalAmount,
                "currentFinYear": $scope.currentFinYear,
                "nextFinYear": $scope.nextFinYear,
                "planDetails": $scope.selectedPlanDetails,
                "createdPersonId": loginPersonDetails.employeeId
            };

            $scope.tolAmount = 0;

            $scope.tolAmount = parseInt($scope.currentFinYear) + parseInt($scope.nextFinYear);
//            alert($scope.tolAmount);

            if($scope.tolAmount >= $scope.estimatedTotalAmount) {
                $scope.errorShow = true;
                $scope.showError = 'Estimated Total Amount should be more or equal to the financial total amount';
                $timeout(function(){
                    $scope.errorShow=false;
                }, 5000);
            } else {


            var departmentBudgetDoubleClick=false;
            if(!departmentBudgetDoubleClick){
                departmentBudgetDoubleClick=true;
                $http({
                    method: 'POST',
                    url: 'api/DepartmentWiseBudgets',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data": bgd
            }).success(function (response) {
//                alert(response);
//                alert("fin year.........."+JSON.stringify(response));
                $scope.loadingImage=false;
                location.href = '#/budgetPlans';
                // $scope.getBudgetDetails();
                departmentBudgetDoubleClick=false;
            }).error(function (response) {
                departmentBudgetDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
                if(response.error){
                    if(response.error.message){
                        $scope.deptBudgetError = true;
                        $scope.deptBudgetErrorMessage = response.error.message;
                    }
                }
            });
        }

//        alert("less");
                    }

    }else{
        $scope.deptBudgetError = true;
        $scope.deptBudgetErrorMessage = 'Please select the plan';
    }
};




/* $http({
     method: 'GET',
     url: 'api/PlanDepartments?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
     headers: {"Content-Type": "application/json", "Accept": "application/json"}
 }).success(function (response) {
     $scope.departmentList = response;
    var allObject={
                    'id':"all",
                    'name':"All"
                }
                $scope.departmentList.unshift(allObject);
 }).error(function (response) {
     console.log('Error Response :' + JSON.stringify(response));
 });
*/
/*
    $scope.projectName22222='';

    $scope.selectDepartment = function(deptId){
        $scope.projectName22222 = deptId;

    };
    $scope.projectULB='';

    $scope.selectULB=function(ulbId){

    $scope.projectULB=ulbId;

    }

    $scope.department={financialYear : '2016-2017'};
    $scope.selectPlanId = function(financialYear){
        $scope.deptBudgetError = false;
        $scope.deptBudgetErrorMessage = '';
        $scope.departmentWiseBudget = '';
        $scope.selectedItems = [];
        $scope.showBudget = false;
        //console.log('Entered'+financialYear);
        //console.log("RRRR:" +$scope.projectName22222);
        var checkEditBudget = financialYear;
        if(checkEditBudget == 'edit'){
            $scope.projectName22222 = $scope.editBudgetInfo.department;
            $scope.projectULB = $scope.editBudgetInfo.ulb;
            financialYear = $scope.editBudgetInfo.financialYear;
        }


console.log("azzuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu "+JSON.stringify($scope.testBudget));
        if($scope.projectName22222 && $scope.projectName22222 != '') {
        if($scope.projectULB && $scope.projectULB != '') {
            if(financialYear && financialYear != '') {

            if($scope.testBudget.length==0 || $scope.testBudget[0].departmentId!=undefined && $scope.testBudget[0].departmentId!=$scope.projectName22222 || $scope.testBudget[0].ulbName!=$scope.projectULB || $scope.testBudget[0].financialYear!=financialYear){
            if($scope.projectName22222=='all'){
                    $http({
                    method: 'GET',
//                    url: 'api/ProjectPlans/getBudgetPlans/?filter={"where":{"ulb": "' + $scope.projectULB + '"}}',
                    url: 'api/ProjectPlans/getBudgetPlans',

                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {

                    $scope.loadingImage=false;

                    $scope.selectProjectDetails = response;
                    if(response.length>0){
                        $scope.showBudget = true;
                    }
                    //$scope.selectProjectDetails;
                }).error(function (response) {
                    //console.log('Error Response :' + JSON.stringify(response));
                });

             }
             else{
             alert("in else")
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans/?filter={"where":{"departmentId": "' + $scope.projectName22222 + '","ulb": "' + $scope.projectULB + '","financialYear":"' + financialYear + '"}}',
//                    url: 'api/ProjectPlans/getBudgetPlans?"ulb":"'+$scope.projectULB+'"',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    //alert("222:"+JSON.stringify(response));
                    $scope.loadingImage=false;
                    //console.log('Field visit report :' + JSON.stringify(response));
                    $scope.selectProjectDetails = response;
                    if(response.length>0){
                        $scope.showBudget = true;
                    }
                    //$scope.selectProjectDetails;
                }).error(function (response) {
                    //console.log('Error Response :' + JSON.stringify(response));
                });
                }
                }else{
                alert("you already created budget for choosen plan")
                }

            }else{
                alert('Please select financial year');
            }
            }else{
            alert('Please Select ULB');
            }
        }else{
            alert('Please select department');
        }

    };

    $http({
        "method": "GET",
        "url": 'api/Employees/',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        //console.log("employeesListName " + JSON.stringify(response));
        $scope.employeesListName = response;
    }).error(function (response, data) {
        console.log("failure");
    })


    $scope.editBudget=function(budjet){
        $scope.budgetDetails33=budjet;
        $scope.updateBudgetDetails.comment = '';
        $scope.updateBudgetDetails.totalBudget = budjet.totalBudget;
        $scope.updateBudgetDetails.acceptLevel = budjet.acceptLevel;
        //console.log("Budget Edit......:"+JSON.stringify(budjet));
        var id=budjet.id;
        $window.localStorage.setItem('budgetId',id);
        // alert("257:"+id);
    };

    $scope.budgetChange=function (approvedStatus) {
        // alert(approvedStatus)
        // alert(approvedStatus=="Approved")
        //console.log("Budget Status change" + JSON.stringify(approvedStatus));
        if(approvedStatus=="Approved")
        {
            $scope.onBudget=true;
        }
        else {
            $scope.onBudget=false;
        }
    };

    var subPlanDetails = [];
    $scope.selectedItems = [];
    $scope.departmentWiseBudget='';
    $scope.checkList=function(id,estimationCost,data){
        $scope.deptBudgetError = false;
        $scope.deptBudgetErrorMessage = '';
        //console.log("jghj :" + id);
        //console.log("estimationCost :" + estimationCost);
        //console.log("planId :" + JSON.stringify(data));

        var pos = $scope.selectedItems.indexOf(id);

        if (pos == -1) {

            subPlanDetails.push(data)
            //alert("JJJJJ:" + JSON.stringify(subPlanDetails));
            //console.log("RRRR:" + JSON.stringify(subPlanDetails))
            $scope.selectedItems.push(id);
            //console.log("RRRR:" + JSON.stringify(subPlanDetails))

        } else {
            $scope.selectedItems.splice(pos, 1);
        }
        //$rootScope.curryDeatiles.push(id);
        //console.log('items'+id);

        //console.log("items into array"+ $scope.selectedItems);

        var budgetAmount=$scope.selectedItems;

        //console.log("selected items are::" +budgetAmount);
        var totalAmount=0;

        for (var i = 0; i < $scope.selectedItems.length; i++) {
            //console.log("selected items are::" +splittedSelectedItems);
            totalAmount=parseInt(totalAmount)+parseInt($scope.selectedItems[i]);
        }
        $scope.departmentWiseBudget=totalAmount;
        //console.log(totalAmount);
    };


    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    //var employeeName=$scope.userInfo.name;

    $scope.getBudgetPlanDetails = function() {

        $http({
            method: 'GET',
            url: 'api/DepartmentWiseBudgets/getDetails?employeeId=' + employeeId,
            /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets',*!/
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
console.log('Budget Response ggggggggggggg :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.budgetDetails1 = response;
        $scope.testBudget = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getBudgetPlanDetails();

    $scope.getULB=function () {
                $http({
                    method: 'GET',
                    url: 'api/ULBs',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                   // alert('Users Response :' + JSON.stringify(response));
                    $scope.ulbList = response;
                    console.log("===================="+JSON.stringify(response))
                    $scope.loadingImage=false;
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
$scope.getULB();
    $scope.department={};
    /!*$scope.reset=function () {
     $scope.found={};
     $scope.selectedItems=[];
     $scope.selectProjectDetails=[];
     }*!/

    $scope.deptBudgetError = false;
    $scope.deptBudgetErrorMessage = '';

    var departmentBudgetDoubleClick=false;

    $scope.budgetDetails = function(){
        $scope.deptBudgetError = false;
        $scope.deptBudgetErrorMessage = '';
        // var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        if($scope.departmentWiseBudget != '' && $scope.departmentWiseBudget > 0) {
            var bgd = {
                "departmentId": $scope.projectName22222,
                "ulbName": $scope.projectULB,
                "financialYear": $scope.department.financialYear,
                "totalBudget": $scope.departmentWiseBudget,
                "selectedItems": $scope.selectedItems,
                "planDetails": subPlanDetails,
                "approvedStatus": "",
                "approvedBudget": "",
                "approvedDate": "",
                "approvedBy": "",
                "planId": "",
                "planName": "",
                "budget": "",
                "status": "",
                "createdPersonId": $scope.userInfo.employeeId
            };
            console.log("123:" + JSON.stringify(bgd));

            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            if(!departmentBudgetDoubleClick){
                departmentBudgetDoubleClick=true;
                $http({
                    method: 'POST',
                    url: 'api/DepartmentWiseBudgets',
                    /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets',*!/
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": bgd
                }).success(function (response) {
                     alert("sdfd "+JSON.stringify(response));
                    $scope.loadingImage=false;
                    location.href = '#/budgetPlans';
                    // $scope.getBudgetDetails();
                    departmentBudgetDoubleClick=false;
                }).error(function (response) {
                    departmentBudgetDoubleClick=false;
                    console.log('Error Response :' + JSON.stringify(response));
                    if(response.error){
                        if(response.error.message){
                            $scope.deptBudgetError = true;
                            $scope.deptBudgetErrorMessage = response.error.message;
                        }
                    }
                });
            }

        }else{
            $scope.deptBudgetError = true;
            $scope.deptBudgetErrorMessage = 'Please select the plan';
        }
    };

    $scope.updateBudgetDetails = {};

    var editBugetDoubleClick=false;
    $scope.editBudgetDetails=function (updateBudgetDetails, status) {
        /// alert("gjh:"+JSON.stringify(updateBudgetDetails));

        var budgetCheck = true;
        $scope.updateBudgetDetails.approvedDate = new Date();
        $scope.updateBudgetDetails.approvedBy = $scope.userInfo.name;
        $scope.updateBudgetDetails.employeeId = $scope.userInfo.id;
        if(status == 'approve'){
            $scope.updateBudgetDetails.approvedStatus = 'Approved';
            if($scope.updateBudgetDetails.approvedBudget > 0){
                budgetCheck = true;
            }else{
                budgetCheck = false;
                alert('Please enter a valid budget');
            }
        }else{
            $scope.updateBudgetDetails.approvedStatus = 'Rejected';
            $scope.updateBudgetDetails.approvedBudget = '';
        }
        var budgetId= $window.localStorage.getItem('budgetId');
        $scope.updateBudgetDetails.budgetId = budgetId;
        //console.log("352..........."+JSON.stringify($scope.updateBudgetDetails));
        //alert("353"+JSON.stringify($scope.updateBudgetDetails));

        if(budgetCheck == true) {
            if(!editBugetDoubleClick){
                editBugetDoubleClick=true;
                $http({
                    "method": "POST",
                    url: 'api/DepartmentWiseBudgets/updateDetails',
                    /!*"url": 'http://localhost:8866/api/DepartmentWiseBudgets/'+budgetId,*!/
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": $scope.updateBudgetDetails
                }).success(function (response, data) {
                    // console.log("filter Schemes..: " + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $("#editBudgetPlans").modal("hide");
                    $("#rejectBudgetPlans").modal("hide");
                    editBugetDoubleClick=false;
                    $scope.getBudgetPlanDetails();
                }).error(function (response, data) {
                    editBugetDoubleClick=false;
                    console.log("failure" + JSON.stringify(response));
                });
            }

        }
    };


    $scope.budgetDetailsList = function(id){
        //alert(id);
        //var budgetId= $window.localStorage.getItem('budgetId');
        $scope.selectProjectDetails = false;
        $scope.showBudget = false;
        $http({
            method: 'GET',
            url: 'api/DepartmentWiseBudgets/' +id,
            /!*url: 'http://localhost:8866/api/DepartmentWiseBudgets/'+id,*!/
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.editBudgetInfo = response;
            $scope.getBudgetDetailsListData= response.planDetails;
            // console.log("402:"+JSON.stringify($scope.getBudgetDetailsListData));
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    };
    var updateDoubleClick=false;

    $scope.updateBudgetInfo = function(){
        $scope.editBudgetInfo.planDetails = subPlanDetails;
        $scope.editBudgetInfo.selectedItems = $scope.selectedItems;
        $scope.editBudgetInfo.totalBudget = $scope.departmentWiseBudget;
        //console.log('Update budget details:'+JSON.stringify($scope.editBudgetInfo));
        if($scope.editBudgetInfo.totalBudget != '' && $scope.editBudgetInfo.totalBudget > 0) {
            if(!updateDoubleClick){
                updateDoubleClick=true;
                $http({
                    "method": "POST",
                    url: 'api/DepartmentWiseBudgets/updateContent',
                    /!*"url": 'http://localhost:8866/api/DepartmentWiseBudgets/'+budgetId,*!/
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": $scope.editBudgetInfo
                }).success(function (response, data) {
                    //console.log("filter Schemes..: " + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $("#myModal").modal("hide");
                    updateDoubleClick=false;
                    $scope.getBudgetPlanDetails();
                }).error(function (response, data) {
                    updateDoubleClick=false;
                    console.log("failure" + JSON.stringify(response));
                });
            }

        }else{
            alert('Please enter valid budget');
        }
    };*/
}]);

app.controller('planLayoutController', ['projectWardWorks','$http', '$scope', 'Upload', '$window', '$location', '$rootScope', 'Excel', '$timeout', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, Upload, $window, $location, $rootScope, Excel, $timeout, $q, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("planLayoutController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.planLayout").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.planLayout .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectLayoutUpload'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        $("<tr>" +
            "<th colspan='7' style='background-color: #ebebeb;'><h3>Plan Layout & Upload Details</h3></th>" +
            "</tr>").appendTo("table" + tableId);

        if( $scope.documentsList!=null &&  $scope.documentsList.length>0){

            for(var i=0;i<$scope.documentsList.length;i++){
                var subTask=$scope.documentsList[i];

                $("<tr>" +
                    "<th>Name</th>" +
                    "<th>City</th>" +
                    "<th>Location</th>" +
                    "<th>Village</th>" +
                    "<th>Survey Area</th>" +
                    "<th>Attached Files</th>" +
                    "</tr>").appendTo("table"+tableId);

                $("<tr>" +
                    "<td>"+subTask.createdPerson+"</td>" +
                    "<td>"+subTask.city+"</td>" +
                    "<td>"+subTask.location+"</td>" +
                    "<td>"+subTask.village+"</td>" +
                    "<td>"+subTask.surveyArea+"</td>" +
                    "<td>"+subTask.file.filename+"</td>" +
                    "</tr>").appendTo("table"+tableId);

                if(subTask.workflow!=null && subTask.workflow.length>0){
                    $("<tr>" +
                        "<th>Workflow Id</th>" +
                        "<th>Workflow Level</th>" +
                        "<th>Level No</th>" +
                        "<th>Status</th>" +
                        "<th>Approval Person</th>" +
                        "<th>Date</th>" +
                        "<th>Comment</th>" +
                        "</tr>").appendTo("table"+tableId);

                    for(var i=0;i<subTask.workflow.length;i++){
                        var task=subTask.workflow[i];

                        $("<tr>" +
                            "<td>"+task.workflowId+"</td>" +
                            "<td>"+task.levelId+"</td>" +
                            "<td>"+task.levelNo+"</td>" +
                            "<td>"+task.approvalStatus+"</td>" +
                            "<td>"+task.employeeName+"</td>" +
                            "<td>"+task.date+"</td>" +
                            "<td>"+task.comment+"</td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                }
            }

            $("<tr>" +
                "<td colspan='7'></td>" +
                "</tr>").appendTo("table"+tableId);

        }


        var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }


    $scope.getCharterDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans/getProjectDetails',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getCharterDetails :' + JSON.stringify(response));
            $rootScope.charterList=response;
            //    //alert('successfully created charter');
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
        ////alert('get charter list');
    }
    $scope.getCharterDetails();

    var formIdsArray=[];
    var formUploadStatus=true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        //var count=1;
        //  filedetails=[];
        formIdsArray=[];
        //$scope.docList = [];
        //$scope.forms = files;

        angular.forEach(files, function(file) {
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;var fsize=0;
                                                         var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                                                            if (file.size == 0){
                                                            fsize='0 Byte';
                                                            }else{
                                                              var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                                                                          fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
                                                            }
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'filename': response.data.filename,
                           'size': fsize
                        };
                        formIdsArray.push(response.data);
                        $scope.docList.push(fileDetails);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
                $scope.fileUpload = true;
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });
    };
    $scope.reset = function() {
        $scope.upload = {};
        $scope.docList = []
    }
    $scope.addUploadFileModal = function() {
        $("#addUploadFile").modal("show");
        $scope.upload = {};
        $scope.fileUpload=false;
        $scope.fileErrorMessage=false;

        $scope.docList = [];
    }
    var planUploadDoubleClick=false;

    $scope.projectUploads= function () {
        if(formUploadStatus){
            if($scope.docList.length > 0){
                var uploadDetails=$scope.upload;
                uploadDetails.file=$scope.docList;
                var planId=$scope.selectProjectId;
                uploadDetails['planId']=planId;
                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                uploadDetails['createdPerson']=loginPersonDetails.name;
                uploadDetails['type']='layout';
                uploadDetails['createdPersonId']=loginPersonDetails.employeeId;
                //  //alert('uplaoded data is'+JSON.stringify($scope.docList));
                uploadDetails.status='active';
                $scope.fileUpload = true;
                if(!planUploadDoubleClick){
                    planUploadDoubleClick=true;
                    $http({
                        method: 'POST',
                        url: 'api/ProjectUploads',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        "data":uploadDetails
                    }).success(function (response) {
                        //console.log('Users Response :' + JSON.stringify(response));
                        $("#scroll2").hide();
                        $("#scroll").show();
                        $("#addUploadFile").modal("hide");
                        $scope.docList = [];
                        planUploadDoubleClick=false;
                        $scope.getDocuments();
                    }).error(function (response) {
                        planUploadDoubleClick=false;
                        //console.log('Error Response :' + JSON.stringify(response));
                    });
                }

            }  else{
                $scope.fileErrorMessage=true;
                $timeout(function() {
                    $scope.fileErrorMessage=false;
                }, 3000);

            }
        }

        else{
            $scope.projectUploads();
        }

    }
    $scope.comments;
    $scope.approvalModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#approavlReport").modal("show");
    }
    $scope.rejectModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#rejectReport").modal("show");
    }

    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;
    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        var updateDetails = {
            "acceptLevel":$scope.fieldData.acceptLevel,
            "acceptStatus": "Yes",
            "requestStatus": "Approval",
            "comment":  $scope.comments.comment,
            "requestId":$scope.fieldData.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            $http({
                "method": "POST",
                "url": 'api/ProjectUploads/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    //getTORReport($scope.torId);
                    approvalDoubleClick=false;
                    $scope.getDocuments();
                    $("#approavlReport").modal("hide");
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    approvalDoubleClick=false;
                    $scope.getDocuments();
                    $("#approavlReport").modal("hide");
                }

            }).error(function (response, data) {
                approvalDoubleClick=false;
//                console.log("failure");
            })
        }
    }

    var rejectedDoubleClick=false;
    $scope.rejectData=function(){

        var updateDetails = {
            "acceptLevel":$scope.fieldData.acceptLevel,
            "acceptStatus": "No",
            "requestStatus": "Rejected",
            "comment":  $scope.comments.comment,
            "requestId":$scope.fieldData.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };
        if(!rejectedDoubleClick){
            rejectedDoubleClick=true;
            $http({
                "method": "POST",
                "url": 'api/ProjectUploads/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                if(response.error!=undefined && response.error!=null && response.error.message!=undefined &&
                    response.error.message!=null && response.error.message!=''){
                    $scope.getDocuments();
                    $("#rejectReport").modal("hide");
                    rejectedDoubleClick=false;
                    $("#errorMessageDisplay").modal("show");
                    setTimeout(function(){$('#errorMessageDisplay').modal('hide')}, 3000);
                }else{
                    $scope.getDocuments();
                    $("#rejectReport").modal("hide");
                    rejectedDoubleClick=false;
                }
            }).error(function (response, data) {
//                console.log("failure");
                rejectedDoubleClick=false;
            })

        }

    }

    $scope.docUploadURL = uploadFileURL;
    $scope.getDocuments=function () {

        $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
        var employeeId=$scope.userInfo.employeeId;
        var employeeName=$scope.userInfo.name;
        $http({
            method: 'GET',
            url: 'api/ProjectUploads/getDetails?employeeId='+employeeId+'&planId='+ $scope.selectProjectId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('ProjectUploads :' + JSON.stringify(response));
            $scope.documentsList=response;
            //   //alert('hai'+response.length);
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;

        $http({
            method: 'GET',
            url: 'api/ProjectPlans/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Field visit report :' + JSON.stringify(response));
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
        $scope.getDocuments();
    }

    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function() {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top},
                'slow');
        });
        $("#cancel").click(function() {
            $("#scroll2").hide()
            $("#scroll").show();;
        });
    })




    /*Edit Functionality For Play Layoud And Upload start*/

    $scope.editProjectUploads = function(document) {
//        console.log("editing the data" +JSON.stringify(document));
        //$("#editProjectDeliverables").modal("show");
        $("#UploadFile").modal("show");
        $scope.editId = angular.copy(document);
    }
    var editUploadDoubleClick=false;
    $scope.deleteFile = function(index, fileId){

        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
        });

    };

    $scope.editProjectUploadsDetails=function () {
        if (formUploadStatus) {
            var editProjectUploadDetails = $scope.editId;
            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            editProjectUploadDetails['lastEditPerson']=loginPersonDetails.name;
            editProjectUploadDetails['lastEditPersonId']=loginPersonDetails.employeeId;
            $scope.fileUpload = true;
            if(!editUploadDoubleClick){
                editUploadDoubleClick=true;
                $http({
                    method: 'POST',
                    url: 'api/ProjectUploads/updateContent'  ,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": editProjectUploadDetails
                }).success(function (response) {
                    $scope.getDocuments();
                    $("#UploadFile").modal("hide");
                    $("#editPlanLayoutSuccess").modal("show");
                    editUploadDoubleClick=false;
                    setTimeout(function(){$('#editPlanLayoutSuccess').modal('hide')}, 3000);
                }).error(function (response) {
                    editUploadDoubleClick=false;
//                    console.log('Error Response :' + JSON.stringify(response));
                });
            }

        }
        else {
            $scope.editProjectUploadsDetails();
        }
    }

    $scope.getUploadDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectUploads',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.documentsList=response;

        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    $scope.getCharterDetails();

    $scope.deleteFile = function(fileId, index){
            $scope.docList.splice(index, 1);
           /* $http({
                method: 'DELETE',
                url: 'api/Uploads/dhanbadDb/files/'+fileId,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.docList.splice(index, 1);
            });*/
        };

}]);

app.controller('projectDelivarablesController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("projectDelivarablesController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #projectMgmtPro.active #proMgmtSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectDeliverable").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #projectMgmtPro.active #proMgmtSetup li.projectDeliverable .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectDeliverable'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){

        if( $scope.selectProjectDetails!=null) {
            var schemes = $scope.selectProjectDetails;
            $("<tr>" +
                "<th colspan='2' style='background-color: #ebebeb;'><h3>Plan Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Id</th>" +
                "<td>" + schemes.planId + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Name</th>" +
                "<td>" + schemes.name + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Financial Year</th>" +
                "<td>" + schemes.financialYear + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Start Date</th>" +
                "<td>" + schemes.startDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>End Date</th>" +
                "<td>" + schemes.endDate + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Estimation Cost</th>" +
                "<td>" + schemes.estimationCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Utilized Cost</th>" +
                "<td>" + schemes.utilizedCost + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Task Status</th>" +
                "<td>" + schemes.taskStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Status</th>" +
                "<td>" + schemes.palnStatus + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>% Complete</th>" +
                "<td>" + schemes.completePercentage + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Department</th>" +
                "<td>" + schemes.department + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Plan Description</th>" +
                "<td>" + schemes.description + "</td>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "</tr>").appendTo("table" + tableId);

        }

        if( $scope.projectDeliverablesList!=null &&  $scope.projectDeliverablesList.length>0){
            $("<tr>" +
                "<th colspan='5' style='background-color: #ebebeb;'><h3>Project Deliverable Details</h3></th>" +
                "</tr>").appendTo("table" + tableId);
            $("<tr>" +
                "<th>Deliverable Name</th>" +
                "<th>StakeHolder</th>" +
                "<th>Description</th>" +
                "<th>Acceptance Criteria</th>" +
                "<th>Due Date</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.projectDeliverablesList.length;i++){
                var schemes=$scope.projectDeliverablesList[i];

                $("<tr>" +
                    "<td>"+schemes.deliverableName+"</td>" +
                    "<td>"+schemes.stakeholder+"</td>" +
                    "<td>"+schemes.description+"</td>" +
                    "<td>"+schemes.acceptanceCriteria+"</td>" +
                    "<td>"+schemes.dueDate+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }

        }

        var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
        $timeout(function () {
            location.href = exportHref;
        }, 100);
    }

    $scope.getCharterDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectPlans/getProjectDetails',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getCharterDetails :' + JSON.stringify(response));
            $rootScope.charterList=response;
            $scope.loadingImage=false;
            //    //alert('successfully created charter');
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
        ////alert('get charter list');
    }

    $scope.loadingImage=true;
    $scope.getCharterDetails();

    $scope.selectareas = function(projectId){
        $scope.selectProjectId = projectId;

        $http({
            method: 'GET',
            url: 'api/ProjectPlans/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Field visit report :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectProjectDetails=response[0];
            $scope.showAction = true;
            $scope.getProjectDeliverablesDetails();
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    var createDeliverableDoubleClick=false;

    $scope.openProDeliverable = function() {
        $("#createProjectDeliverables").modal("show");
        $scope.projectDeliverables={};
    }

    $scope.createProjectDeliverables=function () {
        if($scope.projectDeliverables.dueDate!=undefined && $scope.projectDeliverables!=null  && $scope.projectDeliverables!=''){
            if(!createDeliverableDoubleClick){
                createDeliverableDoubleClick=true;
                var projectDeliverablesData=$scope.projectDeliverables;
                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                projectDeliverablesData['createdPerson']=loginPersonDetails.name;
                var planId=$scope.selectProjectId;
                projectDeliverablesData['planId']=planId;
                $http({
                    method: 'POST',
                    url: 'api/ProjectDeliverables',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":projectDeliverablesData
                }).success(function (response) {
                    //console.log('Users Response :' + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $scope.getProjectDeliverablesDetails();
                    $("#createProjectDeliverables").modal("hide");
                    $("#editProjectDeliverableSuccess").modal("show");
                    createDeliverableDoubleClick=false;
                    setTimeout(function(){$('#editProjectDeliverableSuccess').modal('hide')}, 3000);

                }).error(function (response) {
                    createDeliverableDoubleClick=false;
//                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
        }else{
            alert('please select date');
        }

    }

    $scope.getProjectDeliverablesDetails=function () {

        $http({
            method: 'GET',
            url: 'api/ProjectDeliverables/?filter={"where":{"planId":"' + $scope.selectProjectId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getProjectDeliverablesDetails :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.projectDeliverablesList=response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    $scope.editProjectDeliverables = function(projectDeliverables) {
        //console.log('editProjectDeliverables :' + JSON.stringify(projectDeliverables));
        $("#editProjectDeliverables").modal("show");
        $("#viewProjectDeliverables").modal("hide");
        $scope.editDeliverbaleId = angular.copy(projectDeliverables);
    }

    $scope.viewProjectDeliverables = function(projectDeliverables) {
        //console.log('viewProjectDeliverables :' + JSON.stringify(projectDeliverables));
        $("#viewProjectDeliverables").modal("show");
        $scope.viewDeliverbaleId = angular.copy(projectDeliverables);
    }

    var editDeliverableDoubleClick=false;

    $scope.editProjectDeliverablesDetails=function () {
        var editProjectDeliverablesDetails= $scope.editDeliverbaleId;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        if(!editDeliverableDoubleClick){
            editDeliverableDoubleClick=true;
            $http({
                method: 'PUT',
                url: 'api/ProjectDeliverables/'+$scope.editDeliverbaleId.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":editProjectDeliverablesDetails
            }).success(function (response) {
                $scope.getDeliverableDetails();
                $scope.loadingImage=false;
                editDeliverableDoubleClick=false;
                $("#editProjectDeliverables").modal("hide");
                $("#editedProjectDeliverableSuccess").modal("show");
                setTimeout(function(){$('#editedProjectDeliverableSuccess').modal('hide')}, 3000);
            }).error(function (response) {
                editDeliverableDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }

    }

    $scope.getDeliverableDetails=function () {
        $http({
            method: 'GET',
            url: 'api/ProjectDeliverables',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.projectDeliverablesList=response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });

    }


}]);

app.controller('documentTypeController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("documentTypeController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro.active #masterDataProSetUp").addClass("menu-open");
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.documentType").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.documentType .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'documentType'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tableId){
        if( $scope.taskStatustList!=null &&  $scope.taskStatustList.length>0){

            $("<tr>" +
                "<th>Document Type</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.taskStatustList.length;i++){
                var taskStatus=$scope.taskStatustList[i];

                $("<tr>" +
                    "<td>"+taskStatus.name+"</td>" +
                    "<td>"+taskStatus.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Document Type Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    $scope.loadingImage=true;
    $scope.edittaskSatus = function(taskStatus) {
        $("#editTaskStatusModel").modal("show");
        $scope.editCharterArea = true;
        $scope.createCharterArea = false;
        $scope.errorMessage=false;
        $scope.editTaskSatus=angular.copy(taskStatus);
    }

    $scope.getDocumentTypeStatus=function () {
        $http({
            method: 'GET',
            url: 'api/DocumentTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.taskStatustList = response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.errorMessage=false;
    $scope.addDocType = function() {
        $("#addDocType").modal("show");
        $scope.taskStatus = {};
    }
    var addDocumentDoubleClick=false;

    $scope.addDocumentTypeStatus=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var taskStatus=$scope.taskStatus;
        taskStatus['createdPerson']=loginPersonDetails.name;
        if(!addDocumentDoubleClick){
            addDocumentDoubleClick=true;
            $http({
                method: 'POST',
                url: 'api/DocumentTypes',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":taskStatus
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $("#addDocType").modal("hide");
                $("#addDocTypeSuccess").modal("show");
                addDocumentDoubleClick=false;
                setTimeout(function(){$('#addDocTypeSuccess').modal('hide')}, 3000);
                $scope.errorMessage=false;
                $scope.getDocumentTypeStatus();
            }).error(function (response) {
                addDocumentDoubleClick=false;
//                console.log('Error Response :' + JSON.stringify(response));
                if(response.error.details.messages.name) {
                    $scope.errorMessageData=response.error.details.messages.name[0];
                    $scope.errorMessage=true;
                }
            });
        }

    }
    var editDocumentDoubleClick=false;
    $scope.editDocumentTypeButton=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        $scope.editTaskSatus['lastEditPerson']=loginPersonDetails.name;
        if(!editDocumentDoubleClick){
            editDocumentDoubleClick=true;
            $http({
                "method": "PUT",
                "url": 'api/DocumentTypes/'+   $scope.editTaskSatus.id,
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data":  $scope.editTaskSatus
            }).success(function (response, data) {
                //console.log("filter Schemes "+ JSON.stringify(response));
                $scope.loadingImage=false;
                $("#editTaskStatusModel").modal("hide");
                $("#editDocTypeSuccess").modal("show");
                setTimeout(function(){$('#editDocTypeSuccess').modal('hide')}, 3000);
                $scope.getDocumentTypeStatus();
                $scope.errorMessage=false;
                editDocumentDoubleClick=false;
            }).error(function (response, data) {
//                console.log("failure");
                editDocumentDoubleClick=false;
                if(response.error.details.messages.name) {
                    $scope.errorMessageData=response.error.details.messages.name[0];
                    $scope.errorMessage=true;
                }
            })
        }


    }

    $scope.getDocumentTypeStatus();

}]);

app.controller('nocRequestsController', ['projectWardWorks','$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("nocRequestsController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #requestsPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #requestsPro.active #appRequestSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.nocRequests").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.nocRequests .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'nocRequests'}, function (response) {
//	alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;
    $scope.viewRequestDivBack=true;
    $scope.viewRequestDiv=true;
    $scope.getRequestDetails=function(){
        $http({
            "method": "GET",
            //"url": 'api/NocRequests/getDetails?employeeId=%7B%22employeeId%22%3A%22'+employeeId+'%22%7D',
            "url": 'http://54.189.195.233:3000/api/NocRequests?filter={"where":{"status": {"neq":"Completed"}}}',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            console.log("success", JSON.stringify(response));
            $scope.requestLists = response;
            $scope.loadingImage=false;
        }).error(function (response, data) {
//            console.log("failure");
        })
    }

    $scope.getRequestDetails();

         $scope.editAssetType = function(assetType) {
                  $("#editAssetTypeModel").modal("show");
                  $scope.errorMessage=false;
                  /*$scope.editCharterArea = true;
                   $scope.createCharterArea = false;*/
                  $scope.editAssetTypeData=angular.copy(assetType);
         }

         var editRiskGroupFormSubmit = true;
               $scope.editAssetRiskGroupButton=function () {
                   var editAssetTypeData= $scope.editAssetTypeData;
                   var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                   editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
                   editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
                   if(editRiskGroupFormSubmit){
                       editRiskGroupFormSubmit = false;
                       $http({
                           "method": "PUT",
                           "url": 'http://54.189.195.233:3000/api/NocRequests/'+$scope.editAssetTypeData.id+'',
                           "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                           "data": editAssetTypeData
                       }).success(function (response, data) {
                           //console.log("filter Schemes "+ JSON.stringify(response));
                           //$scope.loadingImage=true;
                           $("#editAssetTypeModel").modal("hide");
                           $("#editAssetSuccess").modal("show");
                           setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                           $scope.getRequestDetails();
                           //$window.location.reload();
                       }).error(function (response, data) {

                       });
                   }
                   editRiskGroupFormSubmit = true;
               }

/*    $scope.requestDetails=function(request){
        $scope.viewRequestDivBack=false;
        $scope.viewRequestDiv=false;
        $scope.requestDiv=true;
        $scope.requestDivBack=true;
    $scope.request=request;
    }
    $scope.acceptStatusModal=function(){
        $("#acceptStatus").modal("hide");
        $scope.getRequestDetails();
            $scope.loadingImage=false;
        $scope.viewRequestDivBack=true;
        $scope.viewRequestDiv=true;
        $scope.requestDiv=false;
        $scope.requestDivBack=false;
    }
    $scope.rejectStatusModal=function(){
        $("#rejectStatus").modal("hide");
        $scope.viewRequestDivBack=true;
        $scope.viewRequestDiv=true;
        $scope.requestDiv=false;
        $scope.requestDivBack=false;
    }
    $scope.clickToBack=function(){
        $scope.viewRequestDivBack=true;
        $scope.viewRequestDiv=true;
        $scope.requestDiv=false;
        $scope.requestDivBack=false;
    }
    $scope.acceptStatus= function (request) {
        $scope.request=request;

    }

        $scope.acceptStatus= function (request) {
            var updateDetails = {
                'email':request.email,
                "acceptLevel":request.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment": request.comment,
                "requestId":request.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };

            $http({
                "method": "POST",
                "url": 'api/NocRequests/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
//                console.log("filter Schemes "+ JSON.stringify(response));
                $scope.requestLists = JSON.stringify(response);
                $window.localStorage.setItem('requestFlag',true);
                $("#acceptStatus").modal("show");
                *//*setTimeout(function(){$('#acceptStatus').modal('hide')}, 3000);*//*
                *//*$window.location.reload();*//*
                //$window.localStorage.getItem('requestTab');

            }).error(function (response, data) {
//                console.log("failure");
            })
        }

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }*/

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

            $("<tr>" +
                "<th>Person Name</th>" +
                "<th>Email Id</th>" +
                "<th>Owner Name</th>" +
                "<th>Square Feet</th>" +
                "<th>Yards</th>" +
                "<th>Location</th>" +

                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.requestLists.length;i++){
                var request=$scope.requestLists[i];

                $("<tr>" +
                    "<td>"+request.personName+"</td>" +
                    "<td>"+request.email+"</td>" +
                    "<td>"+request.owner+"</td>" +
                    "<td>"+request.feet+"</td>" +
                    "<td>"+request.yard+"</td>" +
                    "<td>"+request.location+"</td>" +

                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Lists of Nocs ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);

app.controller('nocFiledVisitController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', function(projectWardWorks,$http, $scope, $window, $location, $rootScope, $routeParams) {
//    console.log("nocFiledVisitController");

$(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #requestsPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #requestsPro.active #appRequestSetup").addClass("menu-open");
        $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.nocFieldRequests").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.nocFieldRequests .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'nocRequests'}, function (response) {
//    	alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;
    $scope.getRequestDetails=function(){

        $http({
            "method": "GET",
            "url": 'api/NocRequests/getDetails?employeeId=%7B%22employeeId%22%3A%22'+employeeId+'%22%7D',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            $scope.requestLists = response
            $scope.loadingImage=false;
        }).error(function (response, data) {
//            console.log("failure");
        })
    }

    $scope.getRequestDetails();

    $scope.selectAppId= function (requestId) {
            $scope.nocId=requestId;
        $http({
            "method": "GET",
            "url": 'api/NocRequests?filter=%7B%22where%22%3A%7B%22applicationId%22%3A%20%22'+$scope.nocId+'%22%7D%7D',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            $scope.selectRequestDetails = response[0]
            $scope.showAction=true;
            $scope.getFieldVisitReport();
            $scope.loadingImage=false;
        }).error(function (response, data) {
//            console.log("failure");
        })
    }


    $scope.editFieldVisitors = function(fieldVisit) {
        $("#fieldVisitReportEdit").modal("show");
        $scope.editFieldVisit = angular.copy(fieldVisit);
        //console.log('editFieldVisitReport :' + JSON.stringify($scope.editFieldVisit));
    }

    $scope.reset = function() {
        $scope.fieldVisitData = {};
    }

    var filedVisitEditDoubleClick=false;

    $scope.fieldVisitReportEdit=function () {
        if(!filedVisitEditDoubleClick){
            filedVisitEditDoubleClick=true;
            var editCharterDetails=$scope.editFieldVisit;
            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            editCharterDetails['lastEditPerson']=loginPersonDetails.name;
            editCharterDetails['lastEditPersonId']=loginPersonDetails.employeeId;
//            console.log("editCharterDetails"+editCharterDetails);
            $http({
                method: 'POST',
                url: 'api/NocFieldVisits/updateContent',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data:editCharterDetails
            }).success(function (response) {
                $scope.getFieldVisitReport();
                $("#fieldVisitReportEdit").modal("hide");
                filedVisitEditDoubleClick=false;
                //console.log('getFieldVisitReport :' + JSON.stringify(response));
                //   $scope.filedVisitList=response;
                // $scope.selectProjectDetails = true;
            }).error(function (response) {
                filedVisitEditDoubleClick=false;
                console.log('Error Response :' + JSON.stringify(response));
            });
        }


    }


    var addFiledVisitDoubleClick=false;
    $scope.addFieldVisitReport=function () {
        if($scope.fieldVisitData.visitedDate){
            if(!addFiledVisitDoubleClick){
                addFiledVisitDoubleClick=true;
                var visiterDetails=$scope.fieldVisitData;
                visiterDetails['nocId']=$scope.nocId;
                visiterDetails['createdPersonId']=employeeId;
                visiterDetails['createdPersonName']=employeeName;
                $http({
                    method: 'POST',
                    url: 'api/NocFieldVisits',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data:visiterDetails
                }).success(function (response) {
                    $scope.getFieldVisitReport();
                    $("#createFieldVisitReport").modal("hide");
                    $("#editFieldVisitReportSuccess").modal("show");
                    addFiledVisitDoubleClick=false;
                    setTimeout(function(){$('#editFieldVisitReportSuccess').modal('hide')}, 3000);

                }).error(function (response) {
                    addFiledVisitDoubleClick=false;
//                    console.log('Error Response :' + JSON.stringify(response));
                });

            }

        } else{
            alert("Please Select Visit Date");
        }

    }
    $scope.comments;
    $scope.approvalModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#approavlReport").modal("show");
    }
    $scope.rejectModal=function(fieldViisitData){
        $scope.fieldData=angular.copy(fieldViisitData);
        $("#rejectReport").modal("show");
    }
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;

    var approvalDoubleClick=false;
    $scope.approvalData=function(){
        if(!approvalDoubleClick){
            approvalDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "Yes",
                "requestStatus": "Approval",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/NocFieldVisits/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {

                $scope.getFieldVisitReport();
                approvalDoubleClick=false;
                $("#approavlReport").modal("hide");


            }).error(function (response, data) {
//                console.log("failure");
                approvalDoubleClick=false;
            })

        }



    }
    var rejectDoubleClick=false;
    $scope.rejectData=function(){
        if(!rejectDoubleClick){
            rejectDoubleClick=true;
            var updateDetails = {
                "acceptLevel":$scope.fieldData.acceptLevel,
                "acceptStatus": "No",
                "requestStatus": "Rejected",
                "comment":  $scope.comments.comment,
                "requestId":$scope.fieldData.id,
                "employeeId":employeeId,
                "employeeName":employeeName
            };
            $http({
                "method": "POST",
                "url": 'api/NocFieldVisits/updateDetails',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": updateDetails
            }).success(function (response, data) {
                $scope.getFieldVisitReport();
                $("#rejectReport").modal("hide");
                rejectDoubleClick=false;

            }).error(function (response, data) {
//                console.log("failure");
                rejectDoubleClick=false;
            })
        }


    }


    $scope.getFieldVisitReport=function () {
        $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
        var employeeId=$scope.userInfo.employeeId;
        var employeeName=$scope.userInfo.name;
        $http({
            method: 'GET',
            url: 'api/NocFieldVisits/getDetails?employeeId='+employeeId+'&nocId='+ $scope.nocId ,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getFieldVisitReport :' + JSON.stringify(response));
            // alert(JSON.stringify(response));
            $scope.filedVisitList=response;
            //  console.log('filed visit data is '+JSON.stringify($scope.filedVisitList));

            // $scope.selectProjectDetails = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }



}]);

app.controller('nocReportController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, $routeParams) {
//    console.log("nocReportController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #proWorksLi").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #requestsPro").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #requestsPro.active #appRequestSetup").addClass("menu-open");
            $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.nocReports").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #proWorks #requestsPro.active #appRequestSetup li.nocReports .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'nocRequests'}, function (response) {
//        	alert(JSON.stringify(response));
               if(!response){
                window.location.href = "#/noAccessPage";
               }
            });

    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;


    $scope.getRequestDetails=function(){

        $http({
            "method": "GET",
            //"url": 'api/NocRequests?filter=%7B%22where%22%3A%7B%22finalStatus%22%3A%20%7B%22neq%22%3Afalse%7D%7D%7D',
             "url": 'http://54.189.195.233:3000/api/NocRequests?filter={"where":{"status": "Completed"}}',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            $scope.requestLists = response
            $scope.loadingImage=false;
        }).error(function (response, data) {
//            console.log("failure");
        })
    }

    $scope.getRequestDetails();

/*    $scope.selectAppId= function (requestId) {
            $scope.nocId=requestId;
        $http({
            "method": "GET",
            "url": 'api/NocRequests?filter=%7B%22where%22%3A%7B%22applicationId%22%3A%20%22'+$scope.nocId+'%22%7D%7D',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            $scope.selectRequestDetails = response[0]
            $scope.showAction=true;
            $scope.getFieldVisitReport();
            $scope.loadingImage=false;
        }).error(function (response, data) {
//            console.log("failure");
        })
    }


    $scope.editFieldVisitors = function(fieldVisit) {
        $("#fieldVisitReportEdit").modal("show");
        $scope.editFieldVisit = angular.copy(fieldVisit);
        //console.log('editFieldVisitReport :' + JSON.stringify($scope.editFieldVisit));
    }

    $scope.reset = function() {
        $scope.fieldVisitData = {};
    }*/



   /* $scope.getFieldVisitReport=function () {
        $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
        var employeeId=$scope.userInfo.employeeId;
        var employeeName=$scope.userInfo.name;
        $http({
            method: 'GET',
            url: 'api/NocFieldVisits?filter=%7B%22where%22%3A%7B%22finalStatus%22%3A%20%7B%22neq%22%3Afalse%7D%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getFieldVisitReport :' + JSON.stringify(response));
            // alert(JSON.stringify(response));
            $scope.filedVisitList=response;
            //  console.log('filed visit data is '+JSON.stringify($scope.filedVisitList));

            // $scope.selectProjectDetails = true;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }*/



    $scope.viewRequestDivBack=true;
    $scope.viewRequestDiv=true;
/*    $scope.getRequestDetails=function(){
        $http({
            "method": "GET",
            "url": 'api/NocRequests/getDetails?employeeId=%7B%22employeeId%22%3A%22'+employeeId+'%22%7D',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            $scope.requestLists = response
            $scope.loadingImage=false;
        }).error(function (response, data) {
            console.log("failure");
        })
    }

    $scope.getRequestDetails();*/

/*    $scope.requestDetails=function(request){
        $scope.viewRequestDivBack=false;
        $scope.viewRequestDiv=false;
        $scope.requestDiv=true;
        $scope.requestDivBack=true;
        $scope.nocId=request.nocId
        $scope.request=request;
    }
    $scope.acceptStatusModal=function(){
        $scope.viewRequestDivBack=true;
        $scope.viewRequestDiv=true;
        $scope.requestDiv=false;
        $scope.requestDivBack=false;
    }
    $scope.rejectStatusModal=function(){
        $scope.viewRequestDivBack=true;
        $scope.viewRequestDiv=true;
        $scope.requestDiv=false;
        $scope.requestDivBack=false;
    }
    $scope.clickToBack=function(){
        $scope.viewRequestDivBack=true;
        $scope.viewRequestDiv=true;
        $scope.requestDiv=false;
        $scope.requestDivBack=false;
    }
    $scope.acceptStatus= function (request) {
        $scope.request=request;

    }

    $scope.acceptStatus= function (request) {
        var updateDetails = {
            'email':request.email,
            "acceptLevel":request.acceptLevel,
            "acceptStatus": "Yes",
            "requestStatus": "Approval",
            "comment": request.comment,
            "requestId":request.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };

        $http({
            "method": "POST",
            "url": 'api/NocRequests/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
//            console.log("filter Schemes "+ JSON.stringify(response));
            $scope.requestLists = JSON.stringify(response);
            $window.localStorage.setItem('requestFlag',true);
            $("#acceptStatus").modal("show");
            *//*setTimeout(function(){$('#acceptStatus').modal('hide')}, 3000);*//*
            *//*$window.location.reload();*//*
            //$window.localStorage.getItem('requestTab');

        }).error(function (response, data) {
//            console.log("failure");
        })
    }

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }*/

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

            $("<tr>" +
                "<th>Person Name</th>" +
                "<th>Email Id</th>" +
                "<th>Owner Name</th>" +
                "<th>Square Feet</th>" +
                "<th>Yards</th>" +
                "<th>Location</th>" +

                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.requestLists.length;i++){
                var request=$scope.requestLists[i];

                $("<tr>" +
                    "<td>"+request.personName+"</td>" +
                    "<td>"+request.email+"</td>" +
                    "<td>"+request.owner+"</td>" +
                    "<td>"+request.feet+"</td>" +
                    "<td>"+request.yard+"</td>" +
                    "<td>"+request.location+"</td>" +

                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Lists of Nocs ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }



}]);

app.controller('planDepartmentController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("planDepartmentController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #proWorksLi").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #masterDataPro").addClass("active");
            $(".sidebar-menu #proWorksLi.treeview #masterDataPro.active #masterDataProSetUp").addClass("menu-open");
            $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.planDepartment").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.planDepartment .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    	projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'planDepartment'}, function (response) {
    	//alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    /*$scope.dtColumns = [
        DTColumnBuilder.newColumn('name').withTitle('Name'),
        DTColumnBuilder.newColumn('status').withTitle('Status')
    ];*/

    $scope.editDepartment = function(department) {
        $("#editDepartment").modal("show");
        /*$scope.editCharterArea = true;
         $scope.createCharterArea = false;*/
        $scope.editDepartmentData=angular.copy(department);
    }

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.addDepartmentModal = function() {
        $("#addDepartment").modal("show");
        $scope.department = {};
        $scope.errorMessageData = '';
        $scope.errorMessage=false;
    }
    $scope.addDepartment=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var department=$scope.department;
        department['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":department
        }).success(function (response) {
//            console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $("#addDepartment").modal("hide");
            $("#addDeptSuccess").modal("show");
            setTimeout(function(){$('#addDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
            }
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);

        });
    }

    $scope.editDepartmentButton=function () {
        var editCharterDetails= $scope.editDepartmentData;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/PlanDepartments/'+$scope.editDepartmentData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editCharterDetails
        }).success(function (response, data) {
            //console.log("filter Schemes "+ JSON.stringify(response));
            $scope.loadingImage=false;
            $("#editDepartment").modal("hide");
            $("#editDeptSuccess").modal("show");
            setTimeout(function(){$('#editDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response, data) {
            if(response.error.details.messages.name) {
                $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                $scope.errorUpdateMessage=true;
            }
            $timeout(function(){
                $scope.errorUpdateMessage=false;
            }, 3000);
//            console.log("failure");
        })
    }

    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('budgetUtilizationController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("budgetUtilizationController");

   $(document).ready(function () {
       $('html,body').scrollTop(0);
       $(".sidebar-menu li ul > li").removeClass('active1');
       $('[data-toggle="tooltip"]').tooltip();
       $(".sidebar-menu #proWorksLi").addClass("active");
       $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
       $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
       $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.budgetUtilMIS").addClass("active1").siblings('.active1').removeClass('active1');
       $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.budgetUtilMIS .collapse").addClass("in");
       $(".sidebar-menu li .collapse").removeClass("in");
   });

   var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
//    alert(JSON.stringify(response));
          if(!response){
           window.location.href = "#/noAccessPage";
          }
       });

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);


        $scope.selectDepartment= function (departmentId) {
            $scope.departmentId=departmentId;
        }

    $scope.searchDetails= function () {
        if($scope.departmentId!=undefined && $scope.departmentId!=null && $scope.departmentId!='' ){
            $http({
                method: 'GET',
                url: 'api/ProjectTasks/getBudgetUtilization?deptId='+$scope.departmentId,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                $scope.taskDetails = response;
            }).error(function (response) {
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }else{
            alert('please select dept  ')
        }
    }
/*
    $scope.searchPlans= function () {
        if($scope.departmentId!=undefined && $scope.departmentId!=null && $scope.departmentId!=''){
            if($scope.departmentId=='all'){
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans?filter=%7B%22where%22%3A%7B%22finalStatus%22%3A%20%22Approval%22%7D%7D',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.projectList = response;
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
            else{
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans?filter=%7B%22where%22%3A%7B%22and%22%3A%5B%7B%22finalStatus%22%3A%20%22Approval%22%7D%2C%7B%22departmentId%22%3A%20%22'+$scope.departmentId+'%22%7D%5D%7D%7D',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.projectList = response;
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
        }else{
            alert('please select plan Id');
        }
    }*/
    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
            var allObject={
                'id':"all",
                'name':"All"
            }
            $scope.departmentList.unshift(allObject);
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }


    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('deptOverspendedController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("deptOverspendedController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.deptMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.deptMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
           projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'deptOverspent'}, function (response) {
//           alert(JSON.stringify(response));
                 if(!response){
                  window.location.href = "#/noAccessPage";
                 }
              });

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);


        $scope.selectDepartment= function (departmentId) {
            $scope.departmentId=departmentId;
            $scope.searchPlans();
        }
        $scope.selectPlanId= function (planId) {
            $scope.selectedPlanId=planId;

        }
    $scope.searchDetails= function () {
        if($scope.departmentId!=undefined && $scope.departmentId!=null && $scope.departmentId!='' &&
            $scope.selectedPlanId!=undefined && $scope.selectedPlanId!=null && $scope.selectedPlanId!=''){
            $http({
                method: 'GET',
                url: 'api/ProjectTasks/getTaskDetails?planId='+$scope.selectedPlanId,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                $scope.taskDetails = response;
//             console.log(JSON.stringify($scope.taskDetails));
            }).error(function (response) {
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }else{
            alert('please select dept and planId ')
        }
    }

    $scope.searchPlans= function () {
        if($scope.departmentId!=undefined && $scope.departmentId!=null && $scope.departmentId!=''){
            if($scope.departmentId=='all'){
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans?filter=%7B%22where%22%3A%7B%22finalStatus%22%3A%20%22Approval%22%7D%7D',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.projectList = response;
                }).error(function (response) {
//                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
            else{
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans?filter=%7B%22where%22%3A%7B%22and%22%3A%5B%7B%22finalStatus%22%3A%20%22Approval%22%7D%2C%7B%22departmentId%22%3A%20%22'+$scope.departmentId+'%22%7D%5D%7D%7D',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.projectList = response;
                }).error(function (response) {
//                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
        }else{
            alert('please select plan Id');
        }
    }
    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
            var allObject={
                'id':"all",
                'name':"All"
            }
            $scope.departmentList.unshift(allObject);
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }


    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('projectPerformanceController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("projectPerformanceController");

        $(document).ready(function () {
               $('html,body').scrollTop(0);
               $(".sidebar-menu li ul > li").removeClass('active1');
               $('[data-toggle="tooltip"]').tooltip();
               $(".sidebar-menu #proWorksLi").addClass("active");
               $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
               $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
               $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.proPerMIS").addClass("active1").siblings('.active1').removeClass('active1');
               $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.proPerMIS .collapse").addClass("in");
               $(".sidebar-menu li .collapse").removeClass("in");
           });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
           projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectPerformance'}, function (response) {
//           alert(JSON.stringify(response));
                 if(!response){
                  window.location.href = "#/noAccessPage";
                 }
              });



    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);


        $scope.searchPlans= function () {
            if($scope.departmentId!=undefined && $scope.departmentId!=null && $scope.departmentId!=''){
                    $http({
                        method: 'GET',
                        url: 'api/ProjectTasks/getPerformance?deptId='+$scope.departmentId,
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                    }).success(function (response) {
                        $scope.loadingImage=false;
                        $scope.projectList = response;
                    }).error(function (response) {
//                        console.log('Error Response :' + JSON.stringify(response));
                    });
            }else{
                alert('please select plan Id');
            }
        }
        $scope.selectDepartment= function (departmentId) {
            $scope.departmentId=departmentId;
        }
    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
            var allObject={
                'id':"all",
                'name':"All"
            }
            $scope.departmentList.unshift(allObject);
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }


    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('weeklyProjectPerfoController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("weeklyProjectPerfoController");

    $(document).ready(function () {
                   $('html,body').scrollTop(0);
                   $(".sidebar-menu li ul > li").removeClass('active1');
                   $('[data-toggle="tooltip"]').tooltip();
                   $(".sidebar-menu #proWorksLi").addClass("active");
                   $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
                   $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
                   $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.weeklyProPerMIS").addClass("active1").siblings('.active1').removeClass('active1');
                   $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.weeklyProPerMIS .collapse").addClass("in");
                   $(".sidebar-menu li .collapse").removeClass("in");
               });

               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                          projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'weeklyReport'}, function (response) {
               //           alert(JSON.stringify(response));
                                if(!response){
                                 window.location.href = "#/noAccessPage";
                                }
                             });

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);


        $scope.selectDepartment= function (departmentId) {
            $scope.departmentId=departmentId;
        }
        /*$scope.selectedFinancialyear=function(financialYear){
            $scope.financialYear=financialYear;
        }*/

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
            var allObject={
                'id':"all",
                'name':"All"
            }
            $scope.departmentList.unshift(allObject);
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }


    $scope.getDepartments();


    $scope.searchDetails= function () {
        if($scope.departmentId!=undefined && $scope.departmentId!=null && $scope.departmentId!=''){
            $http({
                method: 'GET',
                url: 'api/ProjectTasks/getWeekDetails?deptId='+$scope.departmentId,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                $scope.taskDetails = response;

            }).error(function (response) {
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }else{
//            alert('please select dept');
        }
    }
    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('weeklyProjectDetailsController', ['projectWardWorks', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $location, $rootScope, $routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
//    console.log("weeklyProjectDetailsController");

   $(document).ready(function () {
      $('html,body').scrollTop(0);
      $(".sidebar-menu li ul > li").removeClass('active1');
      $('[data-toggle="tooltip"]').tooltip();
      $(".sidebar-menu #proWorksLi").addClass("active");
      $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
      $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
      $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.weeklyProPerMIS").addClass("active1").siblings('.active1').removeClass('active1');
      $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.weeklyProPerMIS .collapse").addClass("in");
      $(".sidebar-menu li .collapse").removeClass("in");
  });

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);


      var selectedPlanId=$routeParams.planId;

    $scope.searchDetails= function () {
        if(selectedPlanId!=undefined && selectedPlanId!=null && selectedPlanId!=''){
            $http({
                method: 'GET',
                url: 'api/ProjectTasks/getWeekDetailReport?deptId='+selectedPlanId,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                $scope.taskDetails = response;
            }).error(function (response) {
//                console.log('Error Response :' + JSON.stringify(response));
            });
        }else{
          alert('please select plan');
        }
    }
    $scope.searchDetails();
    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
        }).error(function (response) {
//            console.log('Error Response :' + JSON.stringify(response));
        });
    }


    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

app.controller('ulbController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("ulbController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #proWorksLi").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro").addClass("active");
        $(".sidebar-menu #proWorksLi.treeview #masterDataPro.active #masterDataProSetUp").addClass("menu-open");
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.ulb").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #proWorks #masterDataPro.active #masterDataProSetUp li.ulb .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

		var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
		projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'projectCharter'}, function (response) {
//		alert(JSON.stringify(response));
		   if(!response){
			window.location.href = "#/noAccessPage";
		   }
		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.editULB = function(department) {
        $("#editULBModel").modal("show");
        $scope.editCharterArea = true;
        $scope.createCharterArea = false;
        $scope.editULB=angular.copy(department);
    }

    $scope.getULB=function () {
        $http({
            method: 'GET',
            url: 'api/ULBs',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.ulbList = response;
            $scope.loadingImage=false;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.addULBModal = function() {
        $("#addulbModel").modal("show");
        $scope.ulb = {};
        $scope.errorMessage=false;
    }


    var addDoubleClick=false;
    $scope.addULB=function () {
        if(!addDoubleClick){
            addDoubleClick=true;
            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            var ulb=$scope.ulb;
            ulb['createdPerson']=loginPersonDetails.name;
            $http({
                method: 'POST',
                url: 'api/ULBs',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data":ulb
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $("#addulbModel").modal("hide");
                $("#addulbSuccess").modal("show");
                addDoubleClick=false;
                setTimeout(function(){$('#addulbSuccess').modal('hide')}, 3000);
                $scope.getULB();
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
                if(response.error.details.messages.name) {
                    $scope.errorMessageData=response.error.details.messages.name[0];
                    $scope.errorMessage=true;
                    addDoubleClick=false;
                    $timeout(function(){
                        $scope.errorMessage=false;
                    }, 3000);
                }
            });
        }

    }

    var editDoubleClick=false;
    $scope.editULBButton=function () {
        if(!editDoubleClick){
            editDoubleClick=true;
            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            $scope.editULB['lastEditPerson']=loginPersonDetails.name;
            $http({
                "method": "PUT",
                "url": 'api/ULBs/'+   $scope.editULB.id,
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data":  $scope.editULB
            }).success(function (response, data) {
                //console.log("filter Schemes "+ JSON.stringify(response));
                $scope.loadingImage=false;
                $("#editULBModel").modal("hide");
                $("#editULBSuccess").modal("show");
                editDoubleClick=false;
                setTimeout(function(){$('#editULBSuccess').modal('hide')}, 3000);
                $scope.getULB();
            }).error(function (response, data) {
                console.log("failure");
                if(response.error.details.messages.name) {
                    $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                    $scope.errorUpdateMessage=true;
                    editDoubleClick=false;
                    $timeout(function(){
                        $scope.errorUpdateMessage=false;
                    }, 3000);
                }
            })


        }

    }
    $scope.getULB();

    $scope.exportToExcel=function(tableId){
        if( $scope.ulbList!=null &&  $scope.ulbList.length>0){

            $("<tr>" +
                "<th>ULB Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.ulbList.length;i++){
                var ulb=$scope.ulbList[i];

                $("<tr>" +
                    "<td>"+ulb.name+"</td>" +
                    "<td>"+ulb.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'ULB Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }
}]);

app.controller('rebaselineReportController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("rebaselineReportController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.rebaseLine").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.rebaseLine .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });
        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);
        $scope.getCharterDetails= function () {
            $http({
                method: 'GET',
                url: 'api/ProjectCharters/getCharterDetails',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.charterList=response;
                $scope.loadingImage=false;
                $scope.showProjects=true;
            }).error(function (response) {
                console.log("error"+JSON.stringify(response))
            });
        }
        $scope.getCharterDetails();
        $scope.selectCharter = function(charterId) {
            $http({
                method: 'GET',
                url: 'api/ProjectCharters/getRebaseLineData?charterId='+charterId,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.rebaseLineData=response;
                $scope.loadingImage=false;
                $scope.showProjects=true;
                console.log("response data"+JSON.stringify(response))
            }).error(function (response) {
                console.log("error"+JSON.stringify(response))
            });
        }



}]);

app.controller('billMISDetailsController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("billMISDetailsController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.billMISDetails").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.billMISDetails .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

        $scope.selectProject = function() {
                $scope.seletedYearIs = $scope.selectYear;

//                    $scope.showProjects=false;
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans/?filter={"where":{"and":[{"financialYear":"' + $scope.seletedYearIs  + '"},{"finalStatus":"Approval"}]}}',
//                    url: 'api/ProjectPlans/?filter={"where":{"and":[{"financialYear":"' + $scope.seletedYearIs + '"},{"finalStatus":"Approval"}]}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.schemeListData=response;
                    $scope.loadingImage=false;
                    $scope.showProjects=true;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });

            }

        $scope.selectareas = function(projectId){
            $scope.selectProjectId = projectId;
            console.log($scope.selectProjectId)
            projectWardWorks.getSelectedPlan(projectId, function (response) {
                $scope.selectProjectDetails=response[0];
            });
            $scope.showSubtasks = true;
            getProjectTaskDetails();
        }

        function getProjectTaskDetails(){
            $scope.subTaskList = [];
            $http({
                method: 'GET',
                url: 'api/ProjectTORs/getSubTaskDetails?planId='+$scope.selectProjectId ,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
               console.log("subtasklists"+JSON.stringify(response))
                $scope.subTaskList=response;
            }).error(function (response) {
            });
        }
        $scope.selectSubTask=function(selectedTask){
            $scope.selectedTaskData=selectedTask;
        }
        var acceptDoubleClick=false;
        $scope.billDetails = function() {
        if(!acceptDoubleClick){
            acceptDoubleClick=true;
            $scope.projectIdIs = $scope.selectProjectId;
            $scope.subtaskName = $scope.selectedTaskData;
            $http({
                method: 'GET',
                url: 'api/BillGenerations/?filter={"where":{"and":[{"planId":"' + $scope.projectIdIs  + '"},{"subtask":"' + $scope.subtaskName  + '"}]}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.billData=response;
                $scope.loadingImage=false;
                $scope.showDetails=true;
                acceptDoubleClick=false;
                console.log("response......."+JSON.stringify(response))
            }).error(function (response) {
                console.log("error"+JSON.stringify(response))
            });
        }
        }

        $scope.measureMISModal = function(details) {
                                        $scope.measureDetails = details;
                                        $("#viewMeasureMISDetails").modal("show");
                                    }

}]);

app.controller('billMISViewController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$routeParams', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $routeParams, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("billMISViewController");


    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.billMISDetails").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.billMISDetails .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.requestDetails=$routeParams.requstId;
            $scope.requestDetails1=$routeParams.subtask;

//            alert($scope.requestDetails)
//            alert($scope.requestDetails1)

             $scope.billDetails = function() {
                        $scope.projectIdIs = $scope.selectProjectId;
                        $scope.subtaskName = $scope.selectedTaskData;
                        $http({
                            method: 'GET',
                            url: 'api/BillGenerations/?filter={"where":{"and":[{"planId":"' + $scope.requestDetails  + '"},{"subtask":"' + $scope.requestDetails1  + '"}]}}',
                            headers: {"Content-Type": "application/json", "Accept": "application/json"}
                        }).success(function (response) {
                            $scope.billData=response;
                            $scope.loadingImage=false;
                            $scope.showDetails=true;
//                            console.log("response......."+JSON.stringify(response))
                        }).error(function (response) {
                            console.log("error"+JSON.stringify(response))
                        });
                    }

                    $scope.billDetails();

}]);

app.controller('paymentMISDetailsController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("paymentMISDetailsController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.paymentMISDetails").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.paymentMISDetails .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

//        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

$scope.selectDepartment = function(deptId){
        $scope.selectedDeptId = deptId;
        $scope.showSubmit = true;
    };

            $scope.selectProject = function() {
                $scope.seletedYearIs = $scope.selectYear;
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans/?filter={"where":{"and":[{"financialYear":"' + $scope.seletedYearIs  + '"},{"finalStatus":"Approval"}]}}',
//                    url: 'api/ProjectPlans/?filter={"where":{"and":[{"financialYear":"' + $scope.seletedYearIs + '"},{"finalStatus":"Approval"}]}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.schemeListData=response;
                    $scope.loadingImage=false;
                    $scope.showProjects=true;

                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });

            }

            $scope.paymentData = function() {
                $scope.paymentDetails = $scope.selectedDeptId ;

                 $http({
                    method: 'GET',
                    url: 'api/ProjectPaymentOrders/?filter={"where":{"planId":"' + $scope.paymentDetails  + '"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.projectDetails=response;
                    $scope.loadingImage=false;
                    $scope.showProjects=true;
                    $scope.showDetails=true;

                    console.log("response....... planId "+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });

            }

}]);

app.controller('paymentMISViewController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$routeParams', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $routeParams, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("paymentMISViewController");


    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.paymentMISDetails").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.paymentMISDetails .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.requestDetails=$routeParams.requstId;

//            alert($scope.requestDetails)

            $scope.paymentData = function() {
                $scope.paymentDetails = $scope.selectedDeptId ;

                 $http({
                    method: 'GET',
                    url: 'api/ProjectPaymentOrders/?filter={"where":{"planId":"' + $scope.requestDetails  + '"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.projectRequestDetails=response;
                    $scope.loadingImage=false;
                    $scope.showProjects=true;
                    $scope.showDetails=true;

                    console.log("response....... planId "+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });

            }

            $scope.paymentData();

}]);

app.controller('measurementMISDetailsController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("measurementMISDetailsController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.measurementMISDetails").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.measurementMISDetails .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

//        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.selectProject = function() {
                $scope.seletedYearIs = $scope.selectYear;
                $http({
                    method: 'GET',
                    url: 'api/ProjectPlans/?filter={"where":{"and":[{"financialYear":"' + $scope.seletedYearIs  + '"},{"finalStatus":"Approval"}]}}',
//                    url: 'api/ProjectPlans/?filter={"where":{"and":[{"financialYear":"' + $scope.seletedYearIs + '"},{"finalStatus":"Approval"}]}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.schemeListData=response;
                    $scope.loadingImage=false;
                    $scope.showProjects=true;

                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });

            }

        $scope.selectareas = function(projectId){
            $scope.selectProjectId = projectId;
            console.log($scope.selectProjectId)
            projectWardWorks.getSelectedPlan(projectId, function (response) {
                $scope.selectProjectDetails=response[0];
            });
            $scope.showSubtasks = true;
            getProjectTaskDetails();
        }

            function getProjectTaskDetails(){
                        $scope.subTaskList = [];
                        $http({
                            method: 'GET',
                            url: 'api//ProjectTORs/getSubTaskDetails?planId='+$scope.selectProjectId ,
                            headers: {"Content-Type": "application/json", "Accept": "application/json"}
                        }).success(function (response) {
                           console.log("subtasklists"+JSON.stringify(response))
                            $scope.subTaskList=response;
                        }).error(function (response) {
                        });
                    }

                    $scope.selectSubTask=function(selectedTask){

                        $scope.selectedTaskData=selectedTask;

                    }

        $scope.measurementMISDetails = function() {
                    $scope.projectIdIs = $scope.selectProjectId;
                    $scope.subtaskName = $scope.selectedTaskData;

                    $http({
                        method: 'GET',
                        url: 'api/ProjectMeasurements/?filter={"where":{"and":[{"planId":"' + $scope.projectIdIs  + '"},{"subTaskName":"' + $scope.subtaskName  + '"}]}}',
        //                    url: 'api/ProjectPlans/?filter={"where":{"and":[{"financialYear":"' + $scope.seletedYearIs + '"},{"finalStatus":"Approval"}]}}',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                    }).success(function (response) {
                        $scope.measurementData=response;
                        $scope.loadingImage=false;
                        $scope.showDetails=true;

                        console.log("response......."+JSON.stringify(response))
                    }).error(function (response) {
                        console.log("error"+JSON.stringify(response))
                    });
                }

                $scope.measureMISModal = function(details) {
                    $scope.measureDetails = details;
                    $("#viewMeasureMISDetails").modal("show");
                }

}]);

app.controller('deptWiseBudgetMISController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("deptWiseBudgetMISController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.deptWiseBudgetMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.deptWiseBudgetMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

//        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.selectProject = function() {
                $scope.seletedYearIs = $scope.selectYear;
            }

            $scope.getULB = function() {
                $http({
                    method: 'GET',
                    url: 'api/ULBs/',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.getData=response;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });

            }
            $scope.getULB();

            $scope.selectareas = function(projectId){
                $scope.selectProjectId = projectId;
                console.log($scope.selectProjectId)
                projectWardWorks.getSelectedPlan(projectId, function (response) {
                    $scope.selectProjectDetails=response[0];
                });
            }

            $scope.deptWiseDetails = function() {
                $scope.seletedYearIs = $scope.selectYear;
                $scope.ulbName = $scope.selectProjectId;
                $http({
                    method: 'GET',
                    url: 'api/DepartmentWiseBudgets/?filter={"where":{"and":[{"financialYear":"' + $scope.seletedYearIs  + '"},{"ulbName":"' + $scope.ulbName + '"}]}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.deptWiseData=response;
                    $scope.loadingImage=false;
                    $scope.showDetails=true;

                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });

            }


}]);

app.controller('deptMISViewController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$routeParams', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $routeParams, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("deptMISViewController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.deptWiseBudgetMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.deptWiseBudgetMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

        $scope.loadingImage=true;

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.requestDetails=$routeParams.requstId;
            $scope.requestDetails1=$routeParams.subtask;

            $scope.deptWiseDetails = function() {
                $scope.requestDetails=$routeParams.requstId;
                $scope.requestDetails1=$routeParams.subtask;
                $http({
                    method: 'GET',
                    url: 'api/DepartmentWiseBudgets/?filter={"where":{"and":[{"financialYear":"' + $scope.requestDetails1  + '"},{"ulbName":"' + $scope.requestDetails + '"}]}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.deptWiseData=response;
                    $scope.loadingImage=false;
                    $scope.showDetails=true;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });
            }
            $scope.deptWiseDetails();


}]);

app.controller('departmentMISController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("departmentMISController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.departmentMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.departmentMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

//        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.selectProject = function() {
                $scope.seletedYearIs = $scope.selectYear;
            }

            $scope.getDepartments = function() {
                $http({
                    method: 'GET',
                    url: 'api/Departments/',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.getData=response;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });

            }
            $scope.getDepartments();

            $scope.selectareas = function(projectId){
                $scope.selectProjectId = projectId;
                console.log($scope.selectProjectId)
                projectWardWorks.getSelectedPlan(projectId, function (response) {
                    $scope.selectProjectDetails=response[0];
                });
            }

            $scope.departmentDetails = function() {
                $scope.depatmentName = $scope.selectProjectId;
                $scope.finYear = $scope.seletedYearIs;
                alert($scope.depatmentName);
                alert($scope.finYear);
            }

}]);

app.controller('applicationRequestMISController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("applicationRequestMISController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequestMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequestMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       /*var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });*/

//        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.selectStatus = function() {
                $scope.selectStatus = $scope.statusSelect;
            }

        $scope.applicationDetails = function() {
                $scope.selectStatus = $scope.statusSelect;
            var data={
//                'status':$scope.statusSelect,
                'startDate':$scope.startDate,
                'endDate':$scope.endDate
            }
            if($scope.statusSelect=="false"){
//            alert($scope.statusSelect)
            data.status=false
            }else{
            data.status=$scope.statusSelect;
            }
            console.log(JSON.stringify(data));

            $http({
                "method": "POST",
                "url": 'api/ProjectRequests/getReport/',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": data
            }).success(function (response, data) {
                $scope.loadingImage=false;
                console.log(JSON.stringify(response));
                $scope.applicationRequest = response;
                $scope.showDetails = true;
                $scope.loadingImage = false;
//                $scope.getEmail();
//                $("#registerSuccess").modal("show");
//                setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
            }).error(function (response, data) {
                console.log("failure");
            })
        }


}]);

app.controller('applicationRequestViewMISController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$routeParams', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $routeParams, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("applicationRequestViewMISController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequestMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequestMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

        $scope.loadingImage=true;

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.requestDetails=$routeParams.requstId;

            $scope.deptWiseDetails = function() {
                $scope.requestDetails=$routeParams.requstId;
                $scope.requestDetails1=$routeParams.subtask;
                $http({
                    method: 'GET',
                    url: 'api/ProjectRequests/?filter={"where":{"applicationId":"' + $scope.requestDetails + '"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.deptWiseViewData=response;
                    $scope.loadingImage=false;
                    $scope.showDetails=true;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });
            }
            $scope.deptWiseDetails();

}]);

app.controller('applicationRequesAgingtMISController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("applicationRequesAgingtMISController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequesAgingtMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequesAgingtMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

//        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.deptWiseDetails = function() {
                $http({
                    method: 'GET',
                    url: 'api/ProjectRequests/getPendingReq/',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.requestAging=response;
                    $scope.loadingImage=false;
                    $scope.showDetails=true;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });
            }
            $scope.deptWiseDetails();
}]);

app.controller('appRequestAgingDaysController', ['projectWardWorks', '$http', '$scope', '$window', '$routeParams', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $routeParams, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("appRequestAgingDaysController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequesAgingtMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequesAgingtMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

       $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.deptWiseDetails = function() {
                $http({
                    method: 'GET',
                    url: 'api/ProjectRequests/getPendingReq/',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.requestAging=response;
                    $scope.loadingImage=false;
                    $scope.showDetails=true;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });
            }
            $scope.deptWiseDetails();

            var paramlist=window.location.href.split("/")

                        $scope.countDays = paramlist[paramlist.length-1]
                                $scope.loadingImage=true;
                        var count;
                                if($scope.countDays == "tenDays") {
                                    var date = new Date();
                                                 $scope.tendays = true;
                                    //             alert(date)
                                                    var datestring = ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear() ;
                                                    $scope.currentDateIs  = datestring;
                                                        console.log(" $scope.prevDate"+ $scope.currentDateIs)
                                                        var res = date.setTime(date.getTime() - (10*24*60*60*1000));
                                                        pdate = new Date(res);
                                                    var datestring1 = ("0" + (pdate.getMonth() + 1)).slice(-2) + "-" + ("0" + pdate.getDate()).slice(-2) + "-" + pdate.getFullYear() ;
                                    //               alert(date);
                                                        $scope.prevDate = datestring1;
                                                        console.log(" $scope.prevDate"+ $scope.prevDate)
                                } else if($scope.countDays == "thirtyDays") {
                                    var date = new Date();
                                                 $scope.tendays = true;
                                    //             alert(date)
                                                    var datestring = ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear() ;
                                                    $scope.currentDateIs  = datestring;
                                                        pdate = new Date(date.setMonth(date.getMonth() - 1));
                                                    var datestring1 = ("0" + (pdate.getMonth() + 1)).slice(-2) + "-" + ("0" + pdate.getDate()).slice(-2) + "-" + pdate.getFullYear() ;
                                    //               alert(date);
                                                        $scope.prevDate = datestring1;
                                                        console.log(" $scope.prevDate"+ $scope.prevDate)
                                }else if($scope.countDays == "threeMonths") {
                                     var date = new Date();
                                     $scope.tendays = true;
                        //             alert(date)
                                        var datestring =  ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear() ;
                                        $scope.currentDateIs  = datestring;
                                            pdate = new Date(date.setMonth(date.getMonth() - 3));
                                        var datestring1 = ("0" + (pdate.getMonth() + 1)).slice(-2) + "-" + ("0" + pdate.getDate()).slice(-2) + "-" + pdate.getFullYear() ;
                        //               alert(date);
                                            $scope.prevDate = datestring1;
                                            console.log(" $scope.prevDate"+ $scope.prevDate)
                                 }

                                 /*var myVariable = "28 Aug 2014"
                                 var makeDate = new Date(myVariable);
                                 makeDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));*/



            $scope.selectStatus = function() {
                $scope.selectStatus = $scope.statusSelect;
            }

               $scope.applicationDetails = function() {
                     $scope.selectStatus = $scope.statusSelect;
                           var data={
               //                'status':$scope.statusSelect,
                               'startDate':$scope.prevDate,
                               'endDate':$scope.currentDateIs
                           }
                           if($scope.statusSelect=="false"){
               //            alert($scope.statusSelect)
                           data.status=false
                           }else{
                           data.status=$scope.statusSelect;
                           }
                           console.log(JSON.stringify(data));

                           $http({
                               "method": "POST",
                               "url": 'api/ProjectRequests/getReport/',
                               "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                               "data": data
                           }).success(function (response, data) {
                               $scope.loadingImage=false;
                               console.log(JSON.stringify(response));
                               $scope.applicationRequest = response;
                               $scope.showDetailsData = true;
                               $scope.loadingImage = false;
               //                $scope.getEmail();
               //                $("#registerSuccess").modal("show");
               //                setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
                           }).error(function (response, data) {
                               console.log("failure");
                           })
                       }

}]);

app.controller('NOCRequestMISController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("NOCRequestMISController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.NOCRequestMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.NOCRequestMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

//        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.getDepartments=function () {
                $http({
                    method: 'GET',
                    url: 'api/PlanDepartments',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('getDepartments.................... :' + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $scope.departmentList = response;
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
            $scope.getDepartments();

            $scope.selectDept = function(projectId){
                            $scope.selectProjectId = projectId;
                console.log($scope.selectProjectId)
                /*projectWardWorks.getSelectedPlan(projectId, function (response) {
                    $scope.selectProjectDetails=response[0];
                });*/
            }

            $scope.selectStatus = function() {
                 $scope.selectStatus = $scope.selectYear;
//                 alert($scope.selectStatus)
            }

            $scope.applicationDetails = function() {
                            $scope.selectStatus = $scope.statusSelect;
                        var data={
            //                'status':$scope.statusSelect,
                            'startDate':$scope.startDate,
                            'endDate':$scope.endDate
                        }
                        if($scope.statusSelect=="false"){
            //            alert($scope.statusSelect)
                        data.status=false
                        }else{
                        data.status=$scope.statusSelect;
                        }
                        console.log(JSON.stringify(data));

                        $http({
                            "method": "POST",
                            "url": 'api/NocRequests/getReport',
                            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                            "data": data
                        }).success(function (response, data) {
                            $scope.loadingImage=false;
                            console.log("response ......... "+JSON.stringify(response));
                            $scope.applicationRequest = response;
                            $scope.showDetails1 = true;
                            $scope.loadingImage = false;
            //                $scope.getEmail();
            //                $("#registerSuccess").modal("show");
            //                setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
                        }).error(function (response, data) {
                            console.log("failure");
                        })
                    }

}]);

app.controller('NOCRequestViewController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$routeParams', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $routeParams, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("NOCRequestViewController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.NOCRequestMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.NOCRequestMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

        $scope.loadingImage=true;

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.requestDetails=$routeParams.requstId;

            $scope.deptWiseDetails = function() {
                $scope.requestDetails=$routeParams.requstId;
                $http({
                    method: 'GET',
                    url: 'api/NocRequests/?filter={"where":{"applicationId":"' + $scope.requestDetails + '"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.deptWiseViewData=response;
                    $scope.loadingImage=false;
                    $scope.showDetails=true;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });
            }
            $scope.deptWiseDetails();


}]);

app.controller('NOCRequestAgingMISController', ['projectWardWorks', '$http', '$scope', '$window', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("NOCRequestMISAgingController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.NOCRequestAgingMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.NOCRequestAgingMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

//        $scope.loadingImage=true;
        /*$scope.vm = {};*/

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.deptWiseDetails = function() {
                $http({
                    method: 'GET',
                    url: 'api/NocRequests/getPendingReq/',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.requestAging=response;
                    $scope.loadingImage=false;
                    $scope.showDetails=true;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });
            }
            $scope.deptWiseDetails();

}]);

app.controller('NOCRequestAgingDaysController', ['projectWardWorks', '$http', '$scope', '$window', '$routeParams', '$timeout', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(projectWardWorks, $http, $scope, $window, $timeout, $routeParams, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("NOCRequestAgingDaysController");

    $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #proWorksLi").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS").addClass("active");
           $(".sidebar-menu #proWorksLi.treeview #projectMIS.active #projectMISetUp").addClass("menu-open");
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequesAgingtMIS").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #proWorks #projectMIS.active #projectMISetUp li.applicationRequesAgingtMIS .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });

       var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        projectWardWorks.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'budgetUtilization'}, function (response) {
    //    alert(JSON.stringify(response));
              if(!response){
               window.location.href = "#/noAccessPage";
              }
           });

       $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

            $scope.deptWiseDetails = function() {
                $http({
                    method: 'GET',
                    url: 'api/ProjectRequests/getPendingReq/',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.requestAging=response;
                    $scope.loadingImage=false;
                    $scope.showDetails=true;
                    console.log("response......."+JSON.stringify(response))
                }).error(function (response) {
                    console.log("error"+JSON.stringify(response))
                });
            }
            $scope.deptWiseDetails();

            var paramlist=window.location.href.split("/")

                        $scope.countDays = paramlist[paramlist.length-1]
                                $scope.loadingImage=true;
                        var count;
                                if($scope.countDays == "tenDays") {
                                    var date = new Date();
                                                 $scope.tendays = true;
                                    //             alert(date)
                                                    var datestring = ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear() ;
                                                    $scope.currentDateIs  = datestring;
                                                        console.log(" $scope.prevDate"+ $scope.currentDateIs)
                                                        var res = date.setTime(date.getTime() - (10*24*60*60*1000));
                                                        pdate = new Date(res);
                                                    var datestring1 = ("0" + (pdate.getMonth() + 1)).slice(-2) + "-" + ("0" + pdate.getDate()).slice(-2) + "-" + pdate.getFullYear() ;
                                    //               alert(date);
                                                        $scope.prevDate = datestring1;
                                                        console.log(" $scope.prevDate"+ $scope.prevDate)
                                } else if($scope.countDays == "thirtyDays") {
                                    var date = new Date();
                                                 $scope.tendays = true;
                                    //             alert(date)
                                                    var datestring = ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear() ;
                                                    $scope.currentDateIs  = datestring;
                                                        pdate = new Date(date.setMonth(date.getMonth() - 1));
                                                    var datestring1 = ("0" + (pdate.getMonth() + 1)).slice(-2) + "-" + ("0" + pdate.getDate()).slice(-2) + "-" + pdate.getFullYear() ;
                                    //               alert(date);
                                                        $scope.prevDate = datestring1;
                                                        console.log(" $scope.prevDate"+ $scope.prevDate)
                                }else if($scope.countDays == "threeMonths") {
                                     var date = new Date();
                                     $scope.tendays = true;
                        //             alert(date)
                                        var datestring =  ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear() ;
                                        $scope.currentDateIs  = datestring;
                                            pdate = new Date(date.setMonth(date.getMonth() - 3));
                                        var datestring1 = ("0" + (pdate.getMonth() + 1)).slice(-2) + "-" + ("0" + pdate.getDate()).slice(-2) + "-" + pdate.getFullYear() ;
                        //               alert(date);
                                            $scope.prevDate = datestring1;
                                            console.log(" $scope.prevDate"+ $scope.prevDate)
                                 }

                                 /*var myVariable = "28 Aug 2014"
                                 var makeDate = new Date(myVariable);
                                 makeDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));*/



            $scope.selectStatus = function() {
                $scope.selectStatus = $scope.statusSelect;
            }

               $scope.applicationDetails = function() {
                     $scope.selectStatus = $scope.statusSelect;
                           var data={
               //                'status':$scope.statusSelect,
                               'startDate':$scope.prevDate,
                               'endDate':$scope.currentDateIs
                           }
                           if($scope.statusSelect=="false"){
               //            alert($scope.statusSelect)
                           data.status=false
                           }else{
                           data.status=$scope.statusSelect;
                           }
                           console.log(JSON.stringify(data));

                           $http({
                               "method": "POST",
                               "url": 'api/NocRequests/getReport/',
                               "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                               "data": data
                           }).success(function (response, data) {
                               $scope.loadingImage=false;
                               console.log(JSON.stringify(response));
                               $scope.applicationRequest = response;
                               $scope.showDetailsData = true;
                               $scope.loadingImage = false;
               //                $scope.getEmail();
               //                $("#registerSuccess").modal("show");
               //                setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
                           }).error(function (response, data) {
                               console.log("failure");
                           })
                       }

}]);