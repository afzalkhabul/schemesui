
// var uploadFileURL='http://139.162.27.78:8865/api/Uploads/dhanbadDb/download/';

var uploadFileURL='http://localhost:3004/api/Uploads/dhanbadDb/download/';

var app = angular.module('dhanbad', ['ngRoute', 'ngFileUpload','datatables','pascalprecht.translate']);

app.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: '/schemeManagement.html',
        controller: 'schemeManagementController'
    }).when('/project-WordWorks', {
        templateUrl: '/project-WordWorks.html',
        controller: 'projectWordWorksController'
    }).when('/schemeDetails/:schemeId', {
        templateUrl: '/schemeDetails.html',
        controller: 'schemeDetailsController'
    }).when('/appliedSchemeDetails/:requestId', {
        templateUrl: '/appliedSchemeDetails.html',
        controller: 'appliedSchemeDetailsController'
    }).when('/login', {
        templateUrl: '/login.html',
        controller: 'loginController'
    }).when('/schemeForm', {
        templateUrl: '/schemeForm.html',
        controller: 'schemeFormController'
    }).when('/formSubmitSuccess', {
        templateUrl: '/formSubmitSuccess.html',
        controller: 'formSubmitSuccessController'
    }).when('/legalManagement', {
        templateUrl: '/legalManagement.html',
        controller: 'legalManagementController'
    }).when('/createRequest', {
        templateUrl: '/createRequest.html',
        controller: 'createRequestController'
    }).when('/requestSubmitSuccess', {
        templateUrl: '/requestSubmitSuccess.html',
        controller: 'requestSubmitSuccessController'
    }).when('/requestData', {
        templateUrl: '/requestData.html',
        controller: 'requestDataController'
    }).when('/formsDownload', {
        templateUrl: '/formsDownload.html',
        controller: 'formsDownloadController'
    }).when('/appliedForms', {
        templateUrl: '/appliedForms.html',
        controller: 'appliedFormsController'
    }).when('/userProfile', {
        templateUrl: '/userProfile.html',
        controller: 'profileController'
    }).otherwise({
        redirectTo: '/'
    });
});

app.factory('httpRequestInterceptor', function ($window,$q,$location) {
    return {
        request: function (config) {
            if($window.localStorage.getItem('citizenTokenId')) {
                config.headers['access_token'] = $window.localStorage.getItem('citizenTokenId');
            }
            return config;
        },
        response: function(response) {
            var deferred = $q.defer();
            deferred.resolve(response);
            return deferred.promise;
        },
        responseError: function (rejection) {
            /*if(rejection.status === 401) {
                console.log('reject responce'+JSON.stringify(rejection))
                //if(rejection."Unauthorized")
                $window.localStorage.clear();
                location.reload();
                /!*   if(rejection.data.error.message=="Authorization Required"){

                 }*!/

            }*/
            if(rejection.status === 500){
                var errorUrlData=$location.host()+':3003/error';
                location.href=errorUrlData;
                return false;
            }
            return $q.reject(rejection);
        }
    };
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
});

app.directive('validNumber', function() {
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            if(!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function(val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if(!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean =negativeCheck[0] + '-' + negativeCheck[1];
                    if(negativeCheck[0].length > 0) {
                        clean =negativeCheck[0];
                    }
                }
                if(!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0,2);
                    clean =decimalCheck[0] + '.' + decimalCheck[1];
                }
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function(event) {
                if(event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});


app.controller('profileController', function ($http, $scope, $window, $location, $rootScope, $timeout) {
    console.log('profileController');
    $scope.userInfo = JSON.parse($window.localStorage.getItem('citizenDeatils'));
    //console.log("userInfo:::" + JSON.stringify($scope.userInfo));
//console.log("userInfo name:::" +$scope.userInfo.name);

    /*edit display name start*/

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    $scope.editName = function (userInfo) {
        $("#editName").modal("show");
       // console.log("edit info..... "+ JSON.stringify(userInfo));
        $scope.editNameData = angular.copy(userInfo);
    }

    $scope.getCitizens = function () {

        $http({
            "method": "GET",
            //"url": URL+'/Citizens/',
            "url": 'api/Citizens/?filter=%7B%22where%22%3A%7B%22id%22%3A%20%22' + $scope.userInfo.id + '%22%7D%7D',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (responseData) {
            //console.log("user details " + JSON.stringify(responseData));
            $scope.responseInfo = responseData[0];
            $scope.userInfo = $scope.responseInfo;

            //console.log(" $scope.responseInfo" + JSON.stringify($scope.responseInfo))
            //console.log("responseInfo" + JSON.stringify($scope.userInfo));
        }).error(function (response) {
            //console.log("failure");
        });
    }

    $scope.editProfileSubmit = function () {
        var editCharterDetails = $scope.editNameData;
        var userDetails = JSON.parse($window.localStorage.getItem('citizenDeatils'));
        editCharterDetails['lastEditPerson'] = userDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/Citizens/' + $scope.editNameData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editCharterDetails
        }).success(function (response, data) {
            //console.log("editCharterDetails...... "+ JSON.stringify(response));
            $("#editName").modal("hide");
            $("#editProfileSuccess").modal("show");
            setTimeout(function(){$('#editProfileSuccess').modal('hide')}, 3000);
            $scope.getCitizens();
        }).error(function (response, data) {
            //console.log("failure");
        });
    }

    $scope.getCitizens();

    /*edit display name end*/


    /*edit email start*/

    $scope.editEmail = function (userInfo) {

        //alert("editName::" +userInfo);
        $("#editEmail").modal("show");
        $scope.editEmailData = angular.copy(userInfo);
    }


    $scope.editEmailSubmit = function () {
        var editCharterDetails = $scope.editEmailData;
        var userDetails = JSON.parse($window.localStorage.getItem('citizenDeatils'));
        editCharterDetails['lastEditPerson'] = userDetails.email;
        $http({
            "method": "PUT",
            "url": 'api/Citizens/' + $scope.editEmailData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editCharterDetails
        }).success(function (response, data) {
            //console.log("filter Schemes "+ JSON.stringify(response));
            $("#editEmail").modal("hide");
            $scope.getCitizens();
        }).error(function (response, data) {
            //console.log("failure");
        })
    }

    $scope.getCitizens();

    /*edit email end*/


    /*edit phone start*/

    $scope.editPhone = function (userInfo) {

        //alert("editName::" +userInfo);
        $("#editPhone").modal("show");
        $scope.editPhoneData = angular.copy(userInfo);
    }


    $scope.editPhoneSubmit = function () {
        var editCharterDetails = $scope.editPhoneData;
        var userDetails = JSON.parse($window.localStorage.getItem('citizenDeatils'));
        editCharterDetails['lastEditPerson'] = userDetails.phone;
        $http({
            "method": "PUT",
            "url": 'api/Citizens/' + $scope.editPhoneData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editCharterDetails
        }).success(function (response, data) {
            //console.log("filter Schemes "+ JSON.stringify(response));
            $("#editPhone").modal("hide");
            $scope.getCitizens();
        }).error(function (response, data) {
            //console.log("failure");
        })
    }

    $scope.getCitizens();

    /*edit phone end*/

    /*change password start*/
    $scope.user = {};
    $scope.changePasswordSubmit = function () {

        $scope.tokenId = $window.localStorage.getItem("citizenTokenId");
        //alert("citizenTokenId" +$scope.tokenId);

        var currentpassword = document.getElementById("password").value;
        //alert("current password:::" +currentpassword);
        var password = document.getElementById("pswd").value;
        //alert(" password:::" +password);
        var cpasswor = document.getElementById("pswd1").value;
        //alert("confirm password:::" +cpasswor);
        $scope.passwordError1 = "";

        if (currentpassword != '' && currentpassword != undefined && currentpassword != null
            && password != '' && password != undefined && password != null
            && cpasswor != '' && cpasswor != undefined && cpasswor != null) {
            if (password.length >= 6) {
                if (password == cpasswor) {
                    $http({
                        "method": "POST",
                        "url": 'api/Citizens/login',
                        "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                        "data": {
                            "email": $scope.userInfo.email,
                            "password": $scope.user.password
                        }
                    }).success(function (response) {

                        //console.log("response" +JSON.stringify(response));
                        $scope.tokenId = response.id;
                        $scope.userId = response.userId;
                        $window.localStorage.setItem('accessToken', $scope.tokenId);

                        $http({
                            method: "PUT",
                            url: "api/Citizens/" + $scope.userInfo.id ,
                            headers: {'Accept': 'application/json'},
                            data: {"password": $scope.user.newPassword}
                        }).success(function (response, data) {
                            //console.log("Password Response:" + JSON.stringify(response));
                            document.getElementById('changePasswordForm').reset();
                            $('#changePassword').modal('hide');
                            $('#changePasswordSuccess').modal('show');
                            setTimeout(function(){$('#changePasswordSuccess').modal('hide')}, 2000);
                            $scope.passwordError = "Password Updated Successfully";
                            //console.log("Password Updated Successfully");

                        }).error(function (response) {
                            //console.log("Error:" + JSON.stringify(response));
                            $scope.errorMessage = true;
                            $scope.passwordError1 = "Something Went Wrong. Please Try Again Later";
                            //console.log("Something Went Wrong. Please Try Again Later");
                            $timeout(function(){
                                $scope.errorMessage=false;
                            }, 3000);

                        })
                    }).error(function (data) {
                            $scope.errorMessage = true;
                        $scope.passwordError1 = "Please Enter Correct Password";
                        $timeout(function(){
                            $scope.errorMessage=false;
                        }, 3000);
                            //console.log("Please Enter Correct Password");

                    })
                } else {
                            $scope.errorMessage = true;
                    $scope.passwordError1 = "New Password And Confirm Password does not match";
                    $timeout(function(){
                        $scope.errorMessage=false;
                    }, 3000);
                    //console.log("Password Confirmation Unsuccessful.");

                }
            }
            else {
                //console.log("Enter Minimum 6 Characters");

            }
        } else {
            //console.log("All Fields Are Mandatory.");

        }
    }


    /*change password end*/


});

app.controller('legalManagementController', function ($scope) {
    console.log('legalManagementController');
});

app.controller('indexController', function ($http, $scope, $window, $location, $rootScope, $translate, $timeout) {
    console.log('indexController');

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    $rootScope.logout = function () {
        // alert('hai');
        $window.localStorage.removeItem('citizenDeatils');
        $("#logoutSuccess").modal("show");
        setTimeout(function(){$('#logoutSuccess').modal('hide')}, 3000);
        //$scope.reloadFunction();
        $timeout(reloadFunction, 3000);
    }
    function reloadFunction() {
        $location.url('/');
        $window.location.reload();
    }
    var data = JSON.parse($window.localStorage.getItem('citizenDeatils'));
    $rootScope.userData = data;

    if (data != undefined && data != null) {
        $rootScope.loginStatus = false;
    } else {
        $rootScope.loginStatus = true;
    }

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });


    $(document).ready(function () {
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });

  /*  var lang = new Lang();
    lang.dynamic('hi', 'js/langpack/hi.json');
    lang.init({
        defaultLang: 'en'
    });
    //  window.lang.change('en');


    function language(name){
        if(name.value=='english'){
            window.lang.change('en');
        }else if(name.value=='hindi'){
            window.lang.change('hi');
        }
    }*/

 /*   $scope.changeLanguage = function (key) {
        alert('enter');
        var translateText =
            $translate.use(key);
        var sourceText = document.getElementById("sourceText").innerHTML;
        console.log(sourceText);
        $http({
            method: 'GET',
            url: 'https://www.googleapis.com/language/translate/v2?key=AIzaSyDy-x5atvBbzu8DQ2tMfZrCbM8ii8bmtwo&source=en&target='+key+'&callback=translateText&q=' + sourceText
        }).success(function(response) {
            response = response.replace("// API callback","");
            response = response.replace("translateText(","");
            response = response.replace(");","");
            response = JSON.parse(response);
            //console.log(response.data.translations[0]);
            var mContent = response.data.translations[0].translatedText;
            console.log(mContent);
            document.getElementById("sourceText").innerHTML = mContent;
        });

    };*/
});

app.controller('homeController', function ($http, $scope, $window, $location, $rootScope,$translate) {
    console.log('homeController');
    $scope.changeLanguage = function (key) {
        var translateText =
            $translate.use(key);
        var sourceText = document.getElementById("sourceText").innerHTML;
        $http({
            method: 'GET',
            url: 'https://www.googleapis.com/language/translate/v2?key=AIzaSyDy-x5atvBbzu8DQ2tMfZrCbM8ii8bmtwo&source=en&target='+key+'&callback=translateText&q=' + sourceText
        }).success(function(response) {
            response = response.replace("// API callback","");
            response = response.replace("translateText(","");
            response = response.replace(");","");
            response = JSON.parse(response);
            //console.log(response.data.translations[0]);
            document.getElementById("sourceText").innerHTML = response.data.translations[0].translatedText;
        });

    };
});

app.controller('appliedFormsController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('appliedFormsController');
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.loginUser").addClass("active").siblings('.active').removeClass('active');
    });
    var details=JSON.parse($window.localStorage.getItem('citizenDeatils'));
    $scope.userDetails = details;
          $http({
              "method": "GET",
             url: 'api/Requests?filter=%7B%22where%22%3A%7B%22emailId%22%3A%22'+$scope.userDetails.email+'%22%7D%7D',
              "headers": {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log("filter Schemes "+ JSON.stringify(response));
              $scope.requestList = response;
          }).error(function (response, data) {
              console.log("ERROR Schemes "+ JSON.stringify(response));
          })
//      }
});

app.controller('schemeManagementController', function ($http, $scope, $window, $location, $rootScope, $filter, $routeParams) {
    console.log('schemeManagementController');
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });
    $http({
        "method": "GET",
        "url": 'api/Schemes?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        //console.log("filter Schemes " + JSON.stringify(response));
        $scope.schemeLists = response;

        $scope.items2 = $scope.schemeLists;

        $scope.$watch('search', function (val) {
            $scope.schemeLists = $filter('filter')($scope.items2, val);
        });

    }).error(function (response, data) {
        //console.log("failure");
    })

    /*};
     */
    /*$scope.setTab(1);
     $scope.isSet = function(tabNum){
     //alert($scope.tab);
     return $scope.tab === tabNum;
     };*/

    $scope.schemeForms = function (schemeDetails) {

        $window.localStorage.setItem('schemeDetails', JSON.stringify(schemeDetails));
        location.href = '#/schemeForm';
        $scope.pack = $routeParams.schemeId;
    }

    /*$scope.removeSessionInfo = function(){
        var sessionId = $window.localStorage.getItem('sessionId');
        $http({
            method: 'DELETE',
            url: 'api/SessionInfos/'+sessionId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('Response......:'+JSON.stringify(response));
            $window.localStorage.removeItem('sessionId');
            $window.location.reload();
        });
    };*/

    /*$rootScope.validateSession = function(redirectCheck){
        if(redirectCheck == 'redirectToLogin '){
            $('#sessionTimeOut').modal('hide');
            $location.url('/');
        }else {
            //console.log(' IN SESSION VALIDATOR &&&&&&&&&&&&&&&&&&&&&');
            $window.localStorage.removeItem("userDeatils");
            $window.localStorage.removeItem("citizenTokenId");
            $window.localStorage.removeItem("accessTokenExpireTime");
            sessionActive = true;
            $('#sessionTimeOut').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
            if($window.localStorage.getItem('sessionId')){
                $scope.removeSessionInfo();
            }else{
                $window.location.reload();
            }
            $location.url('/');
        }
    };*/
});

app.controller('schemeDetailsController', function ($http, $scope, $window, $location, $rootScope, $routeParams) {
    console.log('schemeDetailsController');
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });
    $scope.schemeForms = function (schemeDetails) {
        $window.localStorage.setItem('schemeDetails', JSON.stringify(schemeDetails));
        location.href = '#/schemeForm';
        $scope.pack = $routeParams.schemeId;
    }

    $scope.uploadURL = uploadFileURL;
    $scope.pack = $routeParams.schemeId;
    //console.log("scheme pack Id " + $scope.pack);


    $http({
        "method": "GET",
        "url": 'api/Schemes/' + $scope.pack,
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        //console.log("schemeDetailsController " + JSON.stringify(response));
        $scope.filterSchemeLists = response;
    }).error(function (response, data) {
        //console.log("failure");
    });


});

app.controller('schemeFormController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {
    console.log("schemeFormController");
    //var customerDetails=$rootScope.customerDetails;

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });

    var customerDetails = JSON.parse($window.localStorage.getItem('citizenDeatils'));
    $scope.userDetailsList = customerDetails;

    $scope.selectedSchemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
    //console.log("selectedSchemeDetails " + $scope.selectedSchemeDetails);

    if (customerDetails != undefined && customerDetails != null) {


    } else {
        $location.url('/login');
    }

    var filedetails = [];
    var fileIdsArray = [];
    var fileUploadStatus = true;
    $scope.files = [];
    $scope.uploadURL = uploadFileURL;
    $scope.uploadFiles = function (files) {

        $scope.disable = true;
        $scope.errorMssg = true;
        //$scope.files = [];
        $scope.fileUploadLists = true;
        fileIdsArray = [];
        fileUploadStatus = false;
		var fileCount = 0;
        angular.forEach(files, function (file) {
            console.log('File Info:'+JSON.stringify(file));
            //alert('File Info:'+JSON.stringify(file.type));
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {

            var fsize=0;
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
               if (file.size == 0){
               fsize='0 Byte';
               }else{
                 var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                             fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
               }

                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    //console.log(JSON.stringify(response.data.metadata.mimetype));
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename,
                            'size': fsize
                        };
                        fileIdsArray.push(fileDetails);
                        filedetails.push(response.data);
                        $scope.files.push(fileDetails);
                        fileUploadStatus = true;
                        //console.log('details are' + JSON.stringify(fileIdsArray));
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
            if (fileCount == files.length) {
                $scope.fileUploadLists = true;
                $scope.disable = false;
                $scope.errorMssg = false;
            }
        });


    };

    $scope.deleteFile = function(index, fileId){

        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.files.splice(index, 1);
        });

    };

    $scope.citizen = {
        "emailId": $scope.userDetailsList.email
    }

    $scope.schemeFormSubmit = function () {
        if (fileUploadStatus) {
            var schemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
            var citizenData = $scope.citizen;
            citizenData.files = $scope.files;
            citizenData.schemeName = schemeDetails.name;
            citizenData.schemeUniqueId = schemeDetails.schemeUniqueId;
            citizenData.requestStatus = 'New';
            citizenData.acceptStatus = 'No';
            //console.log('Users Response :' + JSON.stringify(citizenData));
            if (citizenData.name != '') {
                $http({
                    method: 'POST',
                    url: 'api/Requests',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: citizenData
                }).success(function (response) {
                    console.log('Users Response....... :' + JSON.stringify(response));
                    $window.localStorage.setItem('requestDetails', JSON.stringify(response));
                    var requestDetails = JSON.parse($window.localStorage.getItem('requestDetails'));
                    //console.log('data is ' + JSON.stringify(response));
                    location.href = '#/formSubmitSuccess';
                    $scope.files =[];
                    $scope.citizen = {};
                    //console.log("success");
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
            else {

                $scope.schemeFormSubmit();
            }

            // }

        }
    };

    $scope.reset = function() {
        $scope.citizen = {};
        $scope.citizen.emailId = '';
        $scope.files = [];
        $scope.errorMssg = false;
    };
       // }
   // }
});

 app.controller('appliedSchemeDetailsController', function ($http, $scope, $window, $location, $rootScope, $routeParams) {
     console.log('appliedSchemeDetailsController');

     /*$scope.schemeForms = function (schemeDetails) {
         $window.localStorage.setItem('schemeDetails', JSON.stringify(schemeDetails));
         location.href = '#/schemeForm';
         $scope.pack = $routeParams.requestId;
     }*/

     $scope.uploadURL = uploadFileURL;
     $scope.pack = $routeParams.requestId;
     //console.log("scheme pack Id " + $scope.pack);

     var details=JSON.parse($window.localStorage.getItem('citizenDeatils'));
     $scope.userDetails = details;
     //alert(JSON.stringify($scope.userDetails));

     $http({
         "method": "GET",
         "url": 'api/Requests/' + $scope.pack,
         "headers": {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response, data) {
         //console.log("appliedSchemeDetailsController " + JSON.stringify(response));
         $scope.requestSuccess = response;
     }).error(function (response, data) {
         //console.log("failure");
     });


 });



app.controller('formSubmitSuccessController', function ($http, $scope, $timeout, $window, $location, $rootScope) {
    console.log('formSubmitSuccessController');
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });

    $scope.requestDetails = JSON.parse($window.localStorage.getItem('requestDetails'));
    $scope.selectedSchemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
   // console.log("selectedSchemeDetails " + $scope.selectedSchemeDetails);

});

app.controller('loginController', function ($http, $scope, $window, $timeout, $location, $rootScope) {
    console.log('loginController');

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
        $("#headerStarts .navbar-nav li.login").addClass("active").siblings('.active').removeClass('active');
    });

    /*Forgot Password start*/

    $scope.forgotPassword = function (email) {

        $rootScope.email = '';
        $scope.successMessage='';
        $scope.message='';

        var email1 = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        $rootScope.email = document.getElementById('email').value;
        //alert("email :" +$rootScope.email)
        var searchData=$rootScope.email;
        /*$rootScope.sucessData = false;*/

        if ($rootScope.email == undefined || $rootScope.email == '' || $rootScope.email == null) {
            $rootScope.notRegisterd = "Enter your Email";
        } else if (email1.test($rootScope.email)) {

            $http({
                method: "GET",
                url: 'api/Citizens/forgotPassword?searchData={"emailId":"' + $rootScope.email+'"}',
                //http://34.218.165.231:8899/api/Citizens/forgotPassword?searchData=%7B%22emailId%22%3A%22pavan%40nibblematrix.com%22%7D
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
            }).success(function (response) {
                //alert("success");
                console.log("response" +JSON.stringify(response.data.message));
                console.log("response" +JSON.stringify(response));
                if(response.data.message=="Please Register With US")
                {
                    $scope.message = true;
                    console.log("email not exist response" +JSON.stringify(response.data.message));
                    $scope.message="Invalid Email"
                }
                else {
                    console.log("email exist response" +JSON.stringify(response.data.email));
                    $scope.successMessage="Your password is send to your Email Id";
                    $("#forgotPassword").modal("hide");
                    $("#changePSWSuccess").modal("show");
                    setTimeout(function(){$('#changePSWSuccess').modal('hide')}, 3000);
                }
            }).error(function (response) {
                //alert("error")
               /* $rootScope.notRegisterd = true;
                $rootScope.sucessData = false;*/
            });
        } else {
            //alert("if else")
            $rootScope.notRegisterd = "Please Enter Valid Email";
        }
    }



    var data = JSON.parse($window.localStorage.getItem('citizenDeatils'));
    if (data != undefined && data != null) {
        location.href = '#/';
    }
    $scope.customerlogin = {
        "email": "",
        "password": ""
    }

    // $scope.loginSubmit = function () {
    //     if ($scope.customerlogin.email && $scope.customerlogin.password) {
    //         console.log("$scope.customer.email" + $scope.customerlogin.email);

    $scope.errorMessage=false;
    $scope.loginSubmit = function() {
      if($scope.customerlogin.email && $scope.customerlogin.password) {
          console.log("$scope.customer.email" + $scope.customerlogin.email);

            console.log("$scope.customer.password" + $scope.customerlogin.password);
            console.log("customer details:" + JSON.stringify($scope.customerlogin));
            $http({
                method: "POST",
                "url": 'api/Citizens/login',
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
                data: $scope.customerlogin
            }).success(function (response) {
                var userId = response.userId;
                $window.localStorage.setItem('citizenTokenId', response.id);
             console.log("---------------"+JSON.stringify(response.userId))
                $http({
                    "method": "GET",
                    "url": 'api/Citizens/?filter=%7B%22where%22%3A%7B%22id%22%3A%20%22' + userId + '%22%7D%7D',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (responseData) {
                    console.log("login details " + JSON.stringify(responseData));
                    $window.localStorage.setItem('citizenDeatils', JSON.stringify(responseData[0]));
                    //$window.localStorage.setItem('oldpassword', responseData[0].password);
                    $location.url('/');
                    $window.location.reload();
                    //  $scope.filterSchemeLists = response;
                }).error(function (response, data) {
                    console.log(JSON.stringify(response));
                });

            }).error(function (response) {
                if(response.error.status==401){
                    $scope.errorMessage=true;
                    $scope.errorMessageDetails='Email Id and Password does not match';
                    //alert('please enter valid details');

                    $timeout(function(){
                                            $scope.errorMessage=false;
                                        }, 3000);

                }
                console.log("LoginFailure:" + JSON.stringify(response));

            });
        } else {
            console.log("error");
        }
    }

    $scope.changeEmail = function () {
        $scope.loginArea = false;
    }

    $scope.addcitizen={}
    $scope.errorInRegisterMessage=false;
    $scope.createCitizenDataModal = function() {
        $("#createCitizenData").modal("show");
        $scope.citizenData = {};
    }
    var formRequestSubmit = true;
    $scope.createCitizen=function () {
        if (formRequestSubmit) {
            formRequestSubmit = false;
            var citizenData = $scope.addcitizen;
            if (citizenData.password == citizenData.confirmPassword) {
                $http({
                    "method": "POST",
                    "url": 'api/Citizens',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    data: citizenData
                }).success(function (response, data) {
                    $window.localStorage.setItem('citizenDeatils', JSON.stringify(response));
                    $('#createCitizenData').modal('hide');
                    $("#registerSuccess").modal("show");
                    // setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
                    //$location.url('/');
                    //$window.location.reload();
                    // $scope.filterSchemeLists = response;
                }).error(function (response, data) {
                    formRequestSubmit = true;
                    if (response.error.status == 422) {
                        $scope.errorInRegisterMessage = true;
                        $scope.errorInRegisterDetails = 'Your Email Id is already register.';

                        $timeout(function(){
                            $scope.errorInRegisterMessage=false;
                        }, 3000);
                    }

                });
            } else {
                formRequestSubmit = true;
                $scope.errorInRegisterMessage = true;
                $scope.errorInRegisterDetails = 'Password and Confirm Password does not match';

                $timeout(function(){
                    $scope.errorInRegisterMessage=false;
                }, 3000);
            }
        }
    }

    $scope.registerSuccessModal = function() {
        $("#registerSuccess").modal("hide");
        $location.url('/') ;
        $window.location.reload();
    }

});

app.controller('projectWordWorksController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('projectWordWorksController');
});

app.controller('createRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {
    console.log('createRequestController');

    var customerDetails = $window.localStorage.getItem('citizenDetails');

    //$scope.selectedSchemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
    //console.log("selectedSchemeDetails "+$scope.selectedSchemeDetails);
    $window.localStorage.setItem('redirectURL', 'createRequest');
    if (customerDetails != undefined && customerDetails != null) {
        $location.url('/createRequest');
    } else {
        $location.url('/login');
    }
    /*$scope.schemeFormSubmit = function () {
     location.href = '#/formSubmitSuccess';
     }*/

    var filedetails = [];
    var fileIdsArray = [];
    $scope.docList = [];
    var fileUploadStatus = true;
    $scope.uploadFiles = function (files) {

        $scope.files = files;
        //$scope.docList = [];
        fileUploadStatus = false;
        angular.forEach(files, function (file) {

            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename
                        }
                        fileIdsArray.push(fileDetails);
                        $scope.docList.push(fileDetails);
                        filedetails.push(response.data);
                        fileUploadStatus = true;
                        //console.log('details are' + JSON.stringify(fileIdsArray));
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
                $scope.fileUpload = true;
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });


    };

    $scope.createRequest = function () {
        var requestData = $scope.request;
        requestData.files = fileIdsArray;
        requestData.requestStatus = 'New';
        requestData.acceptStatus = 'No';
        if (fileUploadStatus) {
            requestData.files = $scope.docList;
            $http({
                method: 'POST',
                url: 'api/ProjectRequests',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: requestData
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                //console.log('data is '+JSON.stringify(response));
                //$("#successRequest").modal('show');
                $window.localStorage.setItem('requestDetails', JSON.stringify(response))
                $scope.requestDetails = $window.localStorage.getItem('requestDetails');
                //console.log('requestDetails '+$scope.requestDetails);
                location.href = '#/requestSubmitSuccess';
                $scope.docList = [];
                $scope.schemeRequset = true;
            }).error(function (response) {
                //console.log('Error Response :' + JSON.stringify(response));
            });

        } else {

            $scope.createRequest();
        }

    }
});

app.controller('requestSubmitSuccessController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('requestSubmitSuccessController');


    //$window.localStorage.setItem('requestDetails', JSON.stringify(response));
    $scope.requestDetails = JSON.parse($window.localStorage.getItem('requestDetails'));
    //console.log("requestDetails ------------"+$scope.requestDetails);

});

app.controller('requestDataController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('requestDataController');

    var customerDetails = $window.localStorage.getItem('citizenDetails');

    //$scope.selectedSchemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
    //console.log("selectedSchemeDetails "+$scope.selectedSchemeDetails);
    $window.localStorage.setItem('redirectURL', 'createRequest');
    if (customerDetails != undefined && customerDetails != null) {
        $location.url('/requestData');
    } else {
        $location.url('/login');
    }

    $scope.requestPersonList = function () {
        $http({
            method: 'GET',
            url: 'api/ProjectRequests',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('ProjectRequests :' + JSON.stringify(response));
            $scope.requestPersonList = response;
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.requestPersonList();

});

//****************************formsDownloadController****************
app.controller('formsDownloadController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('formsDownloadController');
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.forms").addClass("active").siblings('.active').removeClass('active');
    });

    $scope.updatedFiles = uploadFileURL;
    $scope.getScheme = function () {

        $http({
            method: 'GET',
            url: 'api/Schemes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('SchemeForms get method:' + JSON.stringify(response));
            $scope.schemeLists = response;
           // console.log('data is ' + JSON.stringify($scope.schemeLists));
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.getScheme();
});
//**************************formsDownloadController*******************

/*var sessionActive = true;
app.controller('SessionController', function($scope, $rootScope, $window, $http, $location) {
    var sessionCheck = 0;

    console.log("Session Controller");
    function timerCount(counter){
        $rootScope.minTimer = parseInt($rootScope.countDown/60);
        $rootScope.secTimer = $rootScope.countDown%60;
        if(counter <= 0){
            sessionCheck = 0;
            $('#sessionTimer').modal('hide');
            $rootScope.validateSession();
        }else {
            setTimeout(function () {
                $rootScope.countDown = counter -1;
                console.log('Session Active check:'+sessionActive);
                if(sessionActive == false) {
                    timerCount(counter - 1)
                }else{
                    $rootScope.countDown = 0;
                }
            }, 1000);
        }
    }

    $rootScope.continueSession = function(){
        var accessToken = $window.localStorage.getItem('citizenTokenId');
        var sessionId = $window.localStorage.getItem('sessionId');
        var currentTime = new Date();
        $http({
            method: 'PUT',
            url: URL+'/SessionInfos/'+sessionId+'?access_token='+accessToken,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data:{"lastAccessedTime":currentTime}
        }).success(function(data){
            console.log('Session Updated:'+JSON.stringify(data));
            sessionActive = true;
            sessionCheck = 0;
            $('#sessionTimer').modal('hide');
        }).error(function(data){

        });
    };

    setInterval(function () {
        if($window.localStorage.getItem('citizenTokenId') && $window.localStorage.getItem('citizenDeatils')) {
            //console.log('Interval calls:' + $window.localStorage.getItem('accessToken'));
            var accessToken = $window.localStorage.getItem('citizenTokenId');
            var expire =new Date(Number($window.localStorage.getItem('accessTokenExpireTime')));

            expire = expire.getTime();
            var currentTime = new Date().getTime();

            //console.log('Current::'+currentTime+'....Expire:::'+expire);
            if(expire > currentTime) {

                $rootScope.userInfo = JSON.parse($window.localStorage.getItem('citizenDeatils'));

                if((expire - currentTime) < 2*60*1000){
                    //console.log('Session 2 min remaining');
                    $http({
                        method: 'POST',
                        url: URL+ '/AccessTokens?access_token='+accessToken,
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        data:{
                            'userId': $rootScope.userInfo.id
                        }
                    }).success(function(data){
                        //console.log('Data...:'+JSON.stringify(data));
                        $window.localStorage.setItem('citizenTokenId',data.id);
                        $window.localStorage.setItem('accessTokenExpireTime',data.expireTime);
                    }).error(function(data){

                    });
                }
            }else{
                //alert('Entered session end');
                $rootScope.validateSession();
            }
            //console.log('Entered Session Info ******' + accessToken);
            $http({
                method: 'GET',
                url: 'api/SessionInfos?filter={"where":{"accessToken":"'+accessToken+'"}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function(data){
                $window.localStorage.setItem('sessionId',data[0].id);
                var lastAccessedTime = new Date(data[0].lastAccessedTime).getTime();
                if((currentTime - lastAccessedTime) >= (data[0].sessionDuration-(2*60*1000))){
                    sessionActive = false;
                    if(sessionCheck == 0) {
                        console.log('Entered Session counter');
                        sessionCheck = 1;
                        $rootScope.countDown = 2*60;
                        //$('#sessionTimer').modal('show');
                        $('#sessionTimer').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show : true
                        });
                        timerCount($rootScope.countDown);
                    }
                }else{
                    sessionActive = true;
                }

            }).error(function(data){

            });
        }
    }, 1000);
});*/
