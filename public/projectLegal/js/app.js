// var uploadFileURL='http://139.162.27.78:8869/api/Uploads/dhanbadDb/download/';
var uploadFileURL ='http://54.149.172.244:3004/api/Uploads/dhanbadDb/download/';


var app = angular.module('dhanbad', ['ngRoute','ngCalendar','ngFileUpload','datatables']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: './home.html',
            controller: 'homeControllerTest'
        }).when('/login', {
            templateUrl: './login.html',
            controller: 'loginController'
        }).when('/project-WordWorks', {
            templateUrl: './project-WordWorks.html',
            controller: 'projectWordWorksController'
        }).when('/noc', {
            templateUrl: './noc.html',
            controller: 'nocController'
        }).when('/createRequest', {
            templateUrl: './createRequest.html',
            controller: 'createRequestController'
        }).when('/requestSubmitSuccess', {
            templateUrl: './requestSubmitSuccess.html',
            controller: 'requestSubmitSuccessController'
        }).when('/formDownloads', {
            templateUrl: './formDownloads.html',
            controller: 'formDownloadsController'
        }).when('/userProfile', {
            templateUrl: './userProfile.html',
            controller: 'profileController'
        }).when('/appliedForms', {
            templateUrl: './trackRequests.html',
            controller: 'appliedFormsController'
        }).when('/assetRequestForms', {
            templateUrl: './assetRequestForms.html',
            controller: 'assetRequestFormsController'
        }).when('/requestSubmitSuccess', {
            templateUrl: './requestSubmitSuccess.html',
            controller: 'requestSubmitSuccessController'
        }).when('/viewRequetDetails/:requstId', {
            templateUrl: './viewRequetDetails.html',
            controller: 'viewRequetDetailsController'
            }).when('/viewAssetRequestDetails/:assetRequestId', {
            templateUrl: './viewAssetRequestDetails.html',
            controller: 'viewAssetRequestDetailsController'
    }).when('/vehicleAssessment/', {
        templateUrl: './landAndAssesment.html',
        controller: 'landAndAssesmentController'
    }).when('/landForm', {
        templateUrl: './landForm.html',
        controller: 'landFormController'
    }).when('/landLogin', {
        templateUrl: './landLogin.html',
        controller: 'landLoginController'
    }).when('/nocRequest', {
            templateUrl: './nocRequest.html',
            controller: 'nocRequestController'
        }).when('/nocSubmitSuccess', {
            templateUrl: './nocSubmitSuccess.html',
            controller: 'nocSubmitSuccessController'
        }).when('/printNOCRequest', {
          templateUrl: './printNOCRequest.html',
          controller: 'printNOCRequestController'
      }).when('/formLandSubmitSuccess', {
        templateUrl: './formLandSubmitSuccess.html',
        controller: 'formLandSubmitSuccessController'
      }).when('/grievanceRedressal', {
        templateUrl: './grievance_redressal.html',
        controller: 'grievanceRedressalController'
      }).when('/licenseRequest', {
        templateUrl: './propertyTaxAssessmentRequest.html',
        controller: 'licenseRequestController'
      }).when('/viewLicenseRequest', {
        templateUrl: './viewLicenseRequest.html',
        controller: 'viewLicenseRequestController'
      }).when('/viewLeaveRequest', {
        templateUrl: './viewLeaveRequest.html',
        controller: 'viewLeaveRequestController'
      }).when('/viewGrievanceRequest', {
        templateUrl: './viewGrievanceRequest.html',
        controller: 'viewGrievanceRequestController'
      }).when('/viewBirthRequest', {
        templateUrl: './viewBirthRequest.html',
        controller: 'viewBirthRequestController'
      }).when('/viewDeathRequest', {
        templateUrl: './viewDeathRequest.html',
        controller: 'viewDeathRequestController'
      }).when('/viewPropertyTaxRequest', {
        templateUrl: './viewPropertyTaxRequest.html',
        controller: 'viewPropertyTaxRequestController'
      }).when('/viewSchemeRequest', {
        templateUrl: './viewSchemeDetails.html',
        controller: 'viewSchemeDetailsController'
      }).when('/viewProjectRequestDetails', {
        templateUrl: './viewProjectRequestDetails.html',
        controller: 'viewProjectRequestDetailsController'
      }).when('/viewNOCRequestDetails', {
        templateUrl: './viewNOCRequestDetails.html',
        controller: 'viewNOCRequestDetailsController'
       }).when('/birthForm', {
        templateUrl: './birthRegistration.html',
        controller: 'birthFormController'
       }).when('/deathForm', {
        templateUrl: './deathRegistration.html',
        controller: 'deathFormController'
       }).when('/schemes', {
        templateUrl: './schemeManagement.html',
        controller: 'schemeManagementController'
       }).when('/schemeForm', {
        templateUrl: './schemeForm.html',
        controller: 'schemeFormController'
       }) .when('/propertyTaxAssessmentRequest', {
        templateUrl: './propertyTax.html',
        controller: 'propertyTaxAssessmentRequestController'
       }).when('/schemeDetails/:schemeId', {
       templateUrl: '/schemeDetails.html',
       controller: 'schemeDetailsController'
       }).when('/appliedSchemeDetails/:requestId', {
       templateUrl: '/appliedSchemeDetails.html',
       controller: 'appliedSchemeDetailsController'
       }).when('/formSubmitSuccess', {
       templateUrl: './formSubmitSuccess.html',
       controller: 'formSubmitSuccessController'
    }).when('/leaveRequest', {
            templateUrl: './leaveRequest.html',
            controller: 'leaveRequestController'
        }).otherwise({
            redirectTo: '/'
    });
});
app.factory('httpRequestInterceptor', function ($window,$q,$location) {
    return {
        request: function (config) {
            if($window.localStorage.getItem('tokenId')) {
                config.headers['access_token'] = $window.localStorage.getItem('tokenId');
            }
            return config;
        },
        response: function(response) {
            var deferred = $q.defer();
            deferred.resolve(response);
            return deferred.promise;
        },
        responseError: function (rejection) {
            if(rejection.status === 401) {
                console.log('reject responce'+JSON.stringify(rejection))
                //if(rejection."Unauthorized")
                $window.localStorage.clear();
                location.reload();
                /*   if(rejection.data.error.message=="Authorization Required"){

                 }*/

            }if(rejection.status === 500){
                var errorUrlData=$location.host()+':3009/error';
                location.href=errorUrlData;
                return false;
            }
            return $q.reject(rejection);
        }
    };
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
});

app.directive('validNumber', function() {
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            if(!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function(val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if(!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean =negativeCheck[0] + '-' + negativeCheck[1];
                    if(negativeCheck[0].length > 0) {
                        clean =negativeCheck[0];
                    }
                }
                if(!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0,2);
                    clean =decimalCheck[0] + '.' + decimalCheck[1];
                }
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function(event) {
                if(event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

/*schemes start*/

app.controller('schemeManagementController', function ($http, $scope, $window, $location, $rootScope, $filter, $routeParams) {
    console.log('schemeManagementController');
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });
    $http({
        "method": "GET",
        "url": 'api/Schemes?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        //console.log("filter Schemes " + JSON.stringify(response));
        $scope.schemeLists = response;

        $scope.items2 = $scope.schemeLists;

        $scope.$watch('search', function (val) {
            $scope.schemeLists = $filter('filter')($scope.items2, val);
        });

    }).error(function (response, data) {
        //console.log("failure");
    })


    $scope.schemeForms = function (schemeDetails) {

       // $window.localStorage.setItem('schemeDetails', JSON.stringify(schemeDetails));


                var customerDetails = $window.localStorage.getItem('userDeatils');

                 if (customerDetails != undefined && customerDetails != null) {
         $window.localStorage.setItem('schemeDetails', JSON.stringify(schemeDetails));
                    location.href = '/projectLegal/#/schemeForm';
                    $scope.pack = $routeParams.schemeId;

                    } else {
                        location.href = '/projectLegal/#/login';
                    }


        //location.href = '/projectLegal/#/schemeForm';
        //$scope.pack = $routeParams.schemeId;
    }
});


app.controller('schemeFormController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {
    console.log("schemeFormController");
    //var customerDetails=$rootScope.customerDetails;

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });

   // var customerDetails = JSON.parse($window.localStorage.getItem('citizenDeatils'));
   var customerDetails = $window.localStorage.getItem('userDeatils');
    $scope.userDetailsList = JSON.parse(customerDetails);
console.log("user details",JSON.stringify($scope.userDetailsList));
    $scope.selectedSchemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
    //console.log("selectedSchemeDetails " + $scope.selectedSchemeDetails);

    if (customerDetails != undefined && customerDetails != null) {
    location.href = '/projectLegal/#/schemeForm';

    } else {
        $location.url('/login');
    }

    var filedetails = [];
    var fileIdsArray = [];
    var fileUploadStatus = true;
    $scope.files = [];
    $scope.uploadURL = uploadFileURL;
    $scope.uploadFiles = function (files) {

        $scope.disable = true;
        $scope.errorMssg = true;
        //$scope.files = [];
        $scope.fileUploadLists = true;
        fileIdsArray = [];
        fileUploadStatus = false;
		var fileCount = 0;
        angular.forEach(files, function (file) {
            console.log('File Info:'+JSON.stringify(file));
            //alert('File Info:'+JSON.stringify(file.type));
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {

            var fsize=0;
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
               if (file.size == 0){
               fsize='0 Byte';
               }else{
                 var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                             fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
               }

                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    //console.log(JSON.stringify(response.data.metadata.mimetype));
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename,
                            'size': fsize
                        };
                        fileIdsArray.push(fileDetails);
                        filedetails.push(response.data);
                        $scope.files.push(fileDetails);
                        fileUploadStatus = true;
                        //console.log('details are' + JSON.stringify(fileIdsArray));
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
            if (fileCount == files.length) {
                $scope.fileUploadLists = true;
                $scope.disable = false;
                $scope.errorMssg = false;
            }
        });


    };

    $scope.deleteFile = function(index, fileId){

        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.files.splice(index, 1);
        });

    };

    $scope.citizen = {
        "emailId": $scope.userDetailsList.email
    }

    $scope.schemeFormSubmit = function () {
        if (fileUploadStatus) {
            var schemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
            var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
            var citizenData = $scope.citizen;
            citizenData.files = $scope.files;
            citizenData.schemeName = schemeDetails.name;
            citizenData.schemeUniqueId = schemeDetails.schemeUniqueId;
            citizenData.requestStatus = 'New';
            citizenData.acceptStatus = 'No';
             citizenData['status']='NEW';
             citizenData['createdPerson'] = userDetails.email;
            //console.log('Users Response :' + JSON.stringify(citizenData));
            if (citizenData.name != '') {
                $http({
                    method: 'POST',
                    url: 'http://54.189.195.233:3000/api/Requests',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: citizenData
                }).success(function (response) {
                    console.log('Users Response....... :' + JSON.stringify(response));
                    $window.localStorage.setItem('requestDetails', JSON.stringify(response));
                    var requestDetails = JSON.parse($window.localStorage.getItem('requestDetails'));
                    //console.log('data is ' + JSON.stringify(response));
                    //location.href = '#/formSubmitSuccess';
                    location.href = '#/viewSchemeRequest';
                     setTimeout(function(){
                     location.href = $rootScope.prevPath;
                     }, 10000);



                    $scope.files =[];
                    $scope.citizen = {};
                    //console.log("success");
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                });
            }
            else {

                $scope.schemeFormSubmit();
            }

        }
    };

    $scope.reset = function() {
        $scope.citizen = {};
        $scope.citizen.emailId = '';
        $scope.files = [];
        $scope.errorMssg = false;
    };

});

app.controller('schemeDetailsController', function ($http, $scope, $window, $location, $rootScope, $routeParams) {
    console.log('schemeDetailsController');
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });
    $scope.schemeForms = function (schemeDetails) {
        $window.localStorage.setItem('schemeDetails', JSON.stringify(schemeDetails));
        location.href = '/projectLegal/#/schemeForm';
        $scope.pack = $routeParams.schemeId;
    }

    $scope.uploadURL = uploadFileURL;
    $scope.pack = $routeParams.schemeId;
    //console.log("scheme pack Id " + $scope.pack);


    $http({
        "method": "GET",
        "url": 'api/Schemes/' + $scope.pack,
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        //console.log("schemeDetailsController " + JSON.stringify(response));
        $scope.filterSchemeLists = response;
    }).error(function (response, data) {
        //console.log("failure");
    });


});

 app.controller('appliedSchemeDetailsController', function ($http, $scope, $window, $location, $rootScope, $routeParams) {
     console.log('appliedSchemeDetailsController');

     $scope.uploadURL = uploadFileURL;
     $scope.pack = $routeParams.requestId;
     var details=JSON.parse($window.localStorage.getItem('userDeatils'));
     $scope.userDetails = details;
     //alert(JSON.stringify($scope.userDetails));

     $http({
         "method": "GET",
         "url": 'http://54.189.195.233:3000/api/Requests/' + $scope.pack,
         "headers": {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response, data) {
         $scope.requestSuccess = response;
     }).error(function (response, data) {
         //console.log("failure");
     });
 });

 app.controller('formSubmitSuccessController', function ($http, $scope, $timeout, $window, $location, $rootScope) {
     console.log('formSubmitSuccessController');
     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
     });

     $scope.requestDetails = JSON.parse($window.localStorage.getItem('requestDetails'));
     $scope.selectedSchemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
    // console.log("selectedSchemeDetails " + $scope.selectedSchemeDetails);

 });

/*schemes end*/

/*property tax assessment request start*/

app.controller('licenseRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("licenseRequestController started");



var customerDetails = $window.localStorage.getItem('userDeatils');

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/licenseRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }


$scope.property={}
    $scope.errorInRegisterMessage=false;
    $scope.createLicense=function () {
        var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
        var citizenData=$scope.property;
        citizenData['createdPerson'] = userDetails.email;
          citizenData['status']='NEW';
          console.log("final object is" +JSON.stringify(citizenData));
            $http({
                "method": "POST",
                "url": 'http://54.189.195.233:3000/api/licenceRequests',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                data:citizenData
            }).success(function (response, data) {
                // console.log("schemeDetailsController "+ JSON.stringify(response));
                $("#licenseRequest").modal("show");     
                setTimeout(function(){$('#licenseRequest').modal('hide')}, 3000); 
                $scope.property={};
            }).error(function (response, data) {
                if(response.error.status==422){

                }
            });
    }
});

app.controller('viewLicenseRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewLicenseRequestController started");



var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewLicenseRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/licenceRequests?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 //console.log("viewLicenseRequestController "+ JSON.stringify(response));
                 $scope.viewLicDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});


app.controller('viewLeaveRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewLeaveRequestController started");



var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewLeaveRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/employeeLeaves?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                //"url": 'http://54.189.195.233:3000/api/employeeLeaves',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 console.log("viewLicenseRequestController "+ JSON.stringify(response));
                 $scope.viewLicDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});


app.controller('viewPropertyTaxRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewPropertyTaxRequestController started");

var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewPropertyTaxRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/propertyTaxes?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 console.log("viewTaxDetails "+ JSON.stringify(response));
                 $scope.viewTaxDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});

app.controller('viewSchemeDetailsController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewSchemeDetailsController started");

var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewSchemeRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/Requests?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                // "url": 'api/Schemes?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
                //"url": 'http://localhost:3021/api/Requests?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 console.log("viewTaxDetails "+ JSON.stringify(response));
                 $scope.viewTaxDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});

app.controller('viewProjectRequestDetailsController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewProjectRequestDetailsController started");

var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewProjectRequestDetails';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/ProjectRequests?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                // "url": 'api/Schemes?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
                //"url": 'http://localhost:3021/api/ProjectRequests?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 console.log("viewTaxDetails "+ JSON.stringify(response));
                 $scope.viewTaxDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});

app.controller('viewNOCRequestDetailsController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewNOCRequestDetails started");

var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewNOCRequestDetails';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/NocRequests?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                // "url": 'api/Schemes?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
                //"url": 'http://localhost:3021/api/Nocrequests?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 console.log("viewTaxDetails "+ JSON.stringify(response));
                 $scope.viewTaxDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});

app.controller('viewGrievanceRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewGrievanceRequestController started");



var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewGrievanceRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/citizenComplaints?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                //"url": 'http://54.189.195.233:3000/api/employeeLeaves',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 console.log("viewLicenseRequestController "+ JSON.stringify(response));
                 $scope.viewGrievanceRequestDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});

app.controller('viewBirthRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewBirthRequestController started");



var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewBirthRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/BirthCertificates?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                //"url": 'http://54.189.195.233:3000/api/employeeLeaves',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 console.log("viewLicenseRequestController "+ JSON.stringify(response));
                 $scope.viewBirthDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});

app.controller('viewDeathRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("viewDeathRequestController started");



var customerDetails = $window.localStorage.getItem('userDeatils');

var createdEmail = JSON.parse(customerDetails).email;

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/viewDeathRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

            $http({
                "method": "GET",
                "url": 'http://54.189.195.233:3000/api/DeathCertificates?filter={"where":{"createdPerson":"' + createdEmail + '"}}',
                //"url": 'http://54.189.195.233:3000/api/employeeLeaves',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},

            }).success(function (response, data) {
                 console.log("viewLicenseRequestController "+ JSON.stringify(response));
                 $scope.viewDeathDetails = response;

            }).error(function (response, data) {
                if(response.error.status==422){
            }
            });

});


app.controller('birthFormController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

    console.log("birthFormController started");

        var customerDetails = $window.localStorage.getItem('userDeatils');

         if (customerDetails != undefined && customerDetails != null) {

             location.href = '/projectLegal/#/birthForm';

            } else {
                location.href = '/projectLegal/#/login';
            }

    $scope.birthform={}
        $scope.errorInRegisterMessage=false;
        $scope.createBirth=function () {


    var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
    var citizenData=$scope.birthform;
    citizenData['createdPerson'] = userDetails.email;
    citizenData['status']='NEW';
    console.log("final object is birth:::" +JSON.stringify($scope.birthform));
                $http({
                    "method": "POST",
                    "url": 'http://54.189.195.233:3000/api/BirthCertificates',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    data:citizenData
                }).success(function (response, data) {
                    console.log("schemeDetailsController "+ JSON.stringify(response));
                    //$('#createCitizen').modal('hide');
                    //$window.localStorage.setItem('userDeatils',JSON.stringify(response));
                    // alert(response);
                    //$location.url('/') ;
                   // $window.location.reload();
                    // $scope.filterSchemeLists = response;
                    //alert("Birth Deatils are Succesfully Saved");  
                    $("#addAssetSuccess").modal("show");     
                    setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);      
                    $scope.birthform={};         
                }).error(function (response, data) {
                    if(response.error.status==422){
                      //  $scope.errorInRegisterMessage=true;
                      //  $scope.errorInRegisterDetails='You Email is already register please login';
                    }
                });
        }

});


app.controller('deathFormController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {


    console.log("deathFormController started");

     var customerDetails = $window.localStorage.getItem('userDeatils');

             if (customerDetails != undefined && customerDetails != null) {

                 location.href = '/projectLegal/#/deathForm';

                } else {
                    location.href = '/projectLegal/#/login';
                }

    $scope.deathform={}
        $scope.errorInRegisterMessage=false;
        $scope.createDeath=function () {
            var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
                    var citizenData=$scope.deathform;
                    citizenData['createdPerson'] = userDetails.email;
                      citizenData['status']='NEW';
                $http({
                    "method": "POST",
                    "url": 'http://54.189.195.233:3000/api/DeathCertificates',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    data:citizenData
                }).success(function (response, data) {
                    console.log("schemeDetailsController "+ JSON.stringify(response));
                    //$('#createCitizen').modal('hide');
                    //$window.localStorage.setItem('userDeatils',JSON.stringify(response));
                    // alert(response);
                    //$location.url('/') ;
                   // $window.location.reload();
                    // $scope.filterSchemeLists = response;
                    $("#DeathSuccess").modal("show");     
                    setTimeout(function(){$('#DeathSuccess').modal('hide')}, 3000); 
                    $scope.deathform={};  
                    // alert("Death Deatils are Succesfully Saved");
                }).error(function (response, data) {
                    if(response.error.status==422){
                      //  $scope.errorInRegisterMessage=true;
                      //  $scope.errorInRegisterDetails='You Email is already register please login';
                    }
                });
        }

});



app.controller('grievanceRedressalController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

    console.log("grievanceRedressalController started");

    var customerDetails = $window.localStorage.getItem('userDeatils');

     if (customerDetails != undefined && customerDetails != null) {

         location.href = '/projectLegal/#/grievanceRedressal';

        } else {
            location.href = '/projectLegal/#/login';
        }


    $scope.property1={}
        $scope.errorInRegisterMessage=false;
        $scope.createGrievanceRedressal=function () {
            // alert("enter");

            $scope.property1.citizenId = "5af03877aa9cae2e42c1e62b";
            var citizenData=$scope.property1;
            var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
            var citizenData=$scope.property1;
            citizenData['createdPerson'] = userDetails.email;
            citizenData['status']='NEW';
                $http({
                    "method": "POST",
                    "url": 'http://54.189.195.233:3000/api/citizenComplaints',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    data:citizenData
                }).success(function (response, data) {
                    console.log("schemeDetailsController "+ JSON.stringify(response));
                    //$('#createCitizen').modal('hide');
                    //$window.localStorage.setItem('userDeatils',JSON.stringify(response));
                    // alert(response);
                    //$location.url('/') ;
                   // $window.location.reload();
                    // $scope.filterSchemeLists = response;
                    $("#grievanceRequest").modal("show");     
                    setTimeout(function(){$('#grievanceRequest').modal('hide')}, 3000); 
                    $scope.property1={};  
                    // alert("Grievance Deatils are Succesfully Saved");                    
                }).error(function (response, data) {
                    if(response.error.status==422){
                      //  $scope.errorInRegisterMessage=true;
                      //  $scope.errorInRegisterDetails='You Email is already register please login';
                    }
                });
        }


});



app.controller('propertyTaxAssessmentRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("propertyTaxAssessmentRequestController started");

var customerDetails = $window.localStorage.getItem('userDeatils');

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/propertyTaxAssessmentRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

$scope.property={}
    $scope.errorInRegisterMessage=false;
    $scope.createProperty=function () {
        var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
        var citizenData=$scope.property;
        citizenData['createdPerson'] = userDetails.email;
        citizenData['status']='NEW';
        console.log("final object is" +JSON.stringify(citizenData));
            $http({
                "method": "POST",
                "url": 'http://54.189.195.233:3000/api/propertyTaxes',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                data:citizenData
            }).success(function (response, data) {
                // console.log("schemeDetailsController "+ JSON.stringify(response));
                //$('#createCitizen').modal('hide');
                //$window.localStorage.setItem('userDeatils',JSON.stringify(response));
                // alert(response);
                //$location.url('/') ;
               // $window.location.reload();
                // $scope.filterSchemeLists = response;
                $("#propertyRequest").modal("show");     
                setTimeout(function(){$('#propertyRequest').modal('hide')}, 3000); 
                $scope.property={};
            }).error(function (response, data) {
                if(response.error.status==422){
                  //  $scope.errorInRegisterMessage=true;
                  //  $scope.errorInRegisterDetails='You Email is already register please login';
                }
            });
    }
});

app.controller('leaveRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

console.log("leaveRequestController started");

var customerDetails = $window.localStorage.getItem('userDeatils');

 if (customerDetails != undefined && customerDetails != null) {

     location.href = '/projectLegal/#/leaveRequest';

    } else {
        location.href = '/projectLegal/#/login';
    }

$scope.property={}

$scope.dayDiff=function(a,b){
var date1 = new Date(a);
var date2 = new Date(b);
var timeDiff = Math.abs(date2.getTime() - date1.getTime());
var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
$scope.property.numOfDays=diffDays;
}


    $scope.errorInRegisterMessage=false;
    $scope.createProperty=function () {
          var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
                var citizenData=$scope.property;
                citizenData['createdPerson'] = userDetails.email;
                  citizenData['status']='NEW';
                  console.log("final object is" +JSON.stringify(citizenData));

            $http({
                "method": "POST",
                "url": 'http://54.189.195.233:3000/api/employeeLeaves',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                data:citizenData
            }).success(function (response, data) {
                console.log("schemeDetailsController "+ JSON.stringify(response));
                $scope.property={}

            }).error(function (response, data) {
                if(response.error.status==422){

                }
            });
    }
});

/*property tax assessment request end */



// '/landAndAssesment/:paId'

app.controller('formLandSubmitSuccessController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });

    $scope.requestDetails = JSON.parse($window.localStorage.getItem('requestDetails'));
    $scope.selectedSchemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
    console.log("selectedSchemeDetails " + $scope.selectedSchemeDetails);

});


app.controller('landFormController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {


    //var customerDetails=$rootScope.customerDetails;
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });
    var customerDetails = $window.localStorage.getItem('userDeatils');

    $scope.selectedSchemeDetails = JSON.parse($window.localStorage.getItem('schemeDetails'));
    console.log("selectedSchemeDetails " + $scope.selectedSchemeDetails);

    $scope.purchaseAgendaNumber = $window.localStorage.getItem("purchaseAgendaId");
    $scope.purchaseDepartment = $window.localStorage.getItem("purchaseDepartment");

    console.log("purchase number:::" +JSON.stringify($scope.purchaseAgendaNumber));

    document.getElementById("myText").value = $scope.purchaseAgendaNumber;

    if (customerDetails != undefined && customerDetails != null) {


    } else {
        $location.url('/login');
    }

    /*$http({
        "method": "GET",
        //"url": 'api/Citizens/',
        /!*"url": 'api/PurchageAgendas'+$scope.paIDetails,*!/

        "url": 'api/PurchageAgendas',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        console.log("user details " + JSON.stringify(response));
        $scope.landDetails = response;


    }).error(function (response) {
        console.log("failure");
    });*/

    //resolution copy upload start



    var filedetails = [];
    var fileIdsArray = [];
    var fileUploadStatus = true;
    $scope.files = [];
    $scope.uploadURL = uploadFileURL;
    $scope.uploadFiles = function (files) {

        $scope.disable = true;
        $scope.errorMssg = true;
        //$scope.files = [];
        $scope.fileUploadLists = true;
        fileIdsArray = [];
        fileUploadStatus = false;
        var fileCount = 0;
        angular.forEach(files, function (file) {
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {

            var fsize=0;
                        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                           if (file.size == 0){
                           fsize='0 Byte';
                           }else{
                             var i = parseInt(Math.floor(Math.log(file.size) / Math.log(1024)));
                                         fsize= Math.round(file.size / Math.pow(1024, i), 2) + ' ' + sizes[i];
                           }
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    //console.log(JSON.stringify(response.data.metadata.mimetype));
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename,
                            'size': fsize
                        };
                        fileIdsArray.push(fileDetails);
                        filedetails.push(response.data);
                        $scope.files.push(fileDetails);
                        fileUploadStatus = true;
                        //console.log('details are' + JSON.stringify(fileIdsArray));
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.fileUploadLists = true;
                $scope.disable = false;
                $scope.errorMssg = false;
            }
        });


    };
//copy upload end
    $scope.deleteFile = function(index, fileId){

        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.files.splice(index, 1);
        });

    };

    $scope.userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));


    $scope.citizenData= {"email": $scope.userDetails.email};

    //console.log("email is" +JSON.stringify($scope.citizenData))



            $scope.schemeFormSubmit = function () {

                // $window.localStorage.setItem('schemeDetails',schemeDetails)
                if (fileUploadStatus) {

                    var citizenData=$scope.citizenData;

                    citizenData['purchaseAgendaNumber']=$scope.purchaseAgendaNumber;
                    citizenData['purchaseDepartment']=($scope.purchaseDepartment);
                    citizenData.files =  $scope.files;
                    //citizenData.schemeName = schemeDetails.name;
                    //citizenData.schemeUniqueId = schemeDetails.schemeUniqueId;
                    citizenData.requestStatus = 'New';
                    citizenData.acceptStatus = 'No';

                    console.log('Users Response :' + JSON.stringify(citizenData));
                    $http({
                        method: 'POST',
                        /*url: 'api/LandAndAssetManagementPortals/',*/
                        url: 'api/VehiclePurchaseRequests',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        data: citizenData
                    }).success(function (response) {
                        console.log('Users Response :' + JSON.stringify(response));
                        $window.localStorage.setItem('requestDetails', JSON.stringify(response));
                        var requestDetails = JSON.parse($window.localStorage.getItem('requestDetails'));
                        console.log('data is ' + JSON.stringify(response));
                        $scope.files = [];
                        location.href = '#/formLandSubmitSuccess';
                    }).error(function (response) {
                        console.log('Error Response :' + JSON.stringify(response));
                    });
                } else {

                    $scope.schemeFormSubmit();
                }

                function Reset() {
                    document.getElementById("form1").reset();
                }

                document.getElementById("resetBtn").onclick = function () {
                    Reset();
                }
                document.getElementById("resetBtn").onclick = Reset;
            }

});


app.controller('landLoginController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('landLoginController');

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.login").addClass("active").siblings('.active').removeClass('active');
    });

    /*Forgot Password start*/

    $scope.forgotPassword = function (email) {

        $rootScope.email = '';
        $scope.successMessage='';
        $scope.message='';

        var email1 = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        $rootScope.email = document.getElementById('email').value;
        //alert("email :" +$rootScope.email)
        var searchData=$rootScope.email;
        /*$rootScope.sucessData = false;*/

        if ($rootScope.email == undefined || $rootScope.email == '' || $rootScope.email == null) {
            $rootScope.notRegisterd = "Enter your Email";
        } else if (email1.test($rootScope.email)) {

            $http({
                method: "GET",
                url: 'api/Citizens/forgotPassword?searchData={"emailId":"' + $rootScope.email+'"}',
                //http://localhost:8899/api/Citizens/forgotPassword?searchData=%7B%22emailId%22%3A%22pavan%40nibblematrix.com%22%7D
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
            }).success(function (response) {
                //alert("success");
                console.log("response" +JSON.stringify(response.data.message));
                console.log("response" +JSON.stringify(response));
                if(response.data.message=="Please Register With US")
                {
                    $scope.message = true;
                    console.log("email not exist response" +JSON.stringify(response.data.message));
                    $scope.message="Invalid Email"

                }
                else {
                    console.log("email exist response" +JSON.stringify(response.data.email));
                    $scope.successMessage = true;
                    $scope.successMessage="Your password is send to your Email"

                }
            }).error(function (response) {
                //alert("error")
                /* $rootScope.notRegisterd = true;
                 $rootScope.sucessData = false;*/
            });
        } else {
            //alert("if else")
            $rootScope.notRegisterd = "Please Enter Valid Email";
        }
    }


    /*Forgot Password end*/

    /*$scope.login=true;

     $scope.getOtp=function(){
     /!* var mobileNo=$scope.citizen.phoneNo;
     var otp=$scope.citizen.otp;

     if(mobileNo=='9652774037' && otp=='8899'){
     var details={
     "mobileNo":mobileNo
     };
     $window.localStorage.setItem('citizenDetails',details);
     $rootScope.customerDetails=details;
     location.href = '#/schemeForm';

     }else{
     alert('please enter valid mobile number');
     }*!/
     $scope.login=false;
     }
     $scope.verifyOtp=function(){
     var mobileNo=$scope.citizen.phoneNo;
     var otp=$scope.citizen.otp;

     if(mobileNo=='9652774037' && otp=='8899'){
     var details={
     "mobileNo":mobileNo
     };
     $window.localStorage.setItem('citizenDetails',details);
     $rootScope.customerDetails=details;
     var urlData = $window.localStorage.getItem('redirectURL');
     $location.url('/'+urlData);

     }else{
     //    alert('please enter valid mobile number');
     }
     $scope.login=false;
     }*/
    var data = JSON.parse($window.localStorage.getItem('userDeatils'));
    if (data != undefined && data != null) {
        location.href = '#/';
    }
    $scope.customerlogin = {
        "email": "",
        "password": ""
    }

    // $scope.loginSubmit = function () {
    //     if ($scope.customerlogin.email && $scope.customerlogin.password) {
    //         console.log("$scope.customer.email" + $scope.customerlogin.email);

    $scope.errorMessage=false;
    $scope.loginSubmit = function() {
        if($scope.customerlogin.email && $scope.customerlogin.password) {
            console.log("$scope.customer.email" + $scope.customerlogin.email);

            console.log("$scope.customer.password" + $scope.customerlogin.password);
            console.log("customer details:" + JSON.stringify($scope.customerlogin));
            $http({
                method: "POST",
                "url": 'api/Citizens/login',
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
                data: $scope.customerlogin
            }).success(function (response) {
                var userId = response.userId;
                $window.localStorage.setItem('tokenId', response.id);
                $http({
                    "method": "GET",
                    "url": 'api/Citizens/?filter=%7B%22where%22%3A%7B%22id%22%3A%20%22' + userId + '%22%7D%7D',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (responseData) {
                    console.log("login details " + JSON.stringify(responseData));
                    $window.localStorage.setItem('userDeatils', JSON.stringify(responseData[0]));
                    //$window.localStorage.setItem('oldpassword', responseData[0].password);
                    $location.url('/');
                    $window.location.reload();
                    //  $scope.filterSchemeLists = response;
                }).error(function (response, data) {


                    console.log(JSON.stringify(response));
                });

            }).error(function (response) {
                if(response.error.status==401){
                    $scope.errorMessage=true;
                    $scope.errorMessageDetails='Please Enter Valid Email and Password';
                    //alert('please enter valid details');

                }
                console.log("LoginFailure:" + JSON.stringify(response));

            });
        } else {
            console.log("error");
        }
    }
    $scope.changeEmail = function () {
        $scope.loginArea = false;
    }

    // $scope.addcitizen = {}
    // $scope.createCitizen = function () {
    //
    //     var citizenData = $scope.addcitizen;
    //     $http({
    //         "method": "POST",
    //         "url": 'api/Citizens',
    //         "headers": {"Content-Type": "application/json", "Accept": "application/json"},
    //         data: citizenData
    //     }).success(function (response, data) {
    //         console.log("schemeDetailsController " + JSON.stringify(response));
    //         $('#createCitizen').modal('hide');
    //         $window.localStorage.setItem('userDeatils', JSON.stringify(response));
    //         // alert(response);
    //         $location.url('/');
    //         $window.location.reload();
    //         // $scope.filterSchemeLists = response;
    //     }).error(function (response, data) {
    //         console.log("failure");
    //     });

    $scope.addcitizen={}
    $scope.errorInRegisterMessage=false;
    $scope.createCitizen=function () {

        var citizenData=$scope.addcitizen;
        if(citizenData.password==citizenData.confirmPassword){
            $http({
                "method": "POST",
                "url": 'api/Citizens',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                data:citizenData
            }).success(function (response, data) {
                console.log("schemeDetailsController "+ JSON.stringify(response));
                $('#createCitizen').modal('hide');
                $window.localStorage.setItem('userDeatils',JSON.stringify(response));
                // alert(response);
                $location.url('/') ;
                $window.location.reload();
                // $scope.filterSchemeLists = response;
            }).error(function (response, data) {
                if(response.error.status==422){
                    $scope.errorInRegisterMessage=true;
                    $scope.errorInRegisterDetails='You Email is already register please login';
                }
            });
        }else{
            $scope.errorInRegisterMessage=true;
            $scope.errorInRegisterDetails='Please Enter Password and Confirm Password Same';
        }


    }

});

app.controller('landAndAssesmentController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('landAndAssesmentController');

   /* var customerDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
    $scope.userDetails = customerDetails;

    $scope.paIDetails=$routeParams.paId;

    alert("paidetails:::"+$scope.paIDetails);*/

    $http({
        "method": "GET",
        //"url": 'api/Citizens/',
        /*"url": 'api/PurchageAgendas'+$scope.paIDetails,*/

        "url": 'api/PurchageAgendas?filter={"where":{ "visibility": "Yes"}}',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        console.log("user details " + JSON.stringify(response));
        $scope.landDetails = response;

        console.log("details are::" +JSON.stringify(response.purchaseAgendaNumber));

        //$window.localStorage.setItem('purchaseAgendaId', response[0].purchaseAgendaNumber);

    }).error(function (response) {
        console.log("failure");
    });

    $scope.schemeForms = function (land) {

//alert("Proposal Submitted")
        //console.log("land details are::" +JSON.stringify(land.departmentNamee.id));

        $window.localStorage.setItem('purchaseAgendaId', land.purchaseAgendaNumber);
        $window.localStorage.setItem('purchaseDepartment', land.departmentNamee.id);

        var data = JSON.parse($window.localStorage.getItem('userDeatils'));
        if (data != undefined && data != null) {
            //alert(data);
            location.href = '/projectLegal/#/landForm';
        }else{
            location.href = '/projectLegal/#/landLogin';
        }
    }

});



app.controller('homeControllerTest', function ($http, $scope, $window, $location, $rootScope) {
    console.log('homeControllerTest');



});

app.controller('indexController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('indexController');

    $('.nav-tabs-dropdown').each(function(i, elm) {

        $(elm).text($(elm).next('ul').find('li.active a').text());

    });

    $('.nav-tabs-dropdown').on('click', function(e) {

        e.preventDefault();

        $(e.target).toggleClass('open').next('ul').slideToggle();

    });

    $('#nav-tabs-wrapper a[data-toggle="tab"]').on('click', function(e) {

        e.preventDefault();

        $(e.target).closest('ul').hide().prev('a').removeClass('open').text($(this).text());

    });


    $('#nav-list li').on('click', function() {
        alert("sdfjkdf");
        $('#nav-list li').not(this).find('div').hide();
        $(this).find('div').toggle();
      });


    $rootScope.logout = function () {
        // alert('hai');

        $window.localStorage.setItem('userDeatils', null);
        localStorage.clear();
        $location.url('/');
        $window.location.reload();
    }

    var data = JSON.parse($window.localStorage.getItem('userDeatils'));
    $rootScope.userData = data;

    if (data != undefined && data != null) {
        $rootScope.loginStatus = false;
    } else {
        $rootScope.loginStatus = true;
    }

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $("#headerStarts .navbar-nav li.home").addClass("active").siblings('.active').removeClass('active');
    });


    $(document).ready(function () {
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });

    $scope.changeLanguage = function (key) {
        //alert('enter');
        var translateText =
            $translate.use(key);
        var sourceText = document.getElementById("sourceText").innerHTML;
        console.log(sourceText);
        $http({
            method: 'GET',
            url: 'https://www.googleapis.com/language/translate/v2?key=AIzaSyDy-x5atvBbzu8DQ2tMfZrCbM8ii8bmtwo&source=en&target='+key+'&callback=translateText&q=' + sourceText
        }).success(function(response) {
            response = response.replace("// API callback","");
            response = response.replace("translateText(","");
            response = response.replace(");","");
            response = JSON.parse(response);
            //console.log(response.data.translations[0]);
            var mContent = response.data.translations[0].translatedText;
            console.log(mContent);
            document.getElementById("sourceText").innerHTML = mContent;
        });

    };



});

app.controller('loginController', function ($http, $scope, $window, $location, $rootScope) {
     console.log('loginController');

    $scope.forgotPassword = function (email) {
         $rootScope.email = '';
         $scope.successMessage='';
         $scope.message='';
         var email1 = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
         $rootScope.email = document.getElementById('email').value;
         var searchData=$rootScope.email;
         if ($rootScope.email == undefined || $rootScope.email == '' || $rootScope.email == null) {
             $rootScope.notRegisterd = "Enter your Email";
         } else if (email1.test($rootScope.email)) {
             $http({
                 method: "GET",
                 url: 'api/Citizens/forgotPassword?searchData={"emailId":"' + $rootScope.email+'"}',
                 headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
             }).success(function (response) {
                 if(response.data.message=="Please Register With US")
                 {
                     $scope.message = true;
                     $scope.message="Invalid Email"
                 }
                 else {
                     console.log("email exist response" +JSON.stringify(response.data.email));
                     $scope.successMessage = true;
                     $scope.successMessage="Your password is send to your Email"

                 }
             }).error(function (response) {
                 /* $rootScope.notRegisterd = true;
                  $rootScope.sucessData = false;*/
             });
         } else {
             $rootScope.notRegisterd = "Please Enter Valid Email";
         }
     }
     var data = JSON.parse($window.localStorage.getItem('userDeatils'));
     if (data != undefined && data != null) {
         location.href = '/projectLegal/#/createRequest';
     }
     $scope.customerlogin = {
         "email": "",
         "password": ""
     }
    $scope.errorMessage=false;
     $scope.loginSubmit = function() {
         if($scope.customerlogin.email && $scope.customerlogin.password) {
             $http({
                 method: "POST",
                 "url": 'api/Citizens/login',
                 headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
                 data: $scope.customerlogin
             }).success(function (response) {
                 var userId = response.userId;
                 $window.localStorage.setItem('tokenId', response.id);
                 $http({
                     "method": "GET",
                     "url": 'api/Citizens/?filter=%7B%22where%22%3A%7B%22id%22%3A%20%22' + userId + '%22%7D%7D',
                     "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                 }).success(function (responseData) {
                     $window.localStorage.setItem('userDeatils', JSON.stringify(responseData[0]));
                     $window.location.reload();
                     //$location.url('/project-WordWorks');
                     location.href = $rootScope.prevPath;
                 }).error(function (response, data) {
                 });

             }).error(function (response) {
                 if(response.error.status==401){
                     $scope.errorMessage=true;
                     $scope.errorMessageDetails='Please Enter Valid Email and Password';
                 }
             });
         } else {
             console.log("error");
         }
     }
     $scope.changeEmail = function () {
         $scope.loginArea = false;
     }
     $scope.addcitizen={}
     $scope.errorInRegisterMessage=false;
     $scope.createCitizen=function () {
         var citizenData=$scope.addcitizen;
         if(citizenData.password==citizenData.confirmPassword){
             $http({
                 "method": "POST",
                 "url": 'api/Citizens',
                 "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                 data:citizenData
             }).success(function (response, data) {
                 $window.localStorage.setItem('userDeatils',JSON.stringify(response));
                 $scope.citizenData = {};
                 $('#createCitizen').modal('hide');
                 $location.url('/projectLegal/#/createRequest');
                 $window.location.reload();
             }).error(function (response, data) {
                 if(response.error.status==422){
                     $scope.errorInRegisterMessage=true;
                     $scope.errorInRegisterDetails='You Email is already register please login';
                 }
             });
         }else{
             $scope.errorInRegisterMessage=true;
             $scope.errorInRegisterDetails='Please Enter Password and Confirm Password Same';
         }
     }

    var contractorDoubleClick=false;
    $scope.createContractor=function(){
        alert('hai');
        $http({
                     "method": "POST",
                     "url": 'api/contractors',
                     "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                     data: $scope.contractor
                 }).success(function (response, data) {
                     console.log("contractor created " + JSON.stringify(response));
                      alert('created successfully');
                 }).error(function (response, data) {
                     console.log("failure");
                 });

    }

 });

app.controller('nocController', function ($http, $scope, $window, $location, $rootScope) {
 console.log('nocController');

    $scope.createNocRequest = function() {

        var data = JSON.parse($window.localStorage.getItem('userDeatils'));
        if (data != undefined && data != null) {

            location.href = '/projectLegal/#/nocRequest';
        }else{
            location.href = '/projectLegal/#/login';
        }
    }

});
app.controller('projectWordWorksController', function ($http, $scope, $window, $location, $rootScope) {
 console.log('projectWordWorksController');

    $scope.createRequest = function() {
        var data = JSON.parse($window.localStorage.getItem('userDeatils'));
        if (data != undefined && data != null) {
            //alert(data);
            location.href = '/projectLegal/#/createRequest';
        }else{
            location.href = '/projectLegal/#/login';
        }
    }

});

app.controller('createRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {
     console.log('createRequestController');

     var customerDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
    $scope.userDetails = customerDetails;
      if (customerDetails != undefined && customerDetails != null) {

          location.href = '/projectLegal/#/createRequest';

         } else {
             location.href = '/projectLegal/#/login';
         }




    $scope.docUploadURL=uploadFileURL;
    $scope.uploadURL=uploadFileURL;
    var filedetails = [];
    var fileIdsArray = [];
    var fileUploadStatus = true;
    $scope.docList = [];
    $scope.uploadFiles = function (files) {

         $scope.files = files;
         fileUploadStatus = false;
         //$scope.docList = [];
         var fileCount = 0;
         angular.forEach(files, function (file) {
             $scope.disable = true;
             $scope.errorMssg1 = true;
             fileCount++;
             if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                 file.upload = Upload.upload({
                     url: 'api/Uploads/dhanbadDb/upload',
                     data: {file: file}
                 });

                 file.upload.then(function (response) {

                     $timeout(function () {
                         var fileDetails = {
                             'id': response.data._id,
                             'name': response.data.filename
                         }
                         fileIdsArray.push(fileDetails);
                         $scope.docList.push(fileDetails);
                         filedetails.push(response.data);
                         fileUploadStatus = true;
                         console.log('details are' + JSON.stringify(fileIdsArray));
                         file.result = response.data;
                     });

                 }, function (response) {
                     if (response.status > 0)
                         $scope.errorMsg = response.status + ': ' + response.data;
                 }, function (evt) {
                     file.progress = Math.min(100, parseInt(100.0 *
                         evt.loaded / evt.total));
                 });
                 $scope.fileUpload = true;
             }else{
                 alert('Please Upload JPEG or PDF files only');
             }

             if (fileCount == files.length) {
                 $scope.uploadNitif = true;
                 $scope.disable = false;
                 $scope.errorMssg1 = false;
                 $scope.formNotSelected = '';
             }
         });


    };

    $scope.deleteFile = function(index, fileId) {

        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/' + fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
        });
    }

$scope.resetButton=function(){
    $scope.request={};
    $scope.request.email=$scope.userDetails.email
    $scope.docList=[];
    fileIdsArray=[];
    $scope.uploadNitif = false;
    $scope.disable = true;
}


    $scope.request = {
        "email": $scope.userDetails.email
    }

    var formRequestSubmit = true;
     $scope.submitRequest = function () {
        // alert()
         if(formRequestSubmit) {
             $scope.submit = false;
             var requestData = $scope.request;
             var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
             //alert(requestData);
             requestData.file = fileIdsArray;
             //requestData.requestStatus = 'New';
             //requestData.acceptStatus = 'No';
             requestData.status = 'NEW';
             requestData.createdPerson = userDetails.email;
             if (fileUploadStatus) {
                 requestData.file = $scope.docList;

                 formRequestSubmit = false;
                 document.getElementById("submit_btn").value="Processing...........";

                 $http({
                     method: 'POST',
                     url: 'http://54.189.195.233:3000/api/ProjectRequests',
                     headers: {"Content-Type": "application/json", "Accept": "application/json"},
                     data: requestData
                 }).success(function (response) {
                     $scope.submit = true;
                     console.log('Users Response :' + JSON.stringify(response));
                     //console.log('data is '+JSON.stringify(response));
                     //$("#successRequest").modal('show');

                     //setTimeout(function(){$('#successRequest').modal('hide')}, 2000);
                     $window.localStorage.setItem('requestDetails', JSON.stringify(response))
                     $scope.requestDetails = $window.localStorage.getItem('requestDetails');
                     console.log('requestDetails ' + $scope.requestDetails);
                     $scope.docList = []
                     //location.href = '#/requestSubmitSuccess';
                     location.href = '#/viewProjectRequestDetails';
                     $scope.schemeRequset = true;
                 }).error(function (response) {
                     console.log('Error Response :' + JSON.stringify(response));
                 });


             } else {

                 $scope.createRequest();
             }
         }

     }
 });

app.controller('requestSubmitSuccessController', function ($http, $scope, $window, $location, $rootScope) {
     console.log('requestSubmitSuccessController');


     //$window.localStorage.setItem('requestDetails', JSON.stringify(response));
     $scope.nocRequestDetails = JSON.parse($window.localStorage.getItem('nocRequestDetails'));
     //console.log("requestDetails ------------"+$scope.requestDetails);

 });

app.controller('profileController', function ($http, $scope, $window, $location, $rootScope) {
     console.log('profileController');
     $scope.userInfo = JSON.parse($window.localStorage.getItem('userDeatils'));
     console.log("userInfo:::" + JSON.stringify($scope.userInfo));
//console.log("userInfo name:::" +$scope.userInfo.name);

     /*edit display name start*/

     $scope.editName = function (userInfo) {

         //alert("editName::" +userInfo);
         $("#editName").modal("show");
         $scope.editNameData = angular.copy(userInfo);
     }

     $scope.getCitizens = function () {

         $http({
             "method": "GET",
             //"url": 'api/Citizens/',
             "url": 'api/Citizens/?filter=%7B%22where%22%3A%7B%22id%22%3A%20%22' + $scope.userInfo.id + '%22%7D%7D',
             "headers": {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (responseData) {
             console.log("user details " + JSON.stringify(responseData));
             $scope.responseInfo = responseData[0];
             $scope.userInfo = $scope.responseInfo;

             console.log(" $scope.responseInfo" + JSON.stringify($scope.responseInfo))
             console.log("responseInfo" + JSON.stringify($scope.userInfo));
         }).error(function (response) {
             console.log("failure");
         });
     }
    $scope.getCitizens();

     $scope.editNameSubmit = function (name, id) {
         $scope.userId = id;
         //alert($scope.userId);
         var editCharterDetails = $scope.editNameData;
         //alert(JSON.stringify(editCharterDetails));
         //var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
         //editCharterDetails['lastEditPerson'] = userDetails.name;
         //alert(editCharterDetails.id);
         $http({
             "method": "PUT",
             "url": 'api/Citizens/'+id,
             "headers": {"Content-Type": "application/json", "Accept": "application/json"},
             "data": editCharterDetails
         }).success(function (response, data) {
             //console.log("filter Schemes "+ JSON.stringify(response));
             $("#editName").modal("hide");
             $window.location.reload();
             $scope.getCitizens();
         }).error(function (response, data) {
             console.log("failure");
         })
     }


     /*edit display name end*/


     /*edit email start*/

     $scope.editEmail = function (userInfo) {

         //alert("editName::" +userInfo);
         $("#editEmail").modal("show");
         $scope.editEmailData = angular.copy(userInfo);
     }


     $scope.editEmailSubmit = function () {
         var editCharterDetails = $scope.editEmailData;
         var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
         editCharterDetails['lastEditPerson'] = userDetails.email;
         $http({
             "method": "PUT",
             "url": 'api/Citizens/' + $scope.editEmailData.id,
             "headers": {"Content-Type": "application/json", "Accept": "application/json"},
             "data": editCharterDetails
         }).success(function (response, data) {
             //console.log("filter Schemes "+ JSON.stringify(response));
             $("#editEmail").modal("hide");
             $scope.getCitizens();
         }).error(function (response, data) {
             console.log("failure");
         })
     }

     $scope.getCitizens();

     /*edit email end*/


     /*edit phone start*/

     $scope.editPhone = function (userInfo) {

         //alert("editName::" +userInfo);
         $("#editPhone").modal("show");
         $scope.editPhoneData = angular.copy(userInfo);
     }


     $scope.editPhoneSubmit = function () {
         var editCharterDetails = $scope.editPhoneData;
         var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
         editCharterDetails['lastEditPerson'] = userDetails.phone;
         $http({
             "method": "PUT",
             "url": 'api/Citizens/' + $scope.editPhoneData.id,
             "headers": {"Content-Type": "application/json", "Accept": "application/json"},
             "data": editCharterDetails
         }).success(function (response, data) {
             //console.log("filter Schemes "+ JSON.stringify(response));
             $("#editPhone").modal("hide");
             $scope.getCitizens();
         }).error(function (response, data) {
             console.log("failure");
         })
     }

     $scope.getCitizens();

     /*edit phone end*/

     /*change password start*/
     $scope.user = {};
     $scope.changePasswordSubmit = function () {

         $scope.tokenId = $window.localStorage.getItem("tokenId");
         //alert("tokenId" +$scope.tokenId);

         var currentpassword = document.getElementById("password").value;
         //alert("current password:::" +currentpassword);
         var password = document.getElementById("pswd").value;
         //alert(" password:::" +password);
         var cpasswor = document.getElementById("pswd1").value;
         //alert("confirm password:::" +cpasswor);
         $scope.passwordError1 = "";

         if (currentpassword != '' && currentpassword != undefined && currentpassword != null
             && password != '' && password != undefined && password != null
             && cpasswor != '' && cpasswor != undefined && cpasswor != null) {
             if (password.length >= 6) {
                 if (password == cpasswor) {
                     $http({
                         "method": "POST",
                         "url": 'api/Citizens/login',
                         "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                         "data": {
                             "email": $scope.userInfo.email,
                             "password": $scope.user.password
                         }
                     }).success(function (response) {

                         console.log("response" +JSON.stringify(response));
                         $scope.tokenId = response.id;
                         $scope.userId = response.userId;
                         $window.localStorage.setItem('accessToken', $scope.tokenId);

                         $http({
                             method: "PUT",
                             //url: URL + "/Citizens/" + $scope.userInfo.id ,
                             url: 'api/Citizens/' + $scope.userInfo.id,
                             headers: {'Accept': 'application/json'},
                             data: {"password": $scope.user.newPassword}
                         }).success(function (response, data) {
                             console.log("Password Response:" + JSON.stringify(response));
                             document.getElementById('changePasswordForm').reset();
                             $('#changePassword').modal('hide');
                             $('#changePasswordSuccess').modal('show');
                             setTimeout(function(){$('#changePasswordSuccess').modal('hide')}, 2000);
                             $scope.passwordError = "Password Updated Successfully";
                             console.log("Password Updated Successfully");

                         }).error(function (response) {
                             console.log("Error:" + JSON.stringify(response));
                             //$scope.passwordError = true;
                             $scope.passwordError1 = "Something Went Wrong. Please Try Again Later";
                             console.log("Something Went Wrong. Please Try Again Later");

                         })
                     }).error(function (data) {
                         $scope.passwordError1 = "Please Enter Correct Password";
                         console.log("Please Enter Correct Password");

                     })
                 } else {
                     $scope.passwordError1 = "New password & confirm password does not match";
                     console.log("Password Confirmation Unsuccessful.");

                 }
             }
             else {
                 console.log("Enter Minimum 6 Characters");

             }
         } else {
             console.log("All Fields Are Mandatory.");

         }
     }


     /*change password end*/


 });

app.controller('requestSubmitSuccessController', function ($http, $scope, $window, $location, $rootScope) {
     console.log('requestSubmitSuccessController');

     //$window.localStorage.setItem('requestDetails', JSON.stringify(response));
     $scope.requestDetails = JSON.parse($window.localStorage.getItem('requestDetails'));
     //console.log("requestDetails ------------"+$scope.requestDetails);

 });

app.controller('assetRequestFormsController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {
     console.log('assetRequestFormsController');

     $(document).ready(function () {
         $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    $scope.uploadURL=uploadFileURL;
     var customerDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
     $scope.userDetails = customerDetails;
     console.log("data" +JSON.stringify($scope.userDetails));
     if (customerDetails != null) {
         $http({
             "method": "GET",
             /*"url": 'api/ProjectRequest?filter={"where":{"email":"' + $scope.userDetails.email + '"}}',*/
             "url": 'api/VehiclePurchaseRequests?filter={"where":{"email":"' + $scope.userDetails.email + '"}}',
//             "url": 'api/VehiclePurchaseRequests',
             "headers": {'Accept': 'application/json'}
         }).success(function (response, data) {
             console.log("filter asset request " + JSON.stringify(response));
             $scope.requestListDetails = response;
         }).error(function (response, data) {
             console.log("filter asset failure Request " + JSON.stringify(response));
         })
     }

 });

 app.controller('viewAssetRequestDetailsController', function ($http, $scope, $window, $location, $rootScope, $routeParams) {
      console.log('viewAssetRequestDetailsController');

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $('[data-toggle="tooltip"]').tooltip();
     });

      var customerDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
      $scope.userDetails = customerDetails;
 $scope.docUploadURL=uploadFileURL;
      $scope.requestDetails=$routeParams.assetRequestId;
      //alert( $scope.requestDetails);

      $http({
          "method": "GET",
          "url": 'api/VehiclePurchaseRequests/'+$scope.requestDetails,
          "headers": {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response, data) {
          $scope.loadingImage=false;
          console.log("filter Schemes "+ JSON.stringify(response));
          $scope.requestLists = response;
      }).error(function (response, data) {
          console.log("failure");
      })

  });



app.controller('appliedFormsController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {
     console.log('appliedFormsController');

     $(document).ready(function () {
         $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    $scope.uploadURL=uploadFileURL;
     var customerDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
     $scope.userDetails = customerDetails;
     //alert($scope.userDetails);
     if (customerDetails != null) {
         $http({
             "method": "GET",
             /*"url": 'api/ProjectRequest?filter={"where":{"email":"' + $scope.userDetails.email + '"}}',*/
             "url": 'api/ProjectRequests?filter={"where":{"email":"' + $scope.userDetails.email + '"}}',
             "headers": {'Accept': 'application/json'}
         }).success(function (response, data) {
             console.log("filter ProjectRequest " + JSON.stringify(response));
             $scope.requestListDetails = response;
         }).error(function (response, data) {
             console.log("filter failure ProjectRequest " + JSON.stringify(response));
         })
     }

 });

app.controller('viewRequetDetailsController', function ($http, $scope, $window, $location, $rootScope, $routeParams) {
     console.log('viewRequetDetailsController');

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });

     var customerDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
     $scope.userDetails = customerDetails;
$scope.docUploadURL=uploadFileURL;
     $scope.requestDetails=$routeParams.requstId;
     //alert( $scope.requestDetails);

     $http({
         "method": "GET",
         "url": 'api/ProjectRequests/'+$scope.requestDetails,
         "headers": {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response, data) {
         $scope.loadingImage=false;
         console.log("filter Schemes "+ JSON.stringify(response));
         $scope.requestLists = response;
     }).error(function (response, data) {
         console.log("failure");
     })

 });

app.controller('nocRequestController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {
    console.log('nocRequestController');
    var customerDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
    $scope.userDetails = customerDetails;
   // $window.localStorage.setItem('redirectURL', 'nocRequest');

      if (customerDetails != undefined && customerDetails != null) {

          location.href = '/projectLegal/#/nocRequest';

         } else {
             location.href = '/projectLegal/#/login';
         }

    $scope.docUploadURL=uploadFileURL;
    $scope.uploadURL=uploadFileURL;
    var filedetails = [];
    var fileIdsArray = [];
    var fileUploadStatus = true;
    $scope.docList = [];
    $scope.uploadFiles = function (files) {
        $scope.files = files;
        fileUploadStatus = false;
        var fileCount = 0;
        angular.forEach(files, function (file) {
            $scope.disable = true;
            $scope.errorMssg1 = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename
                        }
                        fileIdsArray.push(fileDetails);
                        $scope.docList.push(fileDetails);
                        filedetails.push(response.data);
                        fileUploadStatus = true;
                        console.log('details are' + JSON.stringify(fileIdsArray));
                        file.result = response.data;

                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
                $scope.fileUpload = true;
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.uploadNitif = true;
                $scope.disable = false;
                $scope.errorMssg1 = false;
                $scope.formNotSelected = '';
            }
        });
    };
    $scope.deleteFile = function(index, fileId) {
        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/' + fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
        });
    }
    $scope.resetButton=function(){
        $scope.request={};
        $scope.request.email=$scope.userDetails.email
        $scope.docList=[];
        fileIdsArray=[];
        $scope.uploadNitif = false;
        $scope.disable = true;
    }
        $scope.getDepartments=function () {
        $http({
            method: 'GET',
            //url: 'api/PlanDepartments',
            url: 'http://54.189.195.233:3000/api/PlanDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDepartments();
    $scope.getNocAmount=function () {
            $http({
                method: 'GET',
                url: 'api/AppConfigs',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.loadingImage=false;
                if(response[0].nocAmount){
                    $scope.request.nocAmountAction = response[0].nocAmountAction;
                }else{
                    $scope.request.nocAmountAction = 0;
                }
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
        }
    $scope.getNocAmount();

    $scope.request = {
        "email": $scope.userDetails.email
        //"nocAmount":$scope.nocAmounts.nocAmount
        //alert(JSON.stringify($scope.nocAmounts.nocAmount));
    }

    var formRequestSubmit = true;
    $scope.submitRequest = function () {
        if(formRequestSubmit) {
           // alert("submited");

            $scope.submit = false;
            var requestData = $scope.request;
             var userDetails = JSON.parse($window.localStorage.getItem('userDeatils'));
           // alert(JSON.stringify(requestData));
            requestData.file = fileIdsArray;
            requestData.status = 'NEW';
            requestData.createdPerson = userDetails.email;
            //requestData.requestStatus = 'New';
            //requestData.acceptStatus = 'No';
            if (fileUploadStatus) {
                //if(requestData.nocAmountAction>0){
                    requestData.file = $scope.docList;

                    formRequestSubmit = false;
                    // document.getElementById("submit_btn").value="Processing...........";

                    $http({
                        method: 'POST',
                        //url: 'api/NocRequests',
                        url: 'http://54.189.195.233:3000/api/NocRequests',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        data: requestData
                    }).success(function (response) {
                        $scope.submit = true;
                        console.log('Users Response :' + JSON.stringify(response));
                        //console.log('data is '+JSON.stringify(response));
                        //$("#successRequest").modal('show');

                        //setTimeout(function(){$('#successRequest').modal('hide')}, 2000);
                        $window.localStorage.setItem('nocRequestDetails', JSON.stringify(response))
                        $scope.nocRequestDetails = $window.localStorage.getItem('nocRequestDetails');
                        console.log('nocRequestDetails ' + $scope.nocRequestDetails);
                        $scope.docList = []
                        //location.href = '#/nocSubmitSuccess';
                        location.href = '#/viewNOCRequestDetails';
                        $scope.schemeRequset = true;
                    }).error(function (response) {
                        console.log('Error Response :' + JSON.stringify(response));
                    });
//                }else{
//                    alert('please pay noc amount');
//                }



            } else {

                $scope.createNocRequest();
            }
        }

    }
});


app.controller('nocSubmitSuccessController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('nocSubmitSuccessController');


    //$window.localStorage.setItem('requestDetails', JSON.stringify(response));
                    //$window.localStorage.setItem('nocRequestDetails', JSON.stringify(response))
    $scope.nocRequestDetailss = ($window.localStorage.getItem('nocRequestDetails'));
    $scope.nocRequestDetails = JSON.parse($scope.nocRequestDetailss);
    console.log("requestDetails ------------"+JSON.stringify($scope.nocRequestDetails));

});

app.controller('printNOCRequestController', function ($http, $scope, $window, $location, $rootScope) {
    console.log('printNOCRequestController');

    $scope.nocRequestDetails = JSON.parse(($window.localStorage.getItem('nocRequestDetails')));
    $scope.personName = $scope.nocRequestDetails.id;
    console.log("requestDetails ------------"+$scope.nocRequestDetails);

    $scope.generatePDF = function() {
                var printContents = document.getElementById('formConfirmation').innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();

                document.body.innerHTML = originalContents;

            }

});


app.controller('formDownloadsController', function ($http, $scope, Upload, $timeout, $location, $rootScope, $window) {

    console.log("formDownloadsController");

    $scope.updatedFiles = uploadFileURL;
    $scope.getScheme = function () {

        $http({
            method: 'GET',
            url: 'api/ProjectUploads?filter={"where":{"visible":true}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('SchemeForms get method:' + JSON.stringify(response));
            $scope.formUploads = response;
           // console.log('data is ' + JSON.stringify($scope.schemeLists));
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.getScheme();

});
/*

app.run(['$rootScope', function($rootScope) {
            $rootScope.$on('$locationChangeStart', function() {
                $rootScope.previousPage = location.pathname;
                console.log("previous path is" +JSON.stringify($rootScope.previousPage))
            });
    }]);*/


/*
app.run(function($rootScope, $route){
    //Bind the `$routeChangeSuccess` event on the rootScope, so that we dont need to
    //bind in induvidual controllers.
    $rootScope.$on('$routeChangeSuccess', function(currentRoute, previousRoute) {
        //This will give the custom property that we have defined while configuring the routes.
        console.log($route)
    })
})*/

/*
app.run(function($rootScope, $route,$location){
    //Bind the `$routeChangeSuccess` event on the rootScope, so that we dont need to
    //bind in induvidual controllers.
    $rootScope.$on('$routeChangeSuccess', function(currentRoute, previousRoute) {
        //This will give the custom property that we have defined while configuring the routes.
        console.log($route.current.routeName)
        console.log("hello!!!"+$location.path())
    })
})*/


app.run(function($rootScope,$location,$route){
$rootScope.$on('$locationChangeStart', function (event, current, previous) {
        //console.log("Previous URL" +previous);
        $rootScope.prevPath=previous;
});
})
